/*
  Generated with VPC version 3.3.3.887 Beta
  Application name: l152_blink_pll_hse_byp
  File name: main.c

  Copyright (c) 2019
  Author: Vasile Guta-Ciucur

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
  so, subject to the following conditions:
 
  The above copyright notice and this permission notice shall be included in all copies or substantial
  portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
 
#include "stdint.h"
#include "main.h"
#include "my_gpio.h"
#include "my_uart.h"
 
void vpc_system_init(void){
  NVIC_InitTypeDef NVIC_InitStruct;
  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_COMP, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
  /* NVIC Priority Groups: See misc.h for details. */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
  
  NVIC_InitStruct.NVIC_IRQChannel = MemoryManagement_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStruct);
  
  NVIC_InitStruct.NVIC_IRQChannel = BusFault_IRQn;
  NVIC_Init(&NVIC_InitStruct);
  
  NVIC_InitStruct.NVIC_IRQChannel = UsageFault_IRQn;
  NVIC_Init(&NVIC_InitStruct);
  
  NVIC_InitStruct.NVIC_IRQChannel = SVC_IRQn;
  NVIC_Init(&NVIC_InitStruct);
  
  NVIC_InitStruct.NVIC_IRQChannel = DebugMonitor_IRQn;
  NVIC_Init(&NVIC_InitStruct);
  
  NVIC_InitStruct.NVIC_IRQChannel = PendSV_IRQn;
  NVIC_Init(&NVIC_InitStruct);
  
  my_ticks = 0;
  NVIC_InitStruct.NVIC_IRQChannel = SysTick_IRQn;
  NVIC_Init(&NVIC_InitStruct);
  
  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
  SysTick_Config(SystemCoreClock / 1000);
}
  
void vpc_rtc_init(void){  
  RTC_InitTypeDef RTC_InitStructure;
  
  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
  RTC_InitStructure.RTC_SynchPrediv = (32768 / 128) - 1;
  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
  RTC_Init(&RTC_InitStructure);
  
}  
  
void vpc_gpio_init(void){
  GPIO_InitTypeDef GPIO_InitStruct;
  EXTI_InitTypeDef EXTI_InitStruct;
  NVIC_InitTypeDef NVIC_InitStruct;
 
  /* Activating GPIO ports */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOH, ENABLE);
 
  /* The initial values (Low) of OUTPUT pins */
  GPIO_ResetBits(GPIOA, LD2_Pin );
 
  /* The initial values (High) of OUTPUT pins */
 
  /* Prologue for EXTI pins */
  /* PC13 */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);
  GPIO_SetPinPull(GPIOC, B1_Pin, GPIO_PuPd_NOPULL);
  GPIO_SetPinMode(GPIOC, B1_Pin, GPIO_Mode_IN);
 
  /* PA5 as OUTPUT pin */
  GPIO_InitStruct.GPIO_Pin = LD2_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
 
  /* PC13 as INTERRUPT pin */
  EXTI_InitStruct.EXTI_Line = EXTI_Line13;
  EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStruct.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStruct);
 
  NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 4;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStruct);
 
}
 
void vpc_usart2_uart_init(void){
  GPIO_InitTypeDef GPIO_InitStruct;
  USART_InitTypeDef USART_InitStruct;
  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
  
  GPIO_InitStruct.GPIO_Pin = USART_TX_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
  
  GPIO_InitStruct.GPIO_Pin = USART_RX_Pin;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);
  
  USART_InitStruct.USART_BaudRate = 19200;
  USART_InitStruct.USART_WordLength = USART_WordLength_8b;
  USART_InitStruct.USART_StopBits = USART_StopBits_1;
  USART_InitStruct.USART_Parity = USART_Parity_No;
  USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_Init(USART2, &USART_InitStruct);
  USART_Cmd(USART2, ENABLE);
  
}
 
void SystemCoreClockUpdate (void){
  uint32_t tmp = 0, pllmul = 0, plldiv = 0, pllsource = 0, msirange = 0;

  tmp = RCC->CFGR & RCC_CFGR_SWS;
  
  switch (tmp)
  {
    case 0x00:
      msirange = (RCC->ICSCR & RCC_ICSCR_MSIRANGE) >> 13;
      SystemCoreClock = (32768 * (1 << (msirange + 1)));
      break;
    case 0x04:
      SystemCoreClock = HSI_VALUE;
      break;
    case 0x08:
      SystemCoreClock = HSE_VALUE;
      break;
    case 0x0C:
      pllmul = RCC->CFGR & RCC_CFGR_PLLMUL;
      plldiv = RCC->CFGR & RCC_CFGR_PLLDIV;
      pllmul = PLLMulTable[(pllmul >> 18)];
      plldiv = (plldiv >> 22) + 1;
      
      pllsource = RCC->CFGR & RCC_CFGR_PLLSRC;

      if (pllsource == 0x00)
      {
        SystemCoreClock = (((HSI_VALUE) * pllmul) / plldiv);
      }
      else
      {
        SystemCoreClock = (((HSE_VALUE) * pllmul) / plldiv);
      }
      break;
    default:
      msirange = (RCC->ICSCR & RCC_ICSCR_MSIRANGE) >> 13;
      SystemCoreClock = (32768 * (1 << (msirange + 1)));
      break;
  }
  tmp = AHBPrescTable[((RCC->CFGR & RCC_CFGR_HPRE) >> 4)];
  SystemCoreClock >>= tmp;
}

static void SetSysClock(void){
ErrorStatus er;
  
  FLASH->ACR |= FLASH_ACR_ACC64;
  FLASH->ACR |= FLASH_ACR_PRFTEN;
  FLASH->ACR |= FLASH_ACR_LATENCY;
  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  PWR->CR = PWR_CR_VOS_0;
  while((PWR->CSR & PWR_CSR_VOSF) != RESET){}
  
  RCC_MSIRangeConfig(RCC_MSIRange_6);
  RCC_MSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_MSIRDY) == RESET){}
  
  RCC_HSEConfig(RCC_HSE_Bypass);
  er = RCC_WaitForHSEStartUp();
  if (er == ERROR) {
    // do something
  }
  RCC_ClockSecuritySystemCmd(ENABLE);
  
  RCC_HSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET){}
  
  RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_PLLSRC | RCC_CFGR_PLLMUL | RCC_CFGR_PLLDIV));
  RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC_HSE | RCC_CFGR_PLLMUL12 | RCC_CFGR_PLLDIV3);
  RCC->CR |= RCC_CR_PLLON;
  while((RCC->CR & RCC_CR_PLLRDY) == 0){}
  
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
  
  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV1;
  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV1;
  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV1;
  
  RCC_MCOConfig(RCC_MCOSource_NoClock, RCC_MCODiv_1);
  
  PWR_RTCAccessCmd(ENABLE);
  RCC_LSEConfig(RCC_LSE_ON);
  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET){}
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
  RCC_LSEClockSecuritySystemCmd(ENABLE);
  RCC_LSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET){}
  RCC_RTCCLKCmd(ENABLE);
  
}

void SystemInit (void){
  RCC->CR |= (uint32_t)0x00000100;
  RCC->CFGR &= (uint32_t)0x88FFC00C;
  RCC->CR &= (uint32_t)0xEEFEFFFE;
  RCC->CR &= (uint32_t)0xFFFBFFFF;
  RCC->CFGR &= (uint32_t)0xFF02FFFF;
  RCC->CIR = 0x00000000;
    
  SetSysClock();

#ifdef VECT_TAB_SRAM
  SCB->VTOR = SRAM_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal SRAM. */
#else
  SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH. */
#endif
}
 

void NMI_Handler(void)
{
}

void HardFault_Handler(void){
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

void MemManage_Handler(void){
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

void BusFault_Handler(void){
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}
 
void UsageFault_Handler(void){
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

void SVC_Handler(void)
{
}

void DebugMon_Handler(void)
{
}

void PendSV_Handler(void)
{
}

void SysTick_Handler(void)
{
  if (my_ticks > 0) my_ticks--;
}

void EXTI15_10_IRQHandler(void){
  // Here would be the B1 pin interrupt handler (do where it says "do something..."):
  if ((EXTI_GetITStatus(EXTI_Line13)) != RESET){
    // do something...
    EXTI_ClearITPendingBit(EXTI_Line13);
  }
}
 
  
int main(void){
  /* local variables */
  
  /* mandatory system initializations */
  vpc_system_init();
  //vpc_rtc_init(); /* enable if required */  
  vpc_gpio_init();
  //vpc_usart2_uart_init(); /* enable if required */ 
  /* third-party initializations */
  
  /* do your own initializations below */
  
  while(1){
    /* your forever repeating code */
    GPIO_ToggleBits(LD2_Port, LD2_Pin);
    my_delay_ms(500);
    
  }
  return 0;
}
 
