/*
  Generated with VPC version 3.5.2.919 Beta
  Application name: l152_film_slr_camera
  File name: main.c

  Copyright (c) 2020
  Author: Vasile Guta-Ciucur

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
  so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or substantial
  portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "stdint.h"
#include "stdio.h"
#include "main.h"
#include "my_gpio.h"
#include "my_uart.h"
#include "math.h"
#include "my_delay_us.h"
#define ST7735_SPI_TYPE 2
#include "st7735r_mini.h"
#include "xbm48x48.h"
#include "conversion.h"
#include "max44009.h"
#include "slr.h"
#define TIM6_SNBD_NR_SLOTS 2
#include "tim6_slotted_nb_delays.h"

void vpc_system_init(void)
{
  NVIC_InitTypeDef NVIC_InitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_COMP, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* NVIC Priority Groups: See misc.h for details. */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);

  NVIC_InitStruct.NVIC_IRQChannel = MemoryManagement_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = BusFault_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = UsageFault_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = SVC_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = DebugMonitor_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = PendSV_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  my_ticks = 0;
  NVIC_InitStruct.NVIC_IRQChannel = SysTick_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
  SysTick_Config(SystemCoreClock / 1000);
}

void vpc_rtc_init(void)
{
  RTC_InitTypeDef RTC_InitStructure;

  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
  RTC_InitStructure.RTC_SynchPrediv = (32768 / 128) - 1;
  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
  RTC_Init(&RTC_InitStructure);

}

void vpc_gpio_init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  EXTI_InitTypeDef EXTI_InitStruct;
  NVIC_InitTypeDef NVIC_InitStruct;

  /* Activating GPIO ports */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOH, ENABLE);

  /* The initial values (Low) of OUTPUT pins */
  GPIO_ResetBits(GPIOA, ST7735_CS_Pin );
  GPIO_ResetBits(GPIOB, ST7735_RST_Pin );
  GPIO_ResetBits(GPIOC, ST7735_DC_Pin );

  /* The initial values (High) of OUTPUT pins */

  /* Prologue for EXTI pins */
  /* PC13 */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);
  GPIO_SetPinPull(GPIOC, B1_Pin, GPIO_PuPd_NOPULL);
  GPIO_SetPinMode(GPIOC, B1_Pin, GPIO_Mode_IN);

  /* PA9 as OUTPUT pin */
  GPIO_InitStruct.GPIO_Pin = ST7735_CS_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* PB6 as OUTPUT pin */
  GPIO_InitStruct.GPIO_Pin = ST7735_RST_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* PC13 as INTERRUPT pin */
  EXTI_InitStruct.EXTI_Line = EXTI_Line13;
  EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStruct.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStruct);

  /* PC7 as OUTPUT pin */
  GPIO_InitStruct.GPIO_Pin = ST7735_DC_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOC, &GPIO_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 4;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStruct);

}

void vpc_usart2_uart_init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  USART_InitTypeDef USART_InitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

  GPIO_InitStruct.GPIO_Pin = U2_TX_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);

  GPIO_InitStruct.GPIO_Pin = U2_RX_Pin;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

  USART_InitStruct.USART_BaudRate = 19200;
  USART_InitStruct.USART_WordLength = USART_WordLength_8b;
  USART_InitStruct.USART_StopBits = USART_StopBits_1;
  USART_InitStruct.USART_Parity = USART_Parity_No;
  USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_Init(USART2, &USART_InitStruct);
  USART_Cmd(USART2, ENABLE);

}

void vpc_i2c1_init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  I2C_InitTypeDef I2C_InitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);

  GPIO_InitStruct.GPIO_Pin = SCL1_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOB, &GPIO_InitStruct);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_I2C1);

  GPIO_InitStruct.GPIO_Pin = SDA1_Pin;
  GPIO_Init(GPIOB, &GPIO_InitStruct);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_I2C1);

  I2C_InitStruct.I2C_ClockSpeed = 100000;
  I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;
  I2C_InitStruct.I2C_DutyCycle = I2C_DutyCycle_2;
  I2C_InitStruct.I2C_OwnAddress1 = 0;
  I2C_InitStruct.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  I2C_Init(I2C1, &I2C_InitStruct);
  I2C_Cmd(I2C1, ENABLE);

  I2C_DualAddressCmd(I2C1, DISABLE);
  I2C_GeneralCallCmd(I2C1, DISABLE);
  I2C_OwnAddress2Config(I2C1, 0);
  I2C_StretchClockCmd(I2C1, ENABLE);

}

void vpc_spi1_init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  SPI_InitTypeDef   SPI_InitStruct;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
  /* Configure PA5 as SPI SCK pin */
  GPIO_InitStruct.GPIO_Pin = SCK1_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* Configure PA6 as SPI MISO pin */
  GPIO_InitStruct.GPIO_Pin = MISO1_Pin;
  GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* Configure PA7 as SPI MOSI pin */
  GPIO_InitStruct.GPIO_Pin = MOSI1_Pin;
  GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* Connect SCK, MISO and MOSI pins to SPI alternate */
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);

  /* Configure SPI peripheral */
  SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
  SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStruct.SPI_CPOL = SPI_CPOL_High;
  SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;
  SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStruct.SPI_CRCPolynomial = 7;
  SPI_Init(SPI1, &SPI_InitStruct);

  SPI_NSSInternalSoftwareConfig(SPI1, SPI_NSSInternalSoft_Set);
  /* Enable SPI */
  SPI_Cmd(SPI1, ENABLE);

}

void vpc_spi2_init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  SPI_InitTypeDef   SPI_InitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
  /* Configure PB13 as SPI SCK pin */
  GPIO_InitStruct.GPIO_Pin = SCK2_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* Configure PB14 as SPI MISO pin */
  GPIO_InitStruct.GPIO_Pin = MISO2_Pin;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* Configure PB15 as SPI MOSI pin */
  GPIO_InitStruct.GPIO_Pin = MOSI2_Pin;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* Connect SCK, MISO and MOSI pins to SPI alternate */
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_SPI2);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2);

  /* Configure SPI peripheral */
  SPI_InitStruct.SPI_Direction = SPI_Direction_1Line_Tx;
  SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
  SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
  SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStruct.SPI_CRCPolynomial = 7;
  SPI_Init(SPI2, &SPI_InitStruct);

  SPI_NSSInternalSoftwareConfig(SPI2, SPI_NSSInternalSoft_Set);
  /* Enable SPI */
  SPI_Cmd(SPI2, ENABLE);

}

void SystemCoreClockUpdate (void)
{
  uint32_t tmp = 0, pllmul = 0, plldiv = 0, pllsource = 0, msirange = 0;

  tmp = RCC->CFGR & RCC_CFGR_SWS;

  switch (tmp) {
  case 0x00:
    msirange = (RCC->ICSCR & RCC_ICSCR_MSIRANGE) >> 13;
    SystemCoreClock = (32768 * (1 << (msirange + 1)));
    break;
  case 0x04:
    SystemCoreClock = HSI_VALUE;
    break;
  case 0x08:
    SystemCoreClock = HSE_VALUE;
    break;
  case 0x0C:
    pllmul = RCC->CFGR & RCC_CFGR_PLLMUL;
    plldiv = RCC->CFGR & RCC_CFGR_PLLDIV;
    pllmul = PLLMulTable[(pllmul >> 18)];
    plldiv = (plldiv >> 22) + 1;

    pllsource = RCC->CFGR & RCC_CFGR_PLLSRC;

    if (pllsource == 0x00) {
      SystemCoreClock = (((HSI_VALUE) * pllmul) / plldiv);
    } else {
      SystemCoreClock = (((HSE_VALUE) * pllmul) / plldiv);
    }
    break;
  default:
    msirange = (RCC->ICSCR & RCC_ICSCR_MSIRANGE) >> 13;
    SystemCoreClock = (32768 * (1 << (msirange + 1)));
    break;
  }
  tmp = AHBPrescTable[((RCC->CFGR & RCC_CFGR_HPRE) >> 4)];
  SystemCoreClock >>= tmp;
}

static void SetSysClock(void)
{

  FLASH->ACR |= FLASH_ACR_ACC64;
  FLASH->ACR |= FLASH_ACR_PRFTEN;
  FLASH->ACR |= FLASH_ACR_LATENCY;
  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  PWR->CR = PWR_CR_VOS_0;
  while((PWR->CSR & PWR_CSR_VOSF) != RESET) {}

  RCC_MSIRangeConfig(RCC_MSIRange_5);
  RCC_MSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_MSIRDY) == RESET) {}

  RCC_HSEConfig(RCC_HSE_OFF);

  RCC_HSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET) {}

  RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_PLLSRC | RCC_CFGR_PLLMUL | RCC_CFGR_PLLDIV));
  RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC_HSI | RCC_CFGR_PLLMUL6 | RCC_CFGR_PLLDIV3);
  RCC->CR |= RCC_CR_PLLON;
  while((RCC->CR & RCC_CR_PLLRDY) == 0) {}

  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV1;
  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV1;
  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV1;

  RCC_MCOConfig(RCC_MCOSource_NoClock, RCC_MCODiv_1);

  PWR_RTCAccessCmd(ENABLE);
  RCC_LSEConfig(RCC_LSE_ON);
  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET) {}
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
  RCC_LSEClockSecuritySystemCmd(ENABLE);
  RCC_LSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET) {}
  RCC_RTCCLKCmd(ENABLE);

}

void SystemInit (void)
{
  RCC->CR |= (uint32_t)0x00000100;
  RCC->CFGR &= (uint32_t)0x88FFC00C;
  RCC->CR &= (uint32_t)0xEEFEFFFE;
  RCC->CR &= (uint32_t)0xFFFBFFFF;
  RCC->CFGR &= (uint32_t)0xFF02FFFF;
  RCC->CIR = 0x00000000;

  SetSysClock();

#ifdef VECT_TAB_SRAM
  SCB->VTOR = SRAM_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal SRAM. */
#else
  SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH. */
#endif
}


void NMI_Handler(void)
{
}

void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1) {
  }
}

void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1) {
  }
}

void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1) {
  }
}

void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1) {
  }
}

void SVC_Handler(void)
{
}

void DebugMon_Handler(void)
{
}

void PendSV_Handler(void)
{
}

void SysTick_Handler(void)
{
  if (my_ticks > 0) my_ticks--;
}

void EXTI15_10_IRQHandler(void)
{
  // Here would be the B1 pin interrupt handler (do where it says "do something..."):
  if ((EXTI_GetITStatus(EXTI_Line13)) != RESET) {
    // do something...
    EXTI_ClearITPendingBit(EXTI_Line13);
  }
}

char t32_buff[11];

void main_welcome_scr(void)
{
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(1, 1, "SLR FILM CAMERA", ST7735_YELLOW, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);
  ST7735_PutStr5x7(3, 2, 15, "Welcome!", ST7735_CYAN, ST7735_BLACK);
  ST7735_PutStr5x7(2, 2, 41, "(c) 2020 by", ST7735_WHITE, ST7735_BLACK);
  ST7735_PutStr5x7(1, 4, 62, "Vasile Guta-Ciucur", ST7735_WHITE, ST7735_BLACK);
  ST7735_BattIcn(BATT_FULL, 130, 2, ST7735_WHITE, ST7735_BLACK);
}

void menu_a_scr(void)
{
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(1, 1, "APERT. PRIORITY", ST7735_WHITE, ST7735_BLACK);
  ST7735_BattIcn(BATT_LOW, 130, 2, ST7735_WHITE, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);
  ST7735_PutXBM48x48(56, 22, ST7735_WHITE, ST7735_BLACK, m_a_48x48);
}

void menu_s_scr(void)
{
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(1, 1, "SHUT. PRIORITY", ST7735_WHITE, ST7735_BLACK);
  ST7735_BattIcn(BATT_LOW, 130, 2, ST7735_WHITE, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);
  ST7735_PutXBM48x48(56, 22, ST7735_WHITE, ST7735_BLACK, m_s_48x48);
}

void menu_m_scr(void)
{
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(1, 1, "MANUAL CONTROLS", ST7735_WHITE, ST7735_BLACK);
  ST7735_BattIcn(BATT_LOW, 130, 2, ST7735_WHITE, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);
  ST7735_PutXBM48x48(56, 22, ST7735_WHITE, ST7735_BLACK, m_m_48x48);
}

void menu_iso_scr(void)
{
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(1, 1, "ISO SENSITIVITY", ST7735_WHITE, ST7735_BLACK);
  ST7735_BattIcn(BATT_LOW, 130, 2, ST7735_WHITE, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);
  ST7735_PutXBM48x48(56, 22, ST7735_WHITE, ST7735_BLACK, m_iso_48x48);
}

void menu_clk_scr(void)
{
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(1, 1, "SET TIME & DATE", ST7735_WHITE, ST7735_BLACK);
  ST7735_BattIcn(BATT_LOW, 130, 2, ST7735_WHITE, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);
  ST7735_PutXBM48x48(56, 22, ST7735_WHITE, ST7735_BLACK, m_clk_48x48);
}

void menu_set_scr(void)
{
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(1, 1, "SETTINGS", ST7735_WHITE, ST7735_BLACK);
  ST7735_BattIcn(BATT_LOW, 130, 2, ST7735_WHITE, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);
  ST7735_PutXBM48x48(56, 22, ST7735_WHITE, ST7735_BLACK, m_set_48x48);
}

void menu_lux_scr(void)
{
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(1, 1, "SEE LUX VALUES", ST7735_WHITE, ST7735_BLACK);
  ST7735_BattIcn(BATT_LOW, 130, 2, ST7735_WHITE, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);
  ST7735_PutXBM48x48(56, 22, ST7735_WHITE, ST7735_BLACK, m_lux_48x48);
}

int main(void)
{
  /* local variables */
  uint8_t mant = 0, exponent = 0, j = 0, n = 0;
  int8_t i = 0, batt_status = 3, t;
  uint8_t lux_xpos[] = {146, 134, 121, 97, 78, 59, 40, 21, 2};
  uint8_t lux_ypos = 45;
  SLR_LUX = 0.0;
  /* mandatory system initializations */
  vpc_system_init();
  //vpc_rtc_init(); /* enable if required */
  vpc_gpio_init();
  //vpc_usart2_uart_init(); /* enable if required */
  vpc_i2c1_init();
  vpc_spi1_init();
  vpc_spi2_init();
  /* third-party initializations */

  /* do your own initializations below */
  tim6_snbd_init();
  max44009_setup_auto_noint(0); // auto mode, cycle at every 800ms

  // reset the TFT LCD
  ST7735_RST_LOW();
  my_delay_ms(50);
  ST7735_RST_HIGH();

  // init the TFT LCD
  ST7735_init();
  ST7735_RotationSet(scr_CW);

  main_welcome_scr();
  my_delay_ms(3000);

  menu_a_scr();
  my_delay_ms(3000);

  menu_s_scr();
  my_delay_ms(3000);

  menu_m_scr();
  my_delay_ms(3000);

  menu_iso_scr();
  my_delay_ms(3000);

  menu_clk_scr();
  my_delay_ms(3000);

  menu_set_scr();
  my_delay_ms(3000);

  menu_lux_scr();
  my_delay_ms(3000);


  //LUX VALUES SCREEN
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(1, 1, "LUX & EV VALUES", ST7735_WHITE, ST7735_BLACK);
  ST7735_BattIcn(BATT_EMPTY, 130, 2, ST7735_WHITE, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);

  // set non-blocking delays
  //tim6_snbd_set(0, 500); // 500ms for battery loop
  //tim6_snbd_set(1, 1000); // 800ms for lux sensor loop

  while(1) {
    /* your forever repeating code */
    if (tim6_snbd_check(0)) { // check the slot 0
      ST7735_BattIcn(batt_status, 130, 2, ST7735_WHITE, ST7735_BLACK);
      batt_status = batt_status - 1;
      if(batt_status < 0) batt_status = 3;
      tim6_snbd_set(0, 500); // 500ms for battery loop
    }

    if (tim6_snbd_check(1)) { // check the slot 1
      _max44009_reg0 = 0;
      _max44009_reg1 = 0;
      max44009_read_2regs(MAX44009_REG_HIGH_BYTE);
      exponent = (_max44009_reg0 & 0xf0) >> 4;
      if (exponent < 15) {
        mant = (_max44009_reg0 & 0x0f) << 4 | _max44009_reg1;
        //SLR_LUX = (float)(((0x00000001 << exponent) * (float)mant) * 0.045);
        SLR_LUX = (float) ((pow(2, (float) exponent) * (float) mant) * 0.045);
        sprintf(t32_buff, "%10.3f", SLR_LUX);
        //double2dec((uint32_t)lux, (uint8_t *)t32_buff);
      } else
        SLR_LUX = 0.0;
      j = 9; //sizeof(t32_buff) - 1;
      n = 0;
      // draw the decimal dot
      ST7735_FillRect(116, 62, 116+2, 62+2, ST7735_CYAN);
      // print the lux value
      for(i = j; i > -1; i--) {
        //
        if(i == j - 3) {
          // avoid the dot
        } else {
          if (n < 3) {
            lux_ypos = 45;
            ST7735_SmallDg(t32_buff[i] - '0', lux_xpos[n], lux_ypos, ST7735_CYAN, ST7735_BLACK);
          } else {
            lux_ypos = 22;
            t = t32_buff[i] - '0';
            if(t>-1)
              ST7735_BigDg(t, lux_xpos[n], lux_ypos, ST7735_CYAN, ST7735_BLACK);
            else
              ST7735_FillRect(lux_xpos[n], lux_ypos, lux_xpos[n]+15, lux_ypos+43, ST7735_BLACK);
          }
          //
          n = n + 1;
        }
      }
      // figure the EV value and print it.
      getEV(SLR_LUX);
      if (SLR_EV < 10) {
        //
        ST7735_SmallDg(0, 134, 22, ST7735_YELLOW, ST7735_BLACK);
        ST7735_SmallDg(SLR_EV, 146, 22, ST7735_YELLOW, ST7735_BLACK);
      } else {
        //
        ST7735_SmallDg(1, 134, 22, ST7735_YELLOW, ST7735_BLACK);
        ST7735_SmallDg(SLR_EV - 10, 146, 22, ST7735_YELLOW, ST7735_BLACK);
      }
      // set the interval delay for the slot 1
      tim6_snbd_set(1, 1000); // 800ms for lux sensor loop
    }
  }
  return 0;
}
