unit adc1_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Spin,
  Buttons, data_unit;

type

  { TForm23 }

  TForm23 = class(TForm)
    BitBtn1: TBitBtn;
    cb_adc1_priority: TComboBox;
    cb_adc1_subpriority: TComboBox;
    cb_adc_tc: TComboBox;
    cb_adc_tce: TComboBox;
    cb_adc_res: TComboBox;
    cb_adc_scan: TComboBox;
    cb_adc_align: TComboBox;
    cb_adc_pre: TComboBox;
    cb_adc_continuous: TComboBox;
    CheckBox2: TCheckBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    priority: TLabel;
    priority1: TLabel;
    sp_adc_nr_conv: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private

  public

  end;

var
  Form23: TForm23;

implementation

{$R *.lfm}

{ TForm23 }



procedure TForm23.BitBtn1Click(Sender: TObject);
begin
  v_adc1_res_idx := cb_adc_res.ItemIndex;
  v_adc1_scan_idx := cb_adc_scan.ItemIndex;
  v_adc1_continuous_idx := cb_adc_continuous.ItemIndex;
  v_adc1_tce_idx := cb_adc_tce.ItemIndex;
  v_adc1_tc_idx := cb_adc_tc.ItemIndex;
  v_adc1_align_idx := cb_adc_align.ItemIndex;
  v_adc1_prescaler_idx := cb_adc_pre.ItemIndex;
  v_adc1_nrconv_idx := sp_adc_nr_conv.Value;
  v_c_gb_int_adc1 := CheckBox2.Checked;
  v_adc1_int_priority := cb_adc1_priority.ItemIndex;
  v_adc1_int_subpriority := cb_adc1_subpriority.ItemIndex;
  close;
end;

procedure TForm23.FormShow(Sender: TObject);
begin
  if v_use_adin then
    Label4.Caption:='Good, we have pins set as Analog Input Channels (ADC_INx).'
  else
    Label4.Caption:='There are no ADC_INx pins, set at least one in Pinout settings window.';
  cb_adc_res.ItemIndex := v_adc1_res_idx;
  cb_adc_scan.ItemIndex := v_adc1_scan_idx;
  cb_adc_continuous.ItemIndex := v_adc1_continuous_idx;
  cb_adc_tce.ItemIndex := v_adc1_tce_idx;
  cb_adc_tc.ItemIndex := v_adc1_tc_idx;
  cb_adc_align.ItemIndex := v_adc1_align_idx;
  cb_adc_pre.ItemIndex := v_adc1_prescaler_idx;
  sp_adc_nr_conv.Value := v_adc1_nrconv_idx;
  CheckBox2.Checked := v_c_gb_int_adc1;
  cb_adc1_priority.ItemIndex := v_adc1_int_priority;
  cb_adc1_subpriority.ItemIndex := v_adc1_int_subpriority;
end;

end.

