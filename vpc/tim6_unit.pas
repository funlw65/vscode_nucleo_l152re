unit tim6_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, data_unit, code_unit;

type

  { TForm11 }

  TForm11 = class(TForm)
    BitBtn1: TBitBtn;
    cb_tim6_subpriority: TComboBox;
    cb_tim6_priority: TComboBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    priority: TLabel;
    priority1: TLabel;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form11: TForm11;

implementation

{$R *.lfm}

{ TForm11 }

procedure TForm11.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked then GroupBox1.Enabled:=TRUE
  else GroupBox1.Enabled:=FALSE;
end;

procedure TForm11.FormClose(Sender: TObject);
begin
  v_tim6_check := CheckBox1.Checked;
  v_tim6_prescaler := SpinEdit1.Value;
  v_tim6_period := SpinEdit2.Value;
  v_tim6_event := ComboBox2.ItemIndex;
  v_tim6_global_int := CheckBox2.Checked;
  v_tim6_int_priority:=validate_priority_value(cb_tim6_priority.ItemIndex);
  v_tim6_int_subpriority:=validate_subpriority_value(cb_tim6_subpriority.ItemIndex);
end;

procedure TForm11.FormShow(Sender: TObject);
begin
  CheckBox1.Checked := v_tim6_check;
  CheckBox1.Enabled := TRUE;
  if CheckBox1.Checked then GroupBox1.Enabled := TRUE;
  Label5.Caption := '...';
  SpinEdit1.Value := v_tim6_prescaler;
  ComboBox1.ItemIndex := 0;
  SpinEdit2.Value := v_tim6_period;
  ComboBox2.ItemIndex := v_tim6_event;
  CheckBox2.Checked := v_tim6_global_int;
  cb_tim6_priority.ItemIndex:=validate_priority_value(v_tim6_int_priority);
  cb_tim6_subpriority.ItemIndex:=validate_subpriority_value(v_tim6_int_subpriority);
end;

procedure TForm11.BitBtn1Click(Sender: TObject);
begin
  close;
end;

end.

