unit spi3_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons, Spin, data_unit, code_unit;

type

  { TForm19 }

  TForm19 = class(TForm)
    BitBtn1: TBitBtn;
    cb_spi3_priority: TComboBox;
    cb_spi3_subpriority: TComboBox;
    cb_spi_cpha: TComboBox;
    cb_spi_cpol: TComboBox;
    cb_spi_dir: TComboBox;
    cb_spi_first: TComboBox;
    cb_spi_mode: TComboBox;
    cb_spi_nss: TComboBox;
    cb_spi_prescaler: TComboBox;
    cb_spi_size: TComboBox;
    CheckBox2: TCheckBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    l_spi3_mess: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    priority: TLabel;
    priority1: TLabel;
    se_spi_poly: TSpinEdit;
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private

  public

  end;

var
  Form19: TForm19;

implementation

{$R *.lfm}

{ TForm19 }


procedure TForm19.FormShow(Sender: TObject);
begin
  if v_c_spi3 then begin
    GroupBox1.Enabled := TRUE;
    l_spi3_mess.Caption:='SPI3 peripheral activated.';
  end
  else begin
    GroupBox1.Enabled := FALSE;
    l_spi3_mess.Caption:='Set PC10-PC11-PC12 pin group as SPI3 from Nucleo or LQFP64 window';
  end;

  cb_spi_dir.ItemIndex := v_cb_direction_idx_spi3;
  cb_spi_mode.ItemIndex := v_cb_mode_idx_spi3;
  cb_spi_size.ItemIndex := v_cb_datasize_idx_spi3;
  cb_spi_cpol.ItemIndex := v_cb_cpol_idx_spi3;
  cb_spi_cpha.ItemIndex := v_cb_cpha_idx_spi3;
  cb_spi_nss.ItemIndex := v_cb_nss_idx_spi3;
  cb_spi_prescaler.ItemIndex := v_cb_prescaler_idx_spi3;
  cb_spi_first.ItemIndex := v_cb_firstbit_idx_spi3;
  se_spi_poly.Value := v_cb_crcpoly_idx_spi3;

  
  CheckBox2.Checked := v_c_gb_int_spi3;
  cb_spi3_priority.ItemIndex:=validate_priority_value(v_spi3_int_priority);
  cb_spi3_subpriority.ItemIndex:=validate_subpriority_value(v_spi3_int_subpriority);
end;

procedure TForm19.FormClose(Sender: TObject);
begin
  v_cb_direction_idx_spi3 := cb_spi_dir.ItemIndex;
  v_cb_mode_idx_spi3 := cb_spi_mode.ItemIndex;
  v_cb_datasize_idx_spi3 := cb_spi_size.ItemIndex;
  v_cb_cpol_idx_spi3 := cb_spi_cpol.ItemIndex;
  v_cb_cpha_idx_spi3 := cb_spi_cpha.ItemIndex;
  v_cb_nss_idx_spi3 := cb_spi_nss.ItemIndex;
  v_cb_prescaler_idx_spi3 := cb_spi_prescaler.ItemIndex;
  v_cb_firstbit_idx_spi3 := cb_spi_first.ItemIndex;
  v_cb_crcpoly_idx_spi3 := se_spi_poly.Value;

  v_c_gb_int_spi3 := CheckBox2.Checked;
  v_spi3_int_priority:=validate_priority_value(cb_spi3_priority.ItemIndex);
  v_spi3_int_subpriority:=validate_subpriority_value(cb_spi3_subpriority.ItemIndex);

end;

procedure TForm19.BitBtn1Click(Sender: TObject);
begin
  close;
end;

end.

