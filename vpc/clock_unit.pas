unit clock_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Spin, data_unit, pinout_unit;

type

  { TForm3 }

  TForm3 = class(TForm)
    BitBtn1: TBitBtn;
    cb_hse_div: TComboBox;
    CB_MCO_DIV: TComboBox;
    c_en_css_hse: TCheckBox;
    cb_ahb_pr: TComboBox;
    cb_systick_pr: TComboBox;
    cb_apb1_pr: TComboBox;
    cb_apb2_pr: TComboBox;
    cb_msi: TComboBox;
    cb_pllmul: TComboBox;
    cb_plldiv: TComboBox;
    c_en_css_lse: TCheckBox;
    C_RCC_MCO: TCheckBox;
    CB_RCC_HSE: TComboBox;
    GroupBox1: TGroupBox;
    GB_PLL: TGroupBox;
    GroupBox3: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Label10: TLabel;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    Label106: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    Label109: TLabel;
    Label11: TLabel;
    Label110: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label3: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label4: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label5: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label6: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label7: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label8: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    l_apb1_x2: TLabel;
    l_apb2_x2: TLabel;
    Label9: TLabel;
    Panel19: TPanel;
    Panel20: TPanel;
    p_lsi: TPanel;
    p_msi: TPanel;
    p_osc32_lcd: TPanel;
    p_osc32_rtc: TPanel;
    p_adc_clk: TPanel;
    p_vco: TPanel;
    p_apb2_t: TPanel;
    p_opa8: TPanel;
    p_pllmul: TPanel;
    p_plldiv: TPanel;
    p_sysclk: TPanel;
    p_usb_freq: TPanel;
    p_hclk: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    p_lse: TPanel;
    p_ck_pwr: TPanel;
    p_fclk: TPanel;
    p_dma: TPanel;
    p_csc: TPanel;
    p_apb1_p: TPanel;
    p_apb1_t: TPanel;
    p_apb2_p: TPanel;
    P_HSE: TPanel;
    P_HSIRC: TPanel;
    rg_clock_hub: TRadioGroup;
    rg_mco_src: TRadioGroup;
    rg_vco: TRadioGroup;
    rg_osc32: TRadioGroup;
    se_hse: TSpinEdit;
    procedure FormClose(Sender: TObject);
    procedure se_hseChange(Sender: TObject);
    procedure transfer_gui_to_globals;
    procedure calculate_local;
    procedure display_values;
    procedure C_RCC_MCOClick(Sender: TObject);
    procedure paint_input_source;
    procedure paint_input_osc32_source;
    procedure BitBtn1Click(Sender: TObject);
    procedure cb_ahb_prChange(Sender: TObject);
    procedure cb_apb1_prChange(Sender: TObject);
    procedure cb_apb2_prChange(Sender: TObject);
    procedure CB_MCO_DIVChange(Sender: TObject);
    procedure cb_msiChange(Sender: TObject);
    procedure cb_plldivChange(Sender: TObject);
    procedure cb_pllmulChange(Sender: TObject);
    procedure cb_systick_prChange(Sender: TObject);
    procedure C_RCC_MCOChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rg_clock_hubClick(Sender: TObject);
    procedure rg_mco_srcClick(Sender: TObject);
    procedure rg_osc32Click(Sender: TObject);
    procedure rg_vcoClick(Sender: TObject);
  private
    { private declarations }
    var
      tmp_w_hse,
      tmp_w_hsi,
      tmp_w_lse,
      tmp_w_lsi,
      tmp_w_vco,
      tmp_w_pllmul,
      tmp_w_usb,
      tmp_w_plldiv,
      tmp_w_msi,
      tmp_w_sysclk,
      tmp_w_hclk,
      tmp_w_tick,
      tmp_w_apb1_p,
      tmp_w_apb1_t,
      tmp_w_apb2_p,
      tmp_w_apb2_t,
      tmp_w_mco,
      tmp_w_hse_rtc : LongWord;
      tmp_w_rtc
      : LongInt;
      tmp_calculation_error : boolean; 
  public
    { public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.lfm}

{ TForm3 }


procedure TForm3.paint_input_source;
begin
  if rg_clock_hub.ItemIndex = 0 then begin
    p_hse.Color:=clGray;
    p_hsirc.Color:=clGray;
    p_msi.Color:=clGreen;
  end;
  if rg_clock_hub.ItemIndex = 1 then begin
    p_hse.Color:=clGreen;
    p_hsirc.Color:=clGray;
    p_msi.Color:=clGray;
  end;
  if rg_clock_hub.ItemIndex = 2 then begin
    p_hse.Color:=clGray;
    p_hsirc.Color:=clGreen;
    p_msi.Color:=clGray;
  end;
  if rg_clock_hub.ItemIndex = 3 then begin
    p_msi.Color:=clGray;
    with rg_vco do begin
      if ItemIndex = 0 then begin
        p_hse.Color:=clGreen;
        p_hsirc.Color:=clGray;
      end
      else begin
        p_hse.Color:=clGray;
        p_hsirc.Color:=clGreen;
      end;
    end;
  end;
end;

procedure TForm3.paint_input_osc32_source;
begin
  case rg_osc32.ItemIndex of
    0: begin
         p_lse.Color:=clGreen;
         p_lsi.Color:=clGray;
       end;
    1: begin
         p_lse.Color:=clGray;
         p_lsi.Color:=clGreen;
       end;
    2: begin
         p_lse.Color:=clGray;
         p_lsi.Color:=clGray;
       end;
  end;
end;

procedure TForm3.transfer_gui_to_globals;
begin
  v_se_hse_value := se_hse.Value;
  v_cb_msi_idx := cb_msi.ItemIndex;
  v_rg_vco_idx := rg_vco.ItemIndex;
  v_cb_pllmul_idx := cb_pllmul.ItemIndex;
  v_cb_plldiv_idx := cb_plldiv.ItemIndex;
  v_rg_clock_hub_idx := rg_clock_hub.ItemIndex;
  v_cb_ahb_pr_idx := cb_ahb_pr.ItemIndex;
  v_cb_systick_pr_idx := cb_systick_pr.ItemIndex;
  v_cb_apb1_pr_idx := cb_apb1_pr.ItemIndex;
  v_cb_apb2_pr_idx := cb_apb2_pr.ItemIndex;
  v_c_en_css_hse := c_en_css_hse.Checked;
  //
  v_cb_rcc_hse_idx := CB_RCC_HSE.ItemIndex;
  //v_cb_rcc_lse_idx := CB_RCC_LSE.ItemIndex;
  v_c_rcc_mco := C_RCC_MCO.Checked;
  v_cb_mco_div_idx := CB_MCO_DIV.ItemIndex;
  v_rg_mco_src_idx := rg_mco_src.ItemIndex;
  //
  v_rg_osc32_idx := rg_osc32.ItemIndex;
  v_cb_hse_div_idx := cb_hse_div.ItemIndex;
  v_c_en_css_lse := c_en_css_lse.Checked;
end;

procedure TForm3.se_hseChange(Sender: TObject);
begin
  calculate_local;
  display_values;
end;

procedure TForm3.FormClose(Sender: TObject);
begin
  calculate;
end;


procedure TForm3.C_RCC_MCOClick(Sender: TObject);
begin
  if c_rcc_mco.Checked then p_opa8.Color:=clTeal
  else p_opa8.Color:=clGray;
  calculate_local;
  display_values;
end;

procedure TForm3.calculate_local;
var tmp: LongWord;  
begin
  //SYSTEM
  tmp_calculation_error := FALSE;
  tmp_w_hse := se_hse.Value * 1000000;
  case rg_vco.ItemIndex of
    0: tmp_w_vco := tmp_w_hse;
    1: tmp_w_vco := tmp_w_hsi;
  end;
  if rg_clock_hub.ItemIndex = 3 then
    if ((tmp_w_vco < 2000000) or (tmp_w_vco > 24000000)) then tmp_calculation_error := TRUE; 
  case cb_pllmul.ItemIndex of
    0: tmp_w_pllmul := tmp_w_vco * 3;
    1: tmp_w_pllmul := tmp_w_vco * 4;
    2: tmp_w_pllmul := tmp_w_vco * 6;
    3: tmp_w_pllmul := tmp_w_vco * 8;
    4: tmp_w_pllmul := tmp_w_vco * 12;
    5: tmp_w_pllmul := tmp_w_vco * 16;
    6: tmp_w_pllmul := tmp_w_vco * 24;
    7: tmp_w_pllmul := tmp_w_vco * 32;  
    8: tmp_w_pllmul := tmp_w_vco * 48;
  end;
  if rg_clock_hub.ItemIndex = 3 then
    if tmp_w_pllmul > 96000000 then tmp_calculation_error := TRUE;
  tmp_w_usb := tmp_w_pllmul div 2;
  if ((rg_vco.ItemIndex=0) and (rg_clock_hub.ItemIndex=3) and (v_c_usb)) then begin
    if tmp_w_usb <> 48000000 then tmp_calculation_error := TRUE;
  end;
  case cb_plldiv.ItemIndex of 
    0: tmp_w_plldiv := tmp_w_pllmul div 2;
    1: tmp_w_plldiv := tmp_w_pllmul div 3;
    2: tmp_w_plldiv := tmp_w_pllmul div 4;
  end; 
  if rg_clock_hub.ItemIndex = 3 then
    if ((tmp_w_plldiv < 2000000) or (tmp_w_plldiv > 32000000)) then tmp_calculation_error := TRUE;
  case cb_msi.ItemIndex of
    0: tmp_w_msi := 65536;
    1: tmp_w_msi := 131072;
    2: tmp_w_msi := 262144;
    3: tmp_w_msi := 524288;
    4: tmp_w_msi := 1048000;
    5: tmp_w_msi := 2097000;
    6: tmp_w_msi := 4194000;
  end;  
  case rg_clock_hub.ItemIndex of
    0: tmp_w_sysclk := tmp_w_msi;
    1: tmp_w_sysclk := tmp_w_hse;
    2: tmp_w_sysclk := tmp_w_hsi;
    3: tmp_w_sysclk := tmp_w_plldiv;
  end; 
  if tmp_w_sysclk > 32000000 then tmp_calculation_error := TRUE;
  case cb_ahb_pr.ItemIndex of
    0: tmp_w_hclk := tmp_w_sysclk div 1;
    1: tmp_w_hclk := tmp_w_sysclk div 2;
    2: tmp_w_hclk := tmp_w_sysclk div 4;
    3: tmp_w_hclk := tmp_w_sysclk div 8;
    4: tmp_w_hclk := tmp_w_sysclk div 16;
    5: tmp_w_hclk := tmp_w_sysclk div 64;
    6: tmp_w_hclk := tmp_w_sysclk div 128;
    7: tmp_w_hclk := tmp_w_sysclk div 256;
    8: tmp_w_hclk := tmp_w_sysclk div 512;
  end;
  if tmp_w_hclk > 32000000 then tmp_calculation_error := TRUE;
  case cb_systick_pr.ItemIndex of
    0: tmp_w_tick := tmp_w_hclk div 1;
    1: tmp_w_tick := tmp_w_hclk div 8;
  end;
  case cb_apb1_pr.ItemIndex of
    0: begin tmp_w_apb1_p := tmp_w_hclk div 1; tmp_w_apb1_t := tmp_w_apb1_p; end;
    1: begin tmp_w_apb1_p := tmp_w_hclk div 2; tmp_w_apb1_t := tmp_w_apb1_p * 2; end;
    2: begin tmp_w_apb1_p := tmp_w_hclk div 4; tmp_w_apb1_t := tmp_w_apb1_p * 2; end;
    3: begin tmp_w_apb1_p := tmp_w_hclk div 8; tmp_w_apb1_t := tmp_w_apb1_p * 2; end;
    4: begin tmp_w_apb1_p := tmp_w_hclk div 16; tmp_w_apb1_t := tmp_w_apb1_p * 2; end;
  end;
  if tmp_w_apb1_p > 32000000 then tmp_calculation_error := TRUE;
  case cb_apb2_pr.ItemIndex of
    0: begin tmp_w_apb2_p := tmp_w_hclk div 1; tmp_w_apb2_t := tmp_w_apb2_p; end;
    1: begin tmp_w_apb2_p := tmp_w_hclk div 2; tmp_w_apb2_t := tmp_w_apb2_p * 2; end;
    2: begin tmp_w_apb2_p := tmp_w_hclk div 4; tmp_w_apb2_t := tmp_w_apb2_p * 2; end;
    3: begin tmp_w_apb2_p := tmp_w_hclk div 8; tmp_w_apb2_t := tmp_w_apb2_p * 2; end;
    4: begin tmp_w_apb2_p := tmp_w_hclk div 16; tmp_w_apb2_t := tmp_w_apb2_p * 2; end;
  end;
  if tmp_w_apb2_p > 32000000 then tmp_calculation_error := TRUE;
  // RTC
  case rg_osc32.ItemIndex of
    0: tmp_w_rtc := tmp_w_lse;
    1: tmp_w_rtc := tmp_w_lsi;
    2: 
      begin
        //
        if cb_hse_div.ItemIndex = 0 then tmp_w_hse_rtc := tmp_w_hse div 2;
        if cb_hse_div.ItemIndex = 1 then tmp_w_hse_rtc := tmp_w_hse div 4;
        if cb_hse_div.ItemIndex = 2 then tmp_w_hse_rtc := tmp_w_hse div 8;
        if cb_hse_div.ItemIndex = 3 then tmp_w_hse_rtc := tmp_w_hse div 16;
        tmp_w_rtc := tmp_w_hse_rtc;
      end;
  end;
  if ((tmp_w_rtc < 0) or (tmp_w_rtc > 1000000)) then tmp_calculation_error := TRUE;
  // MCO
  case rg_mco_src.ItemIndex of
    0: tmp := tmp_w_sysclk;
    1: tmp := tmp_w_hsi;
    2: tmp := tmp_w_msi;
    3: tmp := tmp_w_hse;
    4: tmp := tmp_w_plldiv;
    5: tmp := tmp_w_lsi;
    6: tmp := tmp_w_lse;
  end;
  case cb_mco_div.ItemIndex of
    0: tmp_w_mco := tmp div 1;
    1: tmp_w_mco := tmp div 2;
    2: tmp_w_mco := tmp div 4;
    3: tmp_w_mco := tmp div 8;
    4: tmp_w_mco := tmp div 16;
  end;
end;

procedure TForm3.display_values;
begin
  // OSC32
  if tmp_w_rtc > 1000000 then p_osc32_rtc.Color := clMaroon
  else p_osc32_rtc.Color := clTeal;
  p_osc32_rtc.Caption := inttostr(tmp_w_rtc div 1000);
  // PLL
  if rg_clock_hub.ItemIndex = 3 then begin
    if ((tmp_w_vco < 2000000) or (tmp_w_vco > 24000000)) then p_vco.Color := clMaroon
    else p_vco.Color := clTeal;
  end 
  else p_vco.Color := clGray;
  p_vco.Caption := inttostr(tmp_w_vco div 1000000);
  if rg_clock_hub.ItemIndex = 3 then begin
    if tmp_w_pllmul > 96000000 then p_pllmul.Color := clMaroon
    else p_pllmul.Color := clTeal;
  end
  else p_pllmul.Color := clGray;
  if rg_clock_hub.ItemIndex = 3 then begin
    if ((tmp_w_plldiv < 2000000) or (tmp_w_plldiv > 32000000)) then p_plldiv.Color := clMaroon
    else p_plldiv.Color := clTeal;
  end
  else p_plldiv.Color := clGray;
  // USB
  if ((rg_vco.ItemIndex=0) and (rg_clock_hub.ItemIndex=3) and (v_c_usb)) then begin
    if tmp_w_usb <> 48000000 then p_usb_freq.Color := clMaroon
    else p_usb_freq.Color := clTeal;
  end
  else p_usb_freq.Color := clGray;
  p_usb_freq.Caption := inttostr(tmp_w_usb div 1000000);
  // SYSCLK
  if tmp_w_sysclk > 32000000 then p_sysclk.Color := clMaroon
  else p_sysclk.Color := clTeal;
  p_sysclk.Caption := inttostr(tmp_w_sysclk div 1000000);
  p_ck_pwr.Caption := inttostr(tmp_w_sysclk div 1000000);
  // HCLK
  if tmp_w_hclk > 32000000 then p_hclk.Color := clMaroon
  else p_hclk.Color := clTeal;
  p_hclk.Caption := inttostr(tmp_w_hclk div 1000000);
  p_fclk.Caption := inttostr(tmp_w_hclk div 1000000);
  p_dma.Caption := inttostr(tmp_w_hclk div 1000000);
  // SYSTICK
  p_csc.Caption := inttostr(tmp_w_tick div 1000000);
  // APB1 Per.
  if tmp_w_apb1_p > 32000000 then p_apb1_p.Color := clMaroon
  else p_apb1_p.Color := clTeal;
  p_apb1_p.Caption := inttostr(tmp_w_apb1_p div 1000000);
  p_apb1_t.Caption := inttostr(tmp_w_apb1_t div 1000000);
  // APB2 Per.
  if tmp_w_apb2_p > 32000000 then p_apb2_p.Color := clMaroon
  else p_apb2_p.Color := clTeal;
  p_apb2_p.Caption := inttostr(tmp_w_apb2_p div 1000000);
  p_apb2_t.Caption := inttostr(tmp_w_apb2_t div 1000000);
  // MCO
  p_opa8.Caption := inttostr(tmp_w_mco div 1000);
  // DISABLE THE <OK> BUTTON IF THERE IS AN ERROR
  if tmp_calculation_error then BitBtn1.Enabled:=FALSE
  else BitBtn1.Enabled:=TRUE;
  if cb_apb1_pr.ItemIndex <> 0 then l_apb1_x2.Caption:='x 2'
  else l_apb1_x2.Caption:='x 1';
  if cb_apb2_pr.ItemIndex <> 0 then l_apb2_x2.Caption:='x 2'
  else l_apb2_x2.Caption:='x 1';
end;

procedure TForm3.BitBtn1Click(Sender: TObject);
begin
  {Clock Form}
  v_rg_vco_idx := rg_vco.ItemIndex;
  if v_rg_vco_idx = 1 then v_c_usb := FALSE;
  transfer_gui_to_globals;
  Close;
end;

procedure TForm3.cb_ahb_prChange(Sender: TObject);
begin
  //
  calculate_local;
  display_values;
end;

procedure TForm3.cb_apb1_prChange(Sender: TObject);
begin
  if cb_apb1_pr.ItemIndex <> 0 then l_apb1_x2.Caption:='x 2'
  else l_apb1_x2.Caption:='x 1';
  calculate_local;
  display_values;
end;

procedure TForm3.cb_apb2_prChange(Sender: TObject);
begin
  if cb_apb2_pr.ItemIndex <> 0 then l_apb2_x2.Caption:='x 2'
  else l_apb2_x2.Caption:='x 1';
  calculate_local;
  display_values;
end;

procedure TForm3.CB_MCO_DIVChange(Sender: TObject);
begin

  calculate_local;
  display_values;
end;

procedure TForm3.cb_msiChange(Sender: TObject);
begin

  calculate_local;
  display_values;
end;

procedure TForm3.cb_plldivChange(Sender: TObject);
begin

  calculate_local;
  display_values;
end;

procedure TForm3.cb_pllmulChange(Sender: TObject);
begin

  calculate_local;
  display_values;
end;

procedure TForm3.cb_systick_prChange(Sender: TObject);
begin

  calculate_local;
  display_values;
end;

procedure TForm3.C_RCC_MCOChange(Sender: TObject);
begin
  if C_RCC_MCO.Checked = TRUE then begin
    // if lcd4 module is active and in default mode, then there is a conflict
    if (v_c_lcd4 and v_r2_lcd4) then begin
      //
      v_c_lcd4 := FALSE;
      v_r2_lcd4 := FALSE;
      v_r1_lcd4 := TRUE;
      // reset the involved pins
      reset_default_lcd4_pins;
    end;
    if (v_c_usart1{ and ()})then begin
      v_c_usart1 := FALSE;
      // reset all USART1 pins that have SERIAL function
      // but only if syncron. ...
    end;
    GroupBox3.Enabled := TRUE;
    //SET the PA8 pin as RCC MCO
    v_cb_pa8_idx := 1;
    v_e_pa8_txt :='MCO';
    v_pa8_func := 'CLOCK';
    v_pa8_o_lvl_idx := 0;
    v_pa8_o_mode_idx := 0;
    v_pa8_i_mode_idx := 0;
    v_pa8_a_mode_idx := 0;
    v_pa8_i2c_mode_idx := 0;
    v_pa8_spi_mode_idx := 1;
    v_pa8_int_mode_idx := 0;
    v_pa8_pull_updown_idx := 1;
    v_pa8_speed_idx := 1;
    Form2.P_PA8.Color := clTeal;
  end
  else begin
    if (v_pa8_func = 'CLOCK') then begin
      GroupBox3.Enabled := FALSE;
      //RESET the PA8 pin
      v_cb_pa8_idx := 0;
      v_e_pa8_txt :='';
      v_pa8_func := 'N/A';
      v_pa8_o_lvl_idx := 0;
      v_pa8_o_mode_idx := 0;
      v_pa8_i_mode_idx := 0;
      v_pa8_a_mode_idx := 0;
      v_pa8_i2c_mode_idx := 0;
      v_pa8_spi_mode_idx := 0;
      v_pa8_int_mode_idx := 0;
      v_pa8_pull_updown_idx := 0;
      v_pa8_speed_idx := 0;
      v_e_pa8_txt := '';
    end;
  end;
  calculate_local;
  display_values;
end;




procedure TForm3.FormShow(Sender: TObject);
begin
  tmp_w_hsi:=16000000;
  tmp_w_lse:=32768;
  tmp_w_lsi:=37000;
  se_hse.Value := v_se_hse_value;
  cb_msi.ItemIndex := v_cb_msi_idx;
  rg_vco.ItemIndex := v_rg_vco_idx;
  cb_pllmul.ItemIndex := v_cb_pllmul_idx;
  cb_plldiv.ItemIndex := v_cb_plldiv_idx;
  rg_clock_hub.ItemIndex := v_rg_clock_hub_idx;
  cb_ahb_pr.ItemIndex := v_cb_ahb_pr_idx;
  cb_systick_pr.ItemIndex := v_cb_systick_pr_idx;
  cb_apb1_pr.ItemIndex := v_cb_apb1_pr_idx;
  cb_apb2_pr.ItemIndex := v_cb_apb2_pr_idx;
  c_en_css_hse.Checked := v_c_en_css_hse;
  //
  CB_RCC_HSE.ItemIndex := v_cb_rcc_hse_idx;
  //CB_RCC_LSE.ItemIndex := v_cb_rcc_lse_idx;
  C_RCC_MCO.Checked := v_c_rcc_mco;
  if v_c_rcc_mco then GroupBox3.Enabled := TRUE
  else GroupBox3.Enabled := FALSE;
  CB_MCO_DIV.ItemIndex := v_cb_mco_div_idx;
  rg_mco_src.ItemIndex := v_rg_mco_src_idx;
  //
  rg_osc32.ItemIndex := v_rg_osc32_idx;
  cb_hse_div.ItemIndex := v_cb_hse_div_idx;
  c_en_css_lse.Checked := v_c_en_css_lse;


  calculate_local;
  display_values;
end;

procedure TForm3.rg_clock_hubClick(Sender: TObject);
begin
  paint_input_source;
  if ((rg_clock_hub.ItemIndex = 1) or ((rg_vco.ItemIndex = 0) and (rg_clock_hub.ItemIndex = 3))) then begin
    //
    c_en_css_hse.Enabled := TRUE;
  end
  else begin
    c_en_css_hse.Enabled:=FALSE;
    if c_en_css_hse.Checked then c_en_css_hse.Checked := FALSE;
  end;
  if ((rg_clock_hub.ItemIndex<>1) and (rg_clock_hub.ItemIndex<>3)) then begin
    if rg_osc32.ItemIndex=2 then rg_osc32.ItemIndex:=0;
    if cb_hse_div.Enabled then cb_hse_div.Enabled:=FALSE;
  end;
  if ((rg_clock_hub.ItemIndex=3) and (rg_vco.ItemIndex=1)) then begin
    if rg_osc32.ItemIndex=2 then rg_osc32.ItemIndex:=0;
    if cb_hse_div.Enabled then cb_hse_div.Enabled:=FALSE;
  end;
  calculate_local;
  display_values;
end;

procedure TForm3.rg_mco_srcClick(Sender: TObject);
begin
  if rg_mco_src.ItemIndex = 3 then
    if c_en_css_hse.Enabled = FALSE then rg_mco_src.ItemIndex := 0;
  if rg_mco_src.ItemIndex = 6 then
    if rg_osc32.ItemIndex <> 0 then rg_mco_src.ItemIndex := 5;
  calculate_local;
  display_values;
end;

procedure TForm3.rg_osc32Click(Sender: TObject);
begin
  paint_input_osc32_source;
  if rg_osc32.ItemIndex <> 0 then begin
    c_en_css_lse.Enabled := FALSE;
    if c_en_css_lse.Checked then c_en_css_lse.Checked := FALSE;
  end
  else c_en_css_lse.Enabled := TRUE;
  if (rg_osc32.ItemIndex=2) then begin
    if rg_clock_hub.ItemIndex=1 then cb_hse_div.Enabled:=TRUE
    else begin
      if ((rg_clock_hub.ItemIndex=3) and (rg_vco.ItemIndex=0)) then cb_hse_div.Enabled:=TRUE
      else begin
        cb_hse_div.Enabled:=FALSE;
        rg_osc32.ItemIndex:=0;
      end;
    end;
  end
  else cb_hse_div.Enabled:=FALSE;
  calculate_local;
  display_values;
end;

procedure TForm3.rg_vcoClick(Sender: TObject);
begin
  paint_input_source;
  if ((rg_clock_hub.ItemIndex = 1) or ((rg_vco.ItemIndex = 0) and (rg_clock_hub.ItemIndex = 3))) then begin
    //
    c_en_css_hse.Enabled := TRUE;
  end
  else begin
    c_en_css_hse.Enabled:=FALSE;
    if c_en_css_hse.Checked then c_en_css_hse.Checked := FALSE;
  end;
  if ((rg_vco.ItemIndex = 0) and ((rg_clock_hub.ItemIndex<>3) and (rg_clock_hub.ItemIndex<>1))) then begin
    if rg_osc32.ItemIndex=2 then rg_osc32.ItemIndex:=0;
    if cb_hse_div.Enabled then cb_hse_div.Enabled:=FALSE;
  end;
  if ((rg_vco.ItemIndex = 1) and (rg_clock_hub.ItemIndex=3)) then begin
    if rg_osc32.ItemIndex=2 then rg_osc32.ItemIndex:=0;
    if cb_hse_div.Enabled then cb_hse_div.Enabled:=FALSE;
  end;
  calculate_local;
  display_values;
end;

end.

