unit tim7_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, data_unit, code_unit;

type

  { TForm12 }

  TForm12 = class(TForm)
    BitBtn1: TBitBtn;
    cb_tim7_subpriority: TComboBox;
    cb_tim7_priority: TComboBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    priority: TLabel;
    priority1: TLabel;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form12: TForm12;

implementation

{$R *.lfm}

{ TForm12 }

procedure TForm12.BitBtn1Click(Sender: TObject);
begin
  close;
end;

procedure TForm12.CheckBox1Change(Sender: TObject);
begin
  if CheckBox1.Checked then GroupBox1.Enabled:=TRUE
  else GroupBox1.Enabled:=FALSE;
end;

procedure TForm12.FormClose(Sender: TObject);
begin
  v_tim7_check := CheckBox1.Checked;
  v_tim7_prescaler := SpinEdit1.Value;
  v_tim7_period := SpinEdit2.Value;
  v_tim7_event := ComboBox2.ItemIndex;
  v_tim7_global_int := CheckBox2.Checked;
  v_tim7_int_priority:=validate_priority_value(cb_tim7_priority.ItemIndex);
  v_tim7_int_subpriority:=validate_subpriority_value(cb_tim7_subpriority.ItemIndex);
end;

procedure TForm12.FormShow(Sender: TObject);
begin
  CheckBox1.Checked := v_tim7_check;
  CheckBox1.Enabled := TRUE;
  if CheckBox1.Checked then GroupBox1.Enabled := TRUE;
  Label5.Caption := '...';
  SpinEdit1.Value := v_tim7_prescaler;
  ComboBox1.ItemIndex := 0;
  SpinEdit2.Value := v_tim7_period;
  ComboBox2.ItemIndex := v_tim7_event;
  CheckBox2.Checked := v_tim7_global_int;
  cb_tim7_priority.ItemIndex:=validate_priority_value(v_tim7_int_priority);
  cb_tim7_subpriority.ItemIndex:=validate_subpriority_value(v_tim7_int_subpriority);
end;

end.

