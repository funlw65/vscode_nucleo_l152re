unit code_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dialogs, data_unit, utils_unit;


var
  code_integrity:boolean;

function figure_pin_function(vf:string):string;
procedure pin_preliminary_set(f:string);
procedure validate_buffer(f:string);
procedure name_all_the_pins;

procedure set_priority_message;
procedure validate_exti_priorities;
function validate_priority_value(v:byte):byte;
function validate_subpriority_value(v:byte):byte;
{PA}
procedure pin_PA0_to_buffer;
procedure pin_PA10_to_buffer;
procedure pin_PA11_to_buffer;
procedure pin_PA12_to_buffer;
procedure pin_PA15_to_buffer;
procedure pin_PA1_to_buffer;
procedure pin_PA2_to_buffer;
procedure pin_PA3_to_buffer;
procedure pin_PA4_to_buffer;
procedure pin_PA5_to_buffer;
procedure pin_PA6_to_buffer;
procedure pin_PA7_to_buffer;
procedure pin_PA8_to_buffer;
procedure pin_PA9_to_buffer;
{PB}
procedure pin_PB0_to_buffer;
procedure pin_PB10_to_buffer;
procedure pin_PB11_to_buffer;
procedure pin_PB12_to_buffer;
procedure pin_PB13_to_buffer;
procedure pin_PB14_to_buffer;
procedure pin_PB15_to_buffer;
procedure pin_PB1_to_buffer;
procedure pin_PB2_to_buffer;
procedure pin_PB3_to_buffer;
procedure pin_PB4_to_buffer;
procedure pin_PB5_to_buffer;
procedure pin_PB6_to_buffer;
procedure pin_PB7_to_buffer;
procedure pin_PB8_to_buffer;
procedure pin_PB9_to_buffer;
{PC}
procedure pin_PC0_to_buffer;
procedure pin_PC10_to_buffer;
procedure pin_PC11_to_buffer;
procedure pin_PC12_to_buffer;
procedure pin_PC13_to_buffer;
procedure pin_PC1_to_buffer;
procedure pin_PC2_to_buffer;
procedure pin_PC3_to_buffer;
procedure pin_PC4_to_buffer;
procedure pin_PC5_to_buffer;
procedure pin_PC6_to_buffer;
procedure pin_PC7_to_buffer;
procedure pin_PC8_to_buffer;
procedure pin_PC9_to_buffer;
{PD}
procedure pin_PD2_to_buffer;

procedure scan_for_analog;
procedure scan_for_adin;
function scan_for_exti0:boolean;
function scan_for_exti1:boolean;
function scan_for_exti2:boolean;
function scan_for_exti3:boolean;
function scan_for_exti4:boolean;
function scan_for_exti9_5:boolean;
function scan_for_exti15_10:boolean;
//------------------------
//________________________
procedure wr_main_h;
procedure wr_main_c;
procedure wr_makefile;
//========================
function find_label_duplicates(lbl:string; excluded_pin:string):boolean;
function search_label_duplicates:boolean;
function find_label(lbl:string):boolean;
function verify_lcd4_pin_integrity:boolean;

{ workaround for empty items in XML file }
procedure prepare_labels_for_xml;
procedure clean_labels_from_xml;

implementation

function figure_pin_function(vf:string):string;
var res:string;
begin
  {OUTPUT, INPUT, ANALOG, SPI, I2C, INTERRUPT, EVENT, SERIAL, USB, CLOCK, N/A}
  res := '';
  case vf of
    'Reset_State': res := 'N/A';
    'GPIO_Input': res := 'INPUT';
    'GPIO_Output': res := 'OUTPUT';
    'GPIO_Analog': res := 'ANALOG';
    'GPIO_EXTI0','GPIO_EXTI1','GPIO_EXTI2','GPIO_EXTI3','GPIO_EXTI4','GPIO_EXTI5',
    'GPIO_EXTI6','GPIO_EXTI7','GPIO_EXTI8','GPIO_EXTI9','GPIO_EXTI10','GPIO_EXTI11',
    'GPIO_EXTI12','GPIO_EXTI13','GPIO_EXTI14','GPIO_EXTI15': res := 'INTERRUPT';
    'I2C1_SCL','I2C1_SDA','I2C1_SMBA','I2C2_SCL','I2C2_SDA','I2C2_SMBA': res := 'I2C';
    'SPI1_MOSI','SPI1_MISO','SPI1_SCK','SPI1_NSS','SPI2_MOSI','SPI2_MISO','SPI2_SCK','SPI2_NSS','SPI3_MOSI','SPI3_MISO','SPI3_SCK','SPI3_NSS': res :='SPI';
    'EVENTOUT': res:= 'EVENT';
    'USART1_RX','USART1_TX','USART1_CK','USART1_CTS','USART1_RTS','USART2_RX','USART2_TX','UART4_RX','UART4_TX','UART5_RX','UART5_TX': res:='SERIAL';
    'USB_DP', 'USB_DM': res:='USB';
    'RCC_MCO': res:= 'CLOCK';
    'ADC_IN0','ADC_IN1','ADC_IN4','ADC_IN6','ADC_IN7','ADC_IN8','ADC_IN9','ADC_IN10','ADC_IN11','ADC_IN12',
    'ADC_IN13', 'ADC_IN14','ADC_IN15','ADC_IN18','ADC_IN19','ADC_IN20','ADC_IN21','ADC_IN0b': res:= 'ADIN';
  end;
  if res='' then res := 'N/A';
  result := res;
  {let the gpio form figure out the rest?}
  {well, not quite, did a suplemental step between...}
end;  

procedure validate_buffer(f:string);
begin
  case f of
    'OUTPUT' :
      begin
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        if b_pin_o_lvl_idx = 0 then b_pin_o_lvl_idx := 1;
        if b_pin_o_mode_idx = 0 then b_pin_o_mode_idx := 1;
        if b_pin_pull_updown_idx = 0 then b_pin_pull_updown_idx := 1;
        if b_pin_speed_idx = 0 then b_pin_speed_idx := 1;
      end;
    'INPUT' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_speed_idx := 0;
        b_pin_i_mode_idx := 1;
        if b_pin_pull_updown_idx = 0 then b_pin_pull_updown_idx := 1;
      end;
    'ANALOG' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_speed_idx := 0;
        b_pin_a_mode_idx := 1;
        b_pin_pull_updown_idx := 1;
      end;
    'I2C' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_i2c_mode_idx := 1;
        if ((b_pin_pull_updown_idx <> 1) and (b_pin_pull_updown_idx <> 2)) then b_pin_pull_updown_idx := 1;
        if b_pin_speed_idx = 0 then b_pin_speed_idx := 4;
      end;
    'SPI' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_spi_mode_idx := 1;
        if b_pin_pull_updown_idx = 0 then b_pin_pull_updown_idx := 2;
        if b_pin_speed_idx = 0 then b_pin_speed_idx := 3;
      end;
    'SERIAL' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 2;
        b_pin_spi_mode_idx := 1;
        if b_pin_speed_idx = 0 then b_pin_speed_idx := 4;
      end;
    'EVENT' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_spi_mode_idx := 1;
        if b_pin_pull_updown_idx = 0 then b_pin_pull_updown_idx := 1;
        if b_pin_speed_idx = 0 then b_pin_speed_idx := 1;
      end;
    'CLOCK' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_spi_mode_idx := 1;
        if b_pin_pull_updown_idx = 0 then b_pin_pull_updown_idx := 1;
        if b_pin_speed_idx = 0 then b_pin_speed_idx := 1;
      end;
    'USB' :
      begin
        {yeah, all are zero}
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 0;
        b_pin_speed_idx := 0;
      end;
    'ADIN' :
      begin
        {yeah, all are zero}
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 0;
        b_pin_speed_idx := 0;
      end;
    'INTERRUPT' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_speed_idx := 0;
        if b_pin_int_mode_idx = 0 then b_pin_int_mode_idx := 1;
        if b_pin_pull_updown_idx = 0 then b_pin_pull_updown_idx := 1;
      end;
  end;
end;

procedure pin_preliminary_set(f:string);
begin
  case f of
    'N/A' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 0;
        b_pin_speed_idx := 0;
      end;
    'OUTPUT' :
      begin
        b_pin_o_lvl_idx := 1;
        b_pin_o_mode_idx := 1;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 1;
        b_pin_speed_idx := 2;
      end;
    'INPUT' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 1;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 1;
        b_pin_speed_idx := 0;
      end;
    'ANALOG' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 1;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 1;
        b_pin_speed_idx := 0;
      end;
    'I2C' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 1;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 2;
        b_pin_speed_idx := 4;
      end;
    'SERIAL','EVENT','CLOCK' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 1;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 2;
        b_pin_speed_idx := 4;
      end;
    'SPI' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 1;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 2;
        b_pin_speed_idx := 3;
      end;
    'USB' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 0;
        b_pin_speed_idx := 0;
      end;
    'ADIN' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 0;
        b_pin_pull_updown_idx := 0;
        b_pin_speed_idx := 0;
      end;
    'INTERRUPT' :
      begin
        b_pin_o_lvl_idx := 0;
        b_pin_o_mode_idx := 0;
        b_pin_i_mode_idx := 0;
        b_pin_a_mode_idx := 0;
        b_pin_i2c_mode_idx := 0;
        b_pin_spi_mode_idx := 0;
        b_pin_int_mode_idx := 1;
        b_pin_pull_updown_idx := 1;
        b_pin_speed_idx := 0;
      end;
  end;
end;


function validate_priority_value(v:byte):byte;
var bf:byte;
begin
  case v_group_priority of
    0:
      begin
        if v > 0 then bf := 0
        else bf := 0;
      end;
    1:
      begin
        if v > 1 then bf := 1
        else bf := v;
      end;
    2:
      begin
        if v > 3 then bf := 3
        else bf := v;
      end;
    3:
      begin
        if v > 7 then bf := 7
        else bf := v;
      end;
    4:
      begin
        if v > 15 then bf := 15
        else bf := v;
      end;
  end;
  result := bf;
end;

function validate_subpriority_value(v:byte):byte;
var bf:byte;
begin
  case v_group_priority of
    0:
      begin
        if v > 15 then bf := 15
        else bf := v;
      end;
    1:
      begin
        if v > 7 then bf := 7
        else bf := v;
      end;
    2:
      begin
        if v > 3 then bf := 3
        else bf := v;
      end;
    3:
      begin
        if v > 1 then bf := 1
        else bf := v;
      end;
    4:
      begin
        if v > 0 then bf := 0
        else bf := 0;
      end;
  end;
  result := bf;
end;


procedure validate_exti_priorities;
begin
  case v_group_priority of
    0:
      begin
        if (v_exti0_priority > 0) then v_exti0_priority := 0;
        if (v_exti1_priority > 0) then v_exti1_priority := 0;
        if (v_exti2_priority > 0) then v_exti2_priority := 0;
        if (v_exti3_priority > 0) then v_exti3_priority := 0;
        if (v_exti4_priority > 0) then v_exti4_priority := 0;
        if (v_exti9_5_priority > 0) then v_exti9_5_priority := 0;
        if (v_exti15_10_priority > 0) then v_exti15_10_priority := 0;
      end;
    1:
      begin
        if (v_exti0_priority > 1) then v_exti0_priority := 1;
        if (v_exti0_subpriority > 7) then v_exti0_subpriority := 7;
        if (v_exti1_priority > 1) then v_exti1_priority := 1;
        if (v_exti1_subpriority > 7) then v_exti1_subpriority := 7;
        if (v_exti2_priority > 1) then v_exti2_priority := 1;
        if (v_exti2_subpriority > 7) then v_exti2_subpriority := 7;
        if (v_exti3_priority > 1) then v_exti3_priority := 1;
        if (v_exti3_subpriority > 7) then v_exti3_subpriority := 7;
        if (v_exti4_priority > 1) then v_exti4_priority := 1;
        if (v_exti4_subpriority > 7) then v_exti4_subpriority := 7;
        if (v_exti9_5_priority > 1) then v_exti9_5_priority := 1;
        if (v_exti9_5_subpriority > 7) then v_exti9_5_subpriority := 7;
        if (v_exti15_10_priority > 1) then v_exti15_10_priority := 1;
        if (v_exti15_10_subpriority > 7) then v_exti15_10_subpriority := 7;
      end;
    2:
      begin
        if (v_exti0_priority > 3) then v_exti0_priority := 3;
        if (v_exti0_subpriority > 3) then v_exti0_subpriority := 3;
        if (v_exti1_priority > 3) then v_exti1_priority := 3;
        if (v_exti1_subpriority > 3) then v_exti1_subpriority := 3;
        if (v_exti2_priority > 3) then v_exti2_priority := 3;
        if (v_exti2_subpriority > 3) then v_exti2_subpriority := 3;
        if (v_exti3_priority > 3) then v_exti3_priority := 3;
        if (v_exti3_subpriority > 3) then v_exti3_subpriority := 3;
        if (v_exti4_priority > 3) then v_exti4_priority := 3;
        if (v_exti4_subpriority > 3) then v_exti4_subpriority := 3;
        if (v_exti9_5_priority > 3) then v_exti9_5_priority := 3;
        if (v_exti9_5_subpriority > 3) then v_exti9_5_subpriority := 3;
        if (v_exti15_10_priority > 3) then v_exti15_10_priority := 3;
        if (v_exti15_10_subpriority > 3) then v_exti15_10_subpriority := 3;
      end;
    3:
      begin
        if (v_exti0_priority >7) then v_exti0_priority := 7;
        if (v_exti0_subpriority > 1) then v_exti0_subpriority := 1;
        if (v_exti1_priority >7) then v_exti1_priority := 7;
        if (v_exti1_subpriority > 1) then v_exti1_subpriority := 1;
        if (v_exti2_priority >7) then v_exti2_priority := 7;
        if (v_exti2_subpriority > 1) then v_exti2_subpriority := 1;
        if (v_exti3_priority >7) then v_exti3_priority := 7;
        if (v_exti3_subpriority > 1) then v_exti3_subpriority := 1;
        if (v_exti4_priority >7) then v_exti4_priority := 7;
        if (v_exti4_subpriority > 1) then v_exti4_subpriority := 1;
        if (v_exti9_5_priority >7) then v_exti9_5_priority := 7;
        if (v_exti9_5_subpriority > 1) then v_exti9_5_subpriority := 1;
        if (v_exti15_10_priority >7) then v_exti15_10_priority := 7;
        if (v_exti15_10_subpriority > 1) then v_exti15_10_subpriority := 1;
      end;
    4:
      begin
        if (v_exti0_subpriority > 0) then v_exti0_subpriority := 0;
        if (v_exti1_subpriority > 0) then v_exti1_subpriority := 0;
        if (v_exti2_subpriority > 0) then v_exti2_subpriority := 0;
        if (v_exti3_subpriority > 0) then v_exti3_subpriority := 0;
        if (v_exti4_subpriority > 0) then v_exti4_subpriority := 0;
        if (v_exti9_5_subpriority > 0) then v_exti9_5_subpriority := 0;
        if (v_exti15_10_subpriority > 0) then v_exti15_10_subpriority := 0;
      end;
  end;
end;




procedure set_priority_message;
begin
  case v_group_priority of
    0:
      begin
        v_priority_message:='NVIC PriorityGroup 0, priority range = 0, subpriority range = 0-15';
      end;
    1:
      begin
        v_priority_message:='NVIC PriorityGroup 1, priority range = 0-1, subpriority range = 0-7';
      end;
    2:
      begin
        v_priority_message:='NVIC PriorityGroup 2, priority range = 0-3, subpriority range = 0-3';
      end;
    3:
      begin
        v_priority_message:='NVIC PriorityGroup 3, priority range = 0-7, subpriority range = 0-1';
      end;
    4:
      begin
        v_priority_message:='NVIC PriorityGroup 4, priority range = 0-15, subpriority range = 0';
      end;
  end;
end;


function scan_for_exti0:boolean;
var tst:boolean;
begin
  tst := FALSE;
  if ((v_pa0_func = 'INTERRUPT') or (v_pb0_func = 'INTERRUPT') or (v_pc0_func = 'INTERRUPT')) then
    tst := TRUE;
  result := tst;
end;

function scan_for_exti1:boolean;
var tst:boolean;
begin
  tst := FALSE;
  if ((v_pa1_func = 'INTERRUPT') or (v_pb1_func = 'INTERRUPT') or (v_pc1_func = 'INTERRUPT')) then
    tst := TRUE;
  result := tst;
end;

function scan_for_exti2:boolean;
var tst:boolean;
begin
  tst := FALSE;
  if ((v_pa2_func = 'INTERRUPT') or (v_pb2_func = 'INTERRUPT') or (v_pc2_func = 'INTERRUPT') or (v_pd2_func = 'INTERRUPT')) then
    tst := TRUE;

  result := tst;

end;

function scan_for_exti3:boolean;
var tst:boolean;
begin
  tst := FALSE;
  if ((v_pa3_func = 'INTERRUPT') or (v_pb3_func = 'INTERRUPT') or (v_pc3_func = 'INTERRUPT')) then
    tst := TRUE;

  result := tst;

end;

function scan_for_exti4:boolean;
var tst:boolean;
begin
  tst := FALSE;
  if ((v_pa4_func = 'INTERRUPT') or (v_pb4_func = 'INTERRUPT') or (v_pc4_func = 'INTERRUPT')) then
    tst := TRUE;

  result := tst;

end;

function scan_for_exti9_5:boolean;
var tst:boolean;
begin
  tst := FALSE;
  if ((v_pa5_func = 'INTERRUPT') or (v_pb5_func = 'INTERRUPT') or (v_pc5_func = 'INTERRUPT')) then
    tst := TRUE;
  if ((v_pa6_func = 'INTERRUPT') or (v_pb6_func = 'INTERRUPT') or (v_pc6_func = 'INTERRUPT')) then
    tst := TRUE;
  if ((v_pa7_func = 'INTERRUPT') or (v_pb7_func = 'INTERRUPT') or (v_pc7_func = 'INTERRUPT')) then
    tst := TRUE;
  if ((v_pa8_func = 'INTERRUPT') or (v_pb8_func = 'INTERRUPT') or (v_pc8_func = 'INTERRUPT')) then
    tst := TRUE;
  if ((v_pa9_func = 'INTERRUPT') or (v_pb9_func = 'INTERRUPT') or (v_pc9_func = 'INTERRUPT')) then
    tst := TRUE;

  result := tst;

end;

function scan_for_exti15_10:boolean;
var tst:boolean;
begin
  tst := FALSE;
  if ((v_pa10_func = 'INTERRUPT') or (v_pb10_func = 'INTERRUPT') or (v_pc10_func = 'INTERRUPT')) then
    tst := TRUE;
  if ((v_pa11_func = 'INTERRUPT') or (v_pb11_func = 'INTERRUPT') or (v_pc11_func = 'INTERRUPT')) then
    tst := TRUE;
  if ((v_pa12_func = 'INTERRUPT') or (v_pb12_func = 'INTERRUPT') or (v_pc12_func = 'INTERRUPT')) then
    tst := TRUE;
  if ((v_pb13_func = 'INTERRUPT') or (v_pc13_func = 'INTERRUPT')) then
    tst := TRUE;
  if (v_pb14_func = 'INTERRUPT') then
    tst := TRUE;
  if ((v_pa15_func = 'INTERRUPT') or (v_pb15_func = 'INTERRUPT')) then
    tst := TRUE;

  result := tst;

end;

// name_all_the_pins
{the pin will be "named" as on it's label if it has one, or by it's SPL designated name by following:}
{
  if label is not empty, then the name will be the label+'_Pin' text
  else the name will be the SPL name, e.g. GPIO_Pin_x where x is the number of the pin

  This will add a new variable for the pin... v_pin_name in data_unit (ugh, another copy&paste job, I
  thought I was done with data_unit... )
}
procedure name_all_the_pins;
begin
  {PA}//=============================
  {0}
  if v_e_pa0_txt <> '' then
    v_pa0_name := v_e_pa0_txt+'_Pin'
  else
    v_pa0_name := 'GPIO_Pin_0';
  {1}
  if v_e_pa1_txt <> '' then
    v_pa1_name := v_e_pa1_txt+'_Pin '
  else
    v_pa1_name := 'GPIO_Pin_1';
  {2}
  if v_e_pa2_txt <> '' then
    v_pa2_name := v_e_pa2_txt+'_Pin'
  else
    v_pa2_name := 'GPIO_Pin_2';
  {3}
  if v_e_pa3_txt <> '' then
    v_pa3_name := v_e_pa3_txt+'_Pin'
  else
    v_pa3_name := 'GPIO_Pin_3';
  {4}
  if v_e_pa4_txt <> '' then
    v_pa4_name := v_e_pa4_txt+'_Pin'
  else
    v_pa4_name := 'GPIO_Pin_4';
  {5}
  if v_e_pa5_txt <> '' then
    v_pa5_name := v_e_pa5_txt+'_Pin'
  else
    v_pa5_name := 'GPIO_Pin_5';
  {6}
  if v_e_pa6_txt <> '' then
    v_pa6_name := v_e_pa6_txt+'_Pin'
  else
    v_pa6_name := 'GPIO_Pin_6';
  {7}
  if v_e_pa7_txt <> '' then
    v_pa7_name := v_e_pa7_txt+'_Pin'
  else
    v_pa7_name := 'GPIO_Pin_7';
  {8}
  if v_e_pa8_txt <> '' then
    v_pa8_name := v_e_pa8_txt+'_Pin'
  else
    v_pa8_name := 'GPIO_Pin_8';
  {9}
  if v_e_pa9_txt <> '' then
    v_pa9_name := v_e_pa9_txt+'_Pin'
  else
    v_pa9_name := 'GPIO_Pin_9';
  {10}
  if v_e_pa10_txt <> '' then
    v_pa10_name := v_e_pa10_txt+'_Pin'
  else
    v_pa10_name := 'GPIO_Pin_10';
  {11}
  if v_e_pa11_txt <> '' then
    v_pa11_name := v_e_pa11_txt+'_Pin'
  else
    v_pa11_name := 'GPIO_Pin_11';
  {12}
  if v_e_pa12_txt <> '' then
    v_pa12_name := v_e_pa12_txt+'_Pin'
  else
    v_pa12_name := 'GPIO_Pin_12';
  {15}
  if v_e_pa15_txt <> '' then
    v_pa15_name := v_e_pa15_txt+'_Pin'
  else
    v_pa15_name := 'GPIO_Pin_15';

  {PB}//=============================
  {0}
  if v_e_pb0_txt <> '' then
    v_pb0_name := v_e_pb0_txt+'_Pin'
  else
    v_pb0_name := 'GPIO_Pin_0';
  {1}
  if v_e_pb1_txt <> '' then
    v_pb1_name := v_e_pb1_txt+'_Pin'
  else
    v_pb1_name := 'GPIO_Pin_1';
  {2}
  if v_e_pb2_txt <> '' then
    v_pb2_name := v_e_pb2_txt+'_Pin'
  else
    v_pb2_name := 'GPIO_Pin_2';
  {3}
  if v_e_pb3_txt <> '' then
    v_pb3_name := v_e_pb3_txt+'_Pin'
  else
    v_pb3_name := 'GPIO_Pin_3';
  {4}
  if v_e_pb4_txt <> '' then
    v_pb4_name := v_e_pb4_txt+'_Pin'
  else
    v_pb4_name := 'GPIO_Pin_4';
  {5}
  if v_e_pb5_txt <> '' then
    v_pb5_name := v_e_pb5_txt+'_Pin'
  else
    v_pb5_name := 'GPIO_Pin_5';
  {6}
  if v_e_pb6_txt <> '' then
    v_pb6_name := v_e_pb6_txt+'_Pin'
  else
    v_pb6_name := 'GPIO_Pin_6';
  {7}
  if v_e_pb7_txt <> '' then
    v_pb7_name := v_e_pb7_txt+'_Pin'
  else
    v_pb7_name := 'GPIO_Pin_7';
  {8}
  if v_e_pb8_txt <> '' then
    v_pb8_name := v_e_pb8_txt+'_Pin'
  else
    v_pb8_name := 'GPIO_Pin_8';
  {9}
  if v_e_pb9_txt <> '' then
    v_pb9_name := v_e_pb9_txt+'_Pin'
  else
    v_pb9_name := 'GPIO_Pin_9';
  {10}
  if v_e_pb10_txt <> '' then
    v_pb10_name := v_e_pb10_txt+'_Pin'
  else
    v_pb10_name := 'GPIO_Pin_10';
  {11}
  if v_e_pb11_txt <> '' then
    v_pb11_name := v_e_pb11_txt+'_Pin'
  else
    v_pb11_name := 'GPIO_Pin_11';
  {12}
  if v_e_pb12_txt <> '' then
    v_pb12_name := v_e_pb12_txt+'_Pin'
  else
    v_pb12_name := 'GPIO_Pin_12';
  {13}
  if v_e_pb13_txt <> '' then
    v_pb13_name := v_e_pb13_txt+'_Pin'
  else
    v_pb13_name := 'GPIO_Pin_13';
  {14}
  if v_e_pb14_txt <> '' then
    v_pb14_name := v_e_pb14_txt+'_Pin'
  else
    v_pb14_name := 'GPIO_Pin_14';
  {15}
  if v_e_pb15_txt <> '' then
    v_pb15_name := v_e_pb15_txt+'_Pin'
  else
    v_pb15_name := 'GPIO_Pin_15';

  {PC}//=============================
  {0}
  if v_e_pc0_txt <> '' then
    v_pc0_name := v_e_pc0_txt+'_Pin'
  else
    v_pc0_name := 'GPIO_Pin_0';
  {1}
  if v_e_pc1_txt <> '' then
    v_pc1_name := v_e_pc1_txt+'_Pin'
  else
    v_pc1_name := 'GPIO_Pin_1';
  {2}
  if v_e_pc2_txt <> '' then
    v_pc2_name := v_e_pc2_txt+'_Pin'
  else
    v_pc2_name := 'GPIO_Pin_2';
  {3}
  if v_e_pc3_txt <> '' then
    v_pc3_name := v_e_pc3_txt+'_Pin'
  else
    v_pc3_name := 'GPIO_Pin_3';
  {4}
  if v_e_pc4_txt <> '' then
    v_pc4_name := v_e_pc4_txt+'_Pin'
  else
    v_pc4_name := 'GPIO_Pin_4';
  {5}
  if v_e_pc5_txt <> '' then
    v_pc5_name := v_e_pc5_txt+'_Pin'
  else
    v_pc5_name := 'GPIO_Pin_5';
  {6}
  if v_e_pc6_txt <> '' then
    v_pc6_name := v_e_pc6_txt+'_Pin'
  else
    v_pc6_name := 'GPIO_Pin_6';
  {7}
  if v_e_pc7_txt <> '' then
    v_pc7_name := v_e_pc7_txt+'_Pin'
  else
    v_pc7_name := 'GPIO_Pin_7';
  {8}
  if v_e_pc8_txt <> '' then
    v_pc8_name := v_e_pc8_txt+'_Pin'
  else
    v_pc8_name := 'GPIO_Pin_8';
  {9}
  if v_e_pc9_txt <> '' then
    v_pc9_name := v_e_pc9_txt+'_Pin'
  else
    v_pc9_name := 'GPIO_Pin_9';
  {10}
  if v_e_pc10_txt <> '' then
    v_pc10_name := v_e_pc10_txt+'_Pin'
  else
    v_pc10_name := 'GPIO_Pin_10';
  {11}
  if v_e_pc11_txt <> '' then
    v_pc11_name := v_e_pc11_txt+'_Pin'
  else
    v_pc11_name := 'GPIO_Pin_11';
  {12}
  if v_e_pc12_txt <> '' then
    v_pc12_name := v_e_pc12_txt+'_Pin'
  else
    v_pc12_name := 'GPIO_Pin_12';
  {13}
  if v_e_pc13_txt <> '' then
    v_pc13_name := v_e_pc13_txt+'_Pin'
  else
    v_pc13_name := 'GPIO_Pin_13';

  {PD}
  {2}
  if v_e_pd2_txt <> '' then
    v_pd2_name := v_e_pd2_txt+'_Pin'
  else
    v_pd2_name := 'GPIO_Pin_2';
end;

procedure prepare_labels_for_xml;
begin
  {PA}//=============================
  {0}
  if v_e_pa0_txt = '' then
    v_e_pa0_txt := 'N/A';
  {1}
  if v_e_pa1_txt = '' then
    v_e_pa1_txt := 'N/A';
  {2}
  if v_e_pa2_txt = '' then
    v_e_pa2_txt := 'N/A';
  {3}
  if v_e_pa3_txt = '' then
    v_e_pa3_txt := 'N/A';
  {4}
  if v_e_pa4_txt = '' then
    v_e_pa4_txt := 'N/A';
  {5}
  if v_e_pa5_txt = '' then
    v_e_pa5_txt := 'N/A';
  {6}
  if v_e_pa6_txt = '' then
    v_e_pa6_txt := 'N/A';
  {7}
  if v_e_pa7_txt = '' then
    v_e_pa7_txt := 'N/A';
  {8}
  if v_e_pa8_txt = '' then
    v_e_pa8_txt := 'N/A';
  {9}
  if v_e_pa9_txt = '' then
    v_e_pa9_txt := 'N/A';
  {10}
  if v_e_pa10_txt = '' then
    v_e_pa10_txt := 'N/A';
  {11}
  if v_e_pa11_txt = '' then
    v_e_pa11_txt := 'N/A';
  {12}
  if v_e_pa12_txt = '' then
    v_e_pa12_txt := 'N/A';
  {15}
  if v_e_pa15_txt = '' then
    v_e_pa15_txt := 'N/A';

  {PB}//=============================
  {0}
  if v_e_pb0_txt = '' then
    v_e_pb0_txt := 'N/A';
  {1}
  if v_e_pb1_txt = '' then
    v_e_pb1_txt := 'N/A';
  {2}
  if v_e_pb2_txt = '' then
    v_e_pb2_txt := 'N/A';
  {3}
  if v_e_pb3_txt = '' then
    v_e_pb3_txt := 'N/A';
  {4}
  if v_e_pb4_txt = '' then
    v_e_pb4_txt := 'N/A';
  {5}
  if v_e_pb5_txt = '' then
    v_e_pb5_txt := 'N/A';
  {6}
  if v_e_pb6_txt = '' then
    v_e_pb6_txt := 'N/A';
  {7}
  if v_e_pb7_txt = '' then
    v_e_pb7_txt := 'N/A';
  {8}
  if v_e_pb8_txt = '' then
    v_e_pb8_txt := 'N/A';
  {9}
  if v_e_pb9_txt = '' then
    v_e_pb9_txt := 'N/A';
  {10}
  if v_e_pb10_txt = '' then
    v_e_pb10_txt := 'N/A';
  {11}
  if v_e_pb11_txt = '' then
    v_e_pb11_txt := 'N/A';
  {12}
  if v_e_pb12_txt = '' then
    v_e_pb12_txt := 'N/A';
  {13}
  if v_e_pb13_txt = '' then
    v_e_pb13_txt := 'N/A';
  {14}
  if v_e_pb14_txt = '' then
    v_e_pb14_txt := 'N/A';
  {15}
  if v_e_pb15_txt = '' then
    v_e_pb15_txt := 'N/A';

  {PC}//=============================
  {0}
  if v_e_pc0_txt = '' then
    v_e_pc0_txt := 'N/A';
  {1}
  if v_e_pc1_txt = '' then
    v_e_pc1_txt := 'N/A';
  {2}
  if v_e_pc2_txt = '' then
    v_e_pc2_txt := 'N/A';
  {3}
  if v_e_pc3_txt = '' then
    v_e_pc3_txt := 'N/A';
  {4}
  if v_e_pc4_txt = '' then
    v_e_pc4_txt := 'N/A';
  {5}
  if v_e_pc5_txt = '' then
    v_e_pc5_txt := 'N/A';
  {6}
  if v_e_pc6_txt = '' then
    v_e_pc6_txt := 'N/A';
  {7}
  if v_e_pc7_txt = '' then
    v_e_pc7_txt := 'N/A';
  {8}
  if v_e_pc8_txt = '' then
    v_e_pc8_txt := 'N/A';
  {9}
  if v_e_pc9_txt = '' then
    v_e_pc9_txt := 'N/A';
  {10}
  if v_e_pc10_txt = '' then
    v_e_pc10_txt := 'N/A';
  {11}
  if v_e_pc11_txt = '' then
    v_e_pc11_txt := 'N/A';
  {12}
  if v_e_pc12_txt = '' then
    v_e_pc12_txt := 'N/A';
  {13}
  if v_e_pc13_txt = '' then
    v_e_pc13_txt := 'N/A';

  {PD}
  {2}
  if v_e_pd2_txt = '' then
    v_e_pd2_txt := 'N/A';
end;


procedure clean_labels_from_xml;
begin
  {PA}//=============================
  {0}
  if v_e_pa0_txt = 'N/A' then
    v_e_pa0_txt := '';
  {1}
  if v_e_pa1_txt = 'N/A' then
    v_e_pa1_txt := '';
  {2}
  if v_e_pa2_txt = 'N/A' then
    v_e_pa2_txt := '';
  {3}
  if v_e_pa3_txt = 'N/A' then
    v_e_pa3_txt := '';
  {4}
  if v_e_pa4_txt = 'N/A' then
    v_e_pa4_txt := '';
  {5}
  if v_e_pa5_txt = 'N/A' then
    v_e_pa5_txt := '';
  {6}
  if v_e_pa6_txt = 'N/A' then
    v_e_pa6_txt := '';
  {7}
  if v_e_pa7_txt = 'N/A' then
    v_e_pa7_txt := '';
  {8}
  if v_e_pa8_txt = 'N/A' then
    v_e_pa8_txt := '';
  {9}
  if v_e_pa9_txt = 'N/A' then
    v_e_pa9_txt := '';
  {10}
  if v_e_pa10_txt = 'N/A' then
    v_e_pa10_txt := '';
  {11}
  if v_e_pa11_txt = 'N/A' then
    v_e_pa11_txt := '';
  {12}
  if v_e_pa12_txt = 'N/A' then
    v_e_pa12_txt := '';
  {15}
  if v_e_pa15_txt = 'N/A' then
    v_e_pa15_txt := '';

  {PB}//=============================
  {0}
  if v_e_pb0_txt = 'N/A' then
    v_e_pb0_txt := '';
  {1}
  if v_e_pb1_txt = 'N/A' then
    v_e_pb1_txt := '';
  {2}
  if v_e_pb2_txt = 'N/A' then
    v_e_pb2_txt := '';
  {3}
  if v_e_pb3_txt = 'N/A' then
    v_e_pb3_txt := '';
  {4}
  if v_e_pb4_txt = 'N/A' then
    v_e_pb4_txt := '';
  {5}
  if v_e_pb5_txt = 'N/A' then
    v_e_pb5_txt := '';
  {6}
  if v_e_pb6_txt = 'N/A' then
    v_e_pb6_txt := '';
  {7}
  if v_e_pb7_txt = 'N/A' then
    v_e_pb7_txt := '';
  {8}
  if v_e_pb8_txt = 'N/A' then
    v_e_pb8_txt := '';
  {9}
  if v_e_pb9_txt = 'N/A' then
    v_e_pb9_txt := '';
  {10}
  if v_e_pb10_txt = 'N/A' then
    v_e_pb10_txt := '';
  {11}
  if v_e_pb11_txt = 'N/A' then
    v_e_pb11_txt := '';
  {12}
  if v_e_pb12_txt = 'N/A' then
    v_e_pb12_txt := '';
  {13}
  if v_e_pb13_txt = 'N/A' then
    v_e_pb13_txt := '';
  {14}
  if v_e_pb14_txt = 'N/A' then
    v_e_pb14_txt := '';
  {15}
  if v_e_pb15_txt = 'N/A' then
    v_e_pb15_txt := '';

  {PC}//=============================
  {0}
  if v_e_pc0_txt = 'N/A' then
    v_e_pc0_txt := '';
  {1}
  if v_e_pc1_txt = 'N/A' then
    v_e_pc1_txt := '';
  {2}
  if v_e_pc2_txt = 'N/A' then
    v_e_pc2_txt := '';
  {3}
  if v_e_pc3_txt = 'N/A' then
    v_e_pc3_txt := '';
  {4}
  if v_e_pc4_txt = 'N/A' then
    v_e_pc4_txt := '';
  {5}
  if v_e_pc5_txt = 'N/A' then
    v_e_pc5_txt := '';
  {6}
  if v_e_pc6_txt = 'N/A' then
    v_e_pc6_txt := '';
  {7}
  if v_e_pc7_txt = 'N/A' then
    v_e_pc7_txt := '';
  {8}
  if v_e_pc8_txt = 'N/A' then
    v_e_pc8_txt := '';
  {9}
  if v_e_pc9_txt = 'N/A' then
    v_e_pc9_txt := '';
  {10}
  if v_e_pc10_txt = 'N/A' then
    v_e_pc10_txt := '';
  {11}
  if v_e_pc11_txt = 'N/A' then
    v_e_pc11_txt := '';
  {12}
  if v_e_pc12_txt = 'N/A' then
    v_e_pc12_txt := '';
  {13}
  if v_e_pc13_txt = 'N/A' then
    v_e_pc13_txt := '';

  {PD}
  {2}
  if v_e_pd2_txt = 'N/A' then
    v_e_pd2_txt := '';
end;


procedure  pin_PA0_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA0_o_lvl_idx;
  b_pin_o_mode_idx := v_PA0_o_mode_idx;
  b_pin_i_mode_idx := v_PA0_i_mode_idx;
  b_pin_a_mode_idx := v_PA0_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA0_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA0_spi_mode_idx;
  b_pin_int_mode_idx := v_PA0_int_mode_idx;
  b_pin_pull_updown_idx := v_PA0_pull_updown_idx;
  b_pin_speed_idx := v_PA0_speed_idx;
  b_pin_func := v_PA0_func;
end;

procedure  pin_PA10_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA10_o_lvl_idx;
  b_pin_o_mode_idx := v_PA10_o_mode_idx;
  b_pin_i_mode_idx := v_PA10_i_mode_idx;
  b_pin_a_mode_idx := v_PA10_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA10_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA10_spi_mode_idx;
  b_pin_int_mode_idx := v_PA10_int_mode_idx;
  b_pin_pull_updown_idx := v_PA10_pull_updown_idx;
  b_pin_speed_idx := v_PA10_speed_idx;
  b_pin_func := v_PA10_func;
end;

procedure  pin_PA11_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA11_o_lvl_idx;
  b_pin_o_mode_idx := v_PA11_o_mode_idx;
  b_pin_i_mode_idx := v_PA11_i_mode_idx;
  b_pin_a_mode_idx := v_PA11_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA11_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA11_spi_mode_idx;
  b_pin_int_mode_idx := v_PA11_int_mode_idx;
  b_pin_pull_updown_idx := v_PA11_pull_updown_idx;
  b_pin_speed_idx := v_PA11_speed_idx;
  b_pin_func := v_PA11_func;
end;

procedure  pin_PA12_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA12_o_lvl_idx;
  b_pin_o_mode_idx := v_PA12_o_mode_idx;
  b_pin_i_mode_idx := v_PA12_i_mode_idx;
  b_pin_a_mode_idx := v_PA12_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA12_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA12_spi_mode_idx;
  b_pin_int_mode_idx := v_PA12_int_mode_idx;
  b_pin_pull_updown_idx := v_PA12_pull_updown_idx;
  b_pin_speed_idx := v_PA12_speed_idx;
  b_pin_func := v_PA12_func;
end;

procedure  pin_PA15_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA15_o_lvl_idx;
  b_pin_o_mode_idx := v_PA15_o_mode_idx;
  b_pin_i_mode_idx := v_PA15_i_mode_idx;
  b_pin_a_mode_idx := v_PA15_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA15_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA15_spi_mode_idx;
  b_pin_int_mode_idx := v_PA15_int_mode_idx;
  b_pin_pull_updown_idx := v_PA15_pull_updown_idx;
  b_pin_speed_idx := v_PA15_speed_idx;
  b_pin_func := v_PA15_func;
end;

procedure  pin_PA1_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA1_o_lvl_idx;
  b_pin_o_mode_idx := v_PA1_o_mode_idx;
  b_pin_i_mode_idx := v_PA1_i_mode_idx;
  b_pin_a_mode_idx := v_PA1_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA1_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA1_spi_mode_idx;
  b_pin_int_mode_idx := v_PA1_int_mode_idx;
  b_pin_pull_updown_idx := v_PA1_pull_updown_idx;
  b_pin_speed_idx := v_PA1_speed_idx;
  b_pin_func := v_PA1_func;
end;

procedure  pin_PA2_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA2_o_lvl_idx;
  b_pin_o_mode_idx := v_PA2_o_mode_idx;
  b_pin_i_mode_idx := v_PA2_i_mode_idx;
  b_pin_a_mode_idx := v_PA2_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA2_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA2_spi_mode_idx;
  b_pin_int_mode_idx := v_PA2_int_mode_idx;
  b_pin_pull_updown_idx := v_PA2_pull_updown_idx;
  b_pin_speed_idx := v_PA2_speed_idx;
  b_pin_func := v_PA2_func;
end;

procedure  pin_PA3_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA3_o_lvl_idx;
  b_pin_o_mode_idx := v_PA3_o_mode_idx;
  b_pin_i_mode_idx := v_PA3_i_mode_idx;
  b_pin_a_mode_idx := v_PA3_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA3_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA3_spi_mode_idx;
  b_pin_int_mode_idx := v_PA3_int_mode_idx;
  b_pin_pull_updown_idx := v_PA3_pull_updown_idx;
  b_pin_speed_idx := v_PA3_speed_idx;
  b_pin_func := v_PA3_func;
end;

procedure  pin_PA4_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA4_o_lvl_idx;
  b_pin_o_mode_idx := v_PA4_o_mode_idx;
  b_pin_i_mode_idx := v_PA4_i_mode_idx;
  b_pin_a_mode_idx := v_PA4_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA4_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA4_spi_mode_idx;
  b_pin_int_mode_idx := v_PA4_int_mode_idx;
  b_pin_pull_updown_idx := v_PA4_pull_updown_idx;
  b_pin_speed_idx := v_PA4_speed_idx;
  b_pin_func := v_PA4_func;
end;

procedure  pin_PA5_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA5_o_lvl_idx;
  b_pin_o_mode_idx := v_PA5_o_mode_idx;
  b_pin_i_mode_idx := v_PA5_i_mode_idx;
  b_pin_a_mode_idx := v_PA5_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA5_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA5_spi_mode_idx;
  b_pin_int_mode_idx := v_PA5_int_mode_idx;
  b_pin_pull_updown_idx := v_PA5_pull_updown_idx;
  b_pin_speed_idx := v_PA5_speed_idx;
  b_pin_func := v_PA5_func;
end;


procedure  pin_PA6_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA6_o_lvl_idx;
  b_pin_o_mode_idx := v_PA6_o_mode_idx;
  b_pin_i_mode_idx := v_PA6_i_mode_idx;
  b_pin_a_mode_idx := v_PA6_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA6_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA6_spi_mode_idx;
  b_pin_int_mode_idx := v_PA6_int_mode_idx;
  b_pin_pull_updown_idx := v_PA6_pull_updown_idx;
  b_pin_speed_idx := v_PA6_speed_idx;
  b_pin_func := v_PA6_func;
end;

procedure  pin_PA7_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA7_o_lvl_idx;
  b_pin_o_mode_idx := v_PA7_o_mode_idx;
  b_pin_i_mode_idx := v_PA7_i_mode_idx;
  b_pin_a_mode_idx := v_PA7_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA7_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA7_spi_mode_idx;
  b_pin_int_mode_idx := v_PA7_int_mode_idx;
  b_pin_pull_updown_idx := v_PA7_pull_updown_idx;
  b_pin_speed_idx := v_PA7_speed_idx;
  b_pin_func := v_PA7_func;
end;

procedure  pin_PA8_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA8_o_lvl_idx;
  b_pin_o_mode_idx := v_PA8_o_mode_idx;
  b_pin_i_mode_idx := v_PA8_i_mode_idx;
  b_pin_a_mode_idx := v_PA8_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA8_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA8_spi_mode_idx;
  b_pin_int_mode_idx := v_PA8_int_mode_idx;
  b_pin_pull_updown_idx := v_PA8_pull_updown_idx;
  b_pin_speed_idx := v_PA8_speed_idx;
  b_pin_func := v_PA8_func;
end;

procedure  pin_PA9_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PA9_o_lvl_idx;
  b_pin_o_mode_idx := v_PA9_o_mode_idx;
  b_pin_i_mode_idx := v_PA9_i_mode_idx;
  b_pin_a_mode_idx := v_PA9_a_mode_idx;
  b_pin_i2c_mode_idx := v_PA9_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PA9_spi_mode_idx;
  b_pin_int_mode_idx := v_PA9_int_mode_idx;
  b_pin_pull_updown_idx := v_PA9_pull_updown_idx;
  b_pin_speed_idx := v_PA9_speed_idx;
  b_pin_func := v_PA9_func;
end;

{PB}
procedure  pin_PB0_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB0_o_lvl_idx;
  b_pin_o_mode_idx := v_PB0_o_mode_idx;
  b_pin_i_mode_idx := v_PB0_i_mode_idx;
  b_pin_a_mode_idx := v_PB0_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB0_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB0_spi_mode_idx;
  b_pin_int_mode_idx := v_PB0_int_mode_idx;
  b_pin_pull_updown_idx := v_PB0_pull_updown_idx;
  b_pin_speed_idx := v_PB0_speed_idx;
  b_pin_func := v_PB0_func;
end;

procedure  pin_PB10_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB10_o_lvl_idx;
  b_pin_o_mode_idx := v_PB10_o_mode_idx;
  b_pin_i_mode_idx := v_PB10_i_mode_idx;
  b_pin_a_mode_idx := v_PB10_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB10_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB10_spi_mode_idx;
  b_pin_int_mode_idx := v_PB10_int_mode_idx;
  b_pin_pull_updown_idx := v_PB10_pull_updown_idx;
  b_pin_speed_idx := v_PB10_speed_idx;
  b_pin_func := v_PB10_func;
end;

procedure  pin_PB11_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB11_o_lvl_idx;
  b_pin_o_mode_idx := v_PB11_o_mode_idx;
  b_pin_i_mode_idx := v_PB11_i_mode_idx;
  b_pin_a_mode_idx := v_PB11_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB11_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB11_spi_mode_idx;
  b_pin_int_mode_idx := v_PB11_int_mode_idx;
  b_pin_pull_updown_idx := v_PB11_pull_updown_idx;
  b_pin_speed_idx := v_PB11_speed_idx;
  b_pin_func := v_PB11_func;
end;

procedure  pin_PB12_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB12_o_lvl_idx;
  b_pin_o_mode_idx := v_PB12_o_mode_idx;
  b_pin_i_mode_idx := v_PB12_i_mode_idx;
  b_pin_a_mode_idx := v_PB12_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB12_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB12_spi_mode_idx;
  b_pin_int_mode_idx := v_PB12_int_mode_idx;
  b_pin_pull_updown_idx := v_PB12_pull_updown_idx;
  b_pin_speed_idx := v_PB12_speed_idx;
  b_pin_func := v_PB12_func;
end;

procedure  pin_PB13_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB13_o_lvl_idx;
  b_pin_o_mode_idx := v_PB13_o_mode_idx;
  b_pin_i_mode_idx := v_PB13_i_mode_idx;
  b_pin_a_mode_idx := v_PB13_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB13_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB13_spi_mode_idx;
  b_pin_int_mode_idx := v_PB13_int_mode_idx;
  b_pin_pull_updown_idx := v_PB13_pull_updown_idx;
  b_pin_speed_idx := v_PB13_speed_idx;
  b_pin_func := v_PB13_func;
end;

procedure  pin_PB14_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB14_o_lvl_idx;
  b_pin_o_mode_idx := v_PB14_o_mode_idx;
  b_pin_i_mode_idx := v_PB14_i_mode_idx;
  b_pin_a_mode_idx := v_PB14_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB14_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB14_spi_mode_idx;
  b_pin_int_mode_idx := v_PB14_int_mode_idx;
  b_pin_pull_updown_idx := v_PB14_pull_updown_idx;
  b_pin_speed_idx := v_PB14_speed_idx;
  b_pin_func := v_PB14_func;
end;

procedure  pin_PB15_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB15_o_lvl_idx;
  b_pin_o_mode_idx := v_PB15_o_mode_idx;
  b_pin_i_mode_idx := v_PB15_i_mode_idx;
  b_pin_a_mode_idx := v_PB15_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB15_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB15_spi_mode_idx;
  b_pin_int_mode_idx := v_PB15_int_mode_idx;
  b_pin_pull_updown_idx := v_PB15_pull_updown_idx;
  b_pin_speed_idx := v_PB15_speed_idx;
  b_pin_func := v_PB15_func;
end;

procedure  pin_PB1_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB1_o_lvl_idx;
  b_pin_o_mode_idx := v_PB1_o_mode_idx;
  b_pin_i_mode_idx := v_PB1_i_mode_idx;
  b_pin_a_mode_idx := v_PB1_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB1_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB1_spi_mode_idx;
  b_pin_int_mode_idx := v_PB1_int_mode_idx;
  b_pin_pull_updown_idx := v_PB1_pull_updown_idx;
  b_pin_speed_idx := v_PB1_speed_idx;
  b_pin_func := v_PB1_func;
end;

procedure  pin_PB2_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB2_o_lvl_idx;
  b_pin_o_mode_idx := v_PB2_o_mode_idx;
  b_pin_i_mode_idx := v_PB2_i_mode_idx;
  b_pin_a_mode_idx := v_PB2_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB2_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB2_spi_mode_idx;
  b_pin_int_mode_idx := v_PB2_int_mode_idx;
  b_pin_pull_updown_idx := v_PB2_pull_updown_idx;
  b_pin_speed_idx := v_PB2_speed_idx;
  b_pin_func := v_PB2_func;
end;

procedure  pin_PB3_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB3_o_lvl_idx;
  b_pin_o_mode_idx := v_PB3_o_mode_idx;
  b_pin_i_mode_idx := v_PB3_i_mode_idx;
  b_pin_a_mode_idx := v_PB3_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB3_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB3_spi_mode_idx;
  b_pin_int_mode_idx := v_PB3_int_mode_idx;
  b_pin_pull_updown_idx := v_PB3_pull_updown_idx;
  b_pin_speed_idx := v_PB3_speed_idx;
  b_pin_func := v_PB3_func;
end;

procedure  pin_PB4_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB4_o_lvl_idx;
  b_pin_o_mode_idx := v_PB4_o_mode_idx;
  b_pin_i_mode_idx := v_PB4_i_mode_idx;
  b_pin_a_mode_idx := v_PB4_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB4_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB4_spi_mode_idx;
  b_pin_int_mode_idx := v_PB4_int_mode_idx;
  b_pin_pull_updown_idx := v_PB4_pull_updown_idx;
  b_pin_speed_idx := v_PB4_speed_idx;
  b_pin_func := v_PB4_func;
end;

procedure  pin_PB5_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB5_o_lvl_idx;
  b_pin_o_mode_idx := v_PB5_o_mode_idx;
  b_pin_i_mode_idx := v_PB5_i_mode_idx;
  b_pin_a_mode_idx := v_PB5_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB5_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB5_spi_mode_idx;
  b_pin_int_mode_idx := v_PB5_int_mode_idx;
  b_pin_pull_updown_idx := v_PB5_pull_updown_idx;
  b_pin_speed_idx := v_PB5_speed_idx;
  b_pin_func := v_PB5_func;
end;

procedure  pin_PB6_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB6_o_lvl_idx;
  b_pin_o_mode_idx := v_PB6_o_mode_idx;
  b_pin_i_mode_idx := v_PB6_i_mode_idx;
  b_pin_a_mode_idx := v_PB6_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB6_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB6_spi_mode_idx;
  b_pin_int_mode_idx := v_PB6_int_mode_idx;
  b_pin_pull_updown_idx := v_PB6_pull_updown_idx;
  b_pin_speed_idx := v_PB6_speed_idx;
  b_pin_func := v_PB6_func;
end;

procedure  pin_PB7_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB7_o_lvl_idx;
  b_pin_o_mode_idx := v_PB7_o_mode_idx;
  b_pin_i_mode_idx := v_PB7_i_mode_idx;
  b_pin_a_mode_idx := v_PB7_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB7_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB7_spi_mode_idx;
  b_pin_int_mode_idx := v_PB7_int_mode_idx;
  b_pin_pull_updown_idx := v_PB7_pull_updown_idx;
  b_pin_speed_idx := v_PB7_speed_idx;
  b_pin_func := v_PB7_func;
end;

procedure  pin_PB8_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB8_o_lvl_idx;
  b_pin_o_mode_idx := v_PB8_o_mode_idx;
  b_pin_i_mode_idx := v_PB8_i_mode_idx;
  b_pin_a_mode_idx := v_PB8_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB8_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB8_spi_mode_idx;
  b_pin_int_mode_idx := v_PB8_int_mode_idx;
  b_pin_pull_updown_idx := v_PB8_pull_updown_idx;
  b_pin_speed_idx := v_PB8_speed_idx;
  b_pin_func := v_PB8_func;
end;

procedure  pin_PB9_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PB9_o_lvl_idx;
  b_pin_o_mode_idx := v_PB9_o_mode_idx;
  b_pin_i_mode_idx := v_PB9_i_mode_idx;
  b_pin_a_mode_idx := v_PB9_a_mode_idx;
  b_pin_i2c_mode_idx := v_PB9_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PB9_spi_mode_idx;
  b_pin_int_mode_idx := v_PB9_int_mode_idx;
  b_pin_pull_updown_idx := v_PB9_pull_updown_idx;
  b_pin_speed_idx := v_PB9_speed_idx;
  b_pin_func := v_PB9_func;
end;

{PC}
procedure  pin_PC0_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC0_o_lvl_idx;
  b_pin_o_mode_idx := v_PC0_o_mode_idx;
  b_pin_i_mode_idx := v_PC0_i_mode_idx;
  b_pin_a_mode_idx := v_PC0_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC0_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC0_spi_mode_idx;
  b_pin_int_mode_idx := v_PC0_int_mode_idx;
  b_pin_pull_updown_idx := v_PC0_pull_updown_idx;
  b_pin_speed_idx := v_PC0_speed_idx;
  b_pin_func := v_PC0_func;
end;

procedure  pin_PC10_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC10_o_lvl_idx;
  b_pin_o_mode_idx := v_PC10_o_mode_idx;
  b_pin_i_mode_idx := v_PC10_i_mode_idx;
  b_pin_a_mode_idx := v_PC10_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC10_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC10_spi_mode_idx;
  b_pin_int_mode_idx := v_PC10_int_mode_idx;
  b_pin_pull_updown_idx := v_PC10_pull_updown_idx;
  b_pin_speed_idx := v_PC10_speed_idx;
  b_pin_func := v_PC10_func;
end;

procedure  pin_PC11_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC11_o_lvl_idx;
  b_pin_o_mode_idx := v_PC11_o_mode_idx;
  b_pin_i_mode_idx := v_PC11_i_mode_idx;
  b_pin_a_mode_idx := v_PC11_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC11_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC11_spi_mode_idx;
  b_pin_int_mode_idx := v_PC11_int_mode_idx;
  b_pin_pull_updown_idx := v_PC11_pull_updown_idx;
  b_pin_speed_idx := v_PC11_speed_idx;
  b_pin_func := v_PC11_func;
end;

procedure  pin_PC12_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC12_o_lvl_idx;
  b_pin_o_mode_idx := v_PC12_o_mode_idx;
  b_pin_i_mode_idx := v_PC12_i_mode_idx;
  b_pin_a_mode_idx := v_PC12_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC12_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC12_spi_mode_idx;
  b_pin_int_mode_idx := v_PC12_int_mode_idx;
  b_pin_pull_updown_idx := v_PC12_pull_updown_idx;
  b_pin_speed_idx := v_PC12_speed_idx;
  b_pin_func := v_PC12_func;
end;

procedure  pin_PC13_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC13_o_lvl_idx;
  b_pin_o_mode_idx := v_PC13_o_mode_idx;
  b_pin_i_mode_idx := v_PC13_i_mode_idx;
  b_pin_a_mode_idx := v_PC13_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC13_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC13_spi_mode_idx;
  b_pin_int_mode_idx := v_PC13_int_mode_idx;
  b_pin_pull_updown_idx := v_PC13_pull_updown_idx;
  b_pin_speed_idx := v_PC13_speed_idx;
  b_pin_func := v_PC13_func;
end;


procedure  pin_PC1_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC1_o_lvl_idx;
  b_pin_o_mode_idx := v_PC1_o_mode_idx;
  b_pin_i_mode_idx := v_PC1_i_mode_idx;
  b_pin_a_mode_idx := v_PC1_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC1_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC1_spi_mode_idx;
  b_pin_int_mode_idx := v_PC1_int_mode_idx;
  b_pin_pull_updown_idx := v_PC1_pull_updown_idx;
  b_pin_speed_idx := v_PC1_speed_idx;
  b_pin_func := v_PC1_func;
end;

procedure  pin_PC2_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC2_o_lvl_idx;
  b_pin_o_mode_idx := v_PC2_o_mode_idx;
  b_pin_i_mode_idx := v_PC2_i_mode_idx;
  b_pin_a_mode_idx := v_PC2_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC2_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC2_spi_mode_idx;
  b_pin_int_mode_idx := v_PC2_int_mode_idx;
  b_pin_pull_updown_idx := v_PC2_pull_updown_idx;
  b_pin_speed_idx := v_PC2_speed_idx;
  b_pin_func := v_PC2_func;
end;

procedure  pin_PC3_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC3_o_lvl_idx;
  b_pin_o_mode_idx := v_PC3_o_mode_idx;
  b_pin_i_mode_idx := v_PC3_i_mode_idx;
  b_pin_a_mode_idx := v_PC3_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC3_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC3_spi_mode_idx;
  b_pin_int_mode_idx := v_PC3_int_mode_idx;
  b_pin_pull_updown_idx := v_PC3_pull_updown_idx;
  b_pin_speed_idx := v_PC3_speed_idx;
  b_pin_func := v_PC3_func;
end;

procedure  pin_PC4_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC4_o_lvl_idx;
  b_pin_o_mode_idx := v_PC4_o_mode_idx;
  b_pin_i_mode_idx := v_PC4_i_mode_idx;
  b_pin_a_mode_idx := v_PC4_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC4_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC4_spi_mode_idx;
  b_pin_int_mode_idx := v_PC4_int_mode_idx;
  b_pin_pull_updown_idx := v_PC4_pull_updown_idx;
  b_pin_speed_idx := v_PC4_speed_idx;
  b_pin_func := v_PC4_func;
end;

procedure  pin_PC5_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC5_o_lvl_idx;
  b_pin_o_mode_idx := v_PC5_o_mode_idx;
  b_pin_i_mode_idx := v_PC5_i_mode_idx;
  b_pin_a_mode_idx := v_PC5_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC5_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC5_spi_mode_idx;
  b_pin_int_mode_idx := v_PC5_int_mode_idx;
  b_pin_pull_updown_idx := v_PC5_pull_updown_idx;
  b_pin_speed_idx := v_PC5_speed_idx;
  b_pin_func := v_PC5_func;
end;

procedure  pin_PC6_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC6_o_lvl_idx;
  b_pin_o_mode_idx := v_PC6_o_mode_idx;
  b_pin_i_mode_idx := v_PC6_i_mode_idx;
  b_pin_a_mode_idx := v_PC6_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC6_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC6_spi_mode_idx;
  b_pin_int_mode_idx := v_PC6_int_mode_idx;
  b_pin_pull_updown_idx := v_PC6_pull_updown_idx;
  b_pin_speed_idx := v_PC6_speed_idx;
  b_pin_func := v_PC6_func;
end;

procedure  pin_PC7_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC7_o_lvl_idx;
  b_pin_o_mode_idx := v_PC7_o_mode_idx;
  b_pin_i_mode_idx := v_PC7_i_mode_idx;
  b_pin_a_mode_idx := v_PC7_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC7_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC7_spi_mode_idx;
  b_pin_int_mode_idx := v_PC7_int_mode_idx;
  b_pin_pull_updown_idx := v_PC7_pull_updown_idx;
  b_pin_speed_idx := v_PC7_speed_idx;
  b_pin_func := v_PC7_func;
end;

procedure  pin_PC8_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC8_o_lvl_idx;
  b_pin_o_mode_idx := v_PC8_o_mode_idx;
  b_pin_i_mode_idx := v_PC8_i_mode_idx;
  b_pin_a_mode_idx := v_PC8_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC8_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC8_spi_mode_idx;
  b_pin_int_mode_idx := v_PC8_int_mode_idx;
  b_pin_pull_updown_idx := v_PC8_pull_updown_idx;
  b_pin_speed_idx := v_PC8_speed_idx;
  b_pin_func := v_PC8_func;
end;

procedure  pin_PC9_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PC9_o_lvl_idx;
  b_pin_o_mode_idx := v_PC9_o_mode_idx;
  b_pin_i_mode_idx := v_PC9_i_mode_idx;
  b_pin_a_mode_idx := v_PC9_a_mode_idx;
  b_pin_i2c_mode_idx := v_PC9_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PC9_spi_mode_idx;
  b_pin_int_mode_idx := v_PC9_int_mode_idx;
  b_pin_pull_updown_idx := v_PC9_pull_updown_idx;
  b_pin_speed_idx := v_PC9_speed_idx;
  b_pin_func := v_PC9_func;
end;

{PD}
procedure pin_PD2_to_buffer;
begin
  { }
  b_pin_o_lvl_idx := v_PD2_o_lvl_idx;
  b_pin_o_mode_idx := v_PD2_o_mode_idx;
  b_pin_i_mode_idx := v_PD2_i_mode_idx;
  b_pin_a_mode_idx := v_PD2_a_mode_idx;
  b_pin_i2c_mode_idx := v_PD2_i2c_mode_idx;
  b_pin_spi_mode_idx := v_PD2_spi_mode_idx;
  b_pin_int_mode_idx := v_PD2_int_mode_idx;
  b_pin_pull_updown_idx := v_PD2_pull_updown_idx;
  b_pin_speed_idx := v_PD2_speed_idx;
  b_pin_func := v_PD2_func;
end;

// needed for peripheral clocks...
procedure scan_for_b_port_usage;
begin
  v_portb_used := FALSE;
  { CN7, first column }
  if v_pb7_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  { CN7, second column }
  if v_pb0_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  { CN10, first column }
  if v_pb8_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb9_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb6_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb10_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb4_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb5_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb3_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  { CN10, second column }
  if v_pb12_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb11_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb2_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb1_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb15_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb14_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
  if v_pb13_func <> 'N/A' then begin
    v_portb_used := TRUE;
    exit;
  end;
end;

// needed for peripheral clocks...
procedure scan_for_d_port_usage;
begin
  if v_pd2_func <> 'N/A' then v_portd_used := TRUE
  else v_portd_used := FALSE;
end;

{analog input channels}
procedure scan_for_adin;
begin
  v_use_adin := FALSE;
  {PC10}
  {PC12}
  {PA13}
  {PA14}
  {PA15}
  //if v_pa15_func  = 'ADIN' then begin
  //  v_use_adc := TRUE;
  //  exit;
  //end;
  {PB7}
  {PC13}
  //if v_pc13_func  = 'ADIN' then begin
  //  v_use_adc := TRUE;
  //  exit;
  //end;
  {PC14}
  {PC15}
  {PH0}
  {PH1}
  {PC2}
  if v_pc2_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PC3}
  if v_pc3_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {-----------------------------------------}
  {PC11}
  //if v_pc11_func  = 'ADIN' then begin
  //  v_use_adc := TRUE;
  //  exit;
  //end;
  {PD2}
  {PA0}
  if v_pa0_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PA1}
  if v_pa1_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PA4}
  if v_pa4_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PB0}
  if v_pb0_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PC1}
  if v_pc1_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PC0}
  if v_pc0_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {-----------------------------------------}
  {PC9}
  {PB8}
  {PB9}
  {PA5}
  {PA6}
  if v_pa6_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PA7}
  if v_pa7_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PB6}
  {PC7}
  {PA9}
  {PA8}
  {PB10}
  {PB4}
  {PB5}
  {PB3}
  {PA10}
  {PA2}
  {PA3}
  {-----------------------------------------}
  {PC8}
  {PC6}
  {PC5}
  if v_pc5_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PA12}
  {PA11}
  //if v_pa11_func  = 'ANALOG' then begin
  //  v_use_adc := TRUE;
  //  exit;
  //end;
  {PB12}
  if v_pb12_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PB11}
  //if v_pb11_func  = 'ANALOG' then begin
  //  v_use_adc := TRUE;
  //  exit;
  //end;
  {PB2}
  if v_pb2_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PB1}
  if v_pb1_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PB15}
  if v_pb15_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PB14}
  if v_pb14_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PB13}
  if v_pb13_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
  {PC4}
  if v_pc4_func  = 'ADIN' then begin
    v_use_adin := TRUE;
    exit;
  end;
end;


procedure generate_exti_prologue(var fv:Text);
var nmr : string;
begin
  nmr := StrOnlyNumbers(b_pin_id);
  writeln(fv, '  /* '+b_pin_id+' */');
  writeln(fv, '  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIO'+b_pin_id[2]+', EXTI_PinSource'+nmr+');');
  write(fv, '  GPIO_SetPinPull(GPIO'+b_pin_id[2]+', '+b_pin_name+', ');
  case b_pin_pull_updown_idx of
    1: writeln(fv, 'GPIO_PuPd_NOPULL);');
    2: writeln(fv, 'GPIO_PuPd_UP);');
    3: writeln(fv, 'GPIO_PuPd_DOWN);');
    else writeln(fv, 'GPIO_PuPd_NOPULL);');
  end;
  writeln(fv, '  GPIO_SetPinMode(GPIO'+b_pin_id[2]+', '+b_pin_name+', GPIO_Mode_IN);');
  writeln(fv, ' ');
end;

procedure vpc_gpio_init_prologue(var fv:Text);
var
  pin_a_started, pin_b_started, pin_c_started:boolean;
begin
  pin_a_started:=FALSE;
  pin_b_started:=FALSE;
  pin_c_started:=FALSE;
  scan_for_b_port_usage;
  scan_for_d_port_usage;
  writeln(fv, '  GPIO_InitTypeDef GPIO_InitStruct;');
  writeln(fv, '  EXTI_InitTypeDef EXTI_InitStruct;');
  writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, ' ');
  writeln(fv, '  /* Activating GPIO ports */');
  if v_porta_used then
    writeln(fv, '  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);');
  if v_portb_used then
    writeln(fv, '  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);');
  if v_portc_used then
    writeln(fv, '  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);');
  if v_portd_used then
    writeln(fv, '  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);');
  if v_porth_used then
    writeln(fv, '  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOH, ENABLE);');
  writeln(fv, ' ');

  writeln(fv, '  /* The initial values (Low) of OUTPUT pins */');
  {reset the PD2 pin if is OUTPUT}
  if ((v_pd2_func = 'OUTPUT') and (v_pd2_o_lvl_idx = 1)) then begin
    writeln(fv, '  GPIO_ResetBits(GPIOD, '+v_pd2_name+');');
  end;

  {search for portA pins that are OUTPUT and set to LOW and reset them}
  {PA0}
  if ((v_pa0_func = 'OUTPUT') and (v_pa0_o_lvl_idx = 1)) then begin
    //
    pin_a_started := TRUE;
    write(fv, '  GPIO_ResetBits(GPIOA, ');
    write(fv, v_pa0_name+' ');
  end;
  {PA1}
  if ((v_pa1_func = 'OUTPUT') and (v_pa1_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa1_name+' ');
    end
    else
      write(fv, '| '+v_pa1_name+' ');
  end;
  {PA2}
  if ((v_pa2_func = 'OUTPUT') and (v_pa2_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa2_name+' ');
    end
    else
      write(fv, '| '+v_pa2_name+' ');
  end;
  if ((v_pa3_func = 'OUTPUT') and (v_pa3_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa3_name+' ');
    end
    else
      write(fv, '| '+v_pa3_name+' ');
  end;
  if ((v_pa4_func = 'OUTPUT') and (v_pa4_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa4_name+' ');
    end
    else
      write(fv, '| '+v_pa4_name+' ');
  end;
  if ((v_pa5_func = 'OUTPUT') and (v_pa5_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa5_name+' ');
    end
    else
      write(fv, '| '+v_pa5_name+' ');
  end;
  if ((v_pa6_func = 'OUTPUT') and (v_pa6_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa6_name+' ');
    end
    else
      write(fv, '| '+v_pa6_name+' ');
  end;
  if ((v_pa7_func = 'OUTPUT') and (v_pa7_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa7_name+' ');
    end
    else
      write(fv, '| '+v_pa7_name+' ');
  end;
  if ((v_pa8_func = 'OUTPUT') and (v_pa8_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa8_name+' ');
    end
    else
        write(fv, '| '+v_pa8_name+' ');
  end;
  if ((v_pa9_func = 'OUTPUT') and (v_pa9_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa9_name+' ');
    end
    else
      write(fv, '| '+v_pa9_name+' ');
  end;
  if ((v_pa10_func = 'OUTPUT') and (v_pa10_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa10_name+' ');
    end
    else
      write(fv, '| '+v_pa10_name+' ');
  end;
  if ((v_pa11_func = 'OUTPUT') and (v_pa11_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa11_name+' ');
    end
    else
      write(fv, '| '+v_pa11_name+' ');
  end;
  if ((v_pa12_func = 'OUTPUT') and (v_pa12_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa12_name+' ');
    end
    else
      write(fv, '| '+v_pa12_name+' ');
  end;
  if ((v_pa15_func = 'OUTPUT') and (v_pa15_o_lvl_idx = 1)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOA, ');
      write(fv, v_pa15_name+' ');
    end
    else
      write(fv, '| '+v_pa15_name+' ');
  end;
  if pin_a_started then begin
    //
    writeln(fv, ');');
    pin_a_started := FALSE;
  end;

  {search for portB pins that are OUTPUT and reset them}
  if ((v_pb0_func = 'OUTPUT') and (v_pb0_o_lvl_idx = 1)) then begin
    //
    pin_b_started := TRUE;
    write(fv, '  GPIO_ResetBits(GPIOB, ');
    write(fv, v_pb0_name+' ');
  end;
  if ((v_pb1_func = 'OUTPUT') and (v_pb1_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb1_name+' ');
    end
    else
      write(fv, '| '+v_pb1_name+' ');
  end;
  if ((v_pb2_func = 'OUTPUT') and (v_pb2_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb2_name+' ');
    end
    else
      write(fv, '| '+v_pb2_name+' ');
  end;
  if ((v_pb3_func = 'OUTPUT') and (v_pb3_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb3_name+' ');
    end
    else
      write(fv, '| '+v_pb3_name+' ');
  end;
  if ((v_pb4_func = 'OUTPUT') and (v_pb4_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb4_name+' ');
    end
    else
      write(fv, '| '+v_pb4_name+' ');
  end;
  if ((v_pb5_func = 'OUTPUT') and (v_pb5_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb5_name+' ');
    end
    else
      write(fv, '| '+v_pb5_name+' ');
  end;
  if ((v_pb6_func = 'OUTPUT') and (v_pb6_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb6_name+' ');
    end
    else
      write(fv, '| '+v_pb6_name+' ');
  end;
  if ((v_pb7_func = 'OUTPUT') and (v_pb7_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb7_name+' ');
    end
    else
      write(fv, '| '+v_pb7_name+' ');
  end;
  if ((v_pb8_func = 'OUTPUT') and (v_pb8_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb8_name+' ');
    end
    else
      write(fv, '| '+v_pb8_name+' ');
  end;
  if ((v_pb9_func = 'OUTPUT') and (v_pb9_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb9_name+' ');
    end
    else
      write(fv, '| '+v_pb9_name+' ');
  end;
  if ((v_pb10_func = 'OUTPUT') and (v_pb10_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb10_name+' ');
    end
    else
      write(fv, '| '+v_pb10_name+' ');
  end;
  if ((v_pb11_func = 'OUTPUT') and (v_pb11_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb11_name+' ');
    end
    else
      write(fv, '| '+v_pb11_name+' ');
  end;
  if ((v_pb12_func = 'OUTPUT') and (v_pb12_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb12_name+' ');
    end
    else
      write(fv, '| '+v_pb12_name+' ');
  end;
  if ((v_pb13_func = 'OUTPUT') and (v_pb13_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb13_name+' ');
    end
    else
      write(fv, '| '+v_pb13_name+' ');
  end;
  if ((v_pb14_func = 'OUTPUT') and (v_pb14_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb14_name+' ');
    end
    else
      write(fv, '| '+v_pb14_name+' ');
  end;
  if ((v_pb15_func = 'OUTPUT') and (v_pb15_o_lvl_idx = 1)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOB, ');
      write(fv, v_pb15_name+' ');
    end
    else
      write(fv, '| '+v_pb15_name+' ');
  end;
  if pin_b_started then begin
    //
    writeln(fv, ');');
    pin_b_started := FALSE;
  end;


  {search for portC pins that are OUTPUT and reset them}
  if ((v_pc0_func = 'OUTPUT') and (v_pc0_o_lvl_idx = 1)) then begin
    //
    pin_c_started := TRUE;
    write(fv, '  GPIO_ResetBits(GPIOC, ');
    write(fv, v_pc0_name+' ');
  end;
  if ((v_pc1_func = 'OUTPUT') and (v_pc1_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc1_name+' ');
    end
    else
      write(fv, '| '+v_pc1_name+' ');
  end;
  if ((v_pc2_func = 'OUTPUT') and (v_pc2_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc2_name+' ');
    end
    else
      write(fv, '| '+v_pc2_name+' ');
  end;
  if ((v_pc3_func = 'OUTPUT') and (v_pc3_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc3_name+' ');
    end
    else
      write(fv, '| '+v_pc3_name+' ');
  end;
  if ((v_pc4_func = 'OUTPUT') and (v_pc4_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc4_name+' ');
    end
    else
      write(fv, '| '+v_pc4_name+' ');
  end;
  if ((v_pc5_func = 'OUTPUT') and (v_pc5_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc5_name+' ');
    end
    else
      write(fv, '| '+v_pc5_name+' ');
  end;
  if ((v_pc6_func = 'OUTPUT') and (v_pc6_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc6_name+' ');
    end
    else
      write(fv, '| '+v_pc6_name+' ');
  end;
  if ((v_pc7_func = 'OUTPUT') and (v_pc7_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc7_name+' ');
    end
    else
      write(fv, '| '+v_pc7_name+' ');
  end;
  if ((v_pc8_func = 'OUTPUT') and (v_pc8_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc8_name+' ');
    end
    else
      write(fv, '| '+v_pc8_name+' ');
  end;
  if ((v_pc9_func = 'OUTPUT') and (v_pc9_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc9_name+' ');
    end
    else
      write(fv, '| '+v_pc9_name+' ');
  end;
  if ((v_pc10_func = 'OUTPUT') and (v_pc10_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc10_name+' ');
    end
    else
      write(fv, '| '+v_pc10_name+' ');
  end;
  if ((v_pc11_func = 'OUTPUT') and (v_pc11_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc11_name+' ');
    end
    else
      write(fv, '| '+v_pc11_name+' ');
  end;
  if ((v_pc12_func = 'OUTPUT') and (v_pc12_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc12_name+' ');
    end
    else
      write(fv, '| '+v_pc12_name+' ');
  end;
  if ((v_pc13_func = 'OUTPUT') and (v_pc13_o_lvl_idx = 1)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_ResetBits(GPIOC, ');
      write(fv, v_pc13_name+' ');
    end
    else
      write(fv, '| '+v_pc13_name+' ');
  end;
  if pin_c_started = TRUE then begin
    //
    writeln(fv, ');');
    pin_c_started := FALSE;
  end;
  writeln(fv, ' ');
  {done reseting the OUTPUT pins}

  writeln(fv, '  /* The initial values (High) of OUTPUT pins */');
  {reset the PD2 pin if is OUTPUT}
  if ((v_pd2_func = 'OUTPUT') and (v_pd2_o_lvl_idx = 2)) then begin
    writeln(fv, '  GPIO_SetBits(GPIOD, '+v_pd2_name+');');
  end;

  {search for portA pins that are OUTPUT and set to HIGH and set them}
  {PA0}
  if ((v_pa0_func = 'OUTPUT') and (v_pa0_o_lvl_idx = 2)) then begin
    //
    pin_a_started := TRUE;
    write(fv, '  GPIO_SetBits(GPIOA, ');
    write(fv, v_pa0_name+' ');
  end;
  {PA1}
  if ((v_pa1_func = 'OUTPUT') and (v_pa1_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa1_name+' ');
    end
    else
      write(fv, '| '+v_pa1_name+' ');
  end;
  {PA2}
  if ((v_pa2_func = 'OUTPUT') and (v_pa2_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa2_name+' ');
    end
    else
      write(fv, '| '+v_pa2_name+' ');
  end;
  if ((v_pa3_func = 'OUTPUT') and (v_pa3_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa3_name+' ');
    end
    else
      write(fv, '| '+v_pa3_name+' ');
  end;
  if ((v_pa4_func = 'OUTPUT') and (v_pa4_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa4_name+' ');
    end
    else
      write(fv, '| '+v_pa4_name+' ');
  end;
  if ((v_pa5_func = 'OUTPUT') and (v_pa5_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa5_name+' ');
    end
    else
      write(fv, '| '+v_pa5_name+' ');
  end;
  if ((v_pa6_func = 'OUTPUT') and (v_pa6_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa6_name+' ');
    end
    else
      write(fv, '| '+v_pa6_name+' ');
  end;
  if ((v_pa7_func = 'OUTPUT') and (v_pa7_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa7_name+' ');
    end
    else
      write(fv, '| '+v_pa7_name+' ');
  end;
  if ((v_pa8_func = 'OUTPUT') and (v_pa8_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa8_name+' ');
    end
    else
        write(fv, '| '+v_pa8_name+' ');
  end;
  if ((v_pa9_func = 'OUTPUT') and (v_pa9_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa9_name+' ');
    end
    else
      write(fv, '| '+v_pa9_name+' ');
  end;
  if ((v_pa10_func = 'OUTPUT') and (v_pa10_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa10_name+' ');
    end
    else
      write(fv, '| '+v_pa10_name+' ');
  end;
  if ((v_pa11_func = 'OUTPUT') and (v_pa11_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa11_name+' ');
    end
    else
      write(fv, '| '+v_pa11_name+' ');
  end;
  if ((v_pa12_func = 'OUTPUT') and (v_pa12_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa12_name+' ');
    end
    else
      write(fv, '| '+v_pa12_name+' ');
  end;
  if ((v_pa15_func = 'OUTPUT') and (v_pa15_o_lvl_idx = 2)) then begin
    //
    if (pin_a_started = FALSE) then begin
      pin_a_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOA, ');
      write(fv, v_pa15_name+' ');
    end
    else
      write(fv, '| '+v_pa15_name+' ');
  end;
  if pin_a_started then begin
    //
    writeln(fv, ');');
    pin_a_started := FALSE;
  end;

  {search for portB pins that are OUTPUT and High and set them}
  if ((v_pa0_func = 'OUTPUT') and (v_pa0_o_lvl_idx = 2)) then begin
    //
    pin_b_started := TRUE;
    write(fv, '  GPIO_SetBits(GPIOB, ');
    write(fv, v_pb0_name+' ');
  end;
  if ((v_pb1_func = 'OUTPUT') and (v_pb1_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb1_name+' ');
    end
    else
      write(fv, '| '+v_pb1_name+' ');
  end;
  if ((v_pb2_func = 'OUTPUT') and (v_pb2_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb2_name+' ');
    end
    else
      write(fv, '| '+v_pb2_name+' ');
  end;
  if ((v_pb3_func = 'OUTPUT') and (v_pb3_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb3_name+' ');
    end
    else
      write(fv, '| '+v_pb3_name+' ');
  end;
  if ((v_pb4_func = 'OUTPUT') and (v_pb4_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb4_name+' ');
    end
    else
      write(fv, '| '+v_pb4_name+' ');
  end;
  if ((v_pb5_func = 'OUTPUT') and (v_pb5_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb5_name+' ');
    end
    else
      write(fv, '| '+v_pb5_name+' ');
  end;
  if ((v_pb6_func = 'OUTPUT') and (v_pb6_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb6_name+' ');
    end
    else
      write(fv, '| '+v_pb6_name+' ');
  end;
  if ((v_pb7_func = 'OUTPUT') and (v_pb7_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb7_name+' ');
    end
    else
      write(fv, '| '+v_pb7_name+' ');
  end;
  if ((v_pb8_func = 'OUTPUT') and (v_pb8_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb8_name+' ');
    end
    else
      write(fv, '| '+v_pb8_name+' ');
  end;
  if ((v_pb9_func = 'OUTPUT') and (v_pb9_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb9_name+' ');
    end
    else
      write(fv, '| '+v_pb9_name+' ');
  end;
  if ((v_pb10_func = 'OUTPUT') and (v_pb10_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb10_name+' ');
    end
    else
      write(fv, '| '+v_pb10_name+' ');
  end;
  if ((v_pb11_func = 'OUTPUT') and (v_pb11_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb11_name+' ');
    end
    else
      write(fv, '| '+v_pb11_name+' ');
  end;
  if ((v_pb12_func = 'OUTPUT') and (v_pb12_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb12_name+' ');
    end
    else
      write(fv, '| '+v_pb12_name+' ');
  end;
  if ((v_pb13_func = 'OUTPUT') and (v_pb13_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb13_name+' ');
    end
    else
      write(fv, '| '+v_pb13_name+' ');
  end;
  if ((v_pb14_func = 'OUTPUT') and (v_pb14_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb14_name+' ');
    end
    else
      write(fv, '| '+v_pb14_name+' ');
  end;
  if ((v_pb15_func = 'OUTPUT') and (v_pb15_o_lvl_idx = 2)) then begin
    //
    if (pin_b_started = FALSE) then begin
      pin_b_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOB, ');
      write(fv, v_pb15_name+' ');
    end
    else
      write(fv, '| '+v_pb15_name+' ');
  end;
  if pin_b_started then begin
    //
    writeln(fv, ');');
    pin_b_started := FALSE;
  end;


  {search for portC pins that are OUTPUT and High and set them}
  if ((v_pc0_func = 'OUTPUT') and (v_pc0_o_lvl_idx = 2)) then begin
    //
    pin_c_started := TRUE;
    write(fv, '  GPIO_SetBits(GPIOC, ');
    write(fv, v_pc0_name+' ');
  end;
  if ((v_pc1_func = 'OUTPUT') and (v_pc1_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc1_name+' ');
    end
    else
      write(fv, '| '+v_pc1_name+' ');
  end;
  if ((v_pc2_func = 'OUTPUT') and (v_pc2_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc2_name+' ');
    end
    else
      write(fv, '| '+v_pc2_name+' ');
  end;
  if ((v_pc3_func = 'OUTPUT') and (v_pc3_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc3_name+' ');
    end
    else
      write(fv, '| '+v_pc3_name+' ');
  end;
  if ((v_pc4_func = 'OUTPUT') and (v_pc4_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc4_name+' ');
    end
    else
      write(fv, '| '+v_pc4_name+' ');
  end;
  if ((v_pc5_func = 'OUTPUT') and (v_pc5_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc5_name+' ');
    end
    else
      write(fv, '| '+v_pc5_name+' ');
  end;
  if ((v_pc6_func = 'OUTPUT') and (v_pc6_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc6_name+' ');
    end
    else
      write(fv, '| '+v_pc6_name+' ');
  end;
  if ((v_pc7_func = 'OUTPUT') and (v_pc7_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc7_name+' ');
    end
    else
      write(fv, '| '+v_pc7_name+' ');
  end;
  if ((v_pc8_func = 'OUTPUT') and (v_pc8_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc8_name+' ');
    end
    else
      write(fv, '| '+v_pc8_name+' ');
  end;
  if ((v_pc9_func = 'OUTPUT') and (v_pc9_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc9_name+' ');
    end
    else
      write(fv, '| '+v_pc9_name+' ');
  end;
  if ((v_pc10_func = 'OUTPUT') and (v_pc10_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc10_name+' ');
    end
    else
      write(fv, '| '+v_pc10_name+' ');
  end;
  if ((v_pc11_func = 'OUTPUT') and (v_pc11_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc11_name+' ');
    end
    else
      write(fv, '| '+v_pc11_name+' ');
  end;
  if ((v_pc12_func = 'OUTPUT') and (v_pc12_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc12_name+' ');
    end
    else
      write(fv, '| '+v_pc12_name+' ');
  end;
  if ((v_pc13_func = 'OUTPUT') and (v_pc13_o_lvl_idx = 2)) then begin
    //
    if (pin_c_started = FALSE) then begin
      pin_c_started := TRUE;
      write(fv, '  GPIO_SetBits(GPIOC, ');
      write(fv, v_pc13_name+' ');
    end
    else
      write(fv, '| '+v_pc13_name+' ');
  end;
  if pin_c_started = TRUE then begin
    //
    writeln(fv, ');');
    pin_c_started := FALSE;
  end;
  writeln(fv, ' ');
  {done setting the output pins}

  {now we should scan for EXTI pins, and generate the "prologue"...}
  {PA}
  writeln(fv, '  /* Prologue for EXTI pins */');
  if v_pa0_func = 'INTERRUPT' then begin
    pin_PA0_to_buffer;
    b_pin_id := v_pa0_id;
    b_pin_name := v_pa0_name;
    generate_exti_prologue(fv);
  end;
  if v_pa10_func = 'INTERRUPT' then begin
    pin_PA10_to_buffer;
    b_pin_id := v_pa10_id;
    b_pin_name := v_pa10_name;
    generate_exti_prologue(fv);
  end;
  if v_pa11_func = 'INTERRUPT' then begin
    pin_PA11_to_buffer;
    b_pin_id := v_pa11_id;
    b_pin_name := v_pa11_name;
    generate_exti_prologue(fv);
  end;
  if v_pa12_func = 'INTERRUPT' then begin
    pin_PA12_to_buffer;
    b_pin_id := v_pa12_id;
    b_pin_name := v_pa12_name;
    generate_exti_prologue(fv);
  end;
  if v_pa15_func = 'INTERRUPT' then begin
    pin_PA15_to_buffer;
    b_pin_id := v_pa15_id;
    b_pin_name := v_pa15_name;
    generate_exti_prologue(fv);
  end;
  if v_pa1_func = 'INTERRUPT' then begin
    pin_PA1_to_buffer;
    b_pin_id := v_pa1_id;
    b_pin_name := v_pa1_name;
    generate_exti_prologue(fv);
  end;
  if v_pa2_func = 'INTERRUPT' then begin
    pin_PA2_to_buffer;
    b_pin_id := v_pa2_id;
    b_pin_name := v_pa2_name;
    generate_exti_prologue(fv);
  end;
  if v_pa3_func = 'INTERRUPT' then begin
    pin_PA3_to_buffer;
    b_pin_id := v_pa3_id;
    b_pin_name := v_pa3_name;
    generate_exti_prologue(fv);
  end;
  if v_pa4_func = 'INTERRUPT' then begin
    pin_PA4_to_buffer;
    b_pin_id := v_pa4_id;
    b_pin_name := v_pa4_name;
    generate_exti_prologue(fv);
  end;
  if v_pa5_func = 'INTERRUPT' then begin
    pin_PA5_to_buffer;
    b_pin_id := v_pa5_id;
    b_pin_name := v_pa5_name;
    generate_exti_prologue(fv);
  end;
  if v_pa6_func = 'INTERRUPT' then begin
    pin_PA6_to_buffer;
    b_pin_id := v_pa6_id;
    b_pin_name := v_pa6_name;
    generate_exti_prologue(fv);
  end;
  if v_pa7_func = 'INTERRUPT' then begin
    pin_PA7_to_buffer;
    b_pin_id := v_pa7_id;
    b_pin_name := v_pa7_name;
    generate_exti_prologue(fv);
  end;
  if v_pa8_func = 'INTERRUPT' then begin
    pin_PA8_to_buffer;
    b_pin_id := v_pa8_id;
    b_pin_name := v_pa8_name;
    generate_exti_prologue(fv);
  end;
  if v_pa9_func = 'INTERRUPT' then begin
    pin_PA9_to_buffer;
    b_pin_id := v_pa9_id;
    b_pin_name := v_pa9_name;
    generate_exti_prologue(fv);
  end;
  {PB}
  if v_pb0_func = 'INTERRUPT' then begin
    pin_PB0_to_buffer;
    b_pin_id := v_pb0_id;
    b_pin_name := v_pb0_name;
    generate_exti_prologue(fv);
  end;
  if v_pb10_func = 'INTERRUPT' then begin
    pin_PB10_to_buffer;
    b_pin_id := v_pb10_id;
    b_pin_name := v_pb10_name;
    generate_exti_prologue(fv);
  end;
  if v_pb11_func = 'INTERRUPT' then begin
    pin_PB11_to_buffer;
    b_pin_id := v_pb11_id;
    b_pin_name := v_pb11_name;
    generate_exti_prologue(fv);
  end;
  if v_pb12_func = 'INTERRUPT' then begin
    pin_PB12_to_buffer;
    b_pin_id := v_pb12_id;
    b_pin_name := v_pb12_name;
    generate_exti_prologue(fv);
  end;
  if v_pb13_func = 'INTERRUPT' then begin
    pin_PB13_to_buffer;
    b_pin_id := v_pb13_id;
    b_pin_name := v_pb13_name;
    generate_exti_prologue(fv);
  end;
  if v_pb14_func = 'INTERRUPT' then begin
    pin_PB14_to_buffer;
    b_pin_id := v_pb14_id;
    b_pin_name := v_pb14_name;
    generate_exti_prologue(fv);
  end;
  if v_pb15_func = 'INTERRUPT' then begin
    pin_PB15_to_buffer;
    b_pin_id := v_pb15_id;
    b_pin_name := v_pb15_name;
    generate_exti_prologue(fv);
  end;
  if v_pb1_func = 'INTERRUPT' then begin
    pin_PB1_to_buffer;
    b_pin_id := v_pb1_id;
    b_pin_name := v_pb1_name;
    generate_exti_prologue(fv);
  end;
  if v_pb2_func = 'INTERRUPT' then begin
    pin_PB2_to_buffer;
    b_pin_id := v_pb2_id;
    b_pin_name := v_pb2_name;
    generate_exti_prologue(fv);
  end;
  if v_pb3_func = 'INTERRUPT' then begin
    pin_PB3_to_buffer;
    b_pin_id := v_pb3_id;
    b_pin_name := v_pb3_name;
    generate_exti_prologue(fv);
  end;
  if v_pb4_func = 'INTERRUPT' then begin
    pin_PB4_to_buffer;
    b_pin_id := v_pb4_id;
    b_pin_name := v_pb4_name;
    generate_exti_prologue(fv);
  end;
  if v_pb5_func = 'INTERRUPT' then begin
    pin_PB5_to_buffer;
    b_pin_id := v_pb5_id;
    b_pin_name := v_pb5_name;
    generate_exti_prologue(fv);
  end;
  if v_pb6_func = 'INTERRUPT' then begin
    pin_PB6_to_buffer;
    b_pin_id := v_pb6_id;
    b_pin_name := v_pb7_name;
    generate_exti_prologue(fv);
  end;
  if v_pb7_func = 'INTERRUPT' then begin
    pin_PB7_to_buffer;
    b_pin_id := v_pb7_id;
    b_pin_name := v_pb7_name;
    generate_exti_prologue(fv);
  end;
  if v_pb8_func = 'INTERRUPT' then begin
    pin_PB8_to_buffer;
    b_pin_id := v_pb8_id;
    b_pin_name := v_pb8_name;
    generate_exti_prologue(fv);
  end;
  if v_pb9_func = 'INTERRUPT' then begin
    pin_PB9_to_buffer;
    b_pin_id := v_pb9_id;
    b_pin_name := v_pb9_name;
    generate_exti_prologue(fv);
  end;
  {PC}
  if v_pc0_func = 'INTERRUPT' then begin
    pin_PC0_to_buffer;
    b_pin_id := v_pc0_id;
    b_pin_name := v_pc0_name;
    generate_exti_prologue(fv);
  end;
  if v_pc10_func = 'INTERRUPT' then begin
    pin_PC10_to_buffer;
    b_pin_id := v_pc10_id;
    b_pin_name := v_pc10_name;
    generate_exti_prologue(fv);
  end;
  if v_pc11_func = 'INTERRUPT' then begin
    pin_PC11_to_buffer;
    b_pin_id := v_pc11_id;
    b_pin_name := v_pc11_name;
    generate_exti_prologue(fv);
  end;
  if v_pc12_func = 'INTERRUPT' then begin
    pin_PC12_to_buffer;
    b_pin_id := v_pc12_id;
    b_pin_name := v_pc12_name;
    generate_exti_prologue(fv);
  end;
  if v_pc13_func = 'INTERRUPT' then begin
    pin_PC13_to_buffer;
    b_pin_id := v_pc13_id;
    b_pin_name := v_pc13_name;
    generate_exti_prologue(fv);
  end;
  if v_pc1_func = 'INTERRUPT' then begin
    pin_PC1_to_buffer;
    b_pin_id := v_pc1_id;
    b_pin_name := v_pc1_name;
    generate_exti_prologue(fv);
  end;
  if v_pc2_func = 'INTERRUPT' then begin
    pin_PC2_to_buffer;
    b_pin_id := v_pc2_id;
    b_pin_name := v_pc2_name;
    generate_exti_prologue(fv);
  end;
  if v_pc3_func = 'INTERRUPT' then begin
    pin_PC3_to_buffer;
    b_pin_id := v_pc3_id;
    b_pin_name := v_pc3_name;
    generate_exti_prologue(fv);
  end;
  if v_pc4_func = 'INTERRUPT' then begin
    pin_PC4_to_buffer;
    b_pin_id := v_pc4_id;
    b_pin_name := v_pc4_name;
    generate_exti_prologue(fv);
  end;
  if v_pc5_func = 'INTERRUPT' then begin
    pin_PC5_to_buffer;
    b_pin_id := v_pc5_id;
    b_pin_name := v_pc5_name;
    generate_exti_prologue(fv);
  end;
  if v_pc6_func = 'INTERRUPT' then begin
    pin_PC6_to_buffer;
    b_pin_id := v_pc6_id;
    b_pin_name := v_pc6_name;
    generate_exti_prologue(fv);
  end;
  if v_pc7_func = 'INTERRUPT' then begin
    pin_PC7_to_buffer;
    b_pin_id := v_pc7_id;
    b_pin_name := v_pc7_name;
    generate_exti_prologue(fv);
  end;
  if v_pc8_func = 'INTERRUPT' then begin
    pin_PC8_to_buffer;
    b_pin_id := v_pc8_id;
    b_pin_name := v_pc8_name;
    generate_exti_prologue(fv);
  end;
  if v_pc9_func = 'INTERRUPT' then begin
    pin_PC9_to_buffer;
    b_pin_id := v_pc9_id;
    b_pin_name := v_pc9_name;
    generate_exti_prologue(fv);
  end;
  {PD}
  if v_pd2_func = 'INTERRUPT' then begin
    pin_PD2_to_buffer;
    b_pin_id := v_pd2_id;
    b_pin_name := v_pd2_name;
    generate_exti_prologue(fv);
  end;
end;

procedure vpc_gpio_init_content(var fv:Text);
begin
  if b_pin_func = 'INTERRUPT' then begin
    //
    writeln(fv, '  /* '+b_pin_id+' as INTERRUPT pin */');
    writeln(fv, '  EXTI_InitStruct.EXTI_Line = EXTI_Line'+StrOnlyNumbers(b_pin_id)+';');
    write(fv, '  EXTI_InitStruct.EXTI_Mode = ');
    if b_pin_int_mode_idx > 3 then
      writeln(fv, 'EXTI_Mode_Event;')
    else
      writeln(fv, 'EXTI_Mode_Interrupt;');
    write(fv, '  EXTI_InitStruct.EXTI_Trigger = ');
    case b_pin_int_mode_idx of
      1,4: writeln(fv, 'EXTI_Trigger_Rising;');
      2,5: writeln(fv, 'EXTI_Trigger_Falling;');
      3,6: writeln(fv, 'EXTI_Trigger_Rising_Falling;');
      else writeln(fv, 'EXTI_Trigger_Rising;');
    end;
    writeln(fv, '  EXTI_InitStruct.EXTI_LineCmd = ENABLE;');
    writeln(fv, '  EXTI_Init(&EXTI_InitStruct);');
    writeln(fv, ' ');
  end;
  if b_pin_func = 'OUTPUT' then begin
    //
    writeln(fv, '  /* '+b_pin_id+' as OUTPUT pin */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+b_pin_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;');
    write(fv, '  GPIO_InitStruct.GPIO_Speed = ');
    case b_pin_speed_idx of
      1: writeln(fv, 'GPIO_Speed_400KHz;');
      2: writeln(fv, 'GPIO_Speed_2MHz;');
      3: writeln(fv, 'GPIO_Speed_10MHz;');
      4: writeln(fv, 'GPIO_Speed_40MHz;');
    else
      writeln(fv, 'GPIO_Speed_2MHz;');
    end;
    write(fv, '  GPIO_InitStruct.GPIO_OType = ');
    case b_pin_o_mode_idx of
      1: writeln(fv, 'GPIO_OType_PP;');
      2: writeln(fv, 'GPIO_OType_OD;');
    else
      writeln(fv, 'GPIO_OType_PP;');
    end;
    write(fv, '  GPIO_InitStruct.GPIO_PuPd = ');
    case b_pin_pull_updown_idx of
      1: writeln(fv, 'GPIO_PuPd_NOPULL;');
      2: writeln(fv, 'GPIO_PuPd_UP;');
      3: writeln(fv, 'GPIO_PuPd_DOWN;');
    else
      writeln(fv, 'GPIO_PuPd_NOPULL;');
    end;
    writeln(fv, '  GPIO_Init(GPIO'+b_pin_id[2]+', &GPIO_InitStruct);');
    writeln(fv, ' ');
  end;
  if b_pin_func = 'INPUT' then begin
    //
    writeln(fv, '  /* '+b_pin_id+' as INPUT pin */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+b_pin_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;');
    write(fv, '  GPIO_InitStruct.GPIO_PuPd = ');
    case b_pin_pull_updown_idx of
      1: writeln(fv, 'GPIO_PuPd_NOPULL;');
      2: writeln(fv, 'GPIO_PuPd_UP;');
      3: writeln(fv, 'GPIO_PuPd_DOWN;');
    else
      writeln(fv, 'GPIO_PuPd_NOPULL;');
    end;
    writeln(fv, '  GPIO_Init(GPIO'+b_pin_id[2]+', &GPIO_InitStruct);');
    writeln(fv, ' ');
  end;
  if b_pin_func = 'ANALOG' then begin
    //
    writeln(fv, '  /* '+b_pin_id+' as ANALOG pin */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+b_pin_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIO'+b_pin_id[2]+', &GPIO_InitStruct);');
    writeln(fv, ' ');
  end;
  if b_pin_func = 'EVENT' then begin
    //
    writeln(fv, '  /* '+b_pin_id+' as EVENTOUT pin */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+b_pin_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
    write(fv, '  GPIO_InitStruct.GPIO_Speed = ');
    case b_pin_speed_idx of
      1: writeln(fv, 'GPIO_Speed_400KHz;');
      2: writeln(fv, 'GPIO_Speed_2MHz;');
      3: writeln(fv, 'GPIO_Speed_10MHz;');
      4: writeln(fv, 'GPIO_Speed_40MHz;');
    else
      writeln(fv, 'GPIO_Speed_40MHz;');
    end;
    writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
    write(fv, '  GPIO_InitStruct.GPIO_PuPd = ');
    case b_pin_pull_updown_idx of
      1: writeln(fv, 'GPIO_PuPd_NOPULL;');
      2: writeln(fv, 'GPIO_PuPd_UP;');
      3: writeln(fv, 'GPIO_PuPd_DOWN;');
    else
      writeln(fv, 'GPIO_PuPd_NOPULL;');
    end;
    writeln(fv, '  GPIO_Init(GPIO'+b_pin_id[2]+', &GPIO_InitStruct);');
    writeln(fv, '  GPIO_PinAFConfig(GPIO'+b_pin_id[2]+', GPIO_PinSource'+StrOnlyNumbers(b_pin_id)+', GPIO_AF_EVENTOUT);');
    writeln(fv, ' ');
  end;
  if b_pin_func = 'CLOCK' then begin
    //
    writeln(fv, '  /* '+b_pin_id+' as CLOCK MCO pin */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+b_pin_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
    write(fv, '  GPIO_InitStruct.GPIO_Speed = ');
    case b_pin_speed_idx of
      1: writeln(fv, 'GPIO_Speed_400KHz;');
      2: writeln(fv, 'GPIO_Speed_2MHz;');
      3: writeln(fv, 'GPIO_Speed_10MHz;');
      4: writeln(fv, 'GPIO_Speed_40MHz;');
    else
      writeln(fv, 'GPIO_Speed_40MHz;');
    end;
    writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
    write(fv, '  GPIO_InitStruct.GPIO_PuPd = ');
    case b_pin_pull_updown_idx of
      1: writeln(fv, 'GPIO_PuPd_NOPULL;');
      2: writeln(fv, 'GPIO_PuPd_UP;');
      3: writeln(fv, 'GPIO_PuPd_DOWN;');
    else
      writeln(fv, 'GPIO_PuPd_NOPULL;');
    end;
    writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');
    writeln(fv, '  GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_MCO);');
    writeln(fv, ' ');
  end;
end;


procedure vpc_gpio_init(var fv:Text);
begin
  writeln(fv, 'void vpc_gpio_init(void){');
  vpc_gpio_init_prologue(fv);
  {PA}
  if v_pa0_func <> 'N/A' then begin
    pin_PA0_to_buffer;
    b_pin_id := v_pa0_id;
    b_pin_name := v_pa0_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa10_func <> 'N/A' then begin
    pin_PA10_to_buffer;
    b_pin_id := v_pa10_id;
    b_pin_name := v_pa10_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa11_func <> 'N/A' then begin
    pin_PA11_to_buffer;
    b_pin_id := v_pa11_id;
    b_pin_name := v_pa11_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa12_func <> 'N/A' then begin
    pin_PA12_to_buffer;
    b_pin_id := v_pa12_id;
    b_pin_name := v_pa12_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa15_func <> 'N/A' then begin
    pin_PA15_to_buffer;
    b_pin_id := v_pa15_id;
    b_pin_name := v_pa15_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa1_func <> 'N/A' then begin
    pin_PA1_to_buffer;
    b_pin_id := v_pa1_id;
    b_pin_name := v_pa1_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa2_func <> 'N/A' then begin
    pin_PA2_to_buffer;
    b_pin_id := v_pa2_id;
    b_pin_name := v_pa2_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa3_func <> 'N/A' then begin
    pin_PA3_to_buffer;
    b_pin_id := v_pa3_id;
    b_pin_name := v_pa3_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa4_func <> 'N/A' then begin
    pin_PA4_to_buffer;
    b_pin_id := v_pa4_id;
    b_pin_name := v_pa4_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa5_func <> 'N/A' then begin
    pin_PA5_to_buffer;
    b_pin_id := v_pa5_id;
    b_pin_name := v_pa5_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa6_func <> 'N/A' then begin
    pin_PA6_to_buffer;
    b_pin_id := v_pa6_id;
    b_pin_name := v_pa6_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa7_func <> 'N/A' then begin
    pin_PA7_to_buffer;
    b_pin_id := v_pa7_id;
    b_pin_name := v_pa7_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa8_func <> 'N/A' then begin
    pin_PA8_to_buffer;
    b_pin_id := v_pa8_id;
    b_pin_name := v_pa8_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pa9_func <> 'N/A' then begin
    pin_PA9_to_buffer;
    b_pin_id := v_pa9_id;
    b_pin_name := v_pa9_name;
    vpc_gpio_init_content(fv);
  end;
  {PB}
  if v_pb0_func <> 'N/A' then begin
    pin_PB0_to_buffer;
    b_pin_id := v_pb0_id;
    b_pin_name := v_pb0_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb10_func <> 'N/A' then begin
    pin_PB10_to_buffer;
    b_pin_id := v_pb10_id;
    b_pin_name := v_pb10_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb11_func <> 'N/A' then begin
    pin_PB11_to_buffer;
    b_pin_id := v_pb11_id;
    b_pin_name := v_pb11_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb12_func <> 'N/A' then begin
    pin_PB12_to_buffer;
    b_pin_id := v_pb12_id;
    b_pin_name := v_pb12_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb13_func <> 'N/A' then begin
    pin_PB13_to_buffer;
    b_pin_id := v_pb13_id;
    b_pin_name := v_pb13_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb14_func <> 'N/A' then begin
    pin_PB14_to_buffer;
    b_pin_id := v_pb14_id;
    b_pin_name := v_pb14_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb15_func <> 'N/A' then begin
    pin_PB15_to_buffer;
    b_pin_id := v_pb15_id;
    b_pin_name := v_pb15_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb1_func <> 'N/A' then begin
    pin_PB1_to_buffer;
    b_pin_id := v_pb1_id;
    b_pin_name := v_pb1_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb2_func <> 'N/A' then begin
    pin_PB2_to_buffer;
    b_pin_id := v_pb2_id;
    b_pin_name := v_pb2_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb3_func <> 'N/A' then begin
    pin_PB3_to_buffer;
    b_pin_id := v_pb3_id;
    b_pin_name := v_pb3_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb4_func <> 'N/A' then begin
    pin_PB4_to_buffer;
    b_pin_id := v_pb4_id;
    b_pin_name := v_pb4_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb5_func <> 'N/A' then begin
    pin_PB5_to_buffer;
    b_pin_id := v_pb5_id;
    b_pin_name := v_pb5_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb6_func <> 'N/A' then begin
    pin_PB6_to_buffer;
    b_pin_id := v_pb6_id;
    b_pin_name := v_pb6_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb7_func <> 'N/A' then begin
    pin_PB7_to_buffer;
    b_pin_id := v_pb7_id;
    b_pin_name := v_pb7_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb8_func <> 'N/A' then begin
    pin_PB8_to_buffer;
    b_pin_id := v_pb8_id;
    b_pin_name := v_pb8_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pb9_func <> 'N/A' then begin
    pin_PB9_to_buffer;
    b_pin_id := v_pb9_id;
    b_pin_name := v_pb9_name;
    vpc_gpio_init_content(fv);
  end;
  {PC}
  if v_pc0_func <> 'N/A' then begin
    pin_PC0_to_buffer;
    b_pin_id := v_pc0_id;
    b_pin_name := v_pc0_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc10_func <> 'N/A' then begin
    pin_PC10_to_buffer;
    b_pin_id := v_pc10_id;
    b_pin_name := v_pc10_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc11_func <> 'N/A' then begin
    pin_PC11_to_buffer;
    b_pin_id := v_pc11_id;
    b_pin_name := v_pc11_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc12_func <> 'N/A' then begin
    pin_PC12_to_buffer;
    b_pin_id := v_pc12_id;
    b_pin_name := v_pc12_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc13_func <> 'N/A' then begin
    pin_PC13_to_buffer;
    b_pin_id := v_pc13_id;
    b_pin_name := v_pc13_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc1_func <> 'N/A' then begin
    pin_PC1_to_buffer;
    b_pin_id := v_pc1_id;
    b_pin_name := v_pc1_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc2_func <> 'N/A' then begin
    pin_PC2_to_buffer;
    b_pin_id := v_pc2_id;
    b_pin_name := v_pc2_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc3_func <> 'N/A' then begin
    pin_PC3_to_buffer;
    b_pin_id := v_pc3_id;
    b_pin_name := v_pc3_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc4_func <> 'N/A' then begin
    pin_PC4_to_buffer;
    b_pin_id := v_pc4_id;
    b_pin_name := v_pc4_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc5_func <> 'N/A' then begin
    pin_PC5_to_buffer;
    b_pin_id := v_pc5_id;
    b_pin_name := v_pc5_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc6_func <> 'N/A' then begin
    pin_PC6_to_buffer;
    b_pin_id := v_pc6_id;
    b_pin_name := v_pc6_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc7_func <> 'N/A' then begin
    pin_PC7_to_buffer;
    b_pin_id := v_pc7_id;
    b_pin_name := v_pc7_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc8_func <> 'N/A' then begin
    pin_PC8_to_buffer;
    b_pin_id := v_pc8_id;
    b_pin_name := v_pc8_name;
    vpc_gpio_init_content(fv);
  end;
  if v_pc9_func <> 'N/A' then begin
    pin_PC9_to_buffer;
    b_pin_id := v_pc9_id;
    b_pin_name := v_pc9_name;
    vpc_gpio_init_content(fv);
  end;
  {PD}
  if v_pd2_func <> 'N/A' then begin
    pin_PD2_to_buffer;
    b_pin_id := v_pd2_id;
    b_pin_name := v_pd2_name;
    vpc_gpio_init_content(fv);
  end;
  if v_exti0_used then begin
    //writeln(fv, '  NVIC_EnableIRQ(EXTI0_IRQn);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = EXTI0_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_exti0_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_exti0_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
    writeln(fv, ' ');
  end;  
  if v_exti1_used then begin
    //writeln(fv, '  NVIC_EnableIRQ(EXTI1_IRQn);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = EXTI1_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_exti1_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_exti1_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
    writeln(fv, ' ');
  end;  
  if v_exti2_used then begin
    //writeln(fv, '  NVIC_EnableIRQ(EXTI2_IRQn);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = EXTI2_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_exti2_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_exti2_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
    writeln(fv, ' ');
  end;  
  if v_exti3_used then begin
    //writeln(fv, '  NVIC_EnableIRQ(EXTI3_IRQn);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = EXTI3_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_exti3_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_exti3_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
    writeln(fv, ' ');
  end;  
  if v_exti4_used then begin
    //writeln(fv, '  NVIC_EnableIRQ(EXTI4_IRQn);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = EXTI4_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_exti4_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_exti4_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
    writeln(fv, ' ');
  end;  
  if v_exti9_5_used then begin
    //writeln(fv, '  NVIC_EnableIRQ(EXTI9_5_IRQn);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = EXTI9_5_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_exti9_5_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_exti9_5_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
    writeln(fv, ' ');
  end;  
  if v_exti15_10_used then begin
    //writeln(fv, '  NVIC_EnableIRQ(EXTI15_10_IRQn);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_exti15_10_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_exti15_10_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
    writeln(fv, ' ');
  end;  
  writeln(fv, '}');
  writeln(fv, ' ');
end;


function find_label(lbl:string):boolean;
begin
  result := FALSE;
  {PC10}
  if v_e_pc10_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PC12}
  if v_e_pc12_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA15}
  if v_e_pa15_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB7}
  if v_e_pb7_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PC2}
  if v_e_pc2_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PC3}
  if v_e_pc3_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {------------------------------}
  {PC11}
  if v_e_pc11_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PD2}
  if v_e_pd2_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA0}
  if v_e_pa0_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA1}
  if v_e_pa1_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA4}
  if v_e_pa4_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB0}
  if v_e_pb0_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PC1}
  if v_e_pc1_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PC0}
  if v_e_pc0_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {------------------------------}
  {PC9}
  if v_e_pc9_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB8}
  if v_e_pb8_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB9}
  if v_e_pb9_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA6}
  if v_e_pa6_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA7}
  if v_e_pa7_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB6}
  if v_e_pb6_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PC7}
  if v_e_pc7_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA9}
  if v_e_pa9_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA8}
  if v_e_pa8_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB10}
  if v_e_pb10_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB4}
  if v_e_pb4_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB5}
  if v_e_pb5_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB3}
  if v_e_pb3_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA10}
  if v_e_pa10_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {------------------------------}
  {PC8}
  if v_e_pc8_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PC6}
  if v_e_pc6_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PC5}
  if v_e_pc5_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA12}
  if v_e_pa12_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PA11}
  if v_e_pa11_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB12}
  if v_e_pb12_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB11}
  if v_e_pb11_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB2}
  if v_e_pb2_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB1}
  if v_e_pb1_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB15}
  if v_e_pb15_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB14}
  if v_e_pb14_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PB13}
  if v_e_pb13_txt = lbl then begin
    result := TRUE;
    exit;
  end;
  {PC4}
  if v_e_pc4_txt = lbl then begin
    result := TRUE;
    exit;
  end;
end;

function verify_lcd4_pin_integrity:boolean;
begin
  //
  result := TRUE;
  if not find_label('LCD_RS') then begin
    ShowMessage('You use LCD4 module but label LCD_RS is not defined!');
    result := FALSE;
    exit;
  end;
  if not find_label('LCD_E') then begin
    ShowMessage('You use LCD4 module but label LCD_E is not defined!');
    result := FALSE;
    exit;
  end;
  if not find_label('LCD_D4') then begin
    ShowMessage('You use LCD4 module but label LCD_D4 is not defined!');
    result := FALSE;
    exit;
  end;
  if not find_label('LCD_D5') then begin
    ShowMessage('You use LCD4 module but label LCD_D5 is not defined!');
    result := FALSE;
    exit;
  end;
  if not find_label('LCD_D6') then begin
    ShowMessage('You use LCD4 module but label LCD_D6 is not defined!');
    result := FALSE;
    exit;
  end;
  if not find_label('LCD_D7') then begin
    ShowMessage('You use LCD4 module but label LCD_D7 is not defined!');
    result := FALSE;
    exit;
  end;
end;


function find_label_duplicates(lbl:string; excluded_pin:string):boolean;
begin
  result := FALSE;
  {PC10}
  if v_pc10_id <> excluded_pin then
    if v_e_pc10_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC12}
  if v_pc12_id <> excluded_pin then
    if v_e_pc12_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA13}
  {PA14}
  {PA15}
  if v_pa15_id <> excluded_pin then
    if v_e_pa15_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB7}
  if v_pb7_id <> excluded_pin then
    if v_e_pb7_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC13}
  if v_pc13_id <> excluded_pin then
    if v_e_pc13_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC14}
  {PC15}
  {PH0}
  {PH1}
  {PC2}
  if v_pc2_id <> excluded_pin then
    if v_e_pc2_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC3}
  if v_pc3_id <> excluded_pin then
    if v_e_pc3_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {-----------------------------------------}
  {PC11}
  if v_pc11_id <> excluded_pin then
    if v_e_pc11_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PD2}
  if v_pd2_id <> excluded_pin then
    if v_e_pd2_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA0}
  if v_pa0_id <> excluded_pin then
    if v_e_pa0_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA1}
  if v_pa1_id <> excluded_pin then
    if v_e_pa1_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA4}
  if v_pa4_id <> excluded_pin then
    if v_e_pa4_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB0}
  if v_pb0_id <> excluded_pin then
    if v_e_pb0_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC1}
  if v_pc1_id <> excluded_pin then
    if v_e_pc1_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC0}
  if v_pc0_id <> excluded_pin then
    if v_e_pc0_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {-----------------------------------------}
  {PC9}
  if v_pc9_id <> excluded_pin then
    if v_e_pc9_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB8}
  if v_pb8_id <> excluded_pin then
    if v_e_pb8_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB9}
  if v_pb9_id <> excluded_pin then
    if v_e_pb9_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA5}
  if v_pa5_id <> excluded_pin then
    if v_e_pa5_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA6}
  if v_pa6_id <> excluded_pin then
    if v_e_pa6_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA7}
  if v_pa7_id <> excluded_pin then
    if v_e_pa7_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB6}
  if v_pb6_id <> excluded_pin then
    if v_e_pb6_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC7}
  if v_pc7_id <> excluded_pin then
    if v_e_pc7_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA9}
  if v_pa9_id <> excluded_pin then
    if v_e_pa9_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA8}
  if v_pa8_id <> excluded_pin then
    if v_e_pa8_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB10}
  if v_pb10_id <> excluded_pin then
    if v_e_pb10_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB4}
  if v_pb4_id <> excluded_pin then
    if v_e_pb4_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB5}
  if v_pb5_id <> excluded_pin then
    if v_e_pb5_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB3}
  if v_pb3_id <> excluded_pin then
    if v_e_pb3_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA10}
  if v_pa10_id <> excluded_pin then
    if v_e_pa10_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA2}
  if v_pa2_id <> excluded_pin then
    if v_e_pa2_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA3}
  if v_pa3_id <> excluded_pin then
    if v_e_pa3_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {-----------------------------------------}
  {PC8}
  if v_pc8_id <> excluded_pin then
    if v_e_pc8_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC6}
  if v_pc6_id <> excluded_pin then
    if v_e_pc6_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC5}
  if v_pc5_id <> excluded_pin then
    if v_e_pc5_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA12}
  if v_pa12_id <> excluded_pin then
    if v_e_pa12_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PA11}
  if v_pa11_id <> excluded_pin then
    if v_e_pa11_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB12}
  if v_pb12_id <> excluded_pin then
    if v_e_pb12_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB11}
  if v_pb11_id <> excluded_pin then
    if v_e_pb11_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB2}
  if v_pb2_id <> excluded_pin then
    if v_e_pb2_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB1}
  if v_pb1_id <> excluded_pin then
    if v_e_pb1_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB15}
  if v_pb15_id <> excluded_pin then
    if v_e_pb15_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB14}
  if v_pb14_id <> excluded_pin then
    if v_e_pb14_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PB13}
  if v_pb13_id <> excluded_pin then
    if v_e_pb13_txt = lbl then begin
      result := TRUE;
      exit;
    end;
  {PC4}
  if v_pc4_id <> excluded_pin then
    if v_e_pc4_txt = lbl then begin
      result := TRUE;
      exit;
    end;
end;

function search_label_duplicates:boolean;
begin
  result := FALSE;
  {PC10}
  if v_e_pc10_txt <> '' then
  if find_label_duplicates(v_e_pc10_txt, v_pc10_id) then begin
    ShowMessage('Error! The label of PC10 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC12}
  if v_e_pc12_txt <> '' then
  if find_label_duplicates(v_e_pc12_txt, v_pc12_id) then begin
    ShowMessage('Error! The label of PC12 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA13}
  {PA14}
  {PA15}
  if v_e_pa15_txt <> '' then
  if find_label_duplicates(v_e_pa15_txt, v_pa15_id) then begin
    ShowMessage('Error! The label of PA15 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB7}
  if v_e_pb7_txt <> '' then
  if find_label_duplicates(v_e_pb7_txt, v_pb7_id) then begin
    ShowMessage('Error! The label of PB7 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC13}
  if v_e_pc13_txt <> '' then
  if find_label_duplicates(v_e_pc13_txt, v_pc13_id) then begin
    ShowMessage('Error! The label of PC13 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC14}
  {PC15}
  {PH0}
  {PH1}
  {PC2}
  if v_e_pc2_txt <> '' then
  if find_label_duplicates(v_e_pc2_txt, v_pc2_id) then begin
    ShowMessage('Error! The label of PC2 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC3}
  if v_e_pc3_txt <> '' then
  if find_label_duplicates(v_e_pc3_txt, v_pc3_id) then begin
    ShowMessage('Error! The label of PC3 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {-----------------------------------------------------------------------}
  {PC11}
  if v_e_pc11_txt <> '' then
  if find_label_duplicates(v_e_pc11_txt, v_pc11_id) then begin
    ShowMessage('Error! The label of PC11 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PD2}
  if v_e_pd2_txt <> '' then
  if find_label_duplicates(v_e_pd2_txt, v_pd2_id) then begin
    ShowMessage('Error! The label of PD2 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA0}
  if v_e_pa0_txt <> '' then
  if find_label_duplicates(v_e_pa0_txt, v_pa0_id) then begin
    ShowMessage('Error! The label of PA0 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA1}
  if v_e_pa1_txt <> '' then
  if find_label_duplicates(v_e_pa1_txt, v_pa1_id) then begin
    ShowMessage('Error! The label of PA1 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA4}
  if v_e_pa4_txt <> '' then
  if find_label_duplicates(v_e_pa4_txt, v_pa4_id) then begin
    ShowMessage('Error! The label of PA4 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB0}
  if v_e_pb0_txt <> '' then
  if find_label_duplicates(v_e_pb0_txt, v_pb0_id) then begin
    ShowMessage('Error! The label of PB0 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC1}
  if v_e_pc1_txt <> '' then
  if find_label_duplicates(v_e_pc1_txt, v_pc1_id) then begin
    ShowMessage('Error! The label of PC1 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC0}
  if v_e_pc0_txt <> '' then
  if find_label_duplicates(v_e_pc0_txt, v_pc0_id) then begin
    ShowMessage('Error! The label of PC0 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {-------------------------------------------------------------------------------}
  {PC9}
  if v_e_pc9_txt <> '' then
  if find_label_duplicates(v_e_pc9_txt, v_pc9_id) then begin
    ShowMessage('Error! The label of PC9 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB8}
  if v_e_pb8_txt <> '' then
  if find_label_duplicates(v_e_pb8_txt, v_pb8_id) then begin
    ShowMessage('Error! The label of PB8 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB9}
  if v_e_pb9_txt <> '' then
  if find_label_duplicates(v_e_pb9_txt, v_pb9_id) then begin
    ShowMessage('Error! The label of PB9 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA5}
  if v_e_pa5_txt <> '' then
  if find_label_duplicates(v_e_pa5_txt, v_pa5_id) then begin
    ShowMessage('Error! The label of PA5 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA6}
  if v_e_pa6_txt <> '' then
  if find_label_duplicates(v_e_pa6_txt, v_pa6_id) then begin
    ShowMessage('Error! The label of PA6 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA7}
  if v_e_pa7_txt <> '' then
  if find_label_duplicates(v_e_pa7_txt, v_pa7_id) then begin
    ShowMessage('Error! The label of PA7 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB6}
  if v_e_pb6_txt <> '' then
  if find_label_duplicates(v_e_pb6_txt, v_pb6_id) then begin
    ShowMessage('Error! The label of PB6 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC7}
  if v_e_pc7_txt <> '' then
  if find_label_duplicates(v_e_pc7_txt, v_pc7_id) then begin
    ShowMessage('Error! The label of PC7 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA9}
  if v_e_pa9_txt <> '' then
  if find_label_duplicates(v_e_pa9_txt, v_pa9_id) then begin
    ShowMessage('Error! The label of PA9 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA8}
  if v_e_pa8_txt <> '' then
  if find_label_duplicates(v_e_pa8_txt, v_pa8_id) then begin
    ShowMessage('Error! The label of PA8 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB10}
  if v_e_pb10_txt <> '' then
  if find_label_duplicates(v_e_pb10_txt, v_pb10_id) then begin
    ShowMessage('Error! The label of PB10 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB4}
  if v_e_pb4_txt <> '' then
  if find_label_duplicates(v_e_pb4_txt, v_pb4_id) then begin
    ShowMessage('Error! The label of PB4 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB5}
  if v_e_pb5_txt <> '' then
  if find_label_duplicates(v_e_pb5_txt, v_pb5_id) then begin
    ShowMessage('Error! The label of PB5 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB3}
  if v_e_pb3_txt <> '' then
  if find_label_duplicates(v_e_pb3_txt, v_pb3_id) then begin
    ShowMessage('Error! The label of PB3 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA10}
  if v_e_pa10_txt <> '' then
  if find_label_duplicates(v_e_pa10_txt, v_pa10_id) then begin
    ShowMessage('Error! The label of PA10 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA2}
  if v_e_pa2_txt <> '' then
  if find_label_duplicates(v_e_pa2_txt, v_pa2_id) then begin
    ShowMessage('Error! The label of PA2 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA3}
  if v_e_pa3_txt <> '' then
  if find_label_duplicates(v_e_pa3_txt, v_pa3_id) then begin
    ShowMessage('Error! The label of PA3 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {-------------------------------------------------------------------------}
  {PC8}
  if v_e_pc8_txt <> '' then
  if find_label_duplicates(v_e_pc8_txt, v_pc8_id) then begin
    ShowMessage('Error! The label of PC8 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC6}
  if v_e_pc6_txt <> '' then
  if find_label_duplicates(v_e_pc6_txt, v_pc6_id) then begin
    ShowMessage('Error! The label of PC6 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC5}
  if v_e_pc5_txt <> '' then
  if find_label_duplicates(v_e_pc5_txt, v_pc5_id) then begin
    ShowMessage('Error! The label of PC5 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA12}
  if v_e_pa12_txt <> '' then
  if find_label_duplicates(v_e_pa12_txt, v_pa12_id) then begin
    ShowMessage('Error! The label of PA12 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PA11}
  if v_e_pa11_txt <> '' then
  if find_label_duplicates(v_e_pa11_txt, v_pa11_id) then begin
    ShowMessage('Error! The label of PA11 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB12}
  if v_e_pb12_txt <> '' then
  if find_label_duplicates(v_e_pb12_txt, v_pb12_id) then begin
    ShowMessage('Error! The label of PB12 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB11}
  if v_e_pb11_txt <> '' then
  if find_label_duplicates(v_e_pb11_txt, v_pb11_id) then begin
    ShowMessage('Error! The label of PB11 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB2}
  if v_e_pb2_txt <> '' then
  if find_label_duplicates(v_e_pb2_txt, v_pb2_id) then begin
    ShowMessage('Error! The label of PB2 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB1}
  if v_e_pb1_txt <> '' then
  if find_label_duplicates(v_e_pb1_txt, v_pb1_id) then begin
    ShowMessage('Error! The label of PB1 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB15}
  if v_e_pb15_txt <> '' then
  if find_label_duplicates(v_e_pb15_txt, v_pb15_id) then begin
    ShowMessage('Error! The label of PB15 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB14}
  if v_e_pb14_txt <> '' then
  if find_label_duplicates(v_e_pb14_txt, v_pb14_id) then begin
    ShowMessage('Error! The label of PB14 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PB13}
  if v_e_pb13_txt <> '' then
  if find_label_duplicates(v_e_pb13_txt, v_pb13_id) then begin
    ShowMessage('Error! The label of PB13 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
  {PC4}
  if v_e_pc4_txt <> '' then
  if find_label_duplicates(v_e_pc4_txt, v_pc4_id) then begin
    ShowMessage('Error! The label of PC4 has duplicates elsewhere.');
    result := TRUE;
    exit;
  end;
end;

procedure copyright_h(var fv:Text);
begin
  writeln(fv, '');
  writeln(fv, '  Copyright (c) '+ v_year);
  writeln(fv, '  Author: ' + v_author);
  writeln(fv, '');
end;

procedure proprietary_lic_h(var fv:Text);
begin
  writeln(fv, '  All rights reserved!');
  writeln(fv, '  Project status: Confidential!');
  writeln(fv, '*/');
end;

procedure mit_lic_h(var fv:Text);
begin
  writeln(fv, '  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and ');
  writeln(fv, '  associated documentation files (the "Software"), to deal in the Software without restriction,');
  writeln(fv, '  including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,');
  writeln(fv, '  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do');
  writeln(fv, '  so, subject to the following conditions:');
  writeln(fv, ' ');
  writeln(fv, '  The above copyright notice and this permission notice shall be included in all copies or substantial');
  writeln(fv, '  portions of the Software.');
  writeln(fv, ' ');
  writeln(fv, '  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,');
  writeln(fv, '  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A');
  writeln(fv, '  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR');
  writeln(fv, '  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN');
  writeln(fv, '  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION');
  writeln(fv, '  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.');
  writeln(fv, '*/');
end;

procedure apache_lic_h(var fv:Text);
begin
  writeln(fv, '  Licensed under the Apache License, Version 2.0 (the "License");');
  writeln(fv, '  you may not use this file except in compliance with the License.');
  writeln(fv, '  You may obtain a copy of the License at');
  writeln(fv, '');
  writeln(fv, '    http://www.apache.org/licenses/LICENSE-2.0');
  writeln(fv, '');
  writeln(fv, '  Unless required by applicable law or agreed to in writing, software');
  writeln(fv, '  distributed under the License is distributed on an "AS IS" BASIS,');
  writeln(fv, '  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.');
  writeln(fv, '  See the License for the specific language governing permissions and');
  writeln(fv, '  limitations under the License.');
  writeln(fv ,'*/');
end;

procedure lgpl_lic_h(var fv:Text);
begin
  writeln(fv, '  * This program is free software: you can redistribute it and/or modify');
  writeln(fv, '  * it under the terms of the GNU Lesser General Public License as published by');
  writeln(fv, '  * the Free Software Foundation, either version 3 of the License, or');
  writeln(fv, '  * (at your option) any later version.');
  writeln(fv, '  *');
  writeln(fv, '  * This program is distributed in the hope that it will be useful,');
  writeln(fv, '  * but WITHOUT ANY WARRANTY; without even the implied warranty of');
  writeln(fv, '  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the');
  writeln(fv, '  * GNU Lesser General Public License for more details.');
  writeln(fv, '  *');
  writeln(fv, '  * You should have received a copy of the GNU Lesser General Public License');
  writeln(fv, '  * along with this program.  If not, see <http://www.gnu.org/licenses/>.');
  writeln(fv ,'  */');
end;

procedure bsd_lic_h(var fv:Text);
begin
  writeln(fv, '  Redistribution and use in source and binary forms, with or without modification,');
  writeln(fv, '  are permitted provided that the following conditions are met:');
  writeln(fv, '');
  writeln(fv, '  1. Redistributions of source code must retain the above copyright notice,');
  writeln(fv, '     this list of conditions and the following disclaimer.');
  writeln(fv, '');
  writeln(fv, '  2. Redistributions in binary form must reproduce the above copyright notice,');
  writeln(fv, '     this list of conditions and the following disclaimer in the documentation');
  writeln(fv, '     and/or other materials provided with the distribution.');
  writeln(fv, '');
  writeln(fv, '  3. Neither the name of the copyright holder nor the names of its contributors');
  writeln(fv, '     may be used to endorse or promote products derived from this software');
  writeln(fv, '     without specific prior written permission.');
  writeln(fv, '');
  writeln(fv, '  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"');
  writeln(fv, '  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE');
  writeln(fv, '  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE');
  writeln(fv, '  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE');
  writeln(fv, '  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL');
  writeln(fv, '  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR');
  writeln(fv, '  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER');
  writeln(fv, '  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,');
  writeln(fv, '  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE');
  writeln(fv, '  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.');
  writeln(fv ,'*/');
end;

// create definitions for pin labels in the main.h file
procedure define_labels(var fv:Text);
begin
  {PC10}
  if ((v_pc10_func <> 'N/A') and (v_e_pc10_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc10_txt+'_Pin GPIO_Pin_10');
    writeln(fv, '#define '+v_e_pc10_txt+'_Port GPIOC');
  end;
  {PC12}
  if ((v_pc12_func <> 'N/A') and (v_e_pc12_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc12_txt+'_Pin GPIO_Pin_12');
    writeln(fv, '#define '+v_e_pc12_txt+'_Port GPIOC');
  end;
  {PA13}
  {PA14}
  {PA15}
  if ((v_pa15_func <> 'N/A') and (v_e_pa15_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa15_txt+'_Pin GPIO_Pin_15');
    writeln(fv, '#define '+v_e_pa15_txt+'_Port GPIOA');
  end;
  {PB7}
  if ((v_pb7_func <> 'N/A') and (v_e_pb7_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb7_txt+'_Pin GPIO_Pin_7');
    writeln(fv, '#define '+v_e_pb7_txt+'_Port GPIOB');
  end;
  {PC13}
  if ((v_pc13_func <> 'N/A') and (v_e_pc13_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc13_txt+'_Pin GPIO_Pin_13');
    writeln(fv, '#define '+v_e_pc13_txt+'_Port GPIOC');
  end;
  {PC14}
  {PC15}
  {PH0}
  {PH1}
  {PC2}
  if ((v_pc2_func <> 'N/A') and (v_e_pc2_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc2_txt+'_Pin GPIO_Pin_2');
    writeln(fv, '#define '+v_e_pc2_txt+'_Port GPIOC');
  end;
  {PC3}
  if ((v_pc3_func <> 'N/A') and (v_e_pc3_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc3_txt+'_Pin GPIO_Pin_3');
    writeln(fv, '#define '+v_e_pc3_txt+'_Port GPIOC');
  end;
  {-----------------------------------------}
  {PC11}
  if ((v_pc11_func <> 'N/A') and (v_e_pc11_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc11_txt+'_Pin GPIO_Pin_11');
    writeln(fv, '#define '+v_e_pc11_txt+'_Port GPIOC');
  end;
  {PD2}
  if ((v_pd2_func <> 'N/A') and (v_e_pd2_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pd2_txt+'_Pin GPIO_Pin_2');
    writeln(fv, '#define '+v_e_pd2_txt+'_Port GPIOD');
  end;
  {PA0}
  if ((v_pa0_func <> 'N/A') and (v_e_pa0_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa0_txt+'_Pin GPIO_Pin_0');
    writeln(fv, '#define '+v_e_pa0_txt+'_Port GPIOA');
  end;
  {PA1}
  if ((v_pa1_func <> 'N/A') and (v_e_pa1_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa1_txt+'_Pin GPIO_Pin_1');
    writeln(fv, '#define '+v_e_pa1_txt+'_Port GPIOA');
  end;
  {PA4}
  if ((v_pa4_func <> 'N/A') and (v_e_pa4_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa4_txt+'_Pin GPIO_Pin_4');
    writeln(fv, '#define '+v_e_pa4_txt+'_Port GPIOA');
  end;
  {PB0}
  if ((v_pb0_func <> 'N/A') and (v_e_pb0_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb0_txt+'_Pin GPIO_Pin_0');
    writeln(fv, '#define '+v_e_pb0_txt+'_Port GPIOB');
  end;
  {PC1}
  if ((v_pc1_func <> 'N/A') and (v_e_pc1_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc1_txt+'_Pin GPIO_Pin_1');
    writeln(fv, '#define '+v_e_pc1_txt+'_Port GPIOC');
  end;
  {PC0}
  if ((v_pc0_func <> 'N/A') and (v_e_pc0_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc0_txt+'_Pin GPIO_Pin_0');
    writeln(fv, '#define '+v_e_pc0_txt+'_Port GPIOC');
  end;
  {-----------------------------------------}
  {PC9}
  if ((v_pc9_func <> 'N/A') and (v_e_pc9_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc9_txt+'_Pin GPIO_Pin_9');
    writeln(fv, '#define '+v_e_pc9_txt+'_Port GPIOC');
  end;
  {PB8}
  if ((v_pb8_func <> 'N/A') and (v_e_pb8_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb8_txt+'_Pin GPIO_Pin_8');
    writeln(fv, '#define '+v_e_pb8_txt+'_Port GPIOB');
  end;
  {PB9}
  if ((v_pb9_func <> 'N/A') and (v_e_pb9_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb9_txt+'_Pin GPIO_Pin_9');
    writeln(fv, '#define '+v_e_pb9_txt+'_Port GPIOB');
  end;
  {PA5}
  if ((v_pa5_func <> 'N/A') and (v_e_pa5_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa5_txt+'_Pin GPIO_Pin_5');
    writeln(fv, '#define '+v_e_pa5_txt+'_Port GPIOA');
  end;
  {PA6}
  if ((v_pa6_func <> 'N/A') and (v_e_pa6_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa6_txt+'_Pin GPIO_Pin_6');
    writeln(fv, '#define '+v_e_pa6_txt+'_Port GPIOA');
  end;
  {PA7}
  if ((v_pa7_func <> 'N/A') and (v_e_pa7_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa7_txt+'_Pin GPIO_Pin_7');
    writeln(fv, '#define '+v_e_pa7_txt+'_Port GPIOA');
  end;
  {PB6}
  if ((v_pb6_func <> 'N/A') and (v_e_pb6_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb6_txt+'_Pin GPIO_Pin_6');
    writeln(fv, '#define '+v_e_pb6_txt+'_Port GPIOB');
  end;
  {PC7}
  if ((v_pc7_func <> 'N/A') and (v_e_pc7_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc7_txt+'_Pin GPIO_Pin_7');
    writeln(fv, '#define '+v_e_pc7_txt+'_Port GPIOC');
  end;
  {PA9}
  if ((v_pa9_func <> 'N/A') and (v_e_pa9_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa9_txt+'_Pin GPIO_Pin_9');
    writeln(fv, '#define '+v_e_pa9_txt+'_Port GPIOA');
  end;
  {PA8}
  if ((v_pa8_func <> 'N/A') and (v_e_pa8_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa8_txt+'_Pin GPIO_Pin_8');
    writeln(fv, '#define '+v_e_pa8_txt+'_Port GPIOA');
  end;
  {PB10}
  if ((v_pb10_func <> 'N/A') and (v_e_pb10_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb10_txt+'_Pin GPIO_Pin_10');
    writeln(fv, '#define '+v_e_pb10_txt+'_Port GPIOB');
  end;
  {PB4}
  if ((v_pb4_func <> 'N/A') and (v_e_pb4_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb4_txt+'_Pin GPIO_Pin_4');
    writeln(fv, '#define '+v_e_pb4_txt+'_Port GPIOB');
  end;
  {PB5}
  if ((v_pb5_func <> 'N/A') and (v_e_pb5_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb5_txt+'_Pin GPIO_Pin_5');
    writeln(fv, '#define '+v_e_pb5_txt+'_Port GPIOB');
  end;
  {PB3}
  if ((v_pb3_func <> 'N/A') and (v_e_pb3_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb3_txt+'_Pin GPIO_Pin_3');
    writeln(fv, '#define '+v_e_pb3_txt+'_Port GPIOB');
  end;
  {PA10}
  if ((v_pa10_func <> 'N/A') and (v_e_pa10_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa10_txt+'_Pin GPIO_Pin_10');
    writeln(fv, '#define '+v_e_pa10_txt+'_Port GPIOA');
  end;
  {PA2}
  if ((v_pa2_func <> 'N/A') and (v_e_pa2_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa2_txt+'_Pin GPIO_Pin_2');
    writeln(fv, '#define '+v_e_pa2_txt+'_Port GPIOA');
  end;
  {PA3}
  if ((v_pa3_func <> 'N/A') and (v_e_pa3_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa3_txt+'_Pin GPIO_Pin_3');
    writeln(fv, '#define '+v_e_pa3_txt+'_Port GPIOA');
  end;
  {-----------------------------------------}
  {PC8}
  if ((v_pc8_func <> 'N/A') and (v_e_pc8_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc8_txt+'_Pin GPIO_Pin_8');
    writeln(fv, '#define '+v_e_pc8_txt+'_Port GPIOC');
  end;
  {PC6}
  if ((v_pc6_func <> 'N/A') and (v_e_pc6_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc6_txt+'_Pin GPIO_Pin_6');
    writeln(fv, '#define '+v_e_pc6_txt+'_Port GPIOC');
  end;
  {PC5}
  if ((v_pc5_func <> 'N/A') and (v_e_pc5_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc5_txt+'_Pin GPIO_Pin_5');
    writeln(fv, '#define '+v_e_pc5_txt+'_Port GPIOC');
  end;
  {PA12}
  if ((v_pa12_func <> 'N/A') and (v_e_pa12_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa12_txt+'_Pin GPIO_Pin_12');
    writeln(fv, '#define '+v_e_pa12_txt+'_Port GPIOA');
  end;
  {PA11}
  if ((v_pa11_func <> 'N/A') and (v_e_pa11_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pa11_txt+'_Pin GPIO_Pin_11');
    writeln(fv, '#define '+v_e_pa11_txt+'_Port GPIOA');
  end;
  {PB12}
  if ((v_pb12_func <> 'N/A') and (v_e_pb12_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb12_txt+'_Pin GPIO_Pin_12');
    writeln(fv, '#define '+v_e_pb12_txt+'_Port GPIOB');
  end;
  {PB11}
  if ((v_pb11_func <> 'N/A') and (v_e_pb11_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb11_txt+'_Pin GPIO_Pin_11');
    writeln(fv, '#define '+v_e_pb11_txt+'_Port GPIOB');
  end;
  {PB2}
  if ((v_pb2_func <> 'N/A') and (v_e_pb2_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb2_txt+'_Pin GPIO_Pin_2');
    writeln(fv, '#define '+v_e_pb2_txt+'_Port GPIOB');
  end;
  {PB1}
  if ((v_pb1_func <> 'N/A') and (v_e_pb1_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb1_txt+'_Pin GPIO_Pin_1');
    writeln(fv, '#define '+v_e_pb1_txt+'_Port GPIOB');
  end;
  {PB15}
  if ((v_pb15_func <> 'N/A') and (v_e_pb15_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb15_txt+'_Pin GPIO_Pin_15');
    writeln(fv, '#define '+v_e_pb15_txt+'_Port GPIOB');
  end;
  {PB14}
  if ((v_pb14_func <> 'N/A') and (v_e_pb14_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb14_txt+'_Pin GPIO_Pin_14');
    writeln(fv, '#define '+v_e_pb14_txt+'_Port GPIOB');
  end;
  {PB13}
  if ((v_pb13_func <> 'N/A') and (v_e_pb13_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pb13_txt+'_Pin GPIO_Pin_13');
    writeln(fv, '#define '+v_e_pb13_txt+'_Port GPIOB');
  end;
  {PC4}
  if ((v_pc4_func <> 'N/A') and (v_e_pc4_txt <> '')) then begin
    writeln(fv, '#define '+v_e_pc4_txt+'_Pin GPIO_Pin_4');
    writeln(fv, '#define '+v_e_pc4_txt+'_Port GPIOC');
  end;
end;

{==============================================================================}
procedure wr_main_h;
begin
  //
  {$I-}
  Assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Inc/main.h');
  Rewrite(F);
  //
  writeln(F, '/*');
  writeln(F, '  Generated with VPC version '+v_app_version);
  writeln(F, '  Application name: ' + v_prj_name);
  writeln(F, '  File name: main.h');
  copyright_h(F);
  if(v_copyright_idx = 0) then proprietary_lic_h(F);
  if(v_copyright_idx = 1) then lgpl_lic_h(F);
  if(v_copyright_idx = 2) then bsd_lic_h(F);
  if(v_copyright_idx = 3) then apache_lic_h(F);
  if(v_copyright_idx = 4) then mit_lic_h(F);
  //start header
  writeln(F, ' ');
  writeln(F, '#ifndef __MAIN_H');
  writeln(F, '#define __MAIN_H');
  writeln(F, ' ');
  //
  writeln(F, '#define HSI_VALUE    ' + inttostr(w_hsi));
  writeln(F, '#define HSE_VALUE    ' + inttostr(w_hse));  
  writeln(F, '#define HCLK_VALUE   ' + inttostr(w_hclk));
  writeln(F, '#define APB1_T_VALUE ' + inttostr(w_apb1_t));  
  writeln(F, '#define APB2_T_VALUE ' + inttostr(w_apb2_t));
  writeln(F, '#include "stm32l1xx.h"');
  writeln(F, '#include "stm32l1xx_comp.h"');
  writeln(F, '#include "stm32l1xx_dma.h"');
  writeln(F, '#include "stm32l1xx_exti.h"');
  writeln(F, '#include "stm32l1xx_flash.h"');
  writeln(F, '#include "stm32l1xx_fsmc.h"');
  writeln(F, '#include "stm32l1xx_gpio.h"');
  writeln(F, '#include "stm32l1xx_pwr.h"');
  writeln(F, '#include "stm32l1xx_rcc.h"');
  writeln(F, '#include "stm32l1xx_rtc.h"');
  writeln(F, '#include "stm32l1xx_syscfg.h"');
  writeln(F, '#include "stm32l1xx_usart.h"');
  writeln(F, '#include "stm32l1xx_wwdg.h"');
  //if ((v_tim6_check = TRUE) or (v_tim7_check = TRUE)) then
  writeln(F, '#include "stm32l1xx_tim.h"');   
  if ((v_c_i2c1 = TRUE) or (v_c_i2c2 = TRUE)) then
    writeln(F, '#include "stm32l1xx_i2c.h"');
  if ((v_c_spi1 = TRUE) or (v_c_spi2 = TRUE) or (v_c_spi3 = TRUE)) then
    writeln(F, '#include "stm32l1xx_spi.h"');
  if ((v_use_adc) or (v_use_adin)) then
    writeln(F, '#include "stm32l1xx_adc.h"');
  writeln(F, '#include "misc.h"');
  //
  writeln(F, ' ');
  define_labels(F);
  writeln(F, ' ');
  writeln(F, 'uint32_t SystemCoreClock      = HCLK_VALUE;');
  writeln(F, '__I uint8_t PLLMulTable[9]    = {3, 4, 6, 8, 12, 16, 24, 32, 48};');
  writeln(F, '__I uint8_t AHBPrescTable[16] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 6, 7, 8, 9};');
  writeln(F, ' ');
  writeln(F, 'volatile uint32_t my_ticks;');
  writeln(F, '/* Uncomment the following line if you need to relocate your vector Table in');
  writeln(F, '   Internal SRAM. */');
  writeln(F, '/*#define VECT_TAB_SRAM */');
  writeln(F, '#define VECT_TAB_OFFSET  0x0');
  writeln(F, '');
  writeln(F, 'void my_delay_ms(uint16_t ms){');
  writeln(F, '  my_ticks = ms;');
  writeln(F, '  while(my_ticks > 0);');
  writeln(F, '}');
  writeln(F, '');


  //end header
  writeln(F, ' ');
  writeln(F, '#endif /*__MAIN_H*/');
  writeln(F, ' ');

  Close(F);
  {$I+}
end;

procedure vpc_core_init(var fv:Text);
begin
  writeln(fv, 'void SystemCoreClockUpdate (void){');
  writeln(fv, '  uint32_t tmp = 0, pllmul = 0, plldiv = 0, pllsource = 0, msirange = 0;');
  writeln(fv, '');
  writeln(fv, '  tmp = RCC->CFGR & RCC_CFGR_SWS;');
  writeln(fv, '  ');
  writeln(fv, '  switch (tmp)');
  writeln(fv, '  {');
  writeln(fv, '    case 0x00:');
  writeln(fv, '      msirange = (RCC->ICSCR & RCC_ICSCR_MSIRANGE) >> 13;');
  writeln(fv, '      SystemCoreClock = (32768 * (1 << (msirange + 1)));');
  writeln(fv, '      break;');
  writeln(fv, '    case 0x04:');
  writeln(fv, '      SystemCoreClock = HSI_VALUE;');
  writeln(fv, '      break;');
  writeln(fv, '    case 0x08:');
  writeln(fv, '      SystemCoreClock = HSE_VALUE;');
  writeln(fv, '      break;');
  writeln(fv, '    case 0x0C:');
  writeln(fv, '      pllmul = RCC->CFGR & RCC_CFGR_PLLMUL;');
  writeln(fv, '      plldiv = RCC->CFGR & RCC_CFGR_PLLDIV;');
  writeln(fv, '      pllmul = PLLMulTable[(pllmul >> 18)];');
  writeln(fv, '      plldiv = (plldiv >> 22) + 1;');
  writeln(fv, '      ');
  writeln(fv, '      pllsource = RCC->CFGR & RCC_CFGR_PLLSRC;');
  writeln(fv, '');
  writeln(fv, '      if (pllsource == 0x00)');
  writeln(fv, '      {');
  writeln(fv, '        SystemCoreClock = (((HSI_VALUE) * pllmul) / plldiv);');
  writeln(fv, '      }');
  writeln(fv, '      else');
  writeln(fv, '      {');
  writeln(fv, '        SystemCoreClock = (((HSE_VALUE) * pllmul) / plldiv);');
  writeln(fv, '      }');
  writeln(fv, '      break;');
  writeln(fv, '    default:');
  writeln(fv, '      msirange = (RCC->ICSCR & RCC_ICSCR_MSIRANGE) >> 13;');
  writeln(fv, '      SystemCoreClock = (32768 * (1 << (msirange + 1)));');
  writeln(fv, '      break;');
  writeln(fv, '  }');
  writeln(fv, '  tmp = AHBPrescTable[((RCC->CFGR & RCC_CFGR_HPRE) >> 4)];');
  writeln(fv, '  SystemCoreClock >>= tmp;');
  writeln(fv, '}');
  writeln(fv, '');
  writeln(fv, 'static void SetSysClock(void){');
  if ((v_rg_clock_hub_idx = 1) or ((v_rg_vco_idx = 0) and (v_rg_clock_hub_idx = 3))) then 
    writeln(fv, 'ErrorStatus er;');
  writeln(fv, '  ');
  writeln(fv, '  FLASH->ACR |= FLASH_ACR_ACC64;');
  writeln(fv, '  FLASH->ACR |= FLASH_ACR_PRFTEN;');
  writeln(fv, '  FLASH->ACR |= FLASH_ACR_LATENCY;');
  writeln(fv, '  RCC->APB1ENR |= RCC_APB1ENR_PWREN;');
  writeln(fv, '  PWR->CR = PWR_CR_VOS_0;');
  writeln(fv, '  while((PWR->CSR & PWR_CSR_VOSF) != RESET){}');
  writeln(fv, '  ');
  { MSI always start first }
  case v_cb_msi_idx of
    0: writeln(fv, '  RCC_MSIRangeConfig(RCC_MSIRange_0);');
    1: writeln(fv, '  RCC_MSIRangeConfig(RCC_MSIRange_1);');
    2: writeln(fv, '  RCC_MSIRangeConfig(RCC_MSIRange_2);');
    3: writeln(fv, '  RCC_MSIRangeConfig(RCC_MSIRange_3);');
    4: writeln(fv, '  RCC_MSIRangeConfig(RCC_MSIRange_4);');
    5: writeln(fv, '  RCC_MSIRangeConfig(RCC_MSIRange_5);');
    6: writeln(fv, '  RCC_MSIRangeConfig(RCC_MSIRange_6);');
  end;
  writeln(fv, '  RCC_MSICmd(ENABLE);');
  writeln(fv, '  while (RCC_GetFlagStatus(RCC_FLAG_MSIRDY) == RESET){}');
  writeln(fv, '  ');
  { HSE - activate or not }
  if ((v_rg_clock_hub_idx = 1) or ((v_rg_vco_idx = 0) and (v_rg_clock_hub_idx = 3))) then begin
    case v_cb_rcc_hse_idx of 
      0: writeln(fv, '  RCC_HSEConfig(RCC_HSE_Bypass);');
      1: writeln(fv, '  RCC_HSEConfig(RCC_HSE_ON);');
    end;
    writeln(fv, '  er = RCC_WaitForHSEStartUp();');
    writeln(fv, '  if (er == ERROR) {');
    writeln(fv, '    // do something');
    writeln(fv, '  }');
    if v_c_en_css_hse then writeln(fv, '  RCC_ClockSecuritySystemCmd(ENABLE);');
  end
  else writeln(fv, '  RCC_HSEConfig(RCC_HSE_OFF);');
  writeln(fv, '  ');
  { HSI } 
  writeln(fv, '  RCC_HSICmd(ENABLE);');
  writeln(fv, '  while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET){}');
  writeln(fv, '  ');
  {PLL}
  if v_rg_clock_hub_idx = 3 then begin
    writeln(fv, '  RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_PLLSRC | RCC_CFGR_PLLMUL | RCC_CFGR_PLLDIV));');
    write(fv, '  RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC_');
    if v_rg_vco_idx = 0 then write(fv, 'HSE | ')
    else write(fv, 'HSI | ');
    case v_cb_pllmul_idx of 
      0: write(fv, 'RCC_CFGR_PLLMUL3 | ');
      1: write(fv, 'RCC_CFGR_PLLMUL4 | ');
      2: write(fv, 'RCC_CFGR_PLLMUL6 | ');
      3: write(fv, 'RCC_CFGR_PLLMUL8 | ');
      4: write(fv, 'RCC_CFGR_PLLMUL12 | ');
      5: write(fv, 'RCC_CFGR_PLLMUL16 | ');
      6: write(fv, 'RCC_CFGR_PLLMUL24 | ');
      7: write(fv, 'RCC_CFGR_PLLMUL32 | ');
      8: write(fv, 'RCC_CFGR_PLLMUL48 | ');
    end;
    case v_cb_plldiv_idx of 
      0: writeln(fv, 'RCC_CFGR_PLLDIV2);');
      1: writeln(fv, 'RCC_CFGR_PLLDIV3);');
      2: writeln(fv, 'RCC_CFGR_PLLDIV4);');
    end;
    writeln(fv, '  RCC->CR |= RCC_CR_PLLON;');  
    writeln(fv, '  while((RCC->CR & RCC_CR_PLLRDY) == 0){}');
  end
  else writeln(fv, '  RCC_PLLCmd(DISABLE);');
  writeln(fv, '  ');
  {SYSCLK}
  case v_rg_clock_hub_idx of
    0: writeln(fv, '  RCC_SYSCLKConfig(RCC_SYSCLKSource_MSI);');
    1: writeln(fv, '  RCC_SYSCLKConfig(RCC_SYSCLKSource_HSE);');
    2: writeln(fv, '  RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);');
    3: writeln(fv, '  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);');
  end;
  writeln(fv, '  ');
  case v_cb_ahb_pr_idx of 
    0: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV1;');
    1: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV2;');
    2: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV4;');
    3: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV8;');
    4: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV16;');
    5: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV64;');
    6: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV128;');
    7: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV256;');
    8: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV512;');
  end;
  case v_cb_apb1_pr_idx of
    0: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV1;');
    1: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV2;');
    2: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV4;');
    3: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV8;');
    4: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV16;');
  end; 
  case v_cb_apb2_pr_idx of 
    0: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV1;');
    1: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV2;');
    2: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV4;');
    3: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV8;');
    4: writeln(fv, '  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV16;');
  end;
  writeln(fv, '  ');
  {MCO}
  if v_c_rcc_mco = FALSE then writeln(fv, '  RCC_MCOConfig(RCC_MCOSource_NoClock, RCC_MCODiv_1);')
  else begin
    //
    write(fv, '  RCC_MCOConfig(');
    case v_rg_mco_src_idx of 
      0: write(fv, 'RCC_MCOSource_SYSCLK, ');
      1: write(fv, 'RCC_MCOSource_HSI, ');
      2: write(fv, 'RCC_MCOSource_MSI, ');
      3: write(fv, 'RCC_MCOSource_HSE, ');
      4: write(fv, 'RCC_MCOSource_PLLCLK, ');
      5: write(fv, 'RCC_MCOSource_LSI, ');
      6: write(fv, 'RCC_MCOSource_LSE, ');
    end; 
    case v_cb_mco_div_idx of
      0: writeln(fv, 'RCC_MCODiv_1);');
      1: writeln(fv, 'RCC_MCODiv_2);');
      2: writeln(fv, 'RCC_MCODiv_4);');
      3: writeln(fv, 'RCC_MCODiv_8);');
      4: writeln(fv, 'RCC_MCODiv_16);');
    end; 
  end;
  writeln(fv, '  ');
  {RTC} 
  writeln(fv, '  PWR_RTCAccessCmd(ENABLE);');
  if v_rg_osc32_idx = 0 then begin
    writeln(fv, '  RCC_LSEConfig(RCC_LSE_ON);');
    writeln(fv, '  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET){}');
    writeln(fv, '  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);');
    if v_c_en_css_lse then writeln(fv, '  RCC_LSEClockSecuritySystemCmd(ENABLE);');
  end  
  else writeln(fv, '  RCC_LSEConfig(RCC_LSE_OFF);');
  //
  writeln(fv, '  RCC_LSICmd(ENABLE);');
  writeln(fv, '  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET){}');
  if v_rg_osc32_idx = 1 then begin   
    writeln(fv, '  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);');
  end; 
  if v_rg_osc32_idx = 2 then begin
    case v_cb_hse_div_idx of
      0: writeln(fv, '  RCC_RTCCLKConfig(RCC_RTCCLKSource_HSE_Div2);');
      1: writeln(fv, '  RCC_RTCCLKConfig(RCC_RTCCLKSource_HSE_Div4);');
      2: writeln(fv, '  RCC_RTCCLKConfig(RCC_RTCCLKSource_HSE_Div8);');
      3: writeln(fv, '  RCC_RTCCLKConfig(RCC_RTCCLKSource_HSE_Div16);');
    end;  
  end;
  writeln(fv, '  RCC_RTCCLKCmd(ENABLE);');
  writeln(fv, '  ');
  writeln(fv, '}');
  writeln(fv, '');
  writeln(fv, 'void SystemInit (void){');
  writeln(fv, '  RCC->CR |= (uint32_t)0x00000100;');
  writeln(fv, '  RCC->CFGR &= (uint32_t)0x88FFC00C;');
  writeln(fv, '  RCC->CR &= (uint32_t)0xEEFEFFFE;');
  writeln(fv, '  RCC->CR &= (uint32_t)0xFFFBFFFF;');
  writeln(fv, '  RCC->CFGR &= (uint32_t)0xFF02FFFF;');
  writeln(fv, '  RCC->CIR = 0x00000000;');
  writeln(fv, '    ');
  writeln(fv, '  SetSysClock();');
  writeln(fv, '');
  writeln(fv, '#ifdef VECT_TAB_SRAM');
  writeln(fv, '  SCB->VTOR = SRAM_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal SRAM. */');
  writeln(fv, '#else');
  writeln(fv, '  SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH. */');
  writeln(fv, '#endif');
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_system_init(var fv:Text);
begin
  writeln(fv, 'void vpc_system_init(void){');
  writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, '  ');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_COMP, ENABLE);');
  writeln(fv, '  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);');
  { the following line is disabled because at the execution time, PWR is already enabled}
  //writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);');
  writeln(fv, '  ');
  writeln(fv, '  /* NVIC Priority Groups: See misc.h for details. */');
  writeln(fv, '  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_'+inttostr(v_group_priority)+');');
  writeln(fv, '  ');
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = MemoryManagement_IRQn;');
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;');
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;');
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
  writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = BusFault_IRQn;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
  writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = UsageFault_IRQn;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
  writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = SVC_IRQn;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
  writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = DebugMonitor_IRQn;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
  writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = PendSV_IRQn;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
  writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  my_ticks = 0;');
  {if v_tick_manager = 0 then begin}
  writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = SysTick_IRQn;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;');
  //writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
  writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  writeln(fv, '  ');
  if v_cb_systick_pr_idx = 0 then
    writeln(fv, '  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);')
  else   
    writeln(fv, '  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);');
  writeln(fv, '  SysTick_Config(SystemCoreClock / 1000);');
  writeln(fv, '}');
  writeln(fv, '  ');
  // it needs a configuration window on it's own ...
  writeln(fv, 'void vpc_rtc_init(void){  ');
  writeln(fv, '  RTC_InitTypeDef RTC_InitStructure;');
  writeln(fv, '  ');
  writeln(fv, '  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;');
  writeln(fv, '  RTC_InitStructure.RTC_SynchPrediv = ('+inttostr(w_rtc)+' / 128) - 1;');
  writeln(fv, '  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;');
  writeln(fv, '  RTC_Init(&RTC_InitStructure);');
  writeln(fv, '  ');
  writeln(fv, '}  ');
  writeln(fv, '  ');
end;

(*
procedure vpc_tim6_systick_init(var fv:Text);
begin
  //
  writeln(fv, 'void vpc_tim6_systick_init(void){');
  writeln(fv, '  TIM_TimeBaseInitTypeDef TIM_InitStruct;');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_InitStruct.TIM_Prescaler = 3199;');
  writeln(fv, '  TIM_InitStruct.TIM_CounterMode = TIM_CounterMode_Up;');
  writeln(fv, '  TIM_InitStruct.TIM_Period = 9;');
  writeln(fv, '  TIM_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;');
  writeln(fv, '  TIM_TimeBaseInit(TIM6, &TIM_InitStruct);');
  writeln(fv, '  TIM_Cmd(TIM6, ENABLE);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update);');
  writeln(fv, '  TIM_SelectMasterSlaveMode(TIM6, TIM_MasterSlaveMode_Disable);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);');
  writeln(fv, '  TIM_SetCounter(TIM6, 0);');
  writeln(fv, '  TIM_GenerateEvent(TIM6, TIM_EventSource_Update);');
  //writeln(fv, '  NVIC_EnableIRQ(TIM6_IRQn);');
  writeln(fv, ' ');
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_tim7_systick_init(var fv:Text);
begin
  //
  writeln(fv, 'void vpc_tim7_systick_init(void){');
  writeln(fv, '  TIM_TimeBaseInitTypeDef TIM_InitStruct;');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_InitStruct.TIM_Prescaler = 3199;');
  writeln(fv, '  TIM_InitStruct.TIM_CounterMode = TIM_CounterMode_Up;');
  writeln(fv, '  TIM_InitStruct.TIM_Period = 9;');
  writeln(fv, '  TIM_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;');
  writeln(fv, '  TIM_TimeBaseInit(TIM7, &TIM_InitStruct);');
  writeln(fv, '  TIM_Cmd(TIM7, ENABLE);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_SelectOutputTrigger(TIM7, TIM_TRGOSource_Update);');
  writeln(fv, '  TIM_SelectMasterSlaveMode(TIM7, TIM_MasterSlaveMode_Disable);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);');
  writeln(fv, '  TIM_SetCounter(TIM7, 0);');
  writeln(fv, '  TIM_GenerateEvent(TIM7, TIM_EventSource_Update);');
  //writeln(fv, '  NVIC_EnableIRQ(TIM7_IRQn);');
  writeln(fv, ' ');
  writeln(fv, '}');
  writeln(fv, ' ');
end;
*)

procedure vpc_tim6_init(var fv:Text);
begin
  //
  writeln(fv, 'void vpc_tim6_init(void){');
  writeln(fv, '  TIM_TimeBaseInitTypeDef TIM_InitStruct;');
  if(v_tim6_global_int) then 
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);');
  writeln(fv, ' ');
  write(fv, '  TIM_InitStruct.TIM_Prescaler = ');
  writeln(fv, inttostr(v_tim6_prescaler)+';');
  writeln(fv, '  TIM_InitStruct.TIM_CounterMode = TIM_CounterMode_Up;');
  write(fv, '  TIM_InitStruct.TIM_Period = ');
  writeln(fv, inttostr(v_tim6_period)+';');
  writeln(fv, '  TIM_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;');
  writeln(fv, '  TIM_TimeBaseInit(TIM6, &TIM_InitStruct);');
  writeln(fv, '  TIM_Cmd(TIM6, ENABLE);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update);');
  writeln(fv, '  TIM_SelectMasterSlaveMode(TIM6, TIM_MasterSlaveMode_Disable);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);');
  writeln(fv, '  TIM_SetCounter(TIM6, 0);');
  writeln(fv, '  TIM_GenerateEvent(TIM6, TIM_EventSource_Update);');
  if(v_tim6_global_int) then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = TIM6_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_tim6_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_tim6_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  end;
  writeln(fv, ' ');
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_tim7_init(var fv:Text);
begin
  //
  writeln(fv, 'void vpc_tim7_init(void){');
  writeln(fv, '  TIM_TimeBaseInitTypeDef TIM_InitStruct;');
  if(v_tim7_global_int) then 
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);');
  writeln(fv, ' ');
  write(fv, '  TIM_InitStruct.TIM_Prescaler = ');
  writeln(fv, inttostr(v_tim7_prescaler)+';');
  writeln(fv, '  TIM_InitStruct.TIM_CounterMode = TIM_CounterMode_Up;');
  write(fv, '  TIM_InitStruct.TIM_Period = ');
  writeln(fv, inttostr(v_tim7_period)+';');
  writeln(fv, '  TIM_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;');
  writeln(fv, '  TIM_TimeBaseInit(TIM7, &TIM_InitStruct);');
  writeln(fv, '  TIM_Cmd(TIM7, ENABLE);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_SelectOutputTrigger(TIM7, TIM_TRGOSource_Update);');
  writeln(fv, '  TIM_SelectMasterSlaveMode(TIM7, TIM_MasterSlaveMode_Disable);');
  writeln(fv, ' ');
  writeln(fv, '  TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);');
  writeln(fv, '  TIM_SetCounter(TIM7, 0);');
  writeln(fv, '  TIM_GenerateEvent(TIM7, TIM_EventSource_Update);');
  if(v_tim7_global_int) then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = TIM7_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_tim7_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_tim7_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  end;
  writeln(fv, ' ');
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_usart1_init(var fv:Text);
begin
  //
  writeln(fv, 'void vpc_usart1_init(void){');
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_usart2_uart_init(var fv:Text);
begin
  {the USART2 is permanently connected on Nucleo L152RE board...}
  writeln(fv, 'void vpc_usart2_uart_init(void){');
  writeln(fv, '  GPIO_InitTypeDef GPIO_InitStruct;');
  writeln(fv, '  USART_InitTypeDef USART_InitStruct;');
  if(v_c_gb_int_usart2) then 
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, '  ');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);');
  writeln(fv, '  ');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa2_name+';');
  writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
  writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
  writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;');
  writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);');
  writeln(fv, '  ');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa3_name+';');
  //writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;');
  writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);');
  writeln(fv, '  ');
  write(fv, '  USART_InitStruct.USART_BaudRate = ');
  case v_cb_baud_idx_usart2 of
    0: writeln(fv, '1200;');
    1: writeln(fv, '2400;');
    2: writeln(fv, '4800;');
    3: writeln(fv, '9600;');
    4: writeln(fv, '19200;');
    5: writeln(fv, '38400;');
    6: writeln(fv, '115200;');
  end;
  write(fv, '  USART_InitStruct.USART_WordLength = ');
  case v_cb_wordlng_idx_usart2 of
    0: writeln(fv, 'USART_WordLength_8b;');
    1: writeln(fv, 'USART_WordLength_9b;');
  end;
  write(fv, '  USART_InitStruct.USART_StopBits = ');
  case v_cb_stop_idx_usart2 of
    0: writeln(fv, 'USART_StopBits_1;');
    1: writeln(fv, 'USART_StopBits_2;');
  end;
  write(fv, '  USART_InitStruct.USART_Parity = ');
  case v_cb_parity_idx_usart2 of
    0: writeln(fv, 'USART_Parity_No;');
    1: writeln(fv, 'USART_Parity_Even;');
    2: writeln(fv, 'USART_Parity_Odd;');
  end;
  write(fv, '  USART_InitStruct.USART_Mode = ');
  case v_cb_datadir_idx_usart2 of
    0: writeln(fv, 'USART_Mode_Rx | USART_Mode_Tx;');
    1: writeln(fv, 'USART_Mode_Rx;');
    2: writeln(fv, 'USART_Mode_Tx;');
  end;
  writeln(fv, '  USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;');
  writeln(fv, '  USART_Init(USART2, &USART_InitStruct);');
  writeln(fv, '  USART_Cmd(USART2, ENABLE);');
  writeln(fv, '  ');
  if v_c_gb_int_usart2 then begin
    writeln(fv, '  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = USART2_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_usart2_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_usart2_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');
  end;
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_i2c1_init(var fv:Text);
begin
  writeln(fv, 'void vpc_i2c1_init(void){');
  writeln(fv, '  GPIO_InitTypeDef GPIO_InitStruct;');
  writeln(fv, '  I2C_InitTypeDef I2C_InitStruct;');
  if((v_c_i2c1_event_int) or (v_c_i2c1_error_int)) then 
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, '  ');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);');
  writeln(fv, ' ');
  if v_c_i2c1_a then begin
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb8_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
    writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
    writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');
    writeln(fv, '  GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_I2C1);');
    writeln(fv, ' ');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb9_name+';');
    //writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
    //writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
    //writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;');
    //writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');
    writeln(fv, '  GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_I2C1);');
  end
  else begin
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb6_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
    writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
    writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');
    writeln(fv, '  GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1);');
    writeln(fv, ' ');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb7_name+';');
    //writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
    //writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
    //writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;');
    //writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');
    writeln(fv, '  GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_I2C1);');
  end;  
  writeln(fv, '  ');
  write(fv, '  I2C_InitStruct.I2C_ClockSpeed = ');
  case v_cb_i2c1_clockspeed_idx of
    0: writeln(fv, '100000;');
    1: writeln(fv, '400000;');
  else writeln(fv, '100000;');
  end;
  writeln(fv, '  I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;');
  write(fv, '  I2C_InitStruct.I2C_DutyCycle = ');
  case v_cb_i2c1_fm_dc_idx of
    1: writeln(fv, 'I2C_DutyCycle_2;');
    2: writeln(fv, 'I2C_DutyCycle_16_9;');
  else writeln(fv, 'I2C_DutyCycle_2;');
  end;
  write(fv, '  I2C_InitStruct.I2C_OwnAddress1 = ');
  writeln(fv, inttostr(v_se_i2c1_s_psa)+';');
  write(fv, '  I2C_InitStruct.I2C_Ack = '); //I2C_Ack_Enable
  writeln(fv, 'I2C_Ack_Enable;');
  write(fv, '  I2C_InitStruct.I2C_AcknowledgedAddress = ');
  case v_cb_i2c1_s_pal_idx of
    0: writeln(fv, 'I2C_AcknowledgedAddress_7bit;');
    1: writeln(fv, 'I2C_AcknowledgedAddress_10bit;');
  end;
  writeln(fv, '  I2C_Init(I2C1, &I2C_InitStruct);');
  writeln(fv, '  I2C_Cmd(I2C1, ENABLE);');
  writeln(fv, ' ');
  write(fv, '  I2C_DualAddressCmd(I2C1, ');
  case v_cb_i2c1_s_daa_idx of
    0: writeln(fv, 'DISABLE);');
    1: writeln(fv, 'ENABLE);');
  end;
  write(fv, '  I2C_GeneralCallCmd(I2C1, ');
  case v_cb_i2c1_s_gcad_idx of
    0: writeln(fv, 'DISABLE);');
    1: writeln(fv, 'ENABLE);');
  end;
  writeln(fv, '  I2C_OwnAddress2Config(I2C1, 0);'); // ToDo! The user interface is for HAL+LL...
  write(fv, '  I2C_StretchClockCmd(I2C1, ');
  case v_cb_i2c1_s_gcad_idx of
    0: writeln(fv, 'ENABLE);'); // confusing - but again, the user interface is for HAL+LL...
    1: writeln(fv, 'DISABLE);'); // the UI needs to be adapted for SPL.
  end;
  writeln(fv, ' ');
  if(v_c_i2c1_event_int) then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = I2C1_EV_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_i2c1_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_i2c1_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  if(v_c_i2c1_error_int) then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = I2C1_ER_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_i2c1_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_i2c1_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_i2c2_init(var fv:Text);
begin
  writeln(fv, 'void vpc_i2c2_init(void){');
  writeln(fv, '  GPIO_InitTypeDef GPIO_InitStruct;');
  writeln(fv, '  I2C_InitTypeDef I2C_InitStruct;');
  if((v_c_i2c2_event_int) or (v_c_i2c2_error_int)) then 
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, '  ');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);');
  writeln(fv, ' ');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb10_name+';');
  writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
  writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;');
  writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
  writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_I2C2);');
  writeln(fv, ' ');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb11_name+';');
  //writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
  writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_I2C2);');
  writeln(fv, '  ');
  write(fv, '  I2C_InitStruct.I2C_ClockSpeed = ');
  case v_cb_i2c2_clockspeed_idx of
    0: writeln(fv, '100000;');
    1: writeln(fv, '400000;');
  else writeln(fv, '100000;');
  end;
  writeln(fv, '  I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;');
  write(fv, '  I2C_InitStruct.I2C_DutyCycle = ');
  case v_cb_i2c2_fm_dc_idx of
    1: writeln(fv, 'I2C_DutyCycle_2;');
    2: writeln(fv, 'I2C_DutyCycle_16_9;');
  else writeln(fv, 'I2C_DutyCycle_2;');
  end;
  write(fv, '  I2C_InitStruct.I2C_OwnAddress1 = ');
  writeln(fv, inttostr(v_se_i2c2_s_psa)+';');
  write(fv, '  I2C_InitStruct.I2C_Ack = '); //I2C_Ack_Enable
  writeln(fv, 'I2C_Ack_Enable;');
  write(fv, '  I2C_InitStruct.I2C_AcknowledgedAddress = ');
  case v_cb_i2c2_s_pal_idx of
    0: writeln(fv, 'I2C_AcknowledgedAddress_7bit;');
    1: writeln(fv, 'I2C_AcknowledgedAddress_10bit;');
  end;
  writeln(fv, '  I2C_Init(I2C2, &I2C_InitStruct);');
  writeln(fv, '  I2C_Cmd(I2C2, ENABLE);');
  writeln(fv, ' ');
  write(fv, '  I2C_DualAddressCmd(I2C2, ');
  case v_cb_i2c2_s_daa_idx of
    0: writeln(fv, 'DISABLE);');
    1: writeln(fv, 'ENABLE);');
  end;
  write(fv, '  I2C_GeneralCallCmd(I2C2, ');
  case v_cb_i2c2_s_gcad_idx of
    0: writeln(fv, 'DISABLE);');
    1: writeln(fv, 'ENABLE);');
  end;
  writeln(fv, '  I2C_OwnAddress2Config(I2C2, 0);'); // ToDo! The user interface is for HAL+LL...
  write(fv, '  I2C_StretchClockCmd(I2C2, ');
  case v_cb_i2c2_s_gcad_idx of
    0: writeln(fv, 'ENABLE);'); // confusing - but again, the user interface is for HAL+LL...
    1: writeln(fv, 'DISABLE);'); // the UI needs to be adapted for SPL.
  end;
  writeln(fv, ' ');
  if(v_c_i2c2_event_int) then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = I2C2_EV_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_i2c2_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_i2c2_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  if(v_c_i2c2_error_int) then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = I2C2_ER_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_i2c2_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_i2c2_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_adc1_init(var fv:Text);
begin
  writeln(fv, 'void vpc_adc1_init(void){');
  writeln(fv, '  ADC_InitTypeDef ADC1_InitStruct;');
  writeln(fv, '  ADC_CommonInitTypeDef ADC1_CommonStruct;');
  writeln(fv, '  GPIO_InitTypeDef GPIO_InitStruct;');
  if(v_c_gb_int_adc1) then 
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, ' ');
  if v_rg_vco_idx <> 1 then begin
    {If HSE or BYPASS are selected, then we have to start the HSI for ADC periph.}
    writeln(fv, '  RCC_HSICmd(ENABLE);');
    writeln(fv, '  while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET);');
  end;  
  writeln(fv, '  RCC_AHBPeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);');
  writeln(fv, ' ');
  {Here, enumerate the GPIO pins set as Analog Input Channels}
  {PC2}
  if v_pc2_func  = 'ADIN' then begin
    writeln(fv, '  /* PC2 as ADC_IN12 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc2_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PC3}
  if v_pc3_func  = 'ADIN' then begin
    writeln(fv, '  /* PC3 as ADC_IN13 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc3_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {-----------------------------------------}
  {PA0}
  if v_pa0_func  = 'ADIN' then begin
    writeln(fv, '  /* PA0 as ADC_IN0 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa0_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PA1}
  if v_pa1_func  = 'ADIN' then begin
    writeln(fv, '  /* PA1 as ADC_IN1 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa1_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PA4}
  if v_pa4_func  = 'ADIN' then begin
    writeln(fv, '  /* PA4 as ADC_IN4 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa4_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PB0}
  if v_pb0_func  = 'ADIN' then begin
    writeln(fv, '  /* PB0 as ADC_IN8 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb0_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PC1}
  if v_pc1_func  = 'ADIN' then begin
    writeln(fv, '  /* PC1 as ADC_IN11 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc1_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PC0}
  if v_pc0_func  = 'ADIN' then begin
    writeln(fv, '  /* PC0 as ADC_IN10 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc0_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {-----------------------------------------}
  {PA6}
  if v_pa6_func  = 'ADIN' then begin
    writeln(fv, '  /* PA6 as ADC_IN6 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa6_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PA7}
  if v_pa7_func  = 'ADIN' then begin
    writeln(fv, '  /* PA7 as ADC_IN7 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa7_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {-----------------------------------------}
  {PC5}
  if v_pc5_func  = 'ADIN' then begin
    writeln(fv, '  /* PC5 as ADC_IN15 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc5_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PB12}
  if v_pb12_func  = 'ADIN' then begin
    writeln(fv, '  /* PB12 as ADC_IN18 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb12_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PB2}
  if v_pb2_func  = 'ADIN' then begin
    writeln(fv, '  /* PB2 as ADC_IN0b */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb2_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PB1}
  if v_pb1_func  = 'ADIN' then begin
    writeln(fv, '  /* PB1 as ADC_IN9 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb1_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PB15}
  if v_pb15_func  = 'ADIN' then begin
    writeln(fv, '  /* PB15 as ADC_IN21 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb15_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PB14}
  if v_pb14_func  = 'ADIN' then begin
    writeln(fv, '  /* PB14 as ADC_IN20 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb14_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PB13}
  if v_pb13_func  = 'ADIN' then begin
    writeln(fv, '  /* PB13 as ADC_IN19 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb13_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');  
    writeln(fv, ' ');
  end;
  {PC4}
  if v_pc4_func  = 'ADIN' then begin
    writeln(fv, '  /* PC4 as ADC_IN14 */');
    writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc4_name+';');
    writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;');
    writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;');
    writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');      
    writeln(fv, ' ');
  end;
  //--//--//
  //
  write(fv, '  ADC1_CommonStruct.ADC_Prescaler = ');
  case v_adc1_prescaler_idx of
    0: writeln(fv, 'ADC_Prescaler_Div1;');
    1: writeln(fv, 'ADC_Prescaler_Div2;');
    2: writeln(fv, 'ADC_Prescaler_Div4;');
  end; 
  //
  writeln(fv, '  ADC_CommonInit(&ADC1_CommonStruct);');
  //
  writeln(fv, ' ');
  write(fv, '  ADC1_InitStruct.ADC_Resolution = ');
  case v_adc1_res_idx of
    0: writeln(fv, 'ADC_Resolution_12b;');
    1: writeln(fv, 'ADC_Resolution_10b;');
    2: writeln(fv, 'ADC_Resolution_8b;');
    3: writeln(fv, 'ADC_Resolution_6b;');
  end;
  //
  write(fv, '  ADC1_InitStruct.ADC_ScanConvMode = ');
  case v_adc1_scan_idx of 
    0: writeln(fv, 'ENABLE;');
    1: writeln(fv, 'DISABLE;');
  end;
  //
  write(fv, '  ADC1_InitStruct.ADC_ContinuousConvMode = ');
  case v_adc1_continuous_idx of 
    0: writeln(fv, 'ENABLE;');
    1: writeln(fv, 'DISABLE;');
  end;
  //
  write(fv, '  ADC1_InitStruct.ADC_ExternalTrigConvEdge = ');
  case v_adc1_tce_idx of 
    0: writeln(fv, 'ADC_ExternalTrigConvEdge_None;');
    1: writeln(fv, 'ADC_ExternalTrigConvEdge_Rising;');
    2: writeln(fv, 'ADC_ExternalTrigConvEdge_Falling;');
    3: writeln(fv, 'ADC_ExternalTrigConvEdge_RisingFalling;');
  end;
  //
  write(fv, '  ADC1_InitStruct.ADC_ExternalTrigConv = ');
  case v_adc1_tc_idx of 
    0: writeln(fv, 'ADC_ExternalTrigConv_T2_CC3;');
    1: writeln(fv, 'ADC_ExternalTrigConv_T2_CC2;');
    2: writeln(fv, 'ADC_ExternalTrigConv_T2_TRGO;');
    3: writeln(fv, 'ADC_ExternalTrigConv_T3_CC1;');
    4: writeln(fv, 'ADC_ExternalTrigConv_T3_CC3;');
    5: writeln(fv, 'ADC_ExternalTrigConv_T3_TRGO;');
    6: writeln(fv, 'ADC_ExternalTrigConv_T4_CC4;');
    7: writeln(fv, 'ADC_ExternalTrigConv_T4_TRGO;');
    8: writeln(fv, 'ADC_ExternalTrigConv_T6_TRGO;');
    9: writeln(fv, 'ADC_ExternalTrigConv_T9_CC2;');
    10: writeln(fv, 'ADC_ExternalTrigConv_T9_TRGO;');
    11: writeln(fv, 'ADC_ExternalTrigConv_Ext_IT11;');
  end;
  //
  write(fv, '  ADC1_InitStruct.ADC_DataAlign = ');
  case v_adc1_align_idx of 
    0: writeln(fv, 'ADC_DataAlign_Right;');
    1: writeln(fv, 'ADC_DataAlign_Left;');
  end;
  //
  writeln(fv, '  ADC1_InitStruct.ADC_NbrOfConversion = '+inttostr(v_adc1_nrconv_idx)+';' );
  //
  writeln(fv, '  ADC_Init(ADC1, &ADC1_InitStruct);');
  writeln(fv, '  ADC_Cmd(ADC1, ENABLE);');
  writeln(fv, ' ');
  //
  if(v_adc1_tce_idx = 0) then begin
    writeln(fv, '  ADC_EOCOnEachRegularChannelCmd(ADC1, ENABLE);');
  end;  
  //
  writeln(fv, ' ');
  if(v_c_gb_int_adc1) then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = ADC1_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_adc1_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_adc1_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_spi1_init(var fv:Text);
begin
  writeln(fv, 'void vpc_spi1_init(void){');
  writeln(fv, '  GPIO_InitTypeDef  GPIO_InitStruct;');
  writeln(fv, '  SPI_InitTypeDef   SPI_InitStruct;');  
  if(v_c_gb_int_spi1) then 
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, ' ');
  writeln(fv, '  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);');
  writeln(fv, '  /* Configure PA5 as SPI SCK pin */');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa5_name+';');
  writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  write(fv, '  GPIO_InitStruct.GPIO_Speed = ');
  case v_pa5_speed_idx of
    0:writeln(fv, 'GPIO_Speed_400KHz;'); 
    1:writeln(fv, 'GPIO_Speed_400KHz;');
    2:writeln(fv, 'GPIO_Speed_2MHz;');
    3:writeln(fv, 'GPIO_Speed_10MHz;');
    4:writeln(fv, 'GPIO_Speed_40MHz;');
  end;  
  writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
  writeln(fv, '  GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;');
  writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  /* Configure PA6 as SPI MISO pin */');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa6_name+';');
  writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  /* Configure PA7 as SPI MOSI pin */');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pa7_name+';');
  writeln(fv, '  GPIO_Init(GPIOA, &GPIO_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  /* Connect SCK, MISO and MOSI pins to SPI alternate */');
  writeln(fv, '  GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1); ');
  writeln(fv, '  ');
  writeln(fv, '  /* Configure SPI peripheral */');
  write(fv, '  SPI_InitStruct.SPI_Direction = ');
  case v_cb_direction_idx_spi1 of 
    0:writeln(fv, 'SPI_Direction_2Lines_FullDuplex;');
    1:writeln(fv, 'SPI_Direction_2Lines_RxOnly;');
    2:writeln(fv, 'SPI_Direction_1Line_Rx;');
    3:writeln(fv, 'SPI_Direction_1Line_Tx;');
  end; 
  write(fv, '  SPI_InitStruct.SPI_Mode = ');
  case v_cb_mode_idx_spi1 of
    0:writeln(fv, 'SPI_Mode_Master;');
    1:writeln(fv, 'SPI_Mode_Slave;');
  end;
  write(fv, '  SPI_InitStruct.SPI_DataSize = ');
  case v_cb_datasize_idx_spi1 of
    0:writeln(fv, 'SPI_DataSize_8b;');
    1:writeln(fv, 'SPI_DataSize_16b;');
  end;
  write(fv, '  SPI_InitStruct.SPI_CPOL = ');
  case v_cb_cpol_idx_spi1 of
    0:writeln(fv, 'SPI_CPOL_Low;');
    1:writeln(fv, 'SPI_CPOL_High;');
  end;
  write(fv, '  SPI_InitStruct.SPI_CPHA = ');
  case v_cb_cpha_idx_spi1 of
    0:writeln(fv, 'SPI_CPHA_1Edge;');
    1:writeln(fv, 'SPI_CPHA_2Edge;');
  end;
  writeln(fv, '  SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;');
  write(fv, '  SPI_InitStruct.SPI_BaudRatePrescaler = ');
  case v_cb_prescaler_idx_spi1 of
    0:writeln(fv, 'SPI_BaudRatePrescaler_2;');
    1:writeln(fv, 'SPI_BaudRatePrescaler_4;');
    2:writeln(fv, 'SPI_BaudRatePrescaler_8;');
    3:writeln(fv, 'SPI_BaudRatePrescaler_16;');
    4:writeln(fv, 'SPI_BaudRatePrescaler_32;');
    5:writeln(fv, 'SPI_BaudRatePrescaler_64;');
    6:writeln(fv, 'SPI_BaudRatePrescaler_128;');
    7:writeln(fv, 'SPI_BaudRatePrescaler_256;');
  end;
  write(fv, '  SPI_InitStruct.SPI_FirstBit = ');
  case v_cb_firstbit_idx_spi1 of 
    0:writeln(fv, 'SPI_FirstBit_MSB;');
    1:writeln(fv, 'SPI_FirstBit_LSB;');
  end;
  writeln(fv, '  SPI_InitStruct.SPI_CRCPolynomial = '+inttostr(v_cb_crcpoly_idx_spi1)+';');
  writeln(fv, '  SPI_Init(SPI1, &SPI_InitStruct);');
  writeln(fv, '    ');
  // NSS must be set to '1' due to NSS_Soft settings (otherwise it will be Multimaster mode).
  // Thank you, Olexandr Davydenko!
  writeln(fv, '  SPI_NSSInternalSoftwareConfig(SPI1, SPI_NSSInternalSoft_Set);');
  writeln(fv, '  /* Enable SPI */');
  writeln(fv, '  SPI_Cmd(SPI1, ENABLE);');
  writeln(fv, '    ');
    
  if v_c_gb_int_spi1 then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = SPI1_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_spi1_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_spi1_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_spi2_init(var fv:Text);
begin
  writeln(fv, 'void vpc_spi2_init(void){');
  writeln(fv, '  GPIO_InitTypeDef  GPIO_InitStruct;');
  writeln(fv, '  SPI_InitTypeDef   SPI_InitStruct;');  
  if(v_c_gb_int_spi2) then 
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, ' ');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);');
  writeln(fv, '  /* Configure PB13 as SPI SCK pin */');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb13_name+';');
  writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  write(fv, '  GPIO_InitStruct.GPIO_Speed = ');
  case v_pb13_speed_idx of 
    0:writeln(fv, 'GPIO_Speed_400KHz;'); 
    1:writeln(fv, 'GPIO_Speed_400KHz;');
    2:writeln(fv, 'GPIO_Speed_2MHz;');
    3:writeln(fv, 'GPIO_Speed_10MHz;');
    4:writeln(fv, 'GPIO_Speed_40MHz;');
  end;  
  writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
  writeln(fv, '  GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;');
  writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  /* Configure PB14 as SPI MISO pin */');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb14_name+';');
  writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  /* Configure PB15 as SPI MOSI pin */');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pb15_name+';');
  writeln(fv, '  GPIO_Init(GPIOB, &GPIO_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  /* Connect SCK, MISO and MOSI pins to SPI alternate */');
  writeln(fv, '  GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_SPI2);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2); ');
  writeln(fv, '  ');
  writeln(fv, '  /* Configure SPI peripheral */');
  write(fv, '  SPI_InitStruct.SPI_Direction = ');
  case v_cb_direction_idx_spi2 of 
    0:writeln(fv, 'SPI_Direction_2Lines_FullDuplex;');
    1:writeln(fv, 'SPI_Direction_2Lines_RxOnly;');
    2:writeln(fv, 'SPI_Direction_1Line_Rx;');
    3:writeln(fv, 'SPI_Direction_1Line_Tx;');
  end; 
  write(fv, '  SPI_InitStruct.SPI_Mode = ');
  case v_cb_mode_idx_spi2 of
    0:writeln(fv, 'SPI_Mode_Master;');
    1:writeln(fv, 'SPI_Mode_Slave;');
  end;
  write(fv, '  SPI_InitStruct.SPI_DataSize = ');
  case v_cb_datasize_idx_spi2 of
    0:writeln(fv, 'SPI_DataSize_8b;');
    1:writeln(fv, 'SPI_DataSize_16b;');
  end;
  write(fv, '  SPI_InitStruct.SPI_CPOL = ');
  case v_cb_cpol_idx_spi2 of
    0:writeln(fv, 'SPI_CPOL_Low;');
    1:writeln(fv, 'SPI_CPOL_High;');
  end;
  write(fv, '  SPI_InitStruct.SPI_CPHA = ');
  case v_cb_cpha_idx_spi2 of
    0:writeln(fv, 'SPI_CPHA_1Edge;');
    1:writeln(fv, 'SPI_CPHA_2Edge;');
  end;
  writeln(fv, '  SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;');
  write(fv, '  SPI_InitStruct.SPI_BaudRatePrescaler = ');
  case v_cb_prescaler_idx_spi2 of
    0:writeln(fv, 'SPI_BaudRatePrescaler_2;');
    1:writeln(fv, 'SPI_BaudRatePrescaler_4;');
    2:writeln(fv, 'SPI_BaudRatePrescaler_8;');
    3:writeln(fv, 'SPI_BaudRatePrescaler_16;');
    4:writeln(fv, 'SPI_BaudRatePrescaler_32;');
    5:writeln(fv, 'SPI_BaudRatePrescaler_64;');
    6:writeln(fv, 'SPI_BaudRatePrescaler_128;');
    7:writeln(fv, 'SPI_BaudRatePrescaler_256;');
  end;
  write(fv, '  SPI_InitStruct.SPI_FirstBit = ');
  case v_cb_firstbit_idx_spi2 of 
    0:writeln(fv, 'SPI_FirstBit_MSB;');
    1:writeln(fv, 'SPI_FirstBit_LSB;');
  end;
  writeln(fv, '  SPI_InitStruct.SPI_CRCPolynomial = '+inttostr(v_cb_crcpoly_idx_spi2)+';');
  writeln(fv, '  SPI_Init(SPI2, &SPI_InitStruct);');
  writeln(fv, '    ');
  writeln(fv, '  SPI_NSSInternalSoftwareConfig(SPI2, SPI_NSSInternalSoft_Set);');
  writeln(fv, '  /* Enable SPI */');
  writeln(fv, '  SPI_Cmd(SPI2, ENABLE);');
  writeln(fv, '    ');

  if v_c_gb_int_spi2 then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = SPI2_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_spi2_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_spi2_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_spi3_init(var fv:Text);
begin
  writeln(fv, 'void vpc_spi3_init(void){');
  writeln(fv, '  GPIO_InitTypeDef  GPIO_InitStruct;');
  writeln(fv, '  SPI_InitTypeDef   SPI_InitStruct;');  
  if(v_c_gb_int_spi3) then 
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');
  writeln(fv, ' ');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);');
  writeln(fv, '  /* Configure PC10 as SPI SCK pin */');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc10_name+';');
  writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  write(fv, '  GPIO_InitStruct.GPIO_Speed = ');
  case v_pc10_speed_idx of 
    0:writeln(fv, 'GPIO_Speed_400KHz;'); 
    1:writeln(fv, 'GPIO_Speed_400KHz;');
    2:writeln(fv, 'GPIO_Speed_2MHz;');
    3:writeln(fv, 'GPIO_Speed_10MHz;');
    4:writeln(fv, 'GPIO_Speed_40MHz;');
  end;  
  writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
  writeln(fv, '  GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;');
  writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  /* Configure PC11 as SPI MISO pin */');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc11_name+';');
  writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  /* Configure PC12 as SPI MOSI pin */');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc12_name+';');
  writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');
  writeln(fv, '  ');
  writeln(fv, '  /* Connect SCK, MISO and MOSI pins to SPI alternate */');
  writeln(fv, '  GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_SPI3);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_SPI3);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_SPI3); ');
  writeln(fv, '  ');
  writeln(fv, '  /* Configure SPI peripheral */');
  write(fv, '  SPI_InitStruct.SPI_Direction = ');
  case v_cb_direction_idx_spi3 of 
    0:writeln(fv, 'SPI_Direction_2Lines_FullDuplex;');
    1:writeln(fv, 'SPI_Direction_2Lines_RxOnly;');
    2:writeln(fv, 'SPI_Direction_1Line_Rx;');
    3:writeln(fv, 'SPI_Direction_1Line_Tx;');
  end; 
  write(fv, '  SPI_InitStruct.SPI_Mode = ');
  case v_cb_mode_idx_spi3 of
    0:writeln(fv, 'SPI_Mode_Master;');
    1:writeln(fv, 'SPI_Mode_Slave;');
  end;
  write(fv, '  SPI_InitStruct.SPI_DataSize = ');
  case v_cb_datasize_idx_spi3 of
    0:writeln(fv, 'SPI_DataSize_8b;');
    1:writeln(fv, 'SPI_DataSize_16b;');
  end;
  write(fv, '  SPI_InitStruct.SPI_CPOL = ');
  case v_cb_cpol_idx_spi3 of
    0:writeln(fv, 'SPI_CPOL_Low;');
    1:writeln(fv, 'SPI_CPOL_High;');
  end;
  write(fv, '  SPI_InitStruct.SPI_CPHA = ');
  case v_cb_cpha_idx_spi3 of
    0:writeln(fv, 'SPI_CPHA_1Edge;');
    1:writeln(fv, 'SPI_CPHA_2Edge;');
  end;
  writeln(fv, '  SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;');
  write(fv, '  SPI_InitStruct.SPI_BaudRatePrescaler = ');
  case v_cb_prescaler_idx_spi3 of
    0:writeln(fv, 'SPI_BaudRatePrescaler_2;');
    1:writeln(fv, 'SPI_BaudRatePrescaler_4;');
    2:writeln(fv, 'SPI_BaudRatePrescaler_8;');
    3:writeln(fv, 'SPI_BaudRatePrescaler_16;');
    4:writeln(fv, 'SPI_BaudRatePrescaler_32;');
    5:writeln(fv, 'SPI_BaudRatePrescaler_64;');
    6:writeln(fv, 'SPI_BaudRatePrescaler_128;');
    7:writeln(fv, 'SPI_BaudRatePrescaler_256;');
  end;
  write(fv, '  SPI_InitStruct.SPI_FirstBit = ');
  case v_cb_firstbit_idx_spi3 of 
    0:writeln(fv, 'SPI_FirstBit_MSB;');
    1:writeln(fv, 'SPI_FirstBit_LSB;');
  end;
  writeln(fv, '  SPI_InitStruct.SPI_CRCPolynomial = '+inttostr(v_cb_crcpoly_idx_spi3)+';');
  writeln(fv, '  SPI_Init(SPI3, &SPI_InitStruct);');
  writeln(fv, '    ');
  writeln(fv, '  SPI_NSSInternalSoftwareConfig(SPI3, SPI_NSSInternalSoft_Set);');
  writeln(fv, '  /* Enable SPI */');
  writeln(fv, '  SPI_Cmd(SPI3, ENABLE);');
  writeln(fv, '    ');

  if v_c_gb_int_spi3 then begin
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = SPI3_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_spi3_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_spi3_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_uart4_init(var fv:Text);
begin
  writeln(fv, 'void vpc_uart4_init(void){');
  if v_c_gb_int_uart4 then
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');  
  writeln(fv, '  GPIO_InitTypeDef GPIO_InitStruct;');
  writeln(fv, '  USART_InitTypeDef USART_InitStruct;');
  writeln(fv, '  ');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);');
  writeln(fv, '  ');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc10_name+';');
  writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
  writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
  writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;');
  writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_UART4);');
  writeln(fv, '  ');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc11_name+';');
  //writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;');
  writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_UART4);');
  writeln(fv, '  ');
  write(fv, '  USART_InitStruct.USART_BaudRate = ');
  case v_cb_baud_idx_uart4 of
    0: writeln(fv, '1200;');
    1: writeln(fv, '2400;');
    2: writeln(fv, '4800;');
    3: writeln(fv, '9600;');
    4: writeln(fv, '19200;');
    5: writeln(fv, '38400;');
    6: writeln(fv, '115200;');
  end;
  write(fv, '  USART_InitStruct.USART_WordLength = ');
  case v_cb_wordlng_idx_uart4 of
    0: writeln(fv, 'USART_WordLength_8b;');
    1: writeln(fv, 'USART_WordLength_9b;');
  end;
  write(fv, '  USART_InitStruct.USART_StopBits = ');
  case v_cb_stop_idx_uart4 of
    0: writeln(fv, 'USART_StopBits_1;');
    1: writeln(fv, 'USART_StopBits_2;');
  end;
  write(fv, '  USART_InitStruct.USART_Parity = ');
  case v_cb_parity_idx_uart4 of
    0: writeln(fv, 'USART_Parity_No;');
    1: writeln(fv, 'USART_Parity_Even;');
    2: writeln(fv, 'USART_Parity_Odd;');
  end;
  write(fv, '  USART_InitStruct.USART_Mode = ');
  case v_cb_datadir_idx_uart4 of
    0: writeln(fv, 'USART_Mode_Rx | USART_Mode_Tx;');
    1: writeln(fv, 'USART_Mode_Rx;');
    2: writeln(fv, 'USART_Mode_Tx;');
  end;
  writeln(fv, '  USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;');
  writeln(fv, '  USART_Init(UART4, &USART_InitStruct);');
  writeln(fv, '  USART_Cmd(UART4, ENABLE);');
  writeln(fv, '  ');
  if v_c_gb_int_uart4 then begin
    writeln(fv, '  USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = UART4_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_uart4_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_uart4_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure vpc_uart5_init(var fv:Text);
begin
  writeln(fv, 'void vpc_uart5_init(void){');
  if v_c_gb_int_uart5 then
    writeln(fv, '  NVIC_InitTypeDef NVIC_InitStruct;');  
  writeln(fv, '  GPIO_InitTypeDef GPIO_InitStruct;');
  writeln(fv, '  USART_InitTypeDef USART_InitStruct;');
  writeln(fv, '  ');
  writeln(fv, '  RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);');
  writeln(fv, '  ');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pd2_name+';');
  writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
  writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
  writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;');
  writeln(fv, '  GPIO_Init(GPIOD, &GPIO_InitStruct);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOD, GPIO_PinSource2, GPIO_AF_UART5);');
  writeln(fv, '  ');
  writeln(fv, '  GPIO_InitStruct.GPIO_Pin = '+v_pc12_name+';');
  //writeln(fv, '  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;');
  //writeln(fv, '  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;');
  writeln(fv, '  GPIO_Init(GPIOC, &GPIO_InitStruct);');
  writeln(fv, '  GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_UART5);');
  writeln(fv, '  ');
  write(fv, '  USART_InitStruct.USART_BaudRate = ');
  case v_cb_baud_idx_uart5 of
    0: writeln(fv, '1200;');
    1: writeln(fv, '2400;');
    2: writeln(fv, '4800;');
    3: writeln(fv, '9600;');
    4: writeln(fv, '19200;');
    5: writeln(fv, '38400;');
    6: writeln(fv, '115200;');
  end;
  write(fv, '  USART_InitStruct.USART_WordLength = ');
  case v_cb_wordlng_idx_uart5 of
    0: writeln(fv, 'USART_WordLength_8b;');
    1: writeln(fv, 'USART_WordLength_9b;');
  end;
  write(fv, '  USART_InitStruct.USART_StopBits = ');
  case v_cb_stop_idx_uart5 of
    0: writeln(fv, 'USART_StopBits_1;');
    1: writeln(fv, 'USART_StopBits_2;');
  end;
  write(fv, '  USART_InitStruct.USART_Parity = ');
  case v_cb_parity_idx_uart5 of
    0: writeln(fv, 'USART_Parity_No;');
    1: writeln(fv, 'USART_Parity_Even;');
    2: writeln(fv, 'USART_Parity_Odd;');
  end;
  write(fv, '  USART_InitStruct.USART_Mode = ');
  case v_cb_datadir_idx_uart5 of
    0: writeln(fv, 'USART_Mode_Rx | USART_Mode_Tx;');
    1: writeln(fv, 'USART_Mode_Rx;');
    2: writeln(fv, 'USART_Mode_Tx;');
  end;
  writeln(fv, '  USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;');
  writeln(fv, '  USART_Init(UART5, &USART_InitStruct);');
  writeln(fv, '  USART_Cmd(UART5, ENABLE);');
  writeln(fv, '  ');
  if v_c_gb_int_uart5 then begin
    writeln(fv, '  USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannel = UART5_IRQn;');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = '+inttostr(v_uart5_int_priority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelSubPriority = '+inttostr(v_uart5_int_subpriority)+';');
    writeln(fv, '  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;');
    writeln(fv, '  NVIC_Init(&NVIC_InitStruct);');    
  end;
  writeln(fv, '}');
  writeln(fv, ' ');
end;

procedure write_global_interrupts(var fv:Text);
begin
  {write all the golbal interrupts, one after another}

  writeln(fv, '');
  writeln(fv, 'void NMI_Handler(void)');
  writeln(fv, '{');
  writeln(fv, '}');
  writeln(fv, '');
  writeln(fv, 'void HardFault_Handler(void){');
  writeln(fv, '  /* Go to infinite loop when Hard Fault exception occurs */');
  writeln(fv, '  while (1)');
  writeln(fv, '  {');
  writeln(fv, '  }');
  writeln(fv, '}');
  writeln(fv, '');
  writeln(fv, 'void MemManage_Handler(void){');
  writeln(fv, '  /* Go to infinite loop when Memory Manage exception occurs */');
  writeln(fv, '  while (1)');
  writeln(fv, '  {');
  writeln(fv, '  }');
  writeln(fv, '}');
  writeln(fv, '');
  writeln(fv, 'void BusFault_Handler(void){');
  writeln(fv, '  /* Go to infinite loop when Bus Fault exception occurs */');
  writeln(fv, '  while (1)');
  writeln(fv, '  {');
  writeln(fv, '  }');
  writeln(fv, '}');
  writeln(fv, ' ');
  writeln(fv, 'void UsageFault_Handler(void){');
  writeln(fv, '  /* Go to infinite loop when Usage Fault exception occurs */');
  writeln(fv, '  while (1)');
  writeln(fv, '  {');
  writeln(fv, '  }');
  writeln(fv, '}');
  writeln(fv, '');
  writeln(fv, 'void SVC_Handler(void)');
  writeln(fv, '{');
  writeln(fv, '}');
  writeln(fv, '');
  writeln(fv, 'void DebugMon_Handler(void)');
  writeln(fv, '{');
  writeln(fv, '}');
  writeln(fv, '');
  writeln(fv, 'void PendSV_Handler(void)');
  writeln(fv, '{');
  writeln(fv, '}');
  writeln(fv, '');

  //if v_tick_manager = 0 then begin
  writeln(fv, 'void SysTick_Handler(void)');
  writeln(fv, '{');
  writeln(fv, '  if (my_ticks > 0) my_ticks--;');
  writeln(fv, '}');
  writeln(fv, '');
  //end;
  if ((v_tim6_global_int) and (v_tim6_check)) then begin
    writeln(fv, 'void TIM6_IRQHandler(void)');
    writeln(fv, '{');
    writeln(fv, '  if (TIM_GetFlagStatus(TIM6, TIM_FLAG_Update) == 1){');
    //if v_tick_manager = 1 then begin
      //writeln(fv, '    my_ticks++;');
    //end;
    writeln(fv, '    TIM_ClearFlag(TIM6, TIM_FLAG_Update);');
    writeln(fv, '  }');
    writeln(fv, '}');
    writeln(fv, '');
  end;
  if ((v_tim7_global_int) and (v_tim7_check)) then begin
    writeln(fv, 'void TIM7_IRQHandler(void)');
    writeln(fv, '{');
    writeln(fv, '  if (TIM_GetFlagStatus(TIM7, TIM_FLAG_Update) == 1){');
    //if v_tick_manager = 2 then begin
      //writeln(fv, '    my_ticks++;');
    //end;
    writeln(fv, '    TIM_ClearFlag(TIM7, TIM_FLAG_Update);');
    writeln(fv, '  }');
    writeln(fv, '}');
    writeln(fv, '');
  end;

  {USART2}
  if ((v_c_gb_int_usart2) and (v_c_usart2)) then begin
    writeln(fv, 'void USART2_IRQHandler(void){');
	  writeln(fv, '  /* RXNE handler */');
	  writeln(fv, '  if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET){');
		writeln(fv, '    /* If received ''t'', do something and transmit ''T'' back */');
    writeln(fv, '    if((char)USART_ReceiveData(USART2) == ''t''){');
	  writeln(fv, '      /* do something here */');
		writeln(fv, '      USART_SendData(USART2, ''T'');');
		writeln(fv, '      /* Wait until Tx data register is empty, not really ');
		writeln(fv, '       * required for this example but put in here anyway.');
		writeln(fv, '       */');
		writeln(fv, '      /*');
		writeln(fv, '      while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET)');
		writeln(fv, '      {');
		writeln(fv, '      }*/');
		writeln(fv, '    }');
	  writeln(fv, '  }');
	  writeln(fv, '  /* Other USART signals here if needed */');
	  writeln(fv, '}');
    writeln(fv, ' ');
  end;

  {UART4}
  if ((v_c_gb_int_uart4) and (v_c_uart4)) then begin
    writeln(fv, 'void UART4_IRQHandler(void){');
    writeln(fv, '  /* RXNE handler */');
    writeln(fv, '  if(USART_GetITStatus(UART4, USART_IT_RXNE) != RESET){');
    writeln(fv, '    /* If received ''t'', do something and transmit ''T'' back */');
    writeln(fv, '    if((char)USART_ReceiveData(UART4) == ''t''){');
    writeln(fv, '      /* do something here */');
    writeln(fv, '      USART_SendData(UART4, ''T'');');
    writeln(fv, '      /* Wait until Tx data register is empty, not really ');
    writeln(fv, '       * required for this example but put in here anyway.');
    writeln(fv, '       */');
    writeln(fv, '      /*');
    writeln(fv, '      while(USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET)');
    writeln(fv, '      {');
    writeln(fv, '      }*/');
    writeln(fv, '    }');
    writeln(fv, '  }');
    writeln(fv, '  /* Other USART signals here if needed */');
    writeln(fv, '}');
    writeln(fv, ' ');
  end;

  {UART5}
  if ((v_c_gb_int_uart5) and (v_c_uart5)) then begin
    writeln(fv, 'void UART5_IRQHandler(void){');
    writeln(fv, '  /* RXNE handler */');
    writeln(fv, '  if(USART_GetITStatus(UART5, USART_IT_RXNE) != RESET){');
    writeln(fv, '    /* If received ''t'', do something and transmit ''T'' back */');
    writeln(fv, '    if((char)USART_ReceiveData(UART5) == ''t''){');
    writeln(fv, '      /* do something here */');
    writeln(fv, '      USART_SendData(UART5, ''T'');');
    writeln(fv, '      /* Wait until Tx data register is empty, not really ');
    writeln(fv, '       * required for this example but put in here anyway.');
    writeln(fv, '       */');
    writeln(fv, '      /*');
    writeln(fv, '      while(USART_GetFlagStatus(UART5, USART_FLAG_TXE) == RESET)');
    writeln(fv, '      {');
    writeln(fv, '      }*/');
    writeln(fv, '    }');
    writeln(fv, '  }');
    writeln(fv, '  /* Other USART signals here if needed */');
    writeln(fv, '}');
    writeln(fv, ' ');
  end;

  if((v_c_i2c1) and (v_c_i2c1_event_int)) then begin
    writeln(fv, 'void I2C1_EV_IRQHandler(void){');
    writeln(fv, '}');
    writeln(fv, ' ');    
  end;

  if((v_c_i2c1) and (v_c_i2c1_error_int)) then begin
    writeln(fv, 'void I2C1_ER_IRQHandler(void){');
    writeln(fv, '}');
    writeln(fv, ' ');    
  end;


  if((v_c_i2c2) and (v_c_i2c2_event_int)) then begin
    writeln(fv, 'void I2C2_EV_IRQHandler(void){');
    writeln(fv, '}');
    writeln(fv, ' ');    
  end;

  if((v_c_i2c2) and (v_c_i2c2_error_int)) then begin
    writeln(fv, 'void I2C2_ER_IRQHandler(void){');
    writeln(fv, '}');
    writeln(fv, ' ');    
  end;

  if(v_c_gb_int_adc1) then begin
    writeln(fv, 'void ADC1_IRQHandler(void){');
    writeln(fv, '}');
    writeln(fv, ' ');    
  end;

  if((v_c_spi1) and (v_c_gb_int_spi1)) then begin
    writeln(fv, 'void SPI1_IRQHandler(void){');
    writeln(fv, '}');
    writeln(fv, ' ');    
  end;

  if((v_c_spi2) and (v_c_gb_int_spi2)) then begin
    writeln(fv, 'void SPI2_IRQHandler(void){');
    writeln(fv, '}');
    writeln(fv, ' ');    
  end;

  if((v_c_spi3) and (v_c_gb_int_spi3)) then begin
    writeln(fv, 'void SPI3_IRQHandler(void){');
    writeln(fv, '}');
    writeln(fv, ' ');    
  end;

  if v_exti0_used then begin
    writeln(fv, 'void EXTI0_IRQHandler(void){');
    writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line0)) != RESET){');
    writeln(fv, '    // do something...');
    writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line0);');
    writeln(fv, '  }');
    writeln(fv, '}');
    writeln(fv, ' ');
  end;

  if v_exti1_used then begin
    writeln(fv, 'void EXTI1_IRQHandler(void){');
    writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line1)) != RESET){');
    writeln(fv, '    // do something...');
    writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line1);');
    writeln(fv, '  }');
    writeln(fv, '}');
    writeln(fv, ' ');
  end;

  if v_exti2_used then begin
    writeln(fv, 'void EXTI2_IRQHandler(void){');
    writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line2)) != RESET){');
    writeln(fv, '    // do something...');
    writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line2);');
    writeln(fv, '  }');
    writeln(fv, '}');
    writeln(fv, ' ');
  end;

  if v_exti3_used then begin
    writeln(fv, 'void EXTI3_IRQHandler(void){');
    writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line3)) != RESET){');
    writeln(fv, '    // do something...');
    writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line3);');
    writeln(fv, '  }');
    writeln(fv, '}');
    writeln(fv, ' ');
  end;

  if v_exti4_used then begin
    writeln(fv, 'void EXTI4_IRQHandler(void){');
    writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line4)) != RESET){');
    writeln(fv, '    // do something...');
    writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line4);');
    writeln(fv, '  }');
    writeln(fv, '}');
    writeln(fv, ' ');
  end;


  if v_exti9_5_used then begin
    writeln(fv, 'void EXTI9_5_IRQHandler(void){');
    if ((v_pa5_func = 'INTERRUPT') or (v_pb5_func = 'INTERRUPT') or (v_pc5_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line5)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line5);');
      writeln(fv, '  }');
    end;
    if ((v_pa6_func = 'INTERRUPT') or (v_pb6_func = 'INTERRUPT') or (v_pc6_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line6)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line6);');
      writeln(fv, '  }');
    end;
    if ((v_pa7_func = 'INTERRUPT') or (v_pb7_func = 'INTERRUPT') or (v_pc7_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line7)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line7);');
      writeln(fv, '  }');
    end;
    if ((v_pa8_func = 'INTERRUPT') or (v_pb8_func = 'INTERRUPT') or (v_pc8_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line8)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line8);');
      writeln(fv, '  }');
    end;
    if ((v_pa9_func = 'INTERRUPT') or (v_pb9_func = 'INTERRUPT') or (v_pc9_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line9)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line9);');
      writeln(fv, '  }');
    end;
    writeln(fv, '}');
    writeln(fv, ' ');
  end;


  if v_exti15_10_used then begin
    writeln(fv, 'void EXTI15_10_IRQHandler(void){');
    if ((v_pa10_func = 'INTERRUPT') or (v_pb10_func = 'INTERRUPT') or (v_pc10_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line10)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line10);');
      writeln(fv, '  }');
    end;
    if ((v_pa11_func = 'INTERRUPT') or (v_pb11_func = 'INTERRUPT') or (v_pc11_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line11)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line11);');
      writeln(fv, '  }');
    end;
    if ((v_pa12_func = 'INTERRUPT') or (v_pb12_func = 'INTERRUPT') or (v_pc12_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line12)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line12);');
      writeln(fv, '  }');
    end;
    if ((v_pb13_func = 'INTERRUPT') or (v_pc13_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  // Here would be the B1 pin interrupt handler (do where it says "do something..."):');
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line13)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line13);');
      writeln(fv, '  }');
    end;
    if (v_pb14_func = 'INTERRUPT') then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line14)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line14);');
      writeln(fv, '  }');
    end;
    if ((v_pa15_func = 'INTERRUPT') or (v_pb15_func = 'INTERRUPT')) then
    begin
      writeln(fv, '  if ((EXTI_GetITStatus(EXTI_Line15)) != RESET){');
      writeln(fv, '    // do something...');
      writeln(fv, '    EXTI_ClearITPendingBit(EXTI_Line15);');
      writeln(fv, '  }');
    end;
    writeln(fv, '}');
    writeln(fv, ' ');
  end;
end;


procedure wr_main_c;
begin
  //
  {$I-}
  Assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Src/main.c');
  Rewrite(G);
  //
  writeln(G, '/*');
  writeln(G, '  Generated with VPC version '+v_app_version);
  writeln(G, '  Application name: ' + v_prj_name);
  writeln(G, '  File name: main.c');
  copyright_h(G);
  if(v_copyright_idx = 0) then proprietary_lic_h(G);
  if(v_copyright_idx = 1) then lgpl_lic_h(G);
  if(v_copyright_idx = 2) then bsd_lic_h(G);
  if(v_copyright_idx = 3) then apache_lic_h(G);
  if(v_copyright_idx = 4) then mit_lic_h(G);
  writeln(G, ' ');
  writeln(G, '#include "stdint.h"');
  writeln(G, '#include "main.h"');
  writeln(G, '#include "my_gpio.h"');
  if (v_c_usart1 or v_c_usart2 or v_c_uart4 or v_c_uart5) then
    writeln(G, '#include "my_uart.h"');
  if v_c_lcd4 then begin
    case v_cb_idx_lcd4 of
      0:
        begin
          writeln(G, '#define LCD_NR_CHARS 16');
          writeln(G, '#define LCD_NR_LINES 2');
        end;
      1:
        begin
          writeln(G, '#define LCD_NR_CHARS 16');
          writeln(G, '#define LCD_NR_LINES 1');
        end;
      2:
        begin
          writeln(G, '#define LCD_NR_CHARS 16');
          writeln(G, '#define LCD_NR_LINES 4');
        end;
      3:
        begin
          writeln(G, '#define LCD_NR_CHARS 8');
          writeln(G, '#define LCD_NR_LINES 1');
        end;
      4:
        begin
          writeln(G, '#define LCD_NR_CHARS 8');
          writeln(G, '#define LCD_NR_LINES 2');
        end;
      5:
        begin
          writeln(G, '#define LCD_NR_CHARS 20');
          writeln(G, '#define LCD_NR_LINES 1');
        end;
      6:
        begin
          writeln(G, '#define LCD_NR_CHARS 20');
          writeln(G, '#define LCD_NR_LINES 2');
        end;
      7:
        begin
          writeln(G, '#define LCD_NR_CHARS 20');
          writeln(G, '#define LCD_NR_LINES 4');
        end;
      else begin
        writeln(G, '#define LCD_NR_CHARS 16');
        writeln(G, '#define LCD_NR_LINES 2');
      end;
    end;
    writeln(G, '#include "lcd4.h"');
  end;
  //
  writeln(G, ' ');
  //if v_tick_manager = 1 then
    //vpc_tim6_systick_init(G);
  //if v_tick_manager = 2 then
    //vpc_tim7_systick_init(G);
  vpc_system_init(G);
  vpc_gpio_init(G);
  if (v_use_adin) then 
    vpc_adc1_init(G);
  if (v_tim6_check) then
    vpc_tim6_init(G);
  if (v_tim7_check) then
    vpc_tim7_init(G);
  vpc_usart2_uart_init(G);
  if v_c_usart1 then
    vpc_usart1_init(G);
  if v_c_i2c1 then
    vpc_i2c1_init(G);
  if v_c_i2c2 then
    vpc_i2c2_init(G);
  if v_c_uart4 then
    vpc_uart4_init(G);
  if v_c_uart5 then
    vpc_uart5_init(G);
  if v_c_spi1 then
    vpc_spi1_init(G);
  if v_c_spi2 then
    vpc_spi2_init(G);
  if v_c_spi3 then
    vpc_spi3_init(G);
  vpc_core_init(G);
  write_global_interrupts(G);
  writeln(G, '  ');
  writeln(G, 'int main(void){');
  writeln(G, '  /* local variables */');
  writeln(G, '  ');
  writeln(G, '  /* mandatory system initializations */');
  writeln(G, '  vpc_system_init();');
  writeln(G, '  //vpc_rtc_init(); /* enable if required */  ');
  writeln(G, '  vpc_gpio_init();');
  writeln(G, '  //vpc_usart2_uart_init(); /* enable if required */ ');
  if (v_use_adin) then 
    writeln(G, '  vpc_adc1_init();');
  if (v_tim6_check) then
    writeln(G, '  vpc_tim6_init();');
  if (v_tim7_check) then
    writeln(G, '  vpc_tim7_init();');
  if v_c_usart1 then
    writeln(G, '  vpc_usart1_init();');
  if v_c_uart4 then
    writeln(G, '  vpc_uart4_init();');
  if v_c_uart5 then
    writeln(G, '  vpc_uart5_init();');
  if v_c_i2c1 then
    writeln(G, '  vpc_i2c1_init();');
  if v_c_i2c2 then
    writeln(G, '  vpc_i2c2_init();');
  if v_c_spi1 then
    writeln(G, '  vpc_spi1_init();');
  if v_c_spi2 then
    writeln(G, '  vpc_spi2_init();');
  if v_c_spi3 then
    writeln(G, '  vpc_spi3_init();');

  writeln(G, '  /* third-party initializations */');
  if v_c_lcd4 then begin
    write(G, '  lcd_init(');
    if(v_cb_idx_lcd4_chipset = 0) then
      writeln(G, 'LCD_HD44780);')
    else
      writeln(G, 'LCD_ST7066U);');
  end;
  writeln(G, '  ');
  writeln(G, '  /* do your own initializations below */');
  writeln(G, '  ');
  writeln(G, '  while(1){');
  writeln(G, '    /* your forever repeating code */');
  writeln(G, '    ');
  writeln(G, '  }');
  writeln(G, '  return 0;');
  writeln(G, '}');
  //
  writeln(G, ' ');
  Close(G);
  {$I+}
end;

// make a procedure to scan the pins for ANALOG function and set v_use_adc accordingly
procedure scan_for_analog;
begin
  v_use_adc := FALSE;
  {PC10}
  if v_pc10_func = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC12}
  if v_pc12_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA13}
  {PA14}
  {PA15}
  if v_pa15_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB7}
  if v_pb7_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC13}
  if v_pc13_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC14}
  {PC15}
  {PH0}
  {PH1}
  {PC2}
  if v_pc2_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC3}
  if v_pc3_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {-----------------------------------------}
  {PC11}
  if v_pc11_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PD2}
  if v_pd2_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA0}
  if v_pa0_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA1}
  if v_pa1_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA4}
  if v_pa4_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB0}
  if v_pb0_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC1}
  if v_pc1_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC0}
  if v_pc0_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {-----------------------------------------}
  {PC9}
  if v_pc9_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB8}
  if v_pb8_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB9}
  if v_pb9_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA5}
  if v_pa5_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA6}
  if v_pa6_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA7}
  if v_pa7_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB6}
  if v_pb6_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC7}
  if v_pc7_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA9}
  if v_pa9_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA8}
  if v_pa8_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB10}
  if v_pb10_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB4}
  if v_pb4_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB5}
  if v_pb5_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB3}
  if v_pb3_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA10}
  if v_pa10_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA2}
  {PA3}
  {-----------------------------------------}
  {PC8}
  if v_pc8_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC6}
  if v_pc6_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC5}
  if v_pc5_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA12}
  if v_pa12_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PA11}
  if v_pa11_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB12}
  if v_pb12_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB11}
  if v_pb11_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB2}
  if v_pc12_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB1}
  if v_pb1_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB15}
  if v_pb15_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB14}
  if v_pb14_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PB13}
  if v_pb13_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
  {PC4}
  if v_pc4_func  = 'ANALOG' then begin
    v_use_adc := TRUE;
    exit;
  end;
end;

procedure wr_makefile;
begin
  //
  {$I-}
  Assign(H, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Makefile');
  Rewrite(H);
  //
  writeln(H, '# Generated with VPC version '+v_app_version);
  writeln(H, '# Project name: '+v_prj_name);
  writeln(H, ' ');
  writeln(H, '# target');
  writeln(H, 'TARGET = '+v_prj_name);
  writeln(H, ' ');
  writeln(H, '# floating point and sprintf enable');
  write(H, 'USEFP = ');
  if v_efp then writeln(H, '1')
  else writeln(H, '0');
  writeln(H, ' ');
  write(H, 'USEFP_IN_PRINTF = ');
  if v_printf_efp then writeln(H, '1')
  else writeln(H, '0');
  writeln(H, ' ');
  writeln(H, '# debug support ');
  write(H, 'DEBUG = ');
  if v_debug then writeln(H, '1')
  else writeln(H, '0');
  writeln(H, ' ');
  writeln(H, '# optimization');
  write(H, 'OPT = -O');
  case (v_optimization_index) of
    0 : writeln(H, '0');
    1 : writeln(H, '1');
    2 : writeln(H, '2');
    3 : writeln(H, '3');
    4 : writeln(H, 's');
  end;
  writeln(H, ' ');
  writeln(H, '# your external library path relative to your project');
  writeln(H, 'EXT_LIB = -I../' + v_user_lib_folder);
  writeln(H, ' ');
  writeln(H, '# source path');
  writeln(H, 'SOURCES_DIR = \');
  writeln(H, 'Src \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src \');
  writeln(H, './ ');
  writeln(H, ' ');
  writeln(H, '# build folder ');
  writeln(H, 'BUILD_DIR = build');
  writeln(H, ' ');
  writeln(H, '# ASM sources ');
  writeln(H, 'ASM_SOURCES = STM32L152RE.s ');
  writeln(H, ' ');
  writeln(H, '# binaries ');
  writeln(H, 'BINPATH = $(HOME)/'+v_toolchain_folder+'/bin');
  writeln(H, 'PREFIX = arm-none-eabi-');
  writeln(H, 'CC = $(BINPATH)/$(PREFIX)gcc');
  writeln(H, 'AS = $(BINPATH)/$(PREFIX)gcc -x assembler-with-cpp');
  writeln(H, 'CP = $(BINPATH)/$(PREFIX)objcopy');
  writeln(H, 'AR = $(BINPATH)/$(PREFIX)ar');
  writeln(H, 'SZ = $(BINPATH)/$(PREFIX)size');
  writeln(H, 'HEX = $(CP) -O ihex');
  writeln(H, 'BIN = $(CP) -O binary -S');
  writeln(H, ' ');
  writeln(H, '# cpu');
  writeln(H, 'CPU = -mcpu=cortex-m3');
  writeln(H, ' ');
  writeln(H, '# float-abi');
  writeln(H, 'ifeq ($(USEFP), 1)');
  writeln(H, 'FLOAT-ABI = -mfloat-abi=soft');
  writeln(H, 'else');
  writeln(H, 'FLOAT-ABI = ');
  writeln(H, 'endif');
  writeln(H, ' ');
  writeln(H, '# mcu');
  writeln(H, 'MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)');
  writeln(H, ' ');
  writeln(H, '# AS defines');
  writeln(H, 'AS_DEFS = ');
  writeln(H, ' ');
  writeln(H, '# C defines');
  writeln(H, 'C_DEFS =  \');
  writeln(H, '-DUSE_STDPERIPH_DRIVER \');
  writeln(H, '-DSTM32L1XX_XL');
  writeln(H, ' ');
  writeln(H, '# AS includes');
  writeln(H, 'AS_INCLUDES = ');
  writeln(H, ' ');
  writeln(H, '# C includes');
  writeln(H, 'C_INCLUDES =  \');
  writeln(H, '-IInc \');
  writeln(H, '-IDrivers/STM32L1xx_StdPeriph_Driver/Inc \');
  writeln(H, '-IDrivers/CMSIS/Device/Include \');
  writeln(H, '-IDrivers/CMSIS/Include');
  writeln(H, ' ');
  writeln(H, 'C_INCLUDES += $(EXT_LIB)');
  writeln(H, ' ');
  writeln(H, '# compile gcc flags');
  writeln(H, 'ASFLAGS = $(MCU) $(AS_DEFS) $(AS_INCLUDES) $(OPT) -Wall -fdata-sections -ffunction-sections');
  writeln(H, ' ');
  writeln(H, 'CFLAGS = $(MCU) $(C_DEFS) $(C_INCLUDES) $(OPT) -Wall -fdata-sections -ffunction-sections');
  writeln(H, ' ');
  writeln(H, 'ifeq ($(DEBUG), 1)');
  writeln(H, 'CFLAGS += -g -gdwarf-2');
  writeln(H, 'endif');
  writeln(H, ' ');
  writeln(H, '# Generate dependency information');
  writeln(H, 'CFLAGS += -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)"');
  writeln(H, ' ');
  writeln(H, '# link script');
  writeln(H, 'LDSCRIPT = STM32L152RE.ld');
  writeln(H, ' ');
  writeln(H, '# libraries');
  writeln(H, 'LIBS = -lc -lm -lnosys');
  writeln(H, 'LIBDIR = ');
  writeln(H, 'LDFLAGS = $(MCU) -specs=nano.specs -T$(LDSCRIPT) $(LIBDIR) $(LIBS) -Wl,-Map=$(BUILD_DIR)/$(TARGET).map,--cref -Wl,--gc-sections');
  writeln(H, ' ');
  writeln(H, 'ifeq ($(USEFP_IN_PRINTF), 1)');
  writeln(H, 'LDFLAGS += -u _printf_float');
  writeln(H, 'endif');
  writeln(H, ' ');
  writeln(H, '# C sources');
  writeln(H, 'C_SOURCES =  \');
  writeln(H, 'Src/main.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_syscfg.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_flash.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_fsmc.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_comp.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_rcc.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_rtc.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_usart.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_wwdg.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_pwr.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_exti.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_gpio.c \');
  //if ((v_tim6_check = TRUE) or (v_tim7_check = TRUE)) then
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_tim.c \');
  if ((v_c_i2c1 = TRUE) or (v_c_i2c2 = TRUE)) then
    writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_i2c.c \');
  if ((v_c_spi1 = TRUE) or (v_c_spi2 = TRUE) or (v_c_spi3 = TRUE)) then
    writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_spi.c \');
  if ((v_use_adc) or (v_use_adin)) then
    writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_adc.c \');
  writeln(H, 'Drivers/STM32L1xx_StdPeriph_Driver/Src/misc.c');
  writeln(H, ' ');
  writeln(H, '# default action: build all');
  writeln(H, 'all: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin');
  writeln(H, ' ');
  writeln(H, '# list of objects');
  writeln(H, 'OBJECTS = $(sort $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o))))');
  writeln(H, 'vpath %.c $(sort $(dir $(C_SOURCES)))');
  writeln(H, '# list of ASM program objects');
  writeln(H, 'OBJECTS += $(sort $(addprefix $(BUILD_DIR)/,$(notdir $(ASM_SOURCES:.s=.o))))');
  writeln(H, 'vpath %.s $(sort $(dir $(ASM_SOURCES)))');
  writeln(H, ' ');
  writeln(H, '$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR)');
  writeln(H, '	$(CC) -c $(CFLAGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.c=.lst)) $< -o $@');
  writeln(H, ' ');
  writeln(H, '$(BUILD_DIR)/%.o: %.s Makefile | $(BUILD_DIR)');
  writeln(H, '	$(AS) -c $(CFLAGS) $< -o $@');
  writeln(H, ' ');
  writeln(H, '$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) Makefile');
  writeln(H, '	$(CC) $(OBJECTS) $(LDFLAGS) -o $@');
  writeln(H, '	$(SZ) $@');
  writeln(H, ' ');
  writeln(H, '$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf | $(BUILD_DIR)');
  writeln(H, '	$(HEX) $< $@');
  writeln(H, ' ');
  writeln(H, '$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf | $(BUILD_DIR)');
  writeln(H, '	$(BIN) $< $@');
  writeln(H, ' ');
  writeln(H, '$(BUILD_DIR):');
  writeln(H, '	mkdir $@');
  writeln(H, ' ');
  writeln(H, '# clean up');
  writeln(H, 'clean:');
  writeln(H, '	-rm -fR .dep $(BUILD_DIR)');
  writeln(H, ' ');
  writeln(H, '# flash the board');
  writeln(H, 'upload:');
  writeln(H, '	st-flash write $(BUILD_DIR)/$(TARGET).bin 0x08000000');
  writeln(H, ' ');
  writeln(H, '# dependencies');
  writeln(H, '-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)');
  writeln(H, ' ');
  writeln(H, '# === DONE === :P ');
  writeln(H, ' ');
  //
  Close(H);
  {$I+}
end;

begin
  code_integrity := TRUE;
end.

