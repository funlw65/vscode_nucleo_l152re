unit spi1_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons,
  Spin, data_unit, code_unit;

type

  { TForm21 }

  TForm21 = class(TForm)
    BitBtn1: TBitBtn;
    cb_spi1_priority: TComboBox;
    cb_spi1_subpriority: TComboBox;
    CheckBox2: TCheckBox;
    cb_spi_dir: TComboBox;
    cb_spi_size: TComboBox;
    cb_spi_first: TComboBox;
    cb_spi_prescaler: TComboBox;
    cb_spi_cpol: TComboBox;
    cb_spi_cpha: TComboBox;
    cb_spi_nss: TComboBox;
    cb_spi_mode: TComboBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    l_spi1_mess: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    priority: TLabel;
    priority1: TLabel;
    se_spi_poly: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form21: TForm21;

implementation

{$R *.lfm}

{ TForm21 }


procedure TForm21.BitBtn1Click(Sender: TObject);
begin
  close;
end;


procedure TForm21.FormClose(Sender: TObject);
begin
  v_cb_direction_idx_spi1 := cb_spi_dir.ItemIndex;
  v_cb_mode_idx_spi1 := cb_spi_mode.ItemIndex;
  v_cb_datasize_idx_spi1 := cb_spi_size.ItemIndex;
  v_cb_cpol_idx_spi1 := cb_spi_cpol.ItemIndex;
  v_cb_cpha_idx_spi1 := cb_spi_cpha.ItemIndex;
  v_cb_nss_idx_spi1 := cb_spi_nss.ItemIndex;
  v_cb_prescaler_idx_spi1 := cb_spi_prescaler.ItemIndex;
  v_cb_firstbit_idx_spi1 := cb_spi_first.ItemIndex;
  v_cb_crcpoly_idx_spi1 := se_spi_poly.Value;

  v_c_gb_int_spi1 := CheckBox2.Checked;
  v_spi1_int_priority:=validate_priority_value(cb_spi1_priority.ItemIndex);
  v_spi1_int_subpriority:=validate_subpriority_value(cb_spi1_subpriority.ItemIndex);

end;

procedure TForm21.FormShow(Sender: TObject);
begin
  if v_c_spi1 then begin
    GroupBox1.Enabled := TRUE;
    l_spi1_mess.Caption:='SPI1 peripheral activated.';
  end
  else begin
    GroupBox1.Enabled := FALSE;
    l_spi1_mess.Caption:='Set PA5-PA6-PA7 pin group as SPI1 from Nucleo or LQFP64 window';
  end;
  cb_spi_dir.ItemIndex := v_cb_direction_idx_spi1;
  cb_spi_mode.ItemIndex := v_cb_mode_idx_spi1;
  cb_spi_size.ItemIndex := v_cb_datasize_idx_spi1;
  cb_spi_cpol.ItemIndex := v_cb_cpol_idx_spi1;
  cb_spi_cpha.ItemIndex := v_cb_cpha_idx_spi1;
  cb_spi_nss.ItemIndex := v_cb_nss_idx_spi1;
  cb_spi_prescaler.ItemIndex := v_cb_prescaler_idx_spi1;
  cb_spi_first.ItemIndex := v_cb_firstbit_idx_spi1;
  se_spi_poly.Value := v_cb_crcpoly_idx_spi1;

  CheckBox2.Checked := v_c_gb_int_spi1;
  cb_spi1_priority.ItemIndex:=validate_priority_value(v_spi1_int_priority);
  cb_spi1_subpriority.ItemIndex:=validate_subpriority_value(v_spi1_int_subpriority);

end;

end.

