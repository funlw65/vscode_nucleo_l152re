unit lqfp64pinsetup_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, ExtCtrls, data_unit, code_unit;

type

  { TForm26 }

  TForm26 = class(TForm)
    BitBtn1: TBitBtn;
    CB_GPIO_A_MODE: TComboBox;
    CB_GPIO_I2C_MODE: TComboBox;
    CB_GPIO_INT_MODE: TComboBox;
    CB_GPIO_I_MODE: TComboBox;
    CB_GPIO_O_LVL: TComboBox;
    CB_GPIO_O_MODE: TComboBox;
    CB_GPIO_PULL_UPDOWN: TComboBox;
    CB_GPIO_SPEED: TComboBox;
    CB_GPIO_SPI_MODE: TComboBox;
    cb_pin_function: TComboBox;
    e_pin_label: TEdit;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    L_GPIO_MSG: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure cb_pin_functionChange(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form26: TForm26;

implementation

{$R *.lfm}

{ TForm26 }

var
  tmp_pin_function: string;


{=========== COMBO =======================================}
{-populating the combobox with the specific pin functions-}

procedure pa0_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN0',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('RTC_TAMP2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SYS_WKUP1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM2_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM2_ETR',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM5_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G1_IO1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART2_CTS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI0',Form26.cb_pin_function);
end;

procedure pa1_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('OPAMP1_VINP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM2_CH2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM5_CH2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G1_IO2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART2_RTS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI1',Form26.cb_pin_function);
end;

{ these are the pins set permanently as serial pins of the nucleo board
procedure pa2_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('OPAMP1_VINM',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM2_CH3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM5_CH3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM9_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G1_IO3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART2_TX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI2',Form26.cb_pin_function);
end;

procedure pa3_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('OPAMP1_VOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM2_CH4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM5_CH4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM9_CH2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G1_IO4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART2_RX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI3',Form26.cb_pin_function);
end;
}

procedure pa4_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('DAC_OUT1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S3_WS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_NSS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI3_NSS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART2_CK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI4',Form26.cb_pin_function);
end;

procedure pa5_combo;
begin
  // 
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_SCK',Form26.cb_pin_function);
  // yep, complete ;-)
  // how? only two functions available for this pin in Nucleo configuration
end;

procedure pa6_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN6',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('OPAMP2_VINP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_MISO',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM10_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G2_IO1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI6',Form26.cb_pin_function);
end;

procedure pa7_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN7',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('OPAMP2_VINM',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_MOSI',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM11_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G2_IO2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI7',Form26.cb_pin_function);
end;

procedure pa8_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('RCC_MCO',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G4_IO1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART1_CK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI8',Form26.cb_pin_function);
end;

procedure pa9_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('DAC_EXTI9',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G4_IO2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART1_TX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI9',Form26.cb_pin_function);
end;

procedure pa10_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G4_IO3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART1_RX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI10',Form26.cb_pin_function);
end;

procedure pa11_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_EXTI11',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_MISO',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART1_CTS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USB_DM',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI11',Form26.cb_pin_function);
end;

procedure pa12_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_MOSI',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART1_RTS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USB_DP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI12',Form26.cb_pin_function);
end;

procedure pa15_combo;
begin
  //
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_EXTI15',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S3_WS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_NSS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI3_NSS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SYS_JTDI',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM2_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_ETR',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G5_IO3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI15',Form26.cb_pin_function);
end;

{ }
procedure pb0_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN8',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('OPAMP2_VOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SYS_V_REF_OUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G3_IO1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI0',Form26.cb_pin_function);
end;

procedure pb1_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN9',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SYS_V_REF_OUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G3_IO2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI1',Form26.cb_pin_function);
end;

procedure pb2_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN0b',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G3_IO3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI2',Form26.cb_pin_function);
end;

procedure pb3_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP2_INM',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S3_CK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_SCK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI3_SCK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SYS_JTDO_TRACESWO',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM2_CH2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI3',Form26.cb_pin_function);
end;

procedure pb4_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP2_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_MISO',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI3_MISO',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SYS_JTRST',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G6_IO1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI4',Form26.cb_pin_function);
end;

procedure pb5_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP2_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2C1_SMBA',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S3_SD',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI1_MOSI',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI3_MOSI',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G6_IO2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI5',Form26.cb_pin_function);
end;

procedure pb6_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP2_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2C1_SCL',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM4_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G6_IO3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART1_TX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI6',Form26.cb_pin_function);
end;

procedure pb7_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP3_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2C1_SDA',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SYS_PVD_IN',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM4_CH2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G6_IO4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART1_RX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI7',Form26.cb_pin_function);
end;

procedure pb8_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2C1_SCL',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM10_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM4_CH3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI8',Form26.cb_pin_function);
end;

procedure pb9_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('DAC_EXTI9',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2C1_SDA',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM11_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM4_CH4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI9',Form26.cb_pin_function);
end;

procedure pb10_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2C2_SCL',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM2_CH3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART3_TX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI10',Form26.cb_pin_function);
end;

procedure pb11_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_EXTI11',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2C2_SDA',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM2_CH4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART3_RX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI11',Form26.cb_pin_function);
end;

procedure pb12_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN18',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2C2_SMBA',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S2_WS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI2_NSS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM10_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G7_IO1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART3_CK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI12',Form26.cb_pin_function);
end;

procedure pb13_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN19',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S2_CK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI2_SCK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM9_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G7_IO2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART3_CTS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI13',Form26.cb_pin_function);
end;

procedure pb14_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN20',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI2_MISO',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM9_CH2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G7_IO3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART2_RTS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI14',Form26.cb_pin_function);
end;

procedure pb15_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN21',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S2_SD',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('RTC_REFIN',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI2_MOSI',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM11_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G7_IO4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI15',Form26.cb_pin_function);
end;

{ }

procedure pc0_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN10',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G8_IO1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI0',Form26.cb_pin_function);
end;

procedure pc1_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN11',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G8_IO2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI1',Form26.cb_pin_function);
end;

procedure pc2_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN12',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G8_IO4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI2',Form26.cb_pin_function);
end;

procedure pc3_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN13',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G8_IO4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI3',Form26.cb_pin_function);
end;

procedure pc4_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN14',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G9_IO1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI4',Form26.cb_pin_function);
end;

procedure pc5_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_IN15',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('COMP1_INP',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G9_IO2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI5',Form26.cb_pin_function);
end;

procedure pc6_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S2_MCK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G10_IO1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI6',Form26.cb_pin_function);
end;

procedure pc7_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S3_MCK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G10_IO2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI7',Form26.cb_pin_function);
end;

procedure pc8_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G10_IO3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI8',Form26.cb_pin_function);
end;

procedure pc9_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('DAC_EXTI9',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_CH4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TS_G10_IO4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI9',Form26.cb_pin_function);
end;

procedure pc10_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S3_CK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI3_SCK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('UART4_TX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART3_TX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI10',Form26.cb_pin_function);
end;

procedure pc11_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('ADC_EXTI11',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI3_MISO',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC4',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('UART4_RX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART3_RX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI11',Form26.cb_pin_function);
end;

procedure pc12_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('I2S3_SD',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SPI3_MOSI',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('UART5_TX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('USART3_CK',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI12',Form26.cb_pin_function);
end;

{user button}
{
procedure pc13_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('RTC_OUT_ALARM',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('RTC_OUT_CALIB',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('RTC_TAMP1',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('RTC_TS',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('SYS_WKUP2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC2',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI13',Form26.cb_pin_function);
end;
}

procedure pd2_combo;
begin
  Form26.cb_pin_function.AddItem('Reset_State',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIM3_ETR',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('TIMX_IC3',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('UART5_RX',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Input',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Output',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_Analog',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('EVENTOUT',Form26.cb_pin_function);
  Form26.cb_pin_function.AddItem('GPIO_EXTI12',Form26.cb_pin_function);
end;



{=========== GET PIN =================}
{-------------------------------------}

procedure pa0_get_pin;
begin
  tmp_pin_function := v_pa0_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa0_idx;
  Form26.e_pin_label.Text := v_e_pa0_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa0_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa0_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa0_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa0_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa0_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa0_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa0_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa0_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa0_speed_idx;
end;

procedure pa1_get_pin;
begin
  tmp_pin_function := v_pa1_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa1_idx;
  Form26.e_pin_label.Text := v_e_pa1_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa1_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa1_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa1_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa1_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa1_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa1_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa1_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa1_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa1_speed_idx;
end;

{ permanently set as serial on nucleo board
procedure pa2_get_pin;
begin
  tmp_pin_function := v_pa2_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa2_idx;
  Form26.e_pin_label.Text := v_e_pa2_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa2_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa2_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa2_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa2_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa2_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa2_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa2_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa2_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa2_speed_idx;
end;

procedure pa3_get_pin;
begin
  tmp_pin_function := v_pa3_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa3_idx;
  Form26.e_pin_label.Text := v_e_pa3_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa3_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa3_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa3_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa3_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa3_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa3_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa3_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa3_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa3_speed_idx;
end;
}

procedure pa4_get_pin;
begin
  tmp_pin_function := v_pa4_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa4_idx;
  Form26.e_pin_label.Text := v_e_pa4_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa4_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa4_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa4_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa4_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa4_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa4_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa4_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa4_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa4_speed_idx;
end;

procedure pa5_get_pin;
begin
  tmp_pin_function := v_pa5_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa5_idx;
  Form26.e_pin_label.Text := v_e_pa5_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa5_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa5_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa5_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa5_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa5_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa5_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa5_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa5_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa5_speed_idx;
end;

procedure pa6_get_pin;
begin
  tmp_pin_function := v_pa6_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa6_idx;
  Form26.e_pin_label.Text := v_e_pa6_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa6_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa6_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa6_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa6_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa6_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa6_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa6_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa6_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa6_speed_idx;
end;

procedure pa7_get_pin;
begin
  tmp_pin_function := v_pa7_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa7_idx;
  Form26.e_pin_label.Text := v_e_pa7_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa7_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa7_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa7_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa7_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa7_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa7_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa7_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa7_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa7_speed_idx;
end;

procedure pa8_get_pin;
begin
  tmp_pin_function := v_pa8_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa8_idx;
  Form26.e_pin_label.Text := v_e_pa8_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa8_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa8_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa8_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa8_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa8_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa8_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa8_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa8_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa8_speed_idx;
end;

procedure pa9_get_pin;
begin
  tmp_pin_function := v_pa9_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa9_idx;
  Form26.e_pin_label.Text := v_e_pa9_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa9_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa9_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa9_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa9_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa9_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa9_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa9_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa9_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa9_speed_idx;
end;

procedure pa10_get_pin;
begin
  tmp_pin_function := v_pa10_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa10_idx;
  Form26.e_pin_label.Text := v_e_pa10_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa10_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa10_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa10_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa10_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa10_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa10_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa10_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa10_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa10_speed_idx;
end;

procedure pa11_get_pin;
begin
  tmp_pin_function := v_pa11_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa11_idx;
  Form26.e_pin_label.Text := v_e_pa11_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa11_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa11_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa11_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa11_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa11_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa11_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa11_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa11_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa11_speed_idx;
end;

procedure pa12_get_pin;
begin
  tmp_pin_function := v_pa12_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa12_idx;
  Form26.e_pin_label.Text := v_e_pa12_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa12_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa12_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa12_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa12_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa12_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa12_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa12_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa12_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa12_speed_idx;
end;

procedure pa15_get_pin;
begin
  tmp_pin_function := v_pa15_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pa15_idx;
  Form26.e_pin_label.Text := v_e_pa15_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pa15_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pa15_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pa15_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pa15_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pa15_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pa15_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pa15_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pa15_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pa15_speed_idx;
end;


{ }
procedure pb0_get_pin;
begin
  tmp_pin_function := v_pb0_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb0_idx;
  Form26.e_pin_label.Text := v_e_pb0_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb0_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb0_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb0_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb0_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb0_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb0_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb0_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb0_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb0_speed_idx;
end;

procedure pb1_get_pin;
begin
  tmp_pin_function := v_pb1_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb1_idx;
  Form26.e_pin_label.Text := v_e_pb1_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb1_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb1_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb1_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb1_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb1_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb1_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb1_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb1_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb1_speed_idx;
end;

procedure pb2_get_pin;
begin
  tmp_pin_function := v_pb2_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb2_idx;
  Form26.e_pin_label.Text := v_e_pb2_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb2_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb2_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb2_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb2_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb2_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb2_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb2_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb2_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb2_speed_idx;
end;

procedure pb3_get_pin;
begin
  tmp_pin_function := v_pb3_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb3_idx;
  Form26.e_pin_label.Text := v_e_pb3_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb3_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb3_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb3_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb3_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb3_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb3_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb3_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb3_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb3_speed_idx;
end;

procedure pb4_get_pin;
begin
  tmp_pin_function := v_pb4_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb4_idx;
  Form26.e_pin_label.Text := v_e_pb4_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb4_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb4_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb4_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb4_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb4_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb4_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb4_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb4_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb4_speed_idx;
end;

procedure pb5_get_pin;
begin
  tmp_pin_function := v_pb5_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb5_idx;
  Form26.e_pin_label.Text := v_e_pb5_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb5_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb5_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb5_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb5_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb5_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb5_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb5_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb5_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb5_speed_idx;
end;

procedure pb6_get_pin;
begin
  tmp_pin_function := v_pb6_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb6_idx;
  Form26.e_pin_label.Text := v_e_pb6_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb6_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb6_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb6_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb6_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb6_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb6_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb6_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb6_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb6_speed_idx;
end;

procedure pb7_get_pin;
begin
  tmp_pin_function := v_pb7_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb7_idx;
  Form26.e_pin_label.Text := v_e_pb7_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb7_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb7_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb7_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb7_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb7_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb7_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb7_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb7_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb7_speed_idx;
end;

procedure pb8_get_pin;
begin
  tmp_pin_function := v_pb8_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb8_idx;
  Form26.e_pin_label.Text := v_e_pb8_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb8_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb8_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb8_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb8_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb8_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb8_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb8_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb8_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb8_speed_idx;
end;

procedure pb9_get_pin;
begin
  tmp_pin_function := v_pb9_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb9_idx;
  Form26.e_pin_label.Text := v_e_pb9_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb9_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb9_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb9_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb9_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb9_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb9_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb9_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb9_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb9_speed_idx;
end;

procedure pb10_get_pin;
begin
  tmp_pin_function := v_pb10_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb10_idx;
  Form26.e_pin_label.Text := v_e_pb10_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb10_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb10_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb10_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb10_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb10_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb10_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb10_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb10_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb10_speed_idx;
end;

procedure pb11_get_pin;
begin
  tmp_pin_function := v_pb11_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb11_idx;
  Form26.e_pin_label.Text := v_e_pb11_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb11_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb11_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb11_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb11_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb11_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb11_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb11_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb11_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb11_speed_idx;
end;

procedure pb12_get_pin;
begin
  tmp_pin_function := v_pb12_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb12_idx;
  Form26.e_pin_label.Text := v_e_pb12_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb12_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb12_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb12_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb12_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb12_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb12_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb12_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb12_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb12_speed_idx;
end;

procedure pb13_get_pin;
begin
  tmp_pin_function := v_pb13_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb13_idx;
  Form26.e_pin_label.Text := v_e_pb13_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb13_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb13_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb13_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb13_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb13_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb13_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb13_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb13_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb13_speed_idx;
end;

procedure pb14_get_pin;
begin
  tmp_pin_function := v_pb14_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb14_idx;
  Form26.e_pin_label.Text := v_e_pb14_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb14_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb14_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb14_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb14_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb14_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb14_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb14_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb14_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb14_speed_idx;
end;

procedure pb15_get_pin;
begin
  tmp_pin_function := v_pb15_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pb15_idx;
  Form26.e_pin_label.Text := v_e_pb15_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pb15_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pb15_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pb15_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pb15_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pb15_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pb15_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pb15_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pb15_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pb15_speed_idx;
end;

{ }

procedure pc0_get_pin;
begin
  tmp_pin_function := v_pc0_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc0_idx;
  Form26.e_pin_label.Text := v_e_pc0_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc0_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc0_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc0_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc0_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc0_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc0_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc0_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc0_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc0_speed_idx;
end;

procedure pc1_get_pin;
begin
  tmp_pin_function := v_pc1_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc1_idx;
  Form26.e_pin_label.Text := v_e_pc1_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc1_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc1_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc1_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc1_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc1_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc1_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc1_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc1_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc1_speed_idx;
end;

procedure pc2_get_pin;
begin
  tmp_pin_function := v_pc2_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc2_idx;
  Form26.e_pin_label.Text := v_e_pc2_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc2_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc2_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc2_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc2_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc2_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc2_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc2_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc2_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc2_speed_idx;
end;

procedure pc3_get_pin;
begin
  tmp_pin_function := v_pc3_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc3_idx;
  Form26.e_pin_label.Text := v_e_pc3_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc3_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc3_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc3_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc3_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc3_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc3_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc3_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc3_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc3_speed_idx;
end;

procedure pc4_get_pin;
begin
  tmp_pin_function := v_pc4_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc4_idx;
  Form26.e_pin_label.Text := v_e_pc4_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc4_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc4_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc4_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc4_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc4_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc4_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc4_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc4_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc4_speed_idx;
end;

procedure pc5_get_pin;
begin
  tmp_pin_function := v_pc5_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc5_idx;
  Form26.e_pin_label.Text := v_e_pc5_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc5_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc5_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc5_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc5_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc5_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc5_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc5_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc5_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc5_speed_idx;
end;

procedure pc6_get_pin;
begin
  tmp_pin_function := v_pc6_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc6_idx;
  Form26.e_pin_label.Text := v_e_pc6_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc6_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc6_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc6_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc6_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc6_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc6_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc6_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc6_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc6_speed_idx;
end;

procedure pc7_get_pin;
begin
  tmp_pin_function := v_pc7_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc7_idx;
  Form26.e_pin_label.Text := v_e_pc7_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc7_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc7_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc7_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc7_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc7_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc7_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc7_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc7_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc7_speed_idx;
end;

procedure pc8_get_pin;
begin
  tmp_pin_function := v_pc8_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc8_idx;
  Form26.e_pin_label.Text := v_e_pc8_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc8_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc8_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc8_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc8_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc8_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc8_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc8_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc8_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc8_speed_idx;
end;

procedure pc9_get_pin;
begin
  tmp_pin_function := v_pc9_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc9_idx;
  Form26.e_pin_label.Text := v_e_pc9_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc9_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc9_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc9_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc9_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc9_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc9_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc9_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc9_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc9_speed_idx;
end;

procedure pc10_get_pin;
begin
  tmp_pin_function := v_pc10_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc10_idx;
  Form26.e_pin_label.Text := v_e_pc10_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc10_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc10_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc10_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc10_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc10_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc10_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc10_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc10_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc10_speed_idx;
end;

procedure pc11_get_pin;
begin
  tmp_pin_function := v_pc11_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc11_idx;
  Form26.e_pin_label.Text := v_e_pc11_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc11_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc11_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc11_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc11_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc11_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc11_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc11_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc11_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc11_speed_idx;
end;

procedure pc12_get_pin;
begin
  tmp_pin_function := v_pc12_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc12_idx;
  Form26.e_pin_label.Text := v_e_pc12_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc12_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc12_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc12_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc12_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc12_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc12_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc12_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc12_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc12_speed_idx;
end;

{ permanently set as user button on nucleo board
procedure pc13_get_pin;
begin
  tmp_pin_function := v_pc13_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pc13_idx;
  Form26.e_pin_label.Text := v_e_pc13_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pc13_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pc13_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pc13_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pc13_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pc13_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pc13_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pc13_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pc13_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pc13_speed_idx;
end;
}

procedure pd2_get_pin;
begin
  tmp_pin_function := v_pd2_func;
  Form26.cb_pin_function.ItemIndex := v_cb_pd2_idx;
  Form26.e_pin_label.Text := v_e_pd2_txt;
  Form26.CB_GPIO_O_LVL.ItemIndex := v_pd2_o_lvl_idx;
  Form26.CB_GPIO_O_MODE.ItemIndex := v_pd2_o_mode_idx;
  Form26.CB_GPIO_I_MODE.ItemIndex := v_pd2_i_mode_idx;
  Form26.CB_GPIO_A_MODE.ItemIndex := v_pd2_a_mode_idx;
  Form26.CB_GPIO_I2C_MODE.ItemIndex := v_pd2_i2c_mode_idx;
  Form26.CB_GPIO_SPI_MODE.ItemIndex := v_pd2_spi_mode_idx;
  Form26.CB_GPIO_INT_MODE.ItemIndex := v_pd2_int_mode_idx;
  Form26.CB_GPIO_PULL_UPDOWN.ItemIndex := v_pd2_pull_updown_idx;
  Form26.CB_GPIO_SPEED.ItemIndex := v_pd2_speed_idx;
end;



{============ SET PIN =================}
{--------------------------------------}

procedure pa0_set_pin;
begin
  v_pa0_func := tmp_pin_function;
  v_pa0_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa0_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa0_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa0_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa0_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa0_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa0_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa0_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa0_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa1_set_pin;
begin
  v_pa1_func := tmp_pin_function;
  v_pa1_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa1_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa1_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa1_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa1_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa1_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa1_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa1_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa1_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

{ permanently set as serial on nucleo board
procedure pa2_set_pin;
begin
  v_pa2_func := tmp_pin_function;
  v_pa2_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa2_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa2_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa2_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa2_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa2_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa2_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa2_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa2_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa3_set_pin;
begin
  v_pa3_func := tmp_pin_function;
  v_pa3_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa3_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa3_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa3_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa3_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa3_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa3_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa3_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa3_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;
}

procedure pa4_set_pin;
begin
  v_pa4_func := tmp_pin_function;
  v_pa4_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa4_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa4_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa4_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa4_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa4_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa4_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa4_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa4_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa5_set_pin;
begin
  v_pa5_func := tmp_pin_function;
  v_pa5_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa5_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa5_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa5_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa5_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa5_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa5_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa5_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa5_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa6_set_pin;
begin
  v_pa6_func := tmp_pin_function;
  v_pa6_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa6_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa6_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa6_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa6_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa6_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa6_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa6_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa6_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa7_set_pin;
begin
  v_pa7_func := tmp_pin_function;
  v_pa7_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa7_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa7_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa7_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa7_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa7_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa7_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa7_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa7_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa8_set_pin;
begin
  v_pa8_func := tmp_pin_function;
  v_pa8_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa8_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa8_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa8_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa8_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa8_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa8_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa8_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa8_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
  // treat the special case of MCO and v_c_rcc_mco
  if ((v_c_rcc_mco = TRUE) and (v_pa8_func <> 'CLOCK')) then v_c_rcc_mco := FALSE;
  if((v_pa8_func = 'CLOCK') and (v_c_rcc_mco <> TRUE)) then begin 
    v_c_rcc_mco := TRUE;
    v_e_pa8_txt := 'MCO';
  end;  
end;

procedure pa9_set_pin;
begin
  v_pa9_func := tmp_pin_function;
  v_pa9_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa9_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa9_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa9_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa9_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa9_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa9_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa9_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa9_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa10_set_pin;
begin
  v_pa10_func := tmp_pin_function;
  v_pa10_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa10_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa10_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa10_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa10_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa10_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa10_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa10_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa10_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa11_set_pin;
begin
  v_pa11_func := tmp_pin_function;
  v_pa11_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa11_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa11_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa11_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa11_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa11_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa11_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa11_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa11_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa12_set_pin;
begin
  v_pa12_func := tmp_pin_function;
  v_pa12_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa12_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa12_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa12_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa12_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa12_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa12_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa12_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa12_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pa15_set_pin;
begin
  v_pa15_func := tmp_pin_function;
  v_pa15_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pa15_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pa15_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pa15_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pa15_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pa15_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pa15_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pa15_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pa15_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;


procedure pb0_set_pin;
begin
  v_pb0_func := tmp_pin_function;
  v_pb0_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb0_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb0_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb0_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb0_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb0_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb0_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb0_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb0_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb1_set_pin;
begin
  v_pb1_func := tmp_pin_function;
  v_pb1_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb1_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb1_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb1_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb1_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb1_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb1_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb1_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb1_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb2_set_pin;
begin
  v_pb2_func := tmp_pin_function;
  v_pb2_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb2_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb2_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb2_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb2_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb2_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb2_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb2_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb2_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb3_set_pin;
begin
  v_pb3_func := tmp_pin_function;
  v_pb3_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb3_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb3_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb3_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb3_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb3_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb3_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb3_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb3_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb4_set_pin;
begin
  v_pb4_func := tmp_pin_function;
  v_pb4_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb4_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb4_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb4_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb4_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb4_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb4_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb4_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb4_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb5_set_pin;
begin
  v_pb5_func := tmp_pin_function;
  v_pb5_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb5_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb5_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb5_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb5_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb5_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb5_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb5_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb5_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb6_set_pin;
begin
  v_pb6_func := tmp_pin_function;
  v_pb6_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb6_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb6_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb6_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb6_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb6_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb6_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb6_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb6_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb7_set_pin;
begin
  v_pb7_func := tmp_pin_function;
  v_pb7_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb7_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb7_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb7_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb7_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb7_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb7_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb7_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb7_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb8_set_pin;
begin
  v_pb8_func := tmp_pin_function;
  v_pb8_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb8_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb8_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb8_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb8_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb8_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb8_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb8_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb8_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb9_set_pin;
begin
  v_pb9_func := tmp_pin_function;
  v_pb9_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb9_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb9_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb9_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb9_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb9_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb9_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb9_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb9_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb10_set_pin;
begin
  v_pb10_func := tmp_pin_function;
  v_pb10_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb10_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb10_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb10_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb10_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb10_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb10_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb10_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb10_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb11_set_pin;
begin
  v_pb11_func := tmp_pin_function;
  v_pb11_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb11_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb11_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb11_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb11_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb11_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb11_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb11_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb11_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb12_set_pin;
begin
  v_pb12_func := tmp_pin_function;
  v_pb12_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb12_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb12_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb12_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb12_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb12_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb12_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb12_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb12_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb13_set_pin;
begin
  v_pb13_func := tmp_pin_function;
  v_pb13_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb13_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb13_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb13_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb13_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb13_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb13_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb13_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb13_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb14_set_pin;
begin
  v_pb14_func := tmp_pin_function;
  v_pb14_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb14_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb14_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb14_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb14_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb14_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb14_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb14_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb14_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pb15_set_pin;
begin
  v_pb15_func := tmp_pin_function;
  v_pb15_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pb15_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pb15_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pb15_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pb15_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pb15_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pb15_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pb15_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pb15_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

{ }

procedure pc0_set_pin;
begin
  v_pc0_func := tmp_pin_function;
  v_pc0_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc0_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc0_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc0_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc0_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc0_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc0_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc0_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc0_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc1_set_pin;
begin
  v_pc1_func := tmp_pin_function;
  v_pc1_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc1_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc1_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc1_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc1_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc1_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc1_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc1_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc1_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc2_set_pin;
begin
  v_pc2_func := tmp_pin_function;
  v_pc2_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc2_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc2_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc2_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc2_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc2_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc2_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc2_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc2_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc3_set_pin;
begin
  v_pc3_func := tmp_pin_function;
  v_pc3_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc3_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc3_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc3_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc3_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc3_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc3_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc3_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc3_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc4_set_pin;
begin
  v_pc4_func := tmp_pin_function;
  v_pc4_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc4_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc4_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc4_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc4_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc4_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc4_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc4_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc4_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc5_set_pin;
begin
  v_pc5_func := tmp_pin_function;
  v_pc5_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc5_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc5_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc5_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc5_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc5_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc5_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc5_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc5_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc6_set_pin;
begin
  v_pc6_func := tmp_pin_function;
  v_pc6_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc6_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc6_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc6_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc6_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc6_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc6_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc6_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc6_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc7_set_pin;
begin
  v_pc7_func := tmp_pin_function;
  v_pc7_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc7_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc7_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc7_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc7_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc7_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc7_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc7_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc7_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc8_set_pin;
begin
  v_pc8_func := tmp_pin_function;
  v_pc8_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc8_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc8_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc8_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc8_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc8_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc8_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc8_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc8_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc9_set_pin;
begin
  v_pc9_func := tmp_pin_function;
  v_pc9_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc9_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc9_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc9_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc9_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc9_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc9_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc9_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc9_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc10_set_pin;
begin
  v_pc10_func := tmp_pin_function;
  v_pc10_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc10_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc10_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc10_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc10_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc10_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc10_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc10_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc10_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc11_set_pin;
begin
  v_pc11_func := tmp_pin_function;
  v_pc11_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc11_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc11_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc11_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc11_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc11_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc11_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc11_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc11_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

procedure pc12_set_pin;
begin
  v_pc12_func := tmp_pin_function;
  v_pc12_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc12_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc12_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc12_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc12_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc12_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc12_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc12_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc12_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

{permanently set as user button on nucleo board
procedure pc13_set_pin;
begin
  v_pc13_func := tmp_pin_function;
  v_pc13_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pc13_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pc13_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pc13_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pc13_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pc13_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pc13_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pc13_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pc13_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;
}

procedure pd2_set_pin;
begin
  v_pd2_func := tmp_pin_function;
  v_pd2_o_lvl_idx := Form26.CB_GPIO_O_LVL.ItemIndex;
  v_pd2_o_mode_idx := Form26.CB_GPIO_O_MODE.ItemIndex;
  v_pd2_i_mode_idx := Form26.CB_GPIO_I_MODE.ItemIndex;
  v_pd2_a_mode_idx := Form26.CB_GPIO_A_MODE.ItemIndex;
  v_pd2_i2c_mode_idx := Form26.CB_GPIO_I2C_MODE.ItemIndex;
  v_pd2_spi_mode_idx := Form26.CB_GPIO_SPI_MODE.ItemIndex;
  v_pd2_int_mode_idx := Form26.CB_GPIO_INT_MODE.ItemIndex;
  v_pd2_pull_updown_idx := Form26.CB_GPIO_PULL_UPDOWN.ItemIndex;
  v_pd2_speed_idx := Form26.CB_GPIO_SPEED.ItemIndex;
end;

{===================================================================}

procedure TForm26.FormClose(Sender: TObject);
begin
  // on closing, remove all the items from the cb_pin_function combobox
  Form26.cb_pin_function.Clear;
  clean_buffer;
  tmp_pin_function := 'N/A';
end;


procedure TForm26.FormShow(Sender: TObject);
begin
  Caption := 'Pin '+v_current_lqfp_pad+' setup';
  tmp_pin_function := 'N/A';
  if v_current_lqfp_pad = 'PA0' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa0_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa0_get_pin;
  end;
  if v_current_lqfp_pad = 'PA1' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa1_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa1_get_pin;
  end;
  { the serial pins of Nucleo - on a different hardware, you have to use also these pins
  if v_current_lqfp_pad = 'PA2' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa2_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa2_get_pin;
  end;
  if v_current_lqfp_pad = 'PA3' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa3_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa3_get_pin;
  end;
  }
  if v_current_lqfp_pad = 'PA4' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa4_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa4_get_pin;
  end;
  if v_current_lqfp_pad = 'PA5' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa5_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa5_get_pin;
  end;
  if v_current_lqfp_pad = 'PA6' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa6_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa6_get_pin;
  end;
  if v_current_lqfp_pad = 'PA7' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa7_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa7_get_pin;
  end;
  if v_current_lqfp_pad = 'PA8' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa8_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa8_get_pin;
  end;
  if v_current_lqfp_pad = 'PA9' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa9_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa9_get_pin;
  end;
  if v_current_lqfp_pad = 'PA10' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa10_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa10_get_pin;
  end;
  if v_current_lqfp_pad = 'PA11' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa11_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa11_get_pin;
  end;
  if v_current_lqfp_pad = 'PA12' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa12_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa12_get_pin;
  end;
  if v_current_lqfp_pad = 'PA15' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pa15_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pa15_get_pin;
  end;
  if v_current_lqfp_pad = 'PB0' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb0_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb0_get_pin;
  end;
  if v_current_lqfp_pad = 'PB1' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb1_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb1_get_pin;
  end;
  if v_current_lqfp_pad = 'PB2' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb2_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb2_get_pin;
  end;
  if v_current_lqfp_pad = 'PB3' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb3_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb3_get_pin;
  end;
  if v_current_lqfp_pad = 'PB4' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb4_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb4_get_pin;
  end;
  if v_current_lqfp_pad = 'PB5' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb5_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb5_get_pin;
  end;
  if v_current_lqfp_pad = 'PB6' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb6_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb6_get_pin;
  end;
  if v_current_lqfp_pad = 'PB7' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb7_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb7_get_pin;
  end;
  if v_current_lqfp_pad = 'PB8' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb8_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb8_get_pin;
  end;
  if v_current_lqfp_pad = 'PB9' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb9_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb9_get_pin;
  end;
  if v_current_lqfp_pad = 'PB10' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb10_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb10_get_pin;
  end;
  if v_current_lqfp_pad = 'PB11' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb11_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb11_get_pin;
  end;
  if v_current_lqfp_pad = 'PB12' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb12_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb12_get_pin;
  end;
  if v_current_lqfp_pad = 'PB13' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb13_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb13_get_pin;
  end;
  if v_current_lqfp_pad = 'PB14' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb14_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb14_get_pin;
  end;
  if v_current_lqfp_pad = 'PB15' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pb15_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pb15_get_pin;
  end;
  if v_current_lqfp_pad = 'PC0' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc0_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc0_get_pin;
  end;
  if v_current_lqfp_pad = 'PC1' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc1_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc1_get_pin;
  end;
  if v_current_lqfp_pad = 'PC2' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc2_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc2_get_pin;
  end;
  if v_current_lqfp_pad = 'PC3' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc3_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc3_get_pin;
  end;
  if v_current_lqfp_pad = 'PC4' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc4_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc4_get_pin;
  end;
  if v_current_lqfp_pad = 'PC5' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc5_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc5_get_pin;
  end;
  if v_current_lqfp_pad = 'PC6' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc6_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc6_get_pin;
  end;
  if v_current_lqfp_pad = 'PC7' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc7_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc7_get_pin;
  end;
  if v_current_lqfp_pad = 'PC8' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc8_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc8_get_pin;
  end;
  if v_current_lqfp_pad = 'PC9' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc9_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc9_get_pin;
  end;
  if v_current_lqfp_pad = 'PC10' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc10_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc10_get_pin;
  end;
  if v_current_lqfp_pad = 'PC11' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc11_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc11_get_pin;
  end;
  if v_current_lqfp_pad = 'PC12' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc12_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc12_get_pin;
  end;
  {On nucleo, this is permanently assigned to an user button}
  {
  if v_current_lqfp_pad = 'PC13' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pc13_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pc13_get_pin;
  end;
  }
  if v_current_lqfp_pad = 'PD2' then begin
    //populate the cb_pin_function combobox according to the pin name global variable
    pd2_combo;
    //get the current values of the pin including setting the current item in
    //the cb_pin_function combobox
    pd2_get_pin;
  end;

  if (cb_pin_function.ItemIndex <= 0) and (tmp_pin_function = 'N/A') and (v_current_lqfp_pad<>'PA5')
    then L_GPIO_MSG.Caption:='Pin has no function assigned!';
  if (cb_pin_function.ItemIndex > 0) and (tmp_pin_function = 'N/A')
    then L_GPIO_MSG.Caption:='Function not supported, choose another!';
  if (cb_pin_function.ItemIndex >= 0) and (tmp_pin_function <> 'N/A')
    then L_GPIO_MSG.Caption:='Pin set as '+tmp_pin_function+'.';
  if (cb_pin_function.ItemIndex <= 0) and (e_pin_label.Text <> '') and (v_current_lqfp_pad<>'PA5')
    then e_pin_label.Text := '';

end;

procedure TForm26.BitBtn1Click(Sender: TObject);
begin
  // lets make some data validation... it will be ugly...
  b_pin_o_lvl_idx := CB_GPIO_O_LVL.ItemIndex;
  b_pin_o_mode_idx := CB_GPIO_O_MODE.ItemIndex;
  b_pin_i_mode_idx := CB_GPIO_I_MODE.ItemIndex;
  b_pin_a_mode_idx := CB_GPIO_A_MODE.ItemIndex;
  b_pin_i2c_mode_idx := CB_GPIO_I2C_MODE.ItemIndex;
  b_pin_spi_mode_idx := CB_GPIO_SPI_MODE.ItemIndex;
  b_pin_int_mode_idx := CB_GPIO_INT_MODE.ItemIndex;
  b_pin_pull_updown_idx := CB_GPIO_PULL_UPDOWN.ItemIndex;
  b_pin_speed_idx := CB_GPIO_SPEED.ItemIndex;
  //---
  validate_buffer(tmp_pin_function);
  //---
  CB_GPIO_O_LVL.ItemIndex := b_pin_o_lvl_idx;
  CB_GPIO_O_MODE.ItemIndex := b_pin_o_mode_idx;
  CB_GPIO_I_MODE.ItemIndex := b_pin_i_mode_idx;
  CB_GPIO_A_MODE.ItemIndex := b_pin_a_mode_idx;
  CB_GPIO_I2C_MODE.ItemIndex := b_pin_i2c_mode_idx;
  CB_GPIO_SPI_MODE.ItemIndex := b_pin_spi_mode_idx;
  CB_GPIO_INT_MODE.ItemIndex := b_pin_int_mode_idx;
  CB_GPIO_PULL_UPDOWN.ItemIndex := b_pin_pull_updown_idx;
  CB_GPIO_SPEED.ItemIndex := b_pin_speed_idx;
  // validation done

  //now, lets treat the pins

  if v_current_lqfp_pad = 'PA0' then begin
    v_cb_pa0_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa0_txt := Form26.e_pin_label.Text;
    pa0_set_pin;
  end;

  if v_current_lqfp_pad = 'PA1' then begin
    v_cb_pa1_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa1_txt := Form26.e_pin_label.Text;
    pa1_set_pin;
  end;

  {
  if v_current_lqfp_pad = 'PA2' then begin
    v_cb_pa2_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa2_txt := Form26.e_pin_label.Text;
    pa2_set_pin;
  end;

  if v_current_lqfp_pad = 'PA3' then begin
    v_cb_pa3_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa3_txt := Form26.e_pin_label.Text;
    pa3_set_pin;
  end;
  }

  if v_current_lqfp_pad = 'PA4' then begin
    v_cb_pa4_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa4_txt := Form26.e_pin_label.Text;
    pa4_set_pin;
  end;

  if v_current_lqfp_pad = 'PA5' then begin
    if (tmp_pin_function <> 'SPI') and (v_c_spi1 = TRUE) then begin 
      reset_spi1_pins;
      v_e_pa5_txt :=  'LD2';
    end;  
    v_cb_pa5_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa5_txt := Form26.e_pin_label.Text;
    pa5_set_pin;
    if (tmp_pin_function = 'SPI') then begin
      v_c_spi1 := TRUE;
      v_cb_pa6_idx:= 4;
      v_cb_pa7_idx:= 4;
      v_e_pa5_txt := 'SCK1';
      v_e_pa6_txt := 'MISO1';
      v_e_pa7_txt := 'MOSI1';
      pa6_set_pin;
      pa7_set_pin;
    end
    else if (v_e_pa5_txt <> 'LD2') then v_e_pa5_txt :=  'LD2';
  end;

  if v_current_lqfp_pad = 'PA6' then begin
    if (tmp_pin_function <> 'SPI') and (v_c_spi1 = TRUE) then begin 
      reset_spi1_pins;
      v_e_pa5_txt :=  'LD2';
    end;  
    v_cb_pa6_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa6_txt := Form26.e_pin_label.Text;
    pa6_set_pin;
    if (tmp_pin_function = 'SPI') then begin
      v_c_spi1 := TRUE;
      v_cb_pa5_idx:= 1;
      v_cb_pa7_idx:= 4;
      v_e_pa5_txt := 'SCK1';
      v_e_pa6_txt := 'MISO1';
      v_e_pa7_txt := 'MOSI1';
      pa5_set_pin;
      pa7_set_pin;
    end;
  end;

  if v_current_lqfp_pad = 'PA7' then begin
    if (tmp_pin_function <> 'SPI') and (v_c_spi1 = TRUE) then begin 
      reset_spi1_pins;
      v_e_pa5_txt :=  'LD2';
    end;  
    v_cb_pa7_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa7_txt := Form26.e_pin_label.Text;
    pa7_set_pin;
    if (tmp_pin_function = 'SPI') then begin
      v_c_spi1 := TRUE;
      v_cb_pa5_idx:= 1;
      v_cb_pa6_idx:= 4;
      v_e_pa5_txt := 'SCK1';
      v_e_pa6_txt := 'MISO1';
      v_e_pa7_txt := 'MOSI1';
      pa5_set_pin;
      pa6_set_pin;
    end;
  end;

  if v_current_lqfp_pad = 'PA8' then begin
    v_cb_pa8_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa8_txt := Form26.e_pin_label.Text;
    pa8_set_pin;
  end;

  if v_current_lqfp_pad = 'PA9' then begin
    v_cb_pa9_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa9_txt := Form26.e_pin_label.Text;
    pa9_set_pin;
  end;

  if v_current_lqfp_pad = 'PA10' then begin
    v_cb_pa10_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa10_txt := Form26.e_pin_label.Text;
    pa10_set_pin;
  end;

  if v_current_lqfp_pad = 'PA11' then begin
    v_cb_pa11_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa11_txt := Form26.e_pin_label.Text;
    pa11_set_pin;
  end;

  if v_current_lqfp_pad = 'PA12' then begin
    v_cb_pa12_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa12_txt := Form26.e_pin_label.Text;
    pa12_set_pin;
  end;

  if v_current_lqfp_pad = 'PA15' then begin
    v_cb_pa15_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pa15_txt := Form26.e_pin_label.Text;
    pa15_set_pin;
  end;
  
  { }

  if v_current_lqfp_pad = 'PB0' then begin
    v_cb_pb0_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb0_txt := Form26.e_pin_label.Text;
    pb0_set_pin;
  end;

  if v_current_lqfp_pad = 'PB1' then begin
    v_cb_pb1_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb1_txt := Form26.e_pin_label.Text;
    pb1_set_pin;
  end;

  if v_current_lqfp_pad = 'PB2' then begin
    v_cb_pb2_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb2_txt := Form26.e_pin_label.Text;
    pb2_set_pin;
  end;

  if v_current_lqfp_pad = 'PB3' then begin
    v_cb_pb3_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb3_txt := Form26.e_pin_label.Text;
    pb3_set_pin;
  end;

  if v_current_lqfp_pad = 'PB4' then begin
    v_cb_pb4_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb4_txt := Form26.e_pin_label.Text;
    pb4_set_pin;
  end;

  if v_current_lqfp_pad = 'PB5' then begin
    v_cb_pb5_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb5_txt := Form26.e_pin_label.Text;
    pb5_set_pin;
  end;


  if v_current_lqfp_pad = 'PB6' then begin
    if (tmp_pin_function <> 'I2C') and (v_c_i2c1_b = TRUE) then reset_i2c1_b_pins;
    v_cb_pb6_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb6_txt := Form26.e_pin_label.Text;
    pb6_set_pin;
    if (tmp_pin_function = 'I2C') then begin
      if v_c_i2c1_a then reset_i2c1_a_pins; {PB8-PB9}
      // activate I2C1
      v_c_i2c1 := TRUE;
      v_c_i2c1_b := TRUE;
      // set the pin PB9 as the other I2C1 pin
      v_cb_pb7_idx:= 2;
      v_e_pb6_txt := 'SCL1';
      v_e_pb7_txt := 'SDA1';
      if(v_e_pb6_txt = v_e_pb8_txt) then v_e_pb8_txt := '';
      if(v_e_pb7_txt = v_e_pb9_txt) then v_e_pb9_txt := '';
      pb7_set_pin;
    end;  
  end;

  if v_current_lqfp_pad = 'PB7' then begin
    if (tmp_pin_function <> 'I2C') and (v_c_i2c1_b = TRUE) then reset_i2c1_b_pins;
    v_cb_pb7_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb7_txt := Form26.e_pin_label.Text;
    pb7_set_pin;
    if (tmp_pin_function = 'I2C') then begin
      if v_c_i2c1_a then reset_i2c1_a_pins; {PB8-PB9}
      // activate I2C1
      v_c_i2c1 := TRUE;
      v_c_i2c1_b := TRUE;
      // set the pin PB9 as the other I2C1 pin
      v_cb_pb6_idx:= 2;
      v_e_pb6_txt := 'SCL1';
      v_e_pb7_txt := 'SDA1';
      if(v_e_pb6_txt = v_e_pb8_txt) then v_e_pb8_txt := '';
      if(v_e_pb7_txt = v_e_pb9_txt) then v_e_pb9_txt := '';
      pb6_set_pin;
    end;  
  end;

  if v_current_lqfp_pad = 'PB8' then begin
    //mitigate conflicts
    if (tmp_pin_function <> 'I2C') and (v_c_i2c1_a = TRUE) then reset_i2c1_a_pins;
    v_cb_pb8_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb8_txt := Form26.e_pin_label.Text;
    pb8_set_pin;
    if (tmp_pin_function = 'I2C') then begin
      if v_c_i2c1_b then reset_i2c1_b_pins; {PB6-PB7}
      // activate I2C1
      v_c_i2c1 := TRUE;
      v_c_i2c1_a := TRUE;
      // set the pin PB9 as the other I2C1 pin
      v_cb_pb9_idx:= 2;
      v_e_pb8_txt := 'SCL1';
      v_e_pb9_txt := 'SDA1';
      if(v_e_pb8_txt = v_e_pb6_txt) then v_e_pb6_txt := '';
      if(v_e_pb9_txt = v_e_pb7_txt) then v_e_pb7_txt := '';
      pb9_set_pin;
    end;  
  end;
  
  if v_current_lqfp_pad = 'PB9' then begin
    //mitigate conflicts
    if (tmp_pin_function <> 'I2C') and (v_c_i2c1_a = TRUE) then reset_i2c1_a_pins;
    v_cb_pb9_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb9_txt := Form26.e_pin_label.Text;
    pb9_set_pin;
    if (tmp_pin_function = 'I2C') then begin
      if v_c_i2c1_b then reset_i2c1_b_pins; {PB6-PB7}
      // activate I2C1
      v_c_i2c1 := TRUE;
      v_c_i2c1_a := TRUE;
      // set the pin PB8 as the other I2C1 pin
      v_cb_pb8_idx:= 1;
      v_e_pb8_txt := 'SCL1';
      v_e_pb9_txt := 'SDA1';
      if(v_e_pb8_txt = v_e_pb6_txt) then v_e_pb6_txt := '';
      if(v_e_pb9_txt = v_e_pb7_txt) then v_e_pb7_txt := '';
      pb8_set_pin;
    end;  
  end;

  if v_current_lqfp_pad = 'PB10' then begin
    if (tmp_pin_function <> 'I2C') and (v_c_i2c2 = TRUE) then reset_i2c2_pins;
    if ((tmp_pin_function = 'I2C') and (v_c_lcd4) and (v_lcd4_defaults)) then reset_default_lcd4_pins;
    v_cb_pb10_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb10_txt := Form26.e_pin_label.Text;
    pb10_set_pin;
    if (tmp_pin_function = 'I2C') then begin
      v_c_i2c2 := TRUE;
      v_cb_pb11_idx:= 2;
      v_e_pb10_txt := 'SCL2';
      v_e_pb11_txt := 'SDA2';
      pb11_set_pin;
    end;  
  end;

  if v_current_lqfp_pad = 'PB11' then begin
    if (tmp_pin_function <> 'I2C') and (v_c_i2c2 = TRUE) then reset_i2c2_pins;
    if ((tmp_pin_function = 'I2C') and (v_c_lcd4) and (v_lcd4_defaults)) then reset_default_lcd4_pins;
    v_cb_pb11_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb11_txt := Form26.e_pin_label.Text;
    pb11_set_pin;
    if (tmp_pin_function = 'I2C') then begin
      v_c_i2c2 := TRUE;
      v_cb_pb10_idx:= 1;
      v_e_pb10_txt := 'SCL2';
      v_e_pb11_txt := 'SDA2';
      pb10_set_pin;
    end;  
  end;

  if v_current_lqfp_pad = 'PB12' then begin
    v_cb_pb12_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb12_txt := Form26.e_pin_label.Text;
    pb12_set_pin;
  end;

  if v_current_lqfp_pad = 'PB13' then begin
    if (tmp_pin_function <> 'SPI') and (v_c_spi2 = TRUE) then reset_spi2_pins;
    v_cb_pb13_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb13_txt := Form26.e_pin_label.Text;
    pb13_set_pin;
    if (tmp_pin_function = 'SPI') then begin
      v_c_spi2 := TRUE;
      v_cb_pb14_idx:= 3;
      v_cb_pb15_idx:= 5;
      v_e_pb13_txt := 'SCK2';
      v_e_pb14_txt := 'MISO2';
      v_e_pb15_txt := 'MOSI2';
      pb14_set_pin;
      pb15_set_pin;
    end;  
  end;


  if v_current_lqfp_pad = 'PB14' then begin
    if (tmp_pin_function <> 'SPI') and (v_c_spi2 = TRUE) then reset_spi2_pins;
    v_cb_pb14_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb14_txt := Form26.e_pin_label.Text;
    pb14_set_pin;
    if (tmp_pin_function = 'SPI') then begin
      v_c_spi2 := TRUE;
      v_cb_pb13_idx:= 4;
      v_cb_pb15_idx:= 5;
      v_e_pb13_txt := 'SCK2';
      v_e_pb14_txt := 'MISO2';
      v_e_pb15_txt := 'MOSI2';
      pb13_set_pin;
      pb15_set_pin;
    end;  
  end;

  if v_current_lqfp_pad = 'PB15' then begin
    if (tmp_pin_function <> 'SPI') and (v_c_spi2 = TRUE) then reset_spi2_pins;
    v_cb_pb15_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pb15_txt := Form26.e_pin_label.Text;
    pb15_set_pin;
    if (tmp_pin_function = 'SPI') then begin
      v_c_spi2 := TRUE;
      v_cb_pb13_idx:= 4;
      v_cb_pb14_idx:= 3;
      v_e_pb13_txt := 'SCK2';
      v_e_pb14_txt := 'MISO2';
      v_e_pb15_txt := 'MOSI2';
      pb13_set_pin;
      pb14_set_pin;
    end;  
  end;

  { }

  if v_current_lqfp_pad = 'PC0' then begin
    v_cb_pc0_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc0_txt := Form26.e_pin_label.Text;
    pc0_set_pin;
  end;

  if v_current_lqfp_pad = 'PC1' then begin
    v_cb_pc1_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc1_txt := Form26.e_pin_label.Text;
    pc1_set_pin;
  end;

  if v_current_lqfp_pad = 'PC2' then begin
    v_cb_pc2_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc2_txt := Form26.e_pin_label.Text;
    pc2_set_pin;
  end;

  if v_current_lqfp_pad = 'PC3' then begin
    v_cb_pc3_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc3_txt := Form26.e_pin_label.Text;
    pc3_set_pin;
  end;

  if v_current_lqfp_pad = 'PC4' then begin
    v_cb_pc4_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc4_txt := Form26.e_pin_label.Text;
    pc4_set_pin;
  end;

  if v_current_lqfp_pad = 'PC5' then begin
    v_cb_pc5_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc5_txt := Form26.e_pin_label.Text;
    pc5_set_pin;
  end;

  if v_current_lqfp_pad = 'PC6' then begin
    v_cb_pc6_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc6_txt := Form26.e_pin_label.Text;
    pc6_set_pin;
  end;

  if v_current_lqfp_pad = 'PC7' then begin
    v_cb_pc7_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc7_txt := Form26.e_pin_label.Text;
    pc7_set_pin;
  end;

  if v_current_lqfp_pad = 'PC8' then begin
    v_cb_pc8_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc8_txt := Form26.e_pin_label.Text;
    pc8_set_pin;
  end;

  if v_current_lqfp_pad = 'PC9' then begin
    v_cb_pc9_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc9_txt := Form26.e_pin_label.Text;
    pc9_set_pin;
  end;

  if v_current_lqfp_pad = 'PC10' then begin
    if (tmp_pin_function <> 'SERIAL') and (v_c_uart4 = TRUE) then reset_uart4_pins;
    if (tmp_pin_function <> 'SPI') and (v_c_spi3 = TRUE) then reset_spi3_pins;
    v_cb_pc10_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc10_txt := Form26.e_pin_label.Text;
    pc10_set_pin;
    if (tmp_pin_function = 'SERIAL') then begin
      if v_c_spi3 then reset_spi3_pins;
      v_c_uart4 := TRUE;
      v_cb_pc11_idx:= 4;
      v_e_pc10_txt := 'U4_TX';
      v_e_pc11_txt := 'U4_RX';
      pc11_set_pin;
    end;
    if (tmp_pin_function = 'SPI') then begin
      if v_c_uart5 then reset_uart5_pins;
      if v_c_uart4 then reset_uart4_pins;
      v_c_spi3 := TRUE;
      v_cb_pc12_idx:= 2;
      v_cb_pc11_idx:= 2;
      v_e_pc10_txt := 'SCK3';
      v_e_pc11_txt := 'MISO3';
      v_e_pc12_txt := 'MOSI3';
      pc12_set_pin;
      pc11_set_pin;
    end;  
  end;

  if v_current_lqfp_pad = 'PC11' then begin
    if (tmp_pin_function <> 'SERIAL') and (v_c_uart4 = TRUE) then reset_uart4_pins;
    if (tmp_pin_function <> 'SPI') and (v_c_spi3 = TRUE) then reset_spi3_pins;
    v_cb_pc11_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc11_txt := Form26.e_pin_label.Text;
    pc11_set_pin;
    if (tmp_pin_function = 'SERIAL') then begin
      if v_c_spi3 then reset_spi3_pins;
      v_c_uart4 := TRUE;
      v_cb_pc10_idx:= 4;
      v_e_pc10_txt := 'U4_TX';
      v_e_pc11_txt := 'U4_RX';
      pc10_set_pin;
    end;
    if (tmp_pin_function = 'SPI') then begin
      if v_c_uart5 then reset_uart5_pins;
      if v_c_uart4 then reset_uart4_pins;
      v_c_spi3 := TRUE;
      v_cb_pc12_idx:= 2;
      v_cb_pc10_idx:= 2;
      v_e_pc10_txt := 'SCK3';
      v_e_pc11_txt := 'MISO3';
      v_e_pc12_txt := 'MOSI3';
      pc12_set_pin;
      pc10_set_pin;
    end;  
  end;

  if v_current_lqfp_pad = 'PC12' then begin
    if (tmp_pin_function <> 'SERIAL') and (v_c_uart5 = TRUE) then reset_uart5_pins;
    if (tmp_pin_function <> 'SPI') and (v_c_spi3 = TRUE) then reset_spi3_pins;
    v_cb_pc12_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc12_txt := Form26.e_pin_label.Text;
    pc12_set_pin;
    if (tmp_pin_function = 'SERIAL') then begin
      if v_c_spi3 then reset_spi3_pins;
      v_c_uart5 := TRUE;
      v_cb_pd2_idx:= 3;
      v_e_pc12_txt := 'U5_TX';
      v_e_pd2_txt := 'U5_RX';
      pd2_set_pin;
    end;
    if (tmp_pin_function = 'SPI') then begin
      if v_c_uart5 then reset_uart5_pins;
      if v_c_uart4 then reset_uart4_pins;
      v_c_spi3 := TRUE;
      v_cb_pc11_idx:= 2;
      v_cb_pc10_idx:= 2;
      v_e_pc10_txt := 'SCK3';
      v_e_pc11_txt := 'MISO3';
      v_e_pc12_txt := 'MOSI3';
      pc11_set_pin;
      pc10_set_pin;
    end;  
  end;


  { user button on nucleo board
  if v_current_lqfp_pad = 'PC13' then begin
    v_cb_pc13_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pc13_txt := Form26.e_pin_label.Text;
    pc13_set_pin;
  end;
  }
  { }

  if v_current_lqfp_pad = 'PD2' then begin
    if (tmp_pin_function <> 'SERIAL') and (v_c_uart5 = TRUE) then reset_uart5_pins;
    v_cb_pd2_idx := Form26.cb_pin_function.ItemIndex; 
    v_e_pd2_txt := Form26.e_pin_label.Text;
    pd2_set_pin;
    if (tmp_pin_function = 'SERIAL') then begin
      if v_c_spi3 then reset_spi3_pins;
      v_c_uart5 := TRUE;
      v_cb_pc12_idx:= 4;
      v_e_pc12_txt := 'U5_TX';
      v_e_pd2_txt := 'U5_RX';
      pc12_set_pin;
    end;
  end;
  close;
end;

procedure TForm26.cb_pin_functionChange(Sender: TObject);
begin
  // figure the pin function first
  tmp_pin_function := figure_pin_function(cb_pin_function.Text);
  // then set the below items accordingly
  pin_preliminary_set(tmp_pin_function);
  //
  CB_GPIO_O_LVL.ItemIndex := b_pin_o_lvl_idx;
  CB_GPIO_O_MODE.ItemIndex := b_pin_o_mode_idx;
  CB_GPIO_I_MODE.ItemIndex := b_pin_i_mode_idx;
  CB_GPIO_A_MODE.ItemIndex := b_pin_a_mode_idx;
  CB_GPIO_I2C_MODE.ItemIndex := b_pin_i2c_mode_idx;
  CB_GPIO_SPI_MODE.ItemIndex := b_pin_spi_mode_idx;
  CB_GPIO_INT_MODE.ItemIndex := b_pin_int_mode_idx;
  CB_GPIO_PULL_UPDOWN.ItemIndex := b_pin_pull_updown_idx;
  CB_GPIO_SPEED.ItemIndex := b_pin_speed_idx;
  // OnPaint event doesn't work well in this case...
  if CB_GPIO_O_LVL.Text = 'N/A' then CB_GPIO_O_LVL.Enabled:=FALSE
  else CB_GPIO_O_LVL.Enabled:=TRUE;
  if CB_GPIO_O_MODE.Text = 'N/A' then CB_GPIO_O_MODE.Enabled:=FALSE
  else CB_GPIO_O_MODE.Enabled:=TRUE;
  if CB_GPIO_INT_MODE.Text = 'N/A' then CB_GPIO_INT_MODE.Enabled:=FALSE
  else CB_GPIO_INT_MODE.Enabled:=TRUE;
  if CB_GPIO_PULL_UPDOWN.Text = 'N/A' then CB_GPIO_PULL_UPDOWN.Enabled:=FALSE
  else CB_GPIO_PULL_UPDOWN.Enabled:=TRUE;
  if CB_GPIO_SPEED.Text = 'N/A' then CB_GPIO_SPEED.Enabled:=FALSE
  else CB_GPIO_SPEED.Enabled:=TRUE;
  if (cb_pin_function.ItemIndex <= 0) and (tmp_pin_function = 'N/A') and (v_current_lqfp_pad<>'PA5')
    then L_GPIO_MSG.Caption:='Pin has no function assigned!';
  if (cb_pin_function.ItemIndex > 0) and (tmp_pin_function = 'N/A')
    then L_GPIO_MSG.Caption:='Function not supported, choose another!';
  if (cb_pin_function.ItemIndex >= 0) and (tmp_pin_function <> 'N/A')
    then L_GPIO_MSG.Caption:='Pin set as '+tmp_pin_function+'.';
  if (cb_pin_function.ItemIndex <= 0) and (e_pin_label.Text <> '') and (v_current_lqfp_pad<>'PA5')
    then e_pin_label.Text := '';
end;


end.

