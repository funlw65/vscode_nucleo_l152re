unit pinout_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Buttons, data_unit, (* gpio_unit, utils_unit,*) code_unit, lqfp64pinsetup_unit;

type

  { TForm2 }

  TForm2 = class(TForm)
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label3: TLabel;
    Label30: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    L_AVDD: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label4: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label5: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    L_PC13: TLabel;
    L_PC14: TLabel;
    L_PC15: TLabel;
    L_PC16: TLabel;
    L_PC17: TLabel;
    L_PC18: TLabel;
    L_PC19: TLabel;
    L_PC20: TLabel;
    L_PC21: TLabel;
    L_PC22: TLabel;
    L_PC23: TLabel;
    L_PC24: TLabel;
    L_PC25: TLabel;
    L_PC26: TLabel;
    L_PC27: TLabel;
    L_PC28: TLabel;
    L_PC29: TLabel;
    L_PC30: TLabel;
    L_PC31: TLabel;
    L_PC32: TLabel;
    L_PC33: TLabel;
    L_PC34: TLabel;
    L_PC35: TLabel;
    L_PC36: TLabel;
    L_PC37: TLabel;
    L_PC38: TLabel;
    L_PC39: TLabel;
    L_PC40: TLabel;
    L_PC41: TLabel;
    L_PC42: TLabel;
    L_PD10: TLabel;
    L_PD3: TLabel;
    L_PD4: TLabel;
    L_PD5: TLabel;
    L_PD6: TLabel;
    L_PD7: TLabel;
    L_PD8: TLabel;
    L_PD9: TLabel;
    Panel1: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    P_PB9: TPanel;
    P_PC5: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel2: TPanel;
    P_PA15: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    P_PB7: TPanel;
    P_PC13: TPanel;
    Panel28: TPanel;
    Panel29: TPanel;
    Panel3: TPanel;
    Panel30: TPanel;
    P_PA0: TPanel;
    Panel32: TPanel;
    Panel33: TPanel;
    Panel34: TPanel;
    P_PC2: TPanel;
    P_PC3: TPanel;
    P_PA1: TPanel;
    P_PA4: TPanel;
    P_PB0: TPanel;
    Panel4: TPanel;
    P_PC1: TPanel;
    P_PC0: TPanel;
    Panel42: TPanel;
    Panel43: TPanel;
    Panel44: TPanel;
    P_PA12: TPanel;
    P_PA11: TPanel;
    P_PB12: TPanel;
    P_PB11: TPanel;
    Panel49: TPanel;
    Panel5: TPanel;
    P_PB2: TPanel;
    P_PB1: TPanel;
    P_PB15: TPanel;
    P_PB14: TPanel;
    P_PB13: TPanel;
    Panel55: TPanel;
    P_PC4: TPanel;
    Panel57: TPanel;
    Panel58: TPanel;
    P_PA5: TPanel;
    Panel6: TPanel;
    P_PA6: TPanel;
    P_PA7: TPanel;
    P_PB6: TPanel;
    P_PC7: TPanel;
    P_PA9: TPanel;
    P_PA8: TPanel;
    P_PB10: TPanel;
    P_PB4: TPanel;
    P_PB5: TPanel;
    P_PB3: TPanel;
    Panel7: TPanel;
    P_PA10: TPanel;
    P_PA2: TPanel;
    P_PA3: TPanel;
    Panel73: TPanel;
    Panel74: TPanel;
    Panel75: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    P_PC10: TPanel;
    P_PC11: TPanel;
    P_PC12: TPanel;
    P_PC9: TPanel;
    P_PC8: TPanel;
    P_PB8: TPanel;
    P_PD2: TPanel;
    P_PC6: TPanel;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    StaticText6: TStaticText;
    StaticText7: TStaticText;
    Timer1: TTimer;
    procedure Image3Click(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure Label81Click(Sender: TObject);
    procedure Label82Click(Sender: TObject);
    procedure Label83Click(Sender: TObject);
    procedure Label84Click(Sender: TObject);
    procedure Label85Click(Sender: TObject);
    procedure Label86Click(Sender: TObject);
    procedure Label87Click(Sender: TObject);
    procedure Label88Click(Sender: TObject);
    procedure Label90Click(Sender: TObject);
    procedure Label91Click(Sender: TObject);
    procedure Label92Click(Sender: TObject);
    procedure Label93Click(Sender: TObject);
    procedure Label94Click(Sender: TObject);
    procedure Label95Click(Sender: TObject);
    procedure Label96Click(Sender: TObject);
    procedure Label97Click(Sender: TObject);
    procedure paint_labels(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure P_PA0Click(Sender: TObject);
    procedure P_PA10Click(Sender: TObject);
    procedure P_PA11Click(Sender: TObject);
    procedure P_PA12Click(Sender: TObject);
    procedure P_PA15Click(Sender: TObject);
    procedure P_PA1Click(Sender: TObject);
    procedure P_PA4Click(Sender: TObject);
    procedure P_PA5Click(Sender: TObject);
    procedure P_PA6Click(Sender: TObject);
    procedure P_PA7Click(Sender: TObject);
    procedure P_PA8Click(Sender: TObject);
    procedure P_PA9Click(Sender: TObject);
    procedure P_PB0Click(Sender: TObject);
    procedure P_PB10Click(Sender: TObject);
    procedure P_PB11Click(Sender: TObject);
    procedure P_PB12Click(Sender: TObject);
    procedure P_PB13Click(Sender: TObject);
    procedure P_PB14Click(Sender: TObject);
    procedure P_PB15Click(Sender: TObject);
    procedure P_PB1Click(Sender: TObject);
    procedure P_PB2Click(Sender: TObject);
    procedure P_PB3Click(Sender: TObject);
    procedure P_PB4Click(Sender: TObject);
    procedure P_PB5Click(Sender: TObject);
    procedure P_PB6Click(Sender: TObject);
    procedure P_PB7Click(Sender: TObject);
    procedure P_PB8Click(Sender: TObject);
    procedure P_PB9Click(Sender: TObject);
    procedure P_PC0Click(Sender: TObject);
    procedure P_PC10Click(Sender: TObject);
    procedure P_PC11Click(Sender: TObject);
    procedure P_PC12Click(Sender: TObject);
    procedure P_PC1Click(Sender: TObject);
    procedure P_PC2Click(Sender: TObject);
    procedure P_PC3Click(Sender: TObject);
    procedure P_PC4Click(Sender: TObject);
    procedure P_PC5Click(Sender: TObject);
    procedure P_PC6Click(Sender: TObject);
    procedure P_PC7Click(Sender: TObject);
    procedure P_PC8Click(Sender: TObject);
    procedure P_PC9Click(Sender: TObject);
    procedure P_PD2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form2: TForm2;

implementation

var blink_index : byte;
{$R *.lfm}

{ TForm2 }


{=========== PA ============================}

procedure TForm2.P_PA0Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa0_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA10Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa10_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA11Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa11_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA12Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa12_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA15Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa15_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA1Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa1_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA4Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa4_id;
  Form26.ShowModal;
end;


procedure TForm2.P_PA5Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa5_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA6Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa6_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA7Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa7_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA8Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa8_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PA9Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa9_id;
  Form26.ShowModal;
end;

{=========== PB ============================}

procedure TForm2.P_PB0Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb0_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB10Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb10_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB11Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb11_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB12Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb12_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB13Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb13_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB14Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb14_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB15Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb15_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB1Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb1_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB2Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb2_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB3Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb3_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB4Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb4_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB5Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb5_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB6Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb6_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB7Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb7_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB8Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb8_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PB9Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb9_id;
  Form26.ShowModal;
end;

{=========== PC ============================}

procedure TForm2.P_PC0Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc0_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC10Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc10_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC11Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc11_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC12Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc12_id;
  Form26.ShowModal;
end;


procedure TForm2.P_PC1Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc1_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC2Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc2_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC3Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc3_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC4Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc4_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC5Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc5_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC6Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc6_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC7Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc7_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC8Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc8_id;
  Form26.ShowModal;
end;

procedure TForm2.P_PC9Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc9_id;
  Form26.ShowModal;
end;

{=========== PD ============================}

procedure TForm2.P_PD2Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pd2_id;
  Form26.ShowModal;
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  blink_index := 0;
  Cursor := crDefault;
  Form2.FormPaint(Form2);
end;

//------------------------------------------------------------------------------
procedure TForm2.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm2.FormClose(Sender: TObject);
begin
  scan_for_analog;
  scan_for_adin;
end;


procedure TForm2.paint_labels(Sender: TObject);
begin
  if v_cb_pB4_idx <> 0 then P_PB4.Hint := v_e_pB4_txt
  else P_PB4.Hint := '';
  if v_cb_pB3_idx <> 0 then P_PB3.Hint := v_e_pB3_txt
  else P_PB3.Hint := '';
  if v_cb_pD2_idx <> 0 then P_PD2.Hint := v_e_pD2_txt
  else P_PD2.Hint := '';
  if v_cb_pC12_idx <> 0 then P_PC12.Hint := v_e_pC12_txt
  else P_PC12.Hint := '';
  if v_cb_pC11_idx <> 0 then P_PC11.Hint := v_e_pC11_txt
  else P_PC11.Hint := '';
  if v_cb_pC10_idx <> 0 then P_PC10.Hint := v_e_pC10_txt
  else P_PC10.Hint := '';
  if v_cb_pA15_idx <> 0 then P_PA15.Hint := v_e_pA15_txt
  else P_PA15.Hint := '';
  if v_cb_pA4_idx <> 0 then  P_PA4.Hint := v_e_pA4_txt
  else P_PA4.Hint := '';
  if v_cb_pA5_idx <> 0 then  P_PA5.Hint := v_e_pA5_txt
  else P_PA5.Hint := '';
  if v_cb_pA6_idx <> 0 then  P_PA6.Hint := v_e_pA6_txt
  else P_PA6.Hint := '';
  if v_cb_pA7_idx <> 0 then  P_PA7.Hint := v_e_pA7_txt
  else P_PA7.Hint := '';
  if v_cb_pC4_idx <> 0 then  P_PC4.Hint := v_e_pC4_txt
  else P_PC4.Hint := '';
  if v_cb_pC5_idx <> 0 then  P_PC5.Hint := v_e_pC5_txt
  else P_PC5.Hint := '';
  if v_cb_pB0_idx <> 0 then  P_PB0.Hint := v_e_pB0_txt
  else P_PB0.Hint := '';
  if v_cb_pB1_idx <> 0 then  P_PB1.Hint := v_e_pB1_txt
  else P_PB1.Hint := '';
  if v_cb_pB2_idx <> 0 then  P_PB2.Hint := v_e_pB2_txt
  else P_PB2.Hint := '';
  if v_cb_pB10_idx <> 0 then  P_PB10.Hint := v_e_pB10_txt
  else P_PB10.Hint := '';
  if v_cb_pB11_idx <> 0 then  P_PB11.Hint := v_e_pB11_txt
  else P_PB11.Hint := '';
  if v_cb_pA12_idx <> 0 then  P_PA12.Hint := v_e_pA12_txt
  else P_PA12.Hint := '';
  if v_cb_pA11_idx <> 0 then  P_PA11.Hint := v_e_pA11_txt
  else P_PA11.Hint := '';
  if v_cb_pA10_idx <> 0 then  P_PA10.Hint := v_e_pA10_txt
  else P_PA10.Hint := '';
  if v_cb_pB9_idx <> 0 then  P_PB9.Hint := v_e_pB9_txt
  else P_PB9.Hint := '';
  if v_cb_pA9_idx <> 0 then  P_PA9.Hint := v_e_pA9_txt
  else P_PA9.Hint := '';
  if v_cb_pA8_idx <> 0 then  P_PA8.Hint := v_e_pA8_txt
  else P_PA8.Hint := '';
  if v_cb_pC9_idx <> 0 then  P_PC9.Hint := v_e_pC9_txt
  else P_PC9.Hint := '';
  if v_cb_pC8_idx <> 0 then  P_PC8.Hint := v_e_pC8_txt
  else P_PC8.Hint := '';
  if v_cb_pC7_idx <> 0 then  P_PC7.Hint := v_e_pC7_txt
  else P_PC7.Hint := '';
  if v_cb_pC6_idx <> 0 then  P_PC6.Hint := v_e_pC6_txt
  else P_PC6.Hint := '';
  if v_cb_pB15_idx <> 0 then  P_PB15.Hint := v_e_pB15_txt
  else P_PB15.Hint := '';
  if v_cb_pB14_idx <> 0 then  P_PB14.Hint := v_e_pB14_txt
  else P_PB14.Hint := '';
  if v_cb_pB13_idx <> 0 then  P_PB13.Hint := v_e_pB13_txt
  else P_PB13.Hint := '';
  if v_cb_pB12_idx <> 0 then  P_PB12.Hint := v_e_pB12_txt
  else P_PB12.Hint := '';
  if v_cb_pB8_idx <> 0 then  P_PB8.Hint := v_e_pB8_txt
  else P_PB8.Hint := '';
  if v_cb_pC0_idx <> 0 then  P_PC0.Hint := v_e_pC0_txt
  else P_PC0.Hint := '';
  if v_cb_pC1_idx <> 0 then  P_PC1.Hint := v_e_pC1_txt
  else P_PC1.Hint := '';
  if v_cb_pC2_idx <> 0 then  P_PC2.Hint := v_e_pC2_txt
  else P_PC2.Hint := '';
  if v_cb_pC3_idx <> 0 then  P_PC3.Hint := v_e_pC3_txt
  else P_PC3.Hint := '';
  if v_cb_pA0_idx <> 0 then  P_PA0.Hint := v_e_pA0_txt
  else P_PA0.Hint := '';
  if v_cb_pA1_idx <> 0 then  P_PA1.Hint := v_e_pA1_txt
  else P_PA1.Hint := '';
  if v_cb_pB7_idx <> 0 then  P_PB7.Hint := v_e_pB7_txt
  else P_PB7.Hint := '';
  if v_cb_pB6_idx <> 0 then  P_PB6.Hint := v_e_pB6_txt
  else P_PB6.Hint := '';
  if v_cb_pB5_idx <> 0 then  P_PB5.Hint := v_e_pB5_txt
  else P_PB5.Hint := '';  
end;

procedure TForm2.Label81Click(Sender: TObject);
begin
  // flash SPI1 pins
  Cursor := crHourGlass;
  blink_index := 1;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Image3Click(Sender: TObject);
begin
  // LED LD2  (PA5)
  Cursor := crHourGlass;
  blink_index := 17;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Image4Click(Sender: TObject);
begin
  // B1 User Button (PC13)
  Cursor := crHourGlass;
  blink_index := 18;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label82Click(Sender: TObject);
begin
  // flash SPI2 pins
  Cursor := crHourGlass;
  blink_index := 2;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label83Click(Sender: TObject);
begin
  // flash SPI3 pins
  Cursor := crHourGlass;
  blink_index := 3;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label84Click(Sender: TObject);
begin
  // flash I2C1 pins
  Cursor := crHourGlass;
  blink_index := 6;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label85Click(Sender: TObject);
begin
  // flash I2C2 pins
  Cursor := crHourGlass;
  blink_index := 7;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label86Click(Sender: TObject);
begin
  // flash USART1 pins
  Cursor := crHourGlass;
  blink_index := 8;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label87Click(Sender: TObject);
begin
  // flash UART4 pins
  Cursor := crHourGlass;
  blink_index := 4;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label88Click(Sender: TObject);
begin
  // flash UART5 pins
  Cursor := crHourGlass;
  blink_index := 5;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label90Click(Sender: TObject);
begin
  // flash USB pins
  Cursor := crHourGlass;
  blink_index := 9;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label91Click(Sender: TObject);
begin
  // flash TIM2 pins
  Cursor := crHourGlass;
  blink_index := 10;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label92Click(Sender: TObject);
begin
  // flash TIM3 pins
  Cursor := crHourGlass;
  blink_index := 11;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label93Click(Sender: TObject);
begin
  // flash TIM4 pins
  Cursor := crHourGlass;
  blink_index := 12;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label94Click(Sender: TObject);
begin
  // flash TIM5 pins
  Cursor := crHourGlass;
  blink_index := 13;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label95Click(Sender: TObject);
begin
  // flash TIM9 pins
  Cursor := crHourGlass;
  blink_index := 14;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label96Click(Sender: TObject);
begin
  // flash TIM10 pins
  Cursor := crHourGlass;
  blink_index := 15;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.Label97Click(Sender: TObject);
begin
  // flash TIM11 pins
  Cursor := crHourGlass;
  blink_index := 16;
  Timer1.Enabled := True;
  Form2.FormPaint(Form2);
end;

procedure TForm2.FormPaint(Sender: TObject);
begin
  if blink_index = 0 then Timer1.Enabled := False;
  case blink_index of
    1: // SPI1
      begin
        P_PA5.Color := clYellow;
        P_PA6.Color := clYellow;
        P_PA7.Color := clYellow;
      end;
    2: // SPI2
      begin
        P_PB13.Color := clYellow;
        P_PB14.Color := clYellow;
        P_PB15.Color := clYellow;
      end;
    3: // SPI3
      begin
        P_PC10.Color := clYellow;
        P_PC11.Color := clYellow;
        P_PC12.Color := clYellow;
      end;
    4: // UART4
      begin
        P_PC10.Color := clYellow;
        P_PC11.Color := clYellow;
      end;
    5: // UART5
      begin
        P_PC12.Color := clYellow;
        P_PD2.Color := clYellow;
      end;
    6: // I2C1
      begin
        P_PB6.Color := clYellow;
        P_PB7.Color := clYellow;
        P_PB8.Color := clYellow;
        P_PB9.Color := clYellow;
      end;
    7: // I2C2
      begin
        P_PB10.Color := clYellow;
        P_PB11.Color := clYellow;
      end;
    8: // USART1
      begin
        P_PA8.Color := clYellow;
        P_PA9.Color := clYellow;
        P_PA10.Color := clYellow;
        P_PA11.Color := clYellow;
        P_PA12.Color := clYellow;
      end;
    9: // USB
      begin
        P_PA11.Color := clYellow;
        P_PA12.Color := clYellow;
      end;
    10: // TOM2
      begin
        P_PA15.Color := clYellow;
        P_PB3.Color := clYellow;
        P_PB10.Color := clYellow;
        P_PB11.Color := clYellow;
      end;
    11: // TIM3
      begin
        P_PA6.Color := clYellow;
        P_PA7.Color := clYellow;
        P_PB0.Color := clYellow;
        P_PB1.Color := clYellow;
      end;
    12: // TIM4
      begin
        P_PB6.Color := clYellow;
        P_PB7.Color := clYellow;
        P_PB8.Color := clYellow;
        P_PB9.Color := clYellow;
      end;
    13: // TIM5
      begin
        P_PA0.Color := clYellow;
        P_PA1.Color := clYellow;
      end;
    14: // TIM9
      begin
        P_PB13.Color := clYellow;
        P_PB14.Color := clYellow;
      end;
    15: // TIM10
      begin
        P_PB12.Color := clYellow;
      end;
    16: // TIM11
      begin
        P_PB15.Color := clYellow;
      end;
    17: // LED LD2
      begin
        P_PA5.Color := clYellow;
      end;
    18: // B1 User Button
      begin
        P_PC13.Color := clYellow;
      end;
  end;
  if v_cb_pB4_idx <> 0 then P_PB4.Color := clGreen
  else P_PB4.Color := clGray;
  if blink_index = 0 then if v_cb_pB3_idx <> 0 then P_PB3.Color := clGreen
  else P_PB3.Color := clGray;
  if blink_index = 0 then if v_cb_pD2_idx <> 0 then P_PD2.Color := clGreen
  else P_PD2.Color := clGray;
  if blink_index = 0 then if v_cb_pC12_idx <> 0 then P_PC12.Color := clGreen
  else P_PC12.Color := clGray;
  if blink_index = 0 then if v_cb_pC11_idx <> 0 then P_PC11.Color := clGreen
  else P_PC11.Color := clGray;
  if blink_index = 0 then if v_cb_pC10_idx <> 0 then P_PC10.Color := clGreen
  else P_PC10.Color := clGray;
  if blink_index = 0 then if v_cb_pA15_idx <> 0 then P_PA15.Color := clGreen
  else P_PA15.Color := clGray;
  if v_cb_pA4_idx <> 0 then  P_PA4.Color := clGreen
  else P_PA4.Color := clGray;

  if blink_index = 0 then P_PA5.Color := clGreen; // special case

  if blink_index = 0 then if v_cb_pA6_idx <> 0 then  P_PA6.Color := clGreen
  else P_PA6.Color := clGray;
  if blink_index = 0 then if v_cb_pA7_idx <> 0 then  P_PA7.Color := clGreen
  else P_PA7.Color := clGray;
  if v_cb_pC4_idx <> 0 then  P_PC4.Color := clGreen
  else P_PC4.Color := clGray;
  if v_cb_pC5_idx <> 0 then  P_PC5.Color := clGreen
  else P_PC5.Color := clGray;
  if blink_index = 0 then if v_cb_pB0_idx <> 0 then  P_PB0.Color := clGreen
  else P_PB0.Color := clGray;
  if blink_index = 0 then if v_cb_pB1_idx <> 0 then  P_PB1.Color := clGreen
  else P_PB1.Color := clGray;
  if v_cb_pB2_idx <> 0 then  P_PB2.Color := clGreen
  else P_PB2.Color := clGray;
  if blink_index = 0 then if v_cb_pB10_idx <> 0 then  P_PB10.Color := clGreen
  else P_PB10.Color := clGray;
  if blink_index = 0 then if v_cb_pB11_idx <> 0 then  P_PB11.Color := clGreen
  else P_PB11.Color := clGray;
  if blink_index = 0 then if v_cb_pA12_idx <> 0 then  P_PA12.Color := clGreen
  else P_PA12.Color := clGray;
  if blink_index = 0 then if v_cb_pA11_idx <> 0 then  P_PA11.Color := clGreen
  else P_PA11.Color := clGray;
  if blink_index = 0 then if v_cb_pA10_idx <> 0 then  P_PA10.Color := clGreen
  else P_PA10.Color := clGray;
  if blink_index = 0 then if v_cb_pB9_idx <> 0 then  P_PB9.Color := clGreen
  else P_PB9.Color := clGray;
  if blink_index = 0 then if v_cb_pA9_idx <> 0 then  P_PA9.Color := clGreen
  else P_PA9.Color := clGray;
  if blink_index = 0 then if v_cb_pA8_idx <> 0 then  P_PA8.Color := clGreen
  else P_PA8.Color := clGray;
  if v_cb_pC9_idx <> 0 then  P_PC9.Color := clGreen
  else P_PC9.Color := clGray;
  if v_cb_pC8_idx <> 0 then  P_PC8.Color := clGreen
  else P_PC8.Color := clGray;
  if v_cb_pC7_idx <> 0 then  P_PC7.Color := clGreen
  else P_PC7.Color := clGray;
  if v_cb_pC6_idx <> 0 then  P_PC6.Color := clGreen
  else P_PC6.Color := clGray;
  if blink_index = 0 then if v_cb_pB15_idx <> 0 then  P_PB15.Color := clGreen
  else P_PB15.Color := clGray;
  if blink_index = 0 then if v_cb_pB14_idx <> 0 then  P_PB14.Color := clGreen
  else P_PB14.Color := clGray;
  if blink_index = 0 then if v_cb_pB13_idx <> 0 then  P_PB13.Color := clGreen
  else P_PB13.Color := clGray;
  if blink_index = 0 then if v_cb_pB12_idx <> 0 then  P_PB12.Color := clGreen
  else P_PB12.Color := clGray;
  if blink_index = 0 then if v_cb_pB8_idx <> 0 then  P_PB8.Color := clGreen
  else P_PB8.Color := clGray;
  if v_cb_pC0_idx <> 0 then  P_PC0.Color := clGreen
  else P_PC0.Color := clGray;
  if v_cb_pC1_idx <> 0 then  P_PC1.Color := clGreen
  else P_PC1.Color := clGray;
  if v_cb_pC2_idx <> 0 then  P_PC2.Color := clGreen
  else P_PC2.Color := clGray;
  if v_cb_pC3_idx <> 0 then  P_PC3.Color := clGreen
  else P_PC3.Color := clGray;
  if blink_index = 0 then if v_cb_pA0_idx <> 0 then  P_PA0.Color := clGreen
  else P_PA0.Color := clGray;
  if blink_index = 0 then if v_cb_pA1_idx <> 0 then  P_PA1.Color := clGreen
  else P_PA1.Color := clGray;
  if blink_index = 0 then if v_cb_pB7_idx <> 0 then  P_PB7.Color := clGreen
  else P_PB7.Color := clGray;
  if blink_index = 0 then if v_cb_pB6_idx <> 0 then  P_PB6.Color := clGreen
  else P_PB6.Color := clGray;
  if v_cb_pB5_idx <> 0 then  P_PB5.Color := clGreen
  else P_PB5.Color := clGray;
  // special case
  if blink_index = 0 then P_PC13.Color := clTeal;
  paint_labels(Form2);
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  paint_labels(Form2);
end;

begin
  blink_index := 0;



end.

