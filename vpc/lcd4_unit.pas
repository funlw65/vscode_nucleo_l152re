unit lcd4_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, ExtCtrls, data_unit;

type

  { TForm20 }

  TForm20 = class(TForm)
    BitBtn1: TBitBtn;
    CheckBox1: TCheckBox;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Memo1: TMemo;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    procedure FormShow(Sender: TObject);
    procedure lcd_set_default_pins;
    procedure lcd_reset_default_pins;
    procedure BitBtn1Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure RadioButton2Change(Sender: TObject);

  private

  public

  end;

var
  Form20: TForm20;

procedure find_and_erase_label(lbl:string);

implementation

{$R *.lfm}

procedure find_and_erase_label(lbl:string);
begin
  {PC10}
  if v_e_pc10_txt = lbl then reset_pc10_pin;
  {PC12}
  if v_e_pc12_txt = lbl then reset_pc12_pin;
  {PA15}
  if v_e_pa15_txt = lbl then reset_pa15_pin;
  {PB7}
  if v_e_pb7_txt = lbl then reset_pb7_pin;
  {PC2}
  if v_e_pc2_txt = lbl then reset_pc2_pin;
  {PC3}
  if v_e_pc3_txt = lbl then reset_pc3_pin;
  {-------------------------------------------}
  {PC11}
  if v_e_pc11_txt = lbl then reset_pc11_pin;
  {PD2}
  if v_e_pd2_txt = lbl then reset_pd2_pin;
  {PA0}
  if v_e_pa0_txt = lbl then reset_pa0_pin;
  {PA1}
  if v_e_pa1_txt = lbl then reset_pa1_pin;
  {PA4}
  if v_e_pa4_txt = lbl then reset_pa4_pin;
  {PB0}
  if v_e_pb0_txt = lbl then reset_pb0_pin;
  {PC1}
  if v_e_pc1_txt = lbl then reset_pc1_pin;
  {PC0}
  if v_e_pc0_txt = lbl then reset_pc0_pin;
  {--------------------------------------------}
  {PC9}
  if v_e_pc9_txt = lbl then reset_pc9_pin;
  {PB8}
  if v_e_pb8_txt = lbl then reset_pb8_pin;
  {PB9}
  if v_e_pb9_txt = lbl then reset_pb9_pin;
  {PA6}
  if v_e_pa6_txt = lbl then reset_pa6_pin;
  {PA7}
  if v_e_pa7_txt = lbl then reset_pa7_pin;
  {PB6}
  if v_e_pb6_txt = lbl then reset_pb6_pin;
  {PC7}
  if v_e_pc7_txt = lbl then reset_pc7_pin;
  {PA9}
  if v_e_pa9_txt = lbl then reset_pa9_pin;
  {--------------------------------------------}
  {PC8}
  if v_e_pc8_txt = lbl then reset_pc8_pin;
  {PC6}
  if v_e_pc6_txt = lbl then reset_pc6_pin;
  {PC5}
  if v_e_pc5_txt = lbl then reset_pc5_pin;
  {PA12}
  if v_e_pa12_txt = lbl then reset_pa12_pin;
  {PA11}
  if v_e_pa11_txt = lbl then reset_pa11_pin;
  {PB12}
  if v_e_pb12_txt = lbl then reset_pb12_pin;
  {PB11}
  if v_e_pb11_txt = lbl then reset_pb11_pin;
  {PB2}
  if v_e_pb2_txt = lbl then reset_pb2_pin;
  {PB1}
  if v_e_pb1_txt = lbl then reset_pb1_pin;
  {PB15}
  if v_e_pb15_txt = lbl then reset_pb15_pin;
  {PB14}
  if v_e_pb14_txt = lbl then reset_pb14_pin;
  {PB13}
  if v_e_pb13_txt = lbl then reset_pb13_pin;
  {PC4}
  if v_e_pc4_txt = lbl then reset_pc4_pin;
end;



{ TForm20 }

procedure TForm20.lcd_set_default_pins;
begin
  v_lcd4_defaults := TRUE;

  {PA10}
  v_cb_pa10_idx:= 5;
  v_e_pa10_txt := 'LCD_RS';
  v_pa10_func := 'OUTPUT';
  v_pa10_o_lvl_idx := 1;
  v_pa10_o_mode_idx := 1;
  v_pa10_i_mode_idx := 0;
  v_pa10_a_mode_idx := 0;
  v_pa10_i2c_mode_idx := 0;
  v_pa10_spi_mode_idx := 0;
  v_pa10_int_mode_idx := 0;
  v_pa10_pull_updown_idx := 1;
  v_pa10_speed_idx := 2;
  find_and_erase_label('LCD_RS'); {except default LCD4 pins}

  {PB3}
  v_cb_pb3_idx:= 8;
  v_e_pb3_txt := 'LCD_E';
  v_pb3_func := 'OUTPUT';
  v_pb3_o_lvl_idx := 1;
  v_pb3_o_mode_idx := 1;
  v_pb3_i_mode_idx := 0;
  v_pb3_a_mode_idx := 0;
  v_pb3_i2c_mode_idx := 0;
  v_pb3_spi_mode_idx := 0;
  v_pb3_int_mode_idx := 0;
  v_pb3_pull_updown_idx := 1;
  v_pb3_speed_idx := 2;
  find_and_erase_label('LCD_E'); {except default LCD4 pins}

  {PB5}
  v_cb_pb5_idx:= 9;
  v_e_pb5_txt := 'LCD_D4';
  v_pb5_func := 'OUTPUT';
  v_pb5_o_lvl_idx := 1;
  v_pb5_o_mode_idx := 1;
  v_pb5_i_mode_idx := 0;
  v_pb5_a_mode_idx := 0;
  v_pb5_i2c_mode_idx := 0;
  v_pb5_spi_mode_idx := 0;
  v_pb5_int_mode_idx := 0;
  v_pb5_pull_updown_idx := 1;
  v_pb5_speed_idx := 2;
  find_and_erase_label('LCD_D4'); {except default LCD4 pins}

  {PB4}
  v_cb_pb4_idx:= 8;
  v_e_pb4_txt := 'LCD_D5';
  v_pb4_func := 'OUTPUT';
  v_pb4_o_lvl_idx := 1;
  v_pb4_o_mode_idx := 1;
  v_pb4_i_mode_idx := 0;
  v_pb4_a_mode_idx := 0;
  v_pb4_i2c_mode_idx := 0;
  v_pb4_spi_mode_idx := 0;
  v_pb4_int_mode_idx := 0;
  v_pb4_pull_updown_idx := 1;
  v_pb4_speed_idx := 2;
  find_and_erase_label('LCD_D5'); {except default LCD4 pins}

  {PB10}
  v_cb_pb10_idx:= 5;
  v_e_pb10_txt := 'LCD_D6';
  v_pb10_func := 'OUTPUT';
  v_pb10_o_lvl_idx := 1;
  v_pb10_o_mode_idx := 1;
  v_pb10_i_mode_idx := 0;
  v_pb10_a_mode_idx := 0;
  v_pb10_i2c_mode_idx := 0;
  v_pb10_spi_mode_idx := 0;
  v_pb10_int_mode_idx := 0;
  v_pb10_pull_updown_idx := 1;
  v_pb10_speed_idx := 2;
  find_and_erase_label('LCD_D6'); {except default LCD4 pins}

  {PA8}
  v_cb_pa8_idx:= 6;
  v_e_pa8_txt := 'LCD_D7';
  v_pa8_func := 'OUTPUT';
  v_pa8_o_lvl_idx := 1;
  v_pa8_o_mode_idx := 1;
  v_pa8_i_mode_idx := 0;
  v_pa8_a_mode_idx := 0;
  v_pa8_i2c_mode_idx := 0;
  v_pa8_spi_mode_idx := 0;
  v_pa8_int_mode_idx := 0;
  v_pa8_pull_updown_idx := 1;
  v_pa8_speed_idx := 2;
  find_and_erase_label('LCD_D7'); {except default LCD4 pins}
end;

procedure TForm20.lcd_reset_default_pins;
begin
  reset_default_lcd4_pins;
end;

procedure TForm20.FormShow(Sender: TObject);
begin
  CheckBox1.Checked := v_c_lcd4;
  RadioButton1.Checked := v_r1_lcd4;
  RadioButton2.Checked := v_r2_lcd4;
  ComboBox1.ItemIndex := v_cb_idx_lcd4;
  ComboBox1.ItemIndex := v_cb_idx_lcd4_chipset;
  if v_c_lcd4 then GroupBox1.Enabled := TRUE
  else GroupBox1.Enabled := FALSE;
  if CheckBox1.Checked and RadioButton2.Checked then begin
    if(v_c_rcc_mco) then v_c_rcc_mco := FALSE; {if "clock out" then disable}
    if(v_c_i2c2) then begin
      v_c_i2c2 := FALSE;
      {reset the second i2c2 pin}
      reset_pb11_pin;
    end;
    lcd_set_default_pins;
    v_lcd4_defaults := TRUE;
  end;
end;


procedure TForm20.BitBtn1Click(Sender: TObject);
begin
  if checkbox1.Checked and radiobutton2.Checked then begin
    if(v_c_rcc_mco) then v_c_rcc_mco := FALSE; {if "clock out" then disable}
    if(v_c_i2c2) then begin
      v_c_i2c2 := FALSE;
      {reset the second i2c2 pin}
      reset_pb11_pin;
    end;
    lcd_set_default_pins;
    v_lcd4_defaults := TRUE;
  end;
  v_c_lcd4 := CheckBox1.Checked;
  v_r1_lcd4 := RadioButton1.Checked;
  v_r2_lcd4 := RadioButton2.Checked;
  v_cb_idx_lcd4 := ComboBox1.ItemIndex;
  v_cb_idx_lcd4_chipset := ComboBox2.ItemIndex;
  close;
end;

procedure TForm20.CheckBox1Change(Sender: TObject);
begin
  if checkbox1.Checked then begin
    {set the global variable...}
    groupbox1.Enabled := TRUE;
  end
  else begin
    {reset the global variable...}
    groupbox1.Enabled := FALSE;
    {do more}
    if radiobutton2.Checked then begin
      radiobutton2.Checked := FALSE;
      radiobutton1.Checked := TRUE;

      if ((v_e_pa8_txt = 'LCD_D7') and (v_lcd4_defaults = TRUE)) then begin
        lcd_reset_default_pins;
        v_lcd4_defaults := FALSE;
      end;
      {}
    end;
  end;
end;

procedure TForm20.RadioButton2Change(Sender: TObject);
begin
  if radiobutton2.Checked then begin
    if(v_c_rcc_mco) then v_c_rcc_mco := FALSE; {if "clock out" then disable}
    if(v_c_i2c2) then begin
      v_c_i2c2 := FALSE;
      {reset the second i2c2 pin}
      reset_pb11_pin;
    end;
    lcd_set_default_pins;
    v_lcd4_defaults := TRUE;
  end
  else
    if (v_lcd4_defaults) then begin
      lcd_reset_default_pins;
      v_lcd4_defaults := FALSE;
    end;
end;



end.

