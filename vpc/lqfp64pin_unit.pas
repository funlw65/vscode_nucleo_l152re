unit lqfp64pin_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  ExtCtrls, StdCtrls, data_unit, lqfp64pinsetup_unit,
  code_unit;

type

  { TForm25 }

  TForm25 = class(TForm)
    B_NEWPRJ_OK: TBitBtn;
    D_PA13: TPanel;
    D_PA14: TPanel;
    D_PB16: TPanel;
    Image1: TImage;
    Label25: TLabel;
    Label27: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    LPA3: TLabel;
    LPA2: TLabel;
    LPA6: TLabel;
    LPA7: TLabel;
    LPC4: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    LPB9: TLabel;
    Label2: TLabel;
    LPA1: TLabel;
    LPA0: TLabel;
    LPB15: TLabel;
    LPB8: TLabel;
    LPC5: TLabel;
    LPB0: TLabel;
    Label26: TLabel;
    LPB1: TLabel;
    LPB7: TLabel;
    LPC6: TLabel;
    LPC7: TLabel;
    LPC8: TLabel;
    LPC9: TLabel;
    LPA8: TLabel;
    LPA9: TLabel;
    LPA10: TLabel;
    LPA11: TLabel;
    LPA12: TLabel;
    LPA13: TLabel;
    Label39: TLabel;
    LPB12: TLabel;
    Label40: TLabel;
    LPB6: TLabel;
    LPB5: TLabel;
    LPB4: TLabel;
    LPB3: TLabel;
    LPD2: TLabel;
    LPC12: TLabel;
    LPC11: TLabel;
    LPC10: TLabel;
    LPA15: TLabel;
    Label5: TLabel;
    LPA14: TLabel;
    LPB2: TLabel;
    LPB10: TLabel;
    LPB11: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    LPC3: TLabel;
    LPC2: TLabel;
    LPA4: TLabel;
    LPC1: TLabel;
    LPC0: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    LPC13: TLabel;
    Label68: TLabel;
    LPA5: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label79: TLabel;
    LPB13: TLabel;
    Label80: TLabel;
    LPB14: TLabel;
    L_PC33: TLabel;
    Panel1: TPanel;
    D_PB4: TPanel;
    D_PB3: TPanel;
    D_PD2: TPanel;
    D_PC12: TPanel;
    D_PC11: TPanel;
    D_PC10: TPanel;
    D_PA15: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel2: TPanel;
    Panel20: TPanel;
    D_PA4: TPanel;
    D_PA5: TPanel;
    D_PA6: TPanel;
    D_PA7: TPanel;
    D_PC4: TPanel;
    D_PC5: TPanel;
    D_PB0: TPanel;
    D_PB1: TPanel;
    D_PB2: TPanel;
    Panel21: TPanel;
    Panel3: TPanel;
    D_PB10: TPanel;
    D_PB11: TPanel;
    Panel32: TPanel;
    Panel33: TPanel;
    Panel34: TPanel;
    Panel35: TPanel;
    Panel36: TPanel;
    D_PA12: TPanel;
    D_PA11: TPanel;
    D_PA10: TPanel;
    D_PB9: TPanel;
    D_PA9: TPanel;
    D_PA8: TPanel;
    D_PC9: TPanel;
    D_PC8: TPanel;
    D_PC7: TPanel;
    D_PC6: TPanel;
    D_PB15: TPanel;
    D_PB14: TPanel;
    D_PB13: TPanel;
    D_PB12: TPanel;
    D_PB8: TPanel;
    Panel38: TPanel;
    Panel50: TPanel;
    Panel51: TPanel;
    Panel52: TPanel;
    Panel53: TPanel;
    Panel54: TPanel;
    Panel55: TPanel;
    Panel56: TPanel;
    D_PC0: TPanel;
    D_PC1: TPanel;
    D_PC2: TPanel;
    Panel6: TPanel;
    D_PC3: TPanel;
    Panel61: TPanel;
    Panel62: TPanel;
    D_PA0: TPanel;
    D_PA1: TPanel;
    Panel65: TPanel;
    D_PB7: TPanel;
    D_PB6: TPanel;
    D_PB5: TPanel;
    Shape1: TShape;
    Timer1: TTimer;
    procedure FormClose(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Label81Click(Sender: TObject);
    procedure Label82Click(Sender: TObject);
    procedure Label83Click(Sender: TObject);
    procedure Label84Click(Sender: TObject);
    procedure Label85Click(Sender: TObject);
    procedure Label86Click(Sender: TObject);
    procedure Label87Click(Sender: TObject);
    procedure Label88Click(Sender: TObject);
    procedure Label90Click(Sender: TObject);
    procedure Label91Click(Sender: TObject);
    procedure Label92Click(Sender: TObject);
    procedure Label93Click(Sender: TObject);
    procedure Label94Click(Sender: TObject);
    procedure Label95Click(Sender: TObject);
    procedure Label96Click(Sender: TObject);
    procedure Label97Click(Sender: TObject);
    procedure paint_labels;
    procedure B_NEWPRJ_OKClick(Sender: TObject);
    procedure D_PA0Click(Sender: TObject);
    procedure D_PA10Click(Sender: TObject);
    procedure D_PA11Click(Sender: TObject);
    procedure D_PA12Click(Sender: TObject);
    procedure D_PA15Click(Sender: TObject);
    procedure D_PA1Click(Sender: TObject);
    procedure D_PA4Click(Sender: TObject);
    procedure D_PA5Click(Sender: TObject);
    procedure D_PA6Click(Sender: TObject);
    procedure D_PA7Click(Sender: TObject);
    procedure D_PA8Click(Sender: TObject);
    procedure D_PA9Click(Sender: TObject);
    procedure D_PB0Click(Sender: TObject);
    procedure D_PB10Click(Sender: TObject);
    procedure D_PB11Click(Sender: TObject);
    procedure D_PB12Click(Sender: TObject);
    procedure D_PB13Click(Sender: TObject);
    procedure D_PB14Click(Sender: TObject);
    procedure D_PB15Click(Sender: TObject);
    procedure D_PB1Click(Sender: TObject);
    procedure D_PB2Click(Sender: TObject);
    procedure D_PB3Click(Sender: TObject);
    procedure D_PB4Click(Sender: TObject);
    procedure D_PB5Click(Sender: TObject);
    procedure D_PB6Click(Sender: TObject);
    procedure D_PB7Click(Sender: TObject);
    procedure D_PB8Click(Sender: TObject);
    procedure D_PB9Click(Sender: TObject);
    procedure D_PC0Click(Sender: TObject);
    procedure D_PC10Click(Sender: TObject);
    procedure D_PC11Click(Sender: TObject);
    procedure D_PC12Click(Sender: TObject);
    procedure D_PC1Click(Sender: TObject);
    procedure D_PC2Click(Sender: TObject);
    procedure D_PC3Click(Sender: TObject);
    procedure D_PC4Click(Sender: TObject);
    procedure D_PC5Click(Sender: TObject);
    procedure D_PC6Click(Sender: TObject);
    procedure D_PC7Click(Sender: TObject);
    procedure D_PC8Click(Sender: TObject);
    procedure D_PC9Click(Sender: TObject);
    procedure D_PD2Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private

  public

  end;

var
  Form25: TForm25;

implementation

var blink_index : byte;
{$R *.lfm}

{ TForm25 }

procedure TForm25.B_NEWPRJ_OKClick(Sender: TObject);
begin
  close;
end;

procedure TForm25.D_PA0Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa0_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA10Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa10_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA11Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa11_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA12Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa12_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA15Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa15_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA1Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa1_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA4Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa4_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA5Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa5_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA6Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa6_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA7Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa7_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA8Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa8_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PA9Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pa9_id;
  Form26.ShowModal;
end;

{}

procedure TForm25.D_PB0Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb0_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB10Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb10_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB11Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb11_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB12Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb12_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB13Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb13_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB14Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb14_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB15Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb15_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB1Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb1_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB2Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb2_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB3Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb3_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB4Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb4_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB5Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb5_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB6Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb6_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB7Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb7_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB8Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb8_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PB9Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pb9_id;
  Form26.ShowModal;
end;

{}

procedure TForm25.D_PC0Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc0_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC10Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc10_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC11Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc11_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC12Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc12_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC1Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc1_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC2Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc2_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC3Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc3_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC4Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc4_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC5Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc5_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC6Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc6_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC7Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc7_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC8Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc8_id;
  Form26.ShowModal;
end;

procedure TForm25.D_PC9Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pc9_id;
  Form26.ShowModal;
end;

{}

procedure TForm25.D_PD2Click(Sender: TObject);
begin
  v_current_lqfp_pad := v_pd2_id;
  Form26.ShowModal;
end;

{---------------------------------------------------}

procedure TForm25.paint_labels;
begin
  if v_cb_pB4_idx <> 0 then LPB4.Caption := '+PB4 - ' + v_e_pB4_txt
  else LPB4.Caption := '+PB4';
  if v_cb_pB3_idx <> 0 then LPB3.Caption := '+PB3 - ' + v_e_pB3_txt
  else LPB3.Caption := '+PB3 - SWO';
  if v_cb_pD2_idx <> 0 then LPD2.Caption := '+PD2 - ' + v_e_pD2_txt
  else LPD2.Caption := '+PD2';
  if v_cb_pC12_idx <> 0 then LPC12.Caption := '+PC12 - ' + v_e_pC12_txt
  else LPC12.Caption := '+PC12';
  if v_cb_pC11_idx <> 0 then LPC11.Caption := '+PC11 - ' + v_e_pC11_txt
  else LPC11.Caption := '+PC11';
  if v_cb_pC10_idx <> 0 then LPC10.Caption := '+PC10 - ' + v_e_pC10_txt
  else LPC10.Caption := '+PC10';
  if v_cb_pA15_idx <> 0 then LPA15.Caption := '+PA15 - ' + v_e_pA15_txt
  else LPA15.Caption := '+PA15';
  if v_cb_pA4_idx <> 0 then LPA4.Caption := '+PA4 - ' + v_e_pA4_txt
  else LPA4.Caption := '+PA4';
  if v_cb_pA5_idx <> 0 then LPA5.Caption := 'PA5 - ' + v_e_pA5_txt
  else LPA5.Caption := 'PA5 - LD2';
  if v_cb_pA6_idx <> 0 then LPA6.Caption := '+PA6 - ' + v_e_pA6_txt
  else LPA6.Caption := '+PA6';
  if v_cb_pA7_idx <> 0 then LPA7.Caption := '+PA7 - ' + v_e_pA7_txt
  else LPA7.Caption := '+PA7';
  if v_cb_pC4_idx <> 0 then LPC4.Caption := '+PC4 - ' + v_e_pC4_txt
  else LPC4.Caption := '+PC4';
  if v_cb_pC5_idx <> 0 then LPC5.Caption := '+PC5 - ' + v_e_pC5_txt
  else LPC5.Caption := '+PC5';
  if v_cb_pB0_idx <> 0 then LPB0.Caption := 'PB0 - ' + v_e_pB0_txt
  else LPB0.Caption := 'PB0';
  if v_cb_pB1_idx <> 0 then LPB1.Caption := '+PB1 - ' + v_e_pB1_txt
  else LPB1.Caption := '+PB1';
  if v_cb_pB2_idx <> 0 then LPB2.Caption := '+PB2 - ' + v_e_pB2_txt
  else LPB2.Caption := '+PB2';
  if v_cb_pB10_idx <> 0 then LPB10.Caption := '+PB10 - ' + v_e_pB10_txt
  else LPB10.Caption := '+PB10';
  if v_cb_pB11_idx <> 0 then LPB11.Caption := '+PB11 - ' + v_e_pB11_txt
  else LPB11.Caption := '+PB11';
  if v_cb_pA12_idx <> 0 then LPA12.Caption := '+PA12 - ' + v_e_pA12_txt
  else LPA12.Caption := '+PA12';
  if v_cb_pA11_idx <> 0 then LPA11.Caption := '+PA11 - ' + v_e_pA11_txt
  else LPA11.Caption := '+PA11';
  if v_cb_pA10_idx <> 0 then LPA10.Caption := '+PA10 - ' + v_e_pA10_txt
  else LPA10.Caption := '+PA10';
  if v_cb_pB9_idx <> 0 then LPB9.Caption := '+PB9 - ' + v_e_pB9_txt
  else LPB9.Caption := '+PB9';
  if v_cb_pA9_idx <> 0 then LPA9.Caption := '+PA9 - ' + v_e_pA9_txt
  else LPA9.Caption := '+PA9';
  if v_cb_pA8_idx <> 0 then LPA8.Caption := '+PA8 - ' + v_e_pA8_txt
  else LPA8.Caption := '+PA8';
  if v_cb_pC9_idx <> 0 then LPC9.Caption := '+PC9 - ' + v_e_pC9_txt
  else LPC9.Caption := '+PC9';
  if v_cb_pC8_idx <> 0 then LPC8.Caption := '+PC8 - ' + v_e_pC8_txt
  else LPC8.Caption := '+PC8';
  if v_cb_pC7_idx <> 0 then LPC7.Caption := '+PC7 - ' + v_e_pC7_txt
  else LPC7.Caption := '+PC7';
  if v_cb_pC6_idx <> 0 then LPC6.Caption := '+PC6 - ' + v_e_pC6_txt
  else LPC6.Caption := '+PC6';
  if v_cb_pB15_idx <> 0 then LPB15.Caption := '+PB15 - ' + v_e_pB15_txt
  else LPB15.Caption := '+PB15';
  if v_cb_pB14_idx <> 0 then LPB14.Caption := '+PB14 - ' + v_e_pB14_txt
  else LPB14.Caption := '+PB14';
  if v_cb_pB13_idx <> 0 then LPB13.Caption := '+PB13 - ' + v_e_pB13_txt
  else LPB13.Caption := '+PB13';
  if v_cb_pB12_idx <> 0 then LPB12.Caption := '+PB12 - ' + v_e_pB12_txt
  else LPB12.Caption := '+PB12';
  if v_cb_pB8_idx <> 0 then LPB8.Caption := '+PB8 - ' + v_e_pB8_txt
  else LPB8.Caption := '+PB8';
  if v_cb_pC0_idx <> 0 then LPC0.Caption := '+PC0 - ' + v_e_pC0_txt
  else LPC0.Caption := '+PC0';
  if v_cb_pC1_idx <> 0 then LPC1.Caption := '+PC1 - ' + v_e_pC1_txt
  else LPC1.Caption := '+PC1';
  if v_cb_pC2_idx <> 0 then LPC2.Caption := '+PC2 - ' + v_e_pC2_txt
  else LPC2.Caption := '+PC2';
  if v_cb_pC3_idx <> 0 then LPC3.Caption := 'PC3 - ' + v_e_pC3_txt
  else LPC3.Caption := 'PC3';
  if v_cb_pA0_idx <> 0 then LPA0.Caption := '+PA0 - ' + v_e_pA0_txt
  else LPA0.Caption := '+PA0';
  if v_cb_pA1_idx <> 0 then LPA1.Caption := '+PA1 - ' + v_e_pA1_txt
  else LPA1.Caption := '+PA1';
  if v_cb_pB7_idx <> 0 then LPB7.Caption := '+PB7 - ' + v_e_pB7_txt
  else LPB7.Caption := '+PB7';
  if v_cb_pB6_idx <> 0 then LPB6.Caption := '+PB6 - ' + v_e_pB6_txt
  else LPB6.Caption := '+PB6';
  if v_cb_pB5_idx <> 0 then LPB5.Caption := '+PB5 - ' + v_e_pB5_txt
  else LPB5.Caption := '+PB5';  
end;


procedure TForm25.FormClose(Sender: TObject);
begin
  scan_for_analog;
  scan_for_adin;
end;

procedure TForm25.FormCreate(Sender: TObject);
begin

end;

procedure TForm25.Label81Click(Sender: TObject);
begin
  // flash SPI1 pins
  Cursor := crHourGlass;
  blink_index := 1;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label82Click(Sender: TObject);
begin
  // flash SPI2 pins
  Cursor := crHourGlass;
  blink_index := 2;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label83Click(Sender: TObject);
begin
  // flash SPI3 pins
  Cursor := crHourGlass;
  blink_index := 3;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label84Click(Sender: TObject);
begin
  // flash I2C1 pins
  Cursor := crHourGlass;
  blink_index := 6;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label85Click(Sender: TObject);
begin
  // flash I2C2 pins
  Cursor := crHourGlass;
  blink_index := 7;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label86Click(Sender: TObject);
begin
  // flash USART1 pins
  Cursor := crHourGlass;
  blink_index := 8;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label87Click(Sender: TObject);
begin
  // flash UART4 pins
  Cursor := crHourGlass;
  blink_index := 4;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label88Click(Sender: TObject);
begin
  // flash UART5 pins
  Cursor := crHourGlass;
  blink_index := 5;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label90Click(Sender: TObject);
begin
  // USB
  Cursor := crHourGlass;
  blink_index := 9;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label91Click(Sender: TObject);
begin
  // TIM2
  Cursor := crHourGlass;
  blink_index := 10;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label92Click(Sender: TObject);
begin
  // TIM3
  Cursor := crHourGlass;
  blink_index := 11;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label93Click(Sender: TObject);
begin
  // TIM4
  Cursor := crHourGlass;
  blink_index := 12;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label94Click(Sender: TObject);
begin
  // TIM5
  Cursor := crHourGlass;
  blink_index := 13;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label95Click(Sender: TObject);
begin
  // TIM9
  Cursor := crHourGlass;
  blink_index := 14;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label96Click(Sender: TObject);
begin
  // TIM10
  Cursor := crHourGlass;
  blink_index := 15;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;

procedure TForm25.Label97Click(Sender: TObject);
begin
  // TIM11
  Cursor := crHourGlass;
  blink_index := 16;
  Timer1.Enabled := True;
  Form25.FormPaint(Form25);
end;


procedure TForm25.FormPaint(Sender: TObject);
begin
  if blink_index = 0 then Timer1.Enabled := False;
  case blink_index of
    1: // SPI1
      begin
        D_PA5.Color := clYellow;
        D_PA6.Color := clYellow;
        D_PA7.Color := clYellow;
      end;
    2: // SPI2
      begin
        D_PB13.Color := clYellow;
        D_PB14.Color := clYellow;
        D_PB15.Color := clYellow;
      end;
    3: // SPI3
      begin
        D_PC10.Color := clYellow;
        D_PC11.Color := clYellow;
        D_PC12.Color := clYellow;
      end;
    4: // UART4
      begin
        D_PC10.Color := clYellow;
        D_PC11.Color := clYellow;
      end;
    5: // UART5
      begin
        D_PC12.Color := clYellow;
        D_PD2.Color := clYellow;
      end;
    6: // I2C1
      begin
        D_PB6.Color := clYellow;
        D_PB7.Color := clYellow;
        D_PB8.Color := clYellow;
        D_PB9.Color := clYellow;
      end;
    7: // I2C2
      begin
        D_PB10.Color := clYellow;
        D_PB11.Color := clYellow;
      end;
    8: // USART1
      begin
        D_PA8.Color := clYellow;
        D_PA9.Color := clYellow;
        D_PA10.Color := clYellow;
        D_PA11.Color := clYellow;
        D_PA12.Color := clYellow;
      end;
    9: // USB
      begin
        D_PA11.Color := clYellow;
        D_PA12.Color := clYellow;
      end;
    10: // TOM2
      begin
        D_PA15.Color := clYellow;
        D_PB3.Color := clYellow;
        D_PB10.Color := clYellow;
        D_PB11.Color := clYellow;
      end;
    11: // TIM3
      begin
        D_PA6.Color := clYellow;
        D_PA7.Color := clYellow;
        D_PB0.Color := clYellow;
        D_PB1.Color := clYellow;
      end;
    12: // TIM4
      begin
        D_PB6.Color := clYellow;
        D_PB7.Color := clYellow;
        D_PB8.Color := clYellow;
        D_PB9.Color := clYellow;
      end;
    13: // TIM5
      begin
        D_PA0.Color := clYellow;
        D_PA1.Color := clYellow;
      end;
    14: // TIM9
      begin
        D_PB13.Color := clYellow;
        D_PB14.Color := clYellow;
      end;
    15: // TIM10
      begin
        D_PB12.Color := clYellow;
      end;
    16: // TIM11
      begin
        D_PB15.Color := clYellow;
      end;
  end;
  if v_cb_pB4_idx <> 0 then D_PB4.Color := clGreen
  else D_PB4.Color := clGray;
  if blink_index = 0 then  if v_cb_pB3_idx <> 0 then D_PB3.Color := clGreen
  else D_PB3.Color := clGray;
  if blink_index = 0 then if v_cb_pD2_idx <> 0 then D_PD2.Color := clGreen
  else D_PD2.Color := clGray;
  if blink_index = 0 then if v_cb_pC12_idx <> 0 then D_PC12.Color := clGreen
  else D_PC12.Color := clGray;
  if blink_index = 0 then if v_cb_pC11_idx <> 0 then D_PC11.Color := clGreen
  else D_PC11.Color := clGray;
  if blink_index = 0 then if v_cb_pC10_idx <> 0 then D_PC10.Color := clGreen
  else D_PC10.Color := clGray;
   if blink_index = 0 then if v_cb_pA15_idx <> 0 then D_PA15.Color := clGreen
  else D_PA15.Color := clGray;
  if v_cb_pA4_idx <> 0 then  D_PA4.Color := clGreen
  else D_PA4.Color := clGray;
  // special case
  if blink_index = 0 then D_PA5.Color := clGreen;

  if blink_index = 0 then if v_cb_pA6_idx <> 0 then  D_PA6.Color := clGreen
  else D_PA6.Color := clGray;
  if blink_index = 0 then if v_cb_pA7_idx <> 0 then  D_PA7.Color := clGreen
  else D_PA7.Color := clGray;
  if v_cb_pC4_idx <> 0 then  D_PC4.Color := clGreen
  else D_PC4.Color := clGray;
  if v_cb_pC5_idx <> 0 then  D_PC5.Color := clGreen
  else D_PC5.Color := clGray;
   if blink_index = 0 then if v_cb_pB0_idx <> 0 then  D_PB0.Color := clGreen
  else D_PB0.Color := clGray;
   if blink_index = 0 then if v_cb_pB1_idx <> 0 then  D_PB1.Color := clGreen
  else D_PB1.Color := clGray;
  if v_cb_pB2_idx <> 0 then  D_PB2.Color := clGreen
  else D_PB2.Color := clGray;
  if blink_index = 0 then if v_cb_pB10_idx <> 0 then  D_PB10.Color := clGreen
  else D_PB10.Color := clGray;
  if blink_index = 0 then if v_cb_pB11_idx <> 0 then  D_PB11.Color := clGreen
  else D_PB11.Color := clGray;
  if blink_index = 0 then if v_cb_pA12_idx <> 0 then  D_PA12.Color := clGreen
  else D_PA12.Color := clGray;
  if blink_index = 0 then if v_cb_pA11_idx <> 0 then  D_PA11.Color := clGreen
  else D_PA11.Color := clGray;
  if blink_index = 0 then if v_cb_pA10_idx <> 0 then  D_PA10.Color := clGreen
  else D_PA10.Color := clGray;
  if blink_index = 0 then if v_cb_pB9_idx <> 0 then  D_PB9.Color := clGreen
  else D_PB9.Color := clGray;
  if blink_index = 0 then if v_cb_pA9_idx <> 0 then  D_PA9.Color := clGreen
  else D_PA9.Color := clGray;
  if blink_index = 0 then if v_cb_pA8_idx <> 0 then  D_PA8.Color := clGreen
  else D_PA8.Color := clGray;
  if v_cb_pC9_idx <> 0 then  D_PC9.Color := clGreen
  else D_PC9.Color := clGray;
  if v_cb_pC8_idx <> 0 then  D_PC8.Color := clGreen
  else D_PC8.Color := clGray;
  if v_cb_pC7_idx <> 0 then  D_PC7.Color := clGreen
  else D_PC7.Color := clGray;
  if v_cb_pC6_idx <> 0 then  D_PC6.Color := clGreen
  else D_PC6.Color := clGray;
  if blink_index = 0 then if v_cb_pB15_idx <> 0 then  D_PB15.Color := clGreen
  else D_PB15.Color := clGray;
  if blink_index = 0 then if v_cb_pB14_idx <> 0 then  D_PB14.Color := clGreen
  else D_PB14.Color := clGray;
  if blink_index = 0 then if v_cb_pB13_idx <> 0 then  D_PB13.Color := clGreen
  else D_PB13.Color := clGray;
   if blink_index = 0 then if v_cb_pB12_idx <> 0 then  D_PB12.Color := clGreen
  else D_PB12.Color := clGray;
  if blink_index = 0 then if v_cb_pB8_idx <> 0 then  D_PB8.Color := clGreen
  else D_PB8.Color := clGray;
  if v_cb_pC0_idx <> 0 then  D_PC0.Color := clGreen
  else D_PC0.Color := clGray;
  if v_cb_pC1_idx <> 0 then  D_PC1.Color := clGreen
  else D_PC1.Color := clGray;
  if v_cb_pC2_idx <> 0 then  D_PC2.Color := clGreen
  else D_PC2.Color := clGray;
  if v_cb_pC3_idx <> 0 then  D_PC3.Color := clGreen
  else D_PC3.Color := clGray;
   if blink_index = 0 then if v_cb_pA0_idx <> 0 then  D_PA0.Color := clGreen
  else D_PA0.Color := clGray;
   if blink_index = 0 then if v_cb_pA1_idx <> 0 then  D_PA1.Color := clGreen
  else D_PA1.Color := clGray;
  if blink_index = 0 then if v_cb_pB7_idx <> 0 then  D_PB7.Color := clGreen
  else D_PB7.Color := clGray;
  if blink_index = 0 then if v_cb_pB6_idx <> 0 then  D_PB6.Color := clGreen
  else D_PB6.Color := clGray;
  if v_cb_pB5_idx <> 0 then  D_PB5.Color := clGreen
  else D_PB5.Color := clGray;
  paint_labels;
end;

procedure TForm25.FormShow(Sender: TObject);
begin
  paint_labels;
end;

procedure TForm25.Timer1Timer(Sender: TObject);
begin
  blink_index := 0;
  Cursor := crDefault;
  Form25.FormPaint(Form25);
end;


begin
  blink_index := 0;
end.

