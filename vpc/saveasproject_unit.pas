unit saveasproject_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  FileCtrl, Buttons, ExtCtrls, utils_unit, data_unit,
  driver_folders_unit, code_unit;

type

  { TForm24 }

  TForm24 = class(TForm)
    B_NEWPRJ_OK: TBitBtn;
    E_PRJNAME: TEdit;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    LST_PrjList: TFileListBox;
    l_newprj_mess: TLabel;
    procedure B_NEWPRJ_OKClick(Sender: TObject);
    procedure E_PRJNAMEEditingDone(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form24: TForm24;

implementation

{$R *.lfm}

{ TForm24 }

procedure TForm24.E_PRJNAMEEditingDone(Sender: TObject);
begin
  E_PRJNAME.Text := StrNoWhite(E_PRJNAME.Text);
  {$I-}
  if(E_PRJNAME.Text <> '')then begin
    //
    chdir(getenvironmentvariable('HOME'));
    chdir(v_workspace);
    if(not DirectoryExists(E_PRJNAME.Text)) then begin
      l_newprj_mess.Caption := 'The name can be used as project name and folder! Press OK button.';
      v_prj_ok := TRUE;
    end
    else begin
      l_newprj_mess.Caption := 'Error! The name is in use, try another.';
      v_prj_ok := FALSE;
    end;
  end
  else begin
    l_newprj_mess.Caption := 'Error! The project name field can not be empty!';
    v_prj_ok := FALSE;
  end;
  {$I+}
end;

procedure TForm24.FormShow(Sender: TObject);
begin
  {populate the project list}
  LST_PrjList.Directory := getenvironmentvariable('HOME') + '/' + v_workspace;
  E_PRJNAME.Text := '';
  l_newprj_mess.Caption := 'Type a project name in the project field that must be unique.';
end;

procedure TForm24.B_NEWPRJ_OKClick(Sender: TObject);
begin
  if(v_prj_ok) then begin
    v_prj_name := E_PRJNAME.Text;
    v_project_status := 'NEW'; // once you want to save it under a new name, it is a new project (with the setup from the old one).
    create_project;
    name_all_the_pins;
    prepare_labels_for_xml;
    save_project;
    clean_labels_from_xml;
    v_project_status := 'CREATED';
    Close;
  end
  else l_newprj_mess.Caption := 'Error! The project name can not be created! Try a different name.';
end;

end.

