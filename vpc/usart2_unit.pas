unit usart2_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons, data_unit, code_unit;

type

  { TForm14 }

  TForm14 = class(TForm)
    BitBtn1: TBitBtn;
    cb_usart2_subpriority: TComboBox;
    cb_usart2_priority: TComboBox;
    c_enable_usart2: TCheckBox;
    c_global_int_usart2: TCheckBox;
    cb_bps_usart2: TComboBox;
    cb_wl_usart2: TComboBox;
    cb_par_usart2: TComboBox;
    cb_stop_usart2: TComboBox;
    cb_dir_rw_usart2: TComboBox;
    cb_sampling_usart2: TComboBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label11: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    priority: TLabel;
    priority1: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form14: TForm14;

implementation

{$R *.lfm}

{ TForm14 }

procedure TForm14.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm14.FormClose(Sender: TObject);
begin
  v_cb_baud_idx_usart2 := cb_bps_usart2.ItemIndex;
  v_cb_wordlng_idx_usart2 := cb_wl_usart2.ItemIndex;
  v_cb_parity_idx_usart2 := cb_par_usart2.ItemIndex;
  v_cb_stop_idx_usart2 := cb_stop_usart2.ItemIndex;
  v_cb_datadir_idx_usart2 := cb_dir_rw_usart2.ItemIndex;
  v_cb_sampling_idx_usart2 := cb_sampling_usart2.ItemIndex;
  v_c_gb_int_usart2 := c_global_int_usart2.Checked;
  v_usart2_int_priority:=validate_priority_value(cb_usart2_priority.ItemIndex);
  v_usart2_int_subpriority:=validate_subpriority_value(cb_usart2_subpriority.ItemIndex);
end;

procedure TForm14.FormShow(Sender: TObject);
begin
  cb_bps_usart2.ItemIndex := v_cb_baud_idx_usart2;
  cb_wl_usart2.ItemIndex := v_cb_wordlng_idx_usart2;
  cb_par_usart2.ItemIndex := v_cb_parity_idx_usart2;
  cb_stop_usart2.ItemIndex := v_cb_stop_idx_usart2;
  cb_dir_rw_usart2.ItemIndex := v_cb_datadir_idx_usart2;
  cb_sampling_usart2.ItemIndex := v_cb_sampling_idx_usart2;
  c_global_int_usart2.Checked := v_c_gb_int_usart2;
  cb_usart2_priority.ItemIndex:=validate_priority_value(v_usart2_int_priority);
  cb_usart2_subpriority.ItemIndex:=validate_subpriority_value(v_usart2_int_subpriority);
end;

end.

