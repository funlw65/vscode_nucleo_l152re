unit environment_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  EditBtn, ExtCtrls, Buttons, data_unit, driver_folders_unit;

type

  { TForm8 }

  TForm8 = class(TForm)
    BitBtn1: TBitBtn;
    B_EnvSave: TBitBtn;
    C_toolF: TCheckBox;
    C_toolsubF: TCheckBox;
    C_workF: TCheckBox;
    C_splF: TCheckBox;
    C_vpcF: TCheckBox;
    D_WorkspaceFolder: TDirectoryEdit;
    D_ToolchainFolder: TDirectoryEdit;
    D_ToolChainSubfolder: TDirectoryEdit;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    L_EnvMess: TLabel;
    Label5: TLabel;
    StaticText2: TStaticText;
    //procedure D_ArduinoFolderEditingDone(Sender: TObject);
    procedure D_ToolChainSubfolderEditingDone(Sender: TObject);
    procedure env_showMess;
    procedure BitBtn1Click(Sender: TObject);
    procedure B_EnvSaveClick(Sender: TObject);
    procedure D_ToolchainFolderEditingDone(Sender: TObject);
    procedure D_WorkspaceFolderEditingDone(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form8: TForm8;

implementation

{$R *.lfm}

{ TForm8 }

procedure TForm8.env_showMess;
begin
  if((v_workspace_ok) and (v_toolchain_ok) and (v_toolchain_subf_ok) and (v_driver_ok) and (v_app_ok)) then begin
    L_EnvMess.Caption := 'All requirements are met (checked). Press <Save> and/or <OK>.';
    BitBtn1.Enabled := TRUE;
    B_EnvSave.Enabled := TRUE;
  end
  else begin
    L_EnvMess.Caption := 'Error in requirements, some might need actions from outside this application.';
    BitBtn1.Enabled := FALSE;
    B_EnvSave.Enabled := FALSE;
  end;
end;

procedure TForm8.D_ToolChainSubfolderEditingDone(Sender: TObject);
begin
  if v_toolchain_ok then begin
    D_ToolchainSubfolder.Directory := ExtractFileName(D_ToolchainSubfolder.Directory);
    sf := D_ToolchainSubfolder.Directory;
    if(check_toolchain_subfolder) then begin
      v_toolchain_subf_ok := TRUE;
      C_toolsubF.Checked := v_toolchain_subf_ok;
    end
    else begin
      C_toolsubF.Checked := FALSE;
      v_toolchain_subf_ok := FALSE;
    end;
  end
  else begin
    C_toolsubF.Checked := FALSE;
    v_toolchain_subf_ok := FALSE;
  end;
  env_showMess;
end;

//procedure TForm8.D_ArduinoFolderEditingDone(Sender: TObject);
//begin
//  D_ArduinoFolder.Directory := ExtractFileName(D_ArduinoFolder.Directory);
//  v_arduino_path := D_ArduinoFolder.Directory;
//end;

procedure TForm8.BitBtn1Click(Sender: TObject);
begin
  close;
end;

procedure TForm8.B_EnvSaveClick(Sender: TObject);
begin
  if((v_workspace_ok) and (v_toolchain_ok) and (v_toolchain_subf_ok) and (v_driver_ok) and (v_app_ok)) then begin
    {creating the XML file in the HOME folder}
    create_envfile;
    L_EnvMess.Caption := 'Environment file .vpc.xml saved in HOME folder.';
  end;
end;

procedure TForm8.D_ToolchainFolderEditingDone(Sender: TObject);
begin
  D_ToolchainFolder.Directory := ExtractFileName(D_ToolchainFolder.Directory);
  v_toolchain_folder := D_ToolchainFolder.Directory;
  if(check_toolchain_folder) then begin
    v_toolchain_ok := TRUE;
    C_toolF.Checked := v_toolchain_ok;
  end
  else begin
    C_toolF.Checked := FALSE;
    v_toolchain_ok := FALSE;
  end;
  env_showMess;
end;

procedure TForm8.D_WorkspaceFolderEditingDone(Sender: TObject);
begin
  D_WorkspaceFolder.Directory := ExtractFileName(D_WorkspaceFolder.Directory);
  v_workspace := D_WorkspaceFolder.Directory;
  if(check_workspace_folder) then begin
    v_workspace_ok := TRUE;
    C_workF.Checked := v_workspace_ok;
  end
  else begin
    C_workF.Checked := FALSE;
    v_workspace_ok := FALSE;
  end;

  if(check_driver_folder) then begin
    v_driver_ok := TRUE;
    C_splF.Checked := v_driver_ok;
  end
  else begin
    C_splF.Checked := FALSE;
    v_driver_ok := FALSE;
  end;

  if(check_app_folder) then begin
    v_app_ok := TRUE;
    C_vpcF.Checked := v_app_ok;
  end
  else begin
    C_vpcF.Checked := FALSE;
    v_app_ok := FALSE;
  end;
  env_showMess;
end;

procedure TForm8.FormShow(Sender: TObject);
begin
  D_ToolchainFolder.RootDir := getenvironmentvariable('HOME');
  D_WorkspaceFolder.RootDir := getenvironmentvariable('HOME');
  D_ToolchainSubfolder.RootDir := getenvironmentvariable('HOME')+v_toolchain_folder+'/lib/gcc/arm-none-eabi';
  D_ToolchainSubfolder.Directory := sf;
  D_ToolchainFolder.Directory := v_toolchain_folder;
  D_WorkspaceFolder.Directory := v_workspace;
  //D_ArduinoFolder.Directory := v_arduino_path;
  v_toolchain_ok := check_toolchain_folder;
  v_workspace_ok := check_workspace_folder;
  v_driver_ok := check_driver_folder;
  v_app_ok := check_app_folder;
  C_toolF.Checked := v_toolchain_ok;
  C_toolsubF.Checked := v_toolchain_subf_ok;
  C_workF.Checked := v_workspace_ok;
  C_splF.Checked := v_driver_ok;
  C_vpcF.Checked := v_app_ok;
  env_showMess;
end;

end.

