unit usb_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons, ExtCtrls, data_unit;

type

  { TForm17 }

  TForm17 = class(TForm)
    BitBtn1: TBitBtn;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form17: TForm17;

implementation

{$R *.lfm}

{ TForm17 }

procedure TForm17.BitBtn1Click(Sender: TObject);
begin
  close;
end;

procedure TForm17.CheckBox1Change(Sender: TObject);
begin
  if CheckBox1.Checked then begin
    GroupBox1.Enabled := TRUE;
    v_c_usb := TRUE;
  end
  else begin
    GroupBox1.Enabled := FALSE;
    v_c_usb := FALSE;
  end;
end;

procedure TForm17.FormClose(Sender: TObject);
begin
  if v_rg_vco_idx = 0 then begin
    v_c_usb := CheckBox1.Checked;
    v_cb_usb_maxpacksize_idx := ComboBox1.ItemIndex;
    v_cb_usb_lowpower_idx := ComboBox2.ItemIndex;
    v_cb_usb_charging_idx := ComboBox3.ItemIndex;
    v_c_usb_hint := CheckBox2.Checked;
    v_c_usb_lint := CheckBox3.Checked;
  end
  else v_c_usb := FALSE;
end;

procedure TForm17.FormShow(Sender: TObject);
begin
  if v_rg_vco_idx = 1 then begin
    v_c_usb := FALSE;
    CheckBox1.Checked := FALSE;
    CheckBox1.Enabled := FALSE;
    GroupBox1.Enabled := FALSE;
    Label9.Caption:='Actual clock configuration does not allow USB!';
  end
  else begin
    CheckBox1.Enabled := TRUE;
    CheckBox1.Checked := v_c_usb;
    //if CheckBox1.Checked then GroupBox1.Enabled := FALSE;
    ComboBox1.ItemIndex:=v_cb_usb_maxpacksize_idx;
    ComboBox2.ItemIndex:=v_cb_usb_lowpower_idx;
    ComboBox3.ItemIndex:=v_cb_usb_charging_idx;
    CheckBox2.Checked := v_c_usb_hint;
    CheckBox3.Checked := v_c_usb_lint;
    Label9.Caption:='...';
  end;
end;

end.

