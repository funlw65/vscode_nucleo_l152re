unit uart4_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons, data_unit, code_unit;

type

  { TForm15 }

  TForm15 = class(TForm)
    BitBtn1: TBitBtn;
    cb_uart4_subpriority: TComboBox;
    cb_uart4_priority: TComboBox;
    CheckBox2: TCheckBox;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    ComboBox5: TComboBox;
    ComboBox6: TComboBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label11: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    l_uart4_mess: TLabel;
    priority: TLabel;
    priority1: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form15: TForm15;

implementation

{$R *.lfm}

{ TForm15 }


procedure TForm15.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm15.FormClose(Sender: TObject);
begin
  v_cb_baud_idx_uart4 := ComboBox1.ItemIndex;
  v_cb_wordlng_idx_uart4 := ComboBox2.ItemIndex;
  v_cb_parity_idx_uart4 := ComboBox3.ItemIndex;
  v_cb_stop_idx_uart4 := ComboBox4.ItemIndex;
  v_cb_datadir_idx_uart4 := ComboBox5.ItemIndex;
  v_cb_sampling_idx_uart4 := ComboBox6.ItemIndex;
  v_c_gb_int_uart4 := CheckBox2.Checked;
  v_uart4_int_priority:=validate_priority_value(cb_uart4_priority.ItemIndex);
  v_uart4_int_subpriority:=validate_subpriority_value(cb_uart4_subpriority.ItemIndex);

end;

procedure TForm15.FormShow(Sender: TObject);
begin
  ComboBox1.ItemIndex := v_cb_baud_idx_uart4;
  ComboBox2.ItemIndex := v_cb_wordlng_idx_uart4;
  ComboBox3.ItemIndex := v_cb_parity_idx_uart4;
  ComboBox4.ItemIndex := v_cb_stop_idx_uart4;
  ComboBox5.ItemIndex := v_cb_datadir_idx_uart4;
  ComboBox6.ItemIndex := v_cb_sampling_idx_uart4;
  CheckBox2.Checked := v_c_gb_int_uart4;
  cb_uart4_priority.ItemIndex:=validate_priority_value(v_uart4_int_priority);
  cb_uart4_subpriority.ItemIndex:=validate_subpriority_value(v_uart4_int_subpriority);


  if v_c_uart4 then begin
    GroupBox1.Enabled := TRUE;
    l_uart4_mess.Caption:='UART4 peripheral activated.';
  end
  else begin
    GroupBox1.Enabled := FALSE;
    l_uart4_mess.Caption:='Set PC10-PC11 pair as UART4 in Nucleo or LQFP64 window.';
  end;
end;

end.

