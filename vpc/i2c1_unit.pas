unit i2c1_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons, data_unit, code_unit;

type

  { TForm10 }

  TForm10 = class(TForm)
    BitBtn1: TBitBtn;
    CB_I2C1_GCAD: TComboBox;
    CB_I2C1_S_DualAddACK: TComboBox;
    CB_I2C1_S_PAL: TComboBox;
    CB_I2C1_S_ClockNoStretch: TComboBox;
    cb_i2c1_subpriority: TComboBox;
    cb_i2c1_priority: TComboBox;
    C_i2C1_Event_INT: TCheckBox;
    C_I2C1_Error_INT: TCheckBox;
    CB_I2C1_FM_DUTYCYCLE: TComboBox;
    CB_I2C1_CLOCKSPEED: TComboBox;
    CB_I2C1_SPEEDMODE: TComboBox;
    GB_I2C1: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    l_i2c1_mess: TLabel;
    priority: TLabel;
    priority1: TLabel;
    SE_I2C1_PSA: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure CB_I2C1_SPEEDMODESelect(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SetInterfaceElements;
  private

  public

  end;

var
  Form10: TForm10;

implementation

{$R *.lfm}

{ TForm10 }

procedure TForm10.SetInterfaceElements;
begin
  if v_c_i2c1 then GB_I2C1.Enabled := TRUE
  else GB_I2C1.Enabled := FALSE;
  if (CB_I2C1_SPEEDMODE.ItemIndex = 0) then begin
    CB_I2C1_CLOCKSPEED.ItemIndex := 0;
    CB_I2C1_CLOCKSPEED.Enabled := FALSE;
    CB_I2C1_FM_DUTYCYCLE.Enabled := FALSE;
    CB_I2C1_FM_DUTYCYCLE.ItemIndex := 0;
  end
  else begin
    if CB_I2C1_CLOCKSPEED.ItemIndex = 0 then CB_I2C1_CLOCKSPEED.ItemIndex := 1;
    CB_I2C1_CLOCKSPEED.Enabled := TRUE;
    CB_I2C1_FM_DUTYCYCLE.Enabled := TRUE;
    if CB_I2C1_FM_DUTYCYCLE.ItemIndex = 0 then CB_I2C1_FM_DUTYCYCLE.ItemIndex := 1;
  end;
end;


procedure TForm10.FormClose(Sender: TObject);
begin
  {put data in global variables}
  if v_c_i2c1 then begin
    v_cb_i2c1_speedmode_idx := CB_I2C1_SPEEDMODE.ItemIndex;
    v_cb_i2c1_clockspeed_idx := CB_I2C1_CLOCKSPEED.ItemIndex;
    v_cb_i2c1_fm_dc_idx := CB_I2C1_FM_DUTYCYCLE.ItemIndex;
    v_cb_i2c1_s_cns_idx := CB_I2C1_S_ClockNoStretch.ItemIndex;
    v_cb_i2c1_s_pal_idx := CB_I2C1_S_PAL.ItemIndex;
    v_cb_i2c1_s_daa_idx := CB_I2C1_S_DualAddACK.ItemIndex;
    v_se_i2c1_s_psa := SE_I2C1_PSA.Value;
    v_cb_i2c1_s_gcad_idx := CB_I2C1_GCAD.ItemIndex;
    v_c_i2c1_event_int := C_I2C1_Event_INT.Checked;
    v_c_i2c1_error_int := C_I2C1_Error_INT.Checked;
    v_i2c1_int_priority:=validate_priority_value(cb_i2c1_priority.ItemIndex);
    v_i2c1_int_subpriority:=validate_subpriority_value(cb_i2c1_subpriority.ItemIndex);
  end;
  {}
end;

procedure TForm10.FormShow(Sender: TObject);
begin
  if v_c_i2c1 then l_i2c1_mess.Caption:='I2C1 peripheral activated.'
  else l_i2c1_mess.Caption:='Set the PB6-PB7 or PB8-PB9 pairs as I2C1 in Nucleo or LQFP64 window.';
  {get data from the global variables}
  CB_I2C1_SPEEDMODE.ItemIndex := v_cb_i2c1_speedmode_idx;
  CB_I2C1_CLOCKSPEED.ItemIndex := v_cb_i2c1_clockspeed_idx;
  CB_I2C1_FM_DUTYCYCLE.ItemIndex := v_cb_i2c1_fm_dc_idx;
  CB_I2C1_S_ClockNoStretch.ItemIndex := v_cb_i2c1_s_cns_idx;
  CB_I2C1_S_PAL.ItemIndex := v_cb_i2c1_s_pal_idx;
  CB_I2C1_S_DualAddACK.ItemIndex := v_cb_i2c1_s_daa_idx;
  SE_I2C1_PSA.Value := v_se_i2c1_s_psa;
  CB_I2C1_GCAD.ItemIndex := v_cb_i2c1_s_gcad_idx;
  C_i2C1_Event_INT.Checked := v_c_i2c1_event_int;
  C_I2C1_Error_INT.Checked := v_c_i2c1_error_int;
  cb_i2c1_priority.ItemIndex:=validate_priority_value(v_i2c1_int_priority);
  cb_i2c1_subpriority.ItemIndex:=validate_subpriority_value(v_i2c1_int_subpriority);
  {}
  SetInterfaceElements;
end;



procedure TForm10.BitBtn1Click(Sender: TObject);
begin
  close;
end;

procedure TForm10.CB_I2C1_SPEEDMODESelect(Sender: TObject);
begin
  SetInterfaceElements;
end;

end.

