unit designer_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics,
  Dialogs, ComCtrls, Menus, StdCtrls, ExtCtrls, Buttons, about_unit, clock_unit,
  pinout_unit, data_unit, newproject_unit, driver_folders_unit, openproject_unit,
  environment_unit, license, DOM, XMLRead, code_unit, i2c1_unit,
  usart2_unit, tim6_unit, tim7_unit, uart4_unit, uart5_unit, usb_unit, i2c2_unit,
  spi2_unit, spi3_unit, lcd4_unit, spi1_unit, prjsettings_unit, adc1_unit, saveasproject_unit,
  lqfp64pin_unit, warning_generating_code_unit;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn10: TBitBtn;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    BitBtn19: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn20: TBitBtn;
    BitBtn21: TBitBtn;
    BitBtn22: TBitBtn;
    BitBtn23: TBitBtn;
    BitBtn24: TBitBtn;
    BitBtn25: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    GroupBox1: TGroupBox;
    GroupBox5: TGroupBox;
    ADC_status: TShape;
    GroupBox6: TGroupBox;
    Image2: TImage;
    Image3: TImage;
    r_oslin: TRadioButton;
    r_osfree: TRadioButton;
    TB_EXIT1: TSpeedButton;
    TIM2_status: TShape;
    TIM3_status: TShape;
    TIM4_status: TShape;
    TIM5_status: TShape;
    TIM9_status: TShape;
    TIM10_status: TShape;
    TIM11_status: TShape;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    USART1_status: TShape;
    Label1: TLabel;
    l_prj_name: TLabel;
    LCD_status: TShape;
    I2C2_status: TShape;
    SD_status: TShape;
    SPI1_status: TShape;
    SPI3_status: TShape;
    UART4_status: TShape;
    UART5_status: TShape;
    SPI2_status: TShape;
    USB_status: TShape;
    Image1: TImage;
    L_DesignerMess: TLabel;
    TIM6_status: TShape;
    TB_GEN_CODE: TSpeedButton;
    TB_REGEN_VSFILES: TSpeedButton;
    TB_PRJ_SET: TSpeedButton;
    TB_INFO: TSpeedButton;
    TB_ENV: TSpeedButton;
    TB_SAVEAS: TSpeedButton;
    TB_CLOSE: TSpeedButton;
    TB_SAVE: TSpeedButton;
    TB_OPEN: TSpeedButton;
    TB_NEW: TSpeedButton;
    TGroup1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label2: TLabel;
    L4WorkspaceFolder: TLabel;
    TIM7_status: TShape;
    I2C1_status: TShape;
    ToolBar1: TToolBar;
    USART2_status: TShape;
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn20Click(Sender: TObject);
    procedure BitBtn22Click(Sender: TObject);
    procedure BitBtn24Click(Sender: TObject);
    procedure BitBtn25Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure if_new_prj_created;
    procedure GenerateCode;
    procedure EnvironmentOK;
    procedure CloseProject;
    procedure FormShow(Sender: TObject);
    procedure r_osfreeClick(Sender: TObject);
    procedure r_oslinClick(Sender: TObject);
    procedure TB_CLOSEClick(Sender: TObject);
    procedure TB_ENVClick(Sender: TObject);
    procedure TB_EXIT1Click(Sender: TObject);
    procedure TB_INFOClick(Sender: TObject);
    procedure TB_GEN_CODEClick(Sender: TObject);
    procedure TB_NEWClick(Sender: TObject);
    procedure TB_OPENClick(Sender: TObject);
    procedure TB_PRJ_SETClick(Sender: TObject);
    procedure TB_REGEN_VSFILESClick(Sender: TObject);
    procedure TB_SAVEASClick(Sender: TObject);
    procedure TB_SAVEClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }
procedure TForm1.EnvironmentOK;
begin
  if((v_workspace_ok) and (v_toolchain_ok) and (v_driver_ok) and (v_app_ok)) then begin
    if not TB_NEW.Enabled then begin
      TB_NEW.Enabled:=TRUE;
      TB_OPEN.Enabled:=TRUE;
      L_DesignerMess.Caption := 'All good! The environment is ok ... again!';
    end;
  end
  else begin
    if TB_NEW.Enabled then begin
      TB_NEW.Enabled:=FALSE;
      TB_OPEN.Enabled:=FALSE;
      L_DesignerMess.Caption := 'ERROR! Environment broken, go to <Environment Settings>!';
    end;
  end;
end;

procedure TForm1.if_new_prj_created;
begin
  if(v_project_status = 'CREATED') then begin
    TB_NEW.Enabled:=FALSE;
    TB_OPEN.Enabled:=FALSE;
    TB_SAVE.Enabled:=TRUE;
    TB_SAVEAS.Enabled:=TRUE;
    TB_CLOSE.Enabled:=TRUE;
    { }
    TB_GEN_CODE.Enabled:=TRUE;
    TB_REGEN_VSFILES.Enabled:=TRUE;
    { }
    TB_PRJ_SET.Enabled:=TRUE;
    TB_ENV.Enabled:=FALSE;
    TGroup1.Enabled := TRUE;
    calculate; // calculate clock settings
  end;
end;

procedure TForm1.BitBtn10Click(Sender: TObject);
begin
  Form10.ShowModal;
end;

procedure TForm1.BitBtn11Click(Sender: TObject);
begin
  Form18.ShowModal;
end;

procedure TForm1.BitBtn12Click(Sender: TObject);
begin
  Form19.ShowModal;
end;

procedure TForm1.BitBtn13Click(Sender: TObject);
begin
  Form14.ShowModal;
end;

procedure TForm1.BitBtn14Click(Sender: TObject);
begin
  Form15.ShowModal;
end;

procedure TForm1.BitBtn15Click(Sender: TObject);
begin
  Form16.ShowModal;
end;

procedure TForm1.BitBtn16Click(Sender: TObject);
begin
  Form17.ShowModal;
end;

procedure TForm1.BitBtn17Click(Sender: TObject);
begin
  Form20.ShowModal;
end;

procedure TForm1.BitBtn18Click(Sender: TObject);
begin
  Form13.ShowModal;
end;

procedure TForm1.BitBtn19Click(Sender: TObject);
begin
  Form3.ShowModal;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  Form11.ShowModal;
end;

procedure TForm1.BitBtn20Click(Sender: TObject);
begin
  Form2.ShowModal;
end;

procedure TForm1.BitBtn22Click(Sender: TObject);
begin
  Form21.ShowModal;
end;

procedure TForm1.BitBtn24Click(Sender: TObject);
begin
  Form23.ShowModal;
end;

procedure TForm1.BitBtn25Click(Sender: TObject);
begin
  Form25.ShowModal;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  Form12.ShowModal;
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  // LEDs
  // Simple Timers
  if v_tim6_check then TIM6_status.Pen.Color := clRed
  else TIM6_status.Pen.Color := clMaroon;
  if v_tim7_check then TIM7_status.Pen.Color := clRed
  else TIM7_status.Pen.Color := clMaroon;
  // Communications
  if v_c_i2c1 then I2C1_status.Pen.Color := clLime
  else I2C1_status.Pen.Color := clGreen;
  if v_c_i2c2 then I2C2_status.Pen.Color := clLime
  else I2C2_status.Pen.Color := clGreen;
  if v_c_usb then USB_status.Pen.Color := clLime
  else USB_status.Pen.Color := clGreen;
  if v_c_usart1 then USART1_status.Pen.Color := clLime
  else USART1_status.Pen.Color := clGreen;
  if v_c_uart4 then UART4_status.Pen.Color := clLime
  else UART4_status.Pen.Color := clGreen;
  if v_c_uart5 then UART5_status.Pen.Color := clLime
  else UART5_status.Pen.Color := clGreen;
  if v_c_spi1 then SPI1_status.Pen.Color := clLime
  else SPI1_status.Pen.Color := clGreen;
  if v_c_spi2 then SPI2_status.Pen.Color := clLime
  else SPI2_status.Pen.Color := clGreen;
  if v_c_spi3 then SPI3_status.Pen.Color := clLime
  else SPI3_status.Pen.Color := clGreen;
  // Third party
  if v_c_lcd4 then LCD_status.Pen.Color := clYellow
  else LCD_status.Pen.Color := clOlive;
  //if v_c_sd then SD_status.Pen.Color := clYellow
  //else SD_status.Pen.Color := clOlive;
  // Others
  if v_use_adin then ADC_status.Pen.Color := clAqua
  else ADC_status.Pen.Color := clTeal;
  // Ok, done with the LEDs

  {refresh the workspace folder}
  L4WorkspaceFolder.Caption := v_workspace;
  l_prj_name.Caption:=v_prj_name;
  if ((calculation_error) and (v_project_status <> 'NEW')) then begin
    TB_SAVE.Enabled:=FALSE;
    TB_SAVEAS.Enabled:=FALSE;
    TB_SAVE.Hint := 'Clock configuration error!';
    TB_SAVEAS.Hint := 'Clock configuration error!';
    TB_GEN_CODE.Enabled:=FALSE;
    TB_GEN_CODE.Hint := 'Clock configuration error!';
  end
  else
    if v_project_status <> 'NEW' then begin
      TB_SAVE.Enabled:=TRUE;
      TB_SAVEAS.Enabled:=TRUE;
      TB_SAVE.Hint := 'Save project';
      TB_SAVEAS.Hint := 'Save project as...';
      TB_GEN_CODE.Enabled:=TRUE;
      TB_GEN_CODE.Hint := 'Generate the code';
    end;
  if (r_osfree.Checked) then begin
    v_r_osfree := TRUE;
    v_r_oslin:= FALSE;
    image3.Show;
    image2.Hide;
  end
  else begin
    //
    v_r_osfree := FALSE;
    v_r_oslin:= TRUE;
    image3.Hide;
    image2.Show;
  end;
end;



procedure TForm1.CloseProject;
begin
  close_project;
  TB_NEW.Enabled:=TRUE;
  TB_OPEN.Enabled:=TRUE;
  TB_SAVE.Enabled:=FALSE;
  TB_SAVEAS.Enabled:=FALSE;
  TB_CLOSE.Enabled:=FALSE;
  { }
  TB_GEN_CODE.Enabled:=FALSE;
  TB_REGEN_VSFILES.Enabled:=FALSE;
  { }
  TB_PRJ_SET.Enabled:=FALSE;
  TB_ENV.Enabled:=TRUE;
  reset_to_nucleo_hardware;
end;

procedure TForm1.GenerateCode;
begin
  v_generating_code_is_ok := FALSE;
  Form27.ShowModal;

  if v_generating_code_is_ok then begin
  if search_label_duplicates then begin
    L_DesignerMess.Caption := 'Error! Label duplication problem. Code generation unsuccessful!';
    exit;
  end;
  if v_c_lcd4 and not verify_lcd4_pin_integrity then begin
    L_DesignerMess.Caption := 'Error! Missing LCD4 pins. Code generation unsuccessful!';
    exit;
  end;
  set_priority_message;
  //scan_for_analog;
  v_exti0_used := scan_for_exti0;
  v_exti1_used := scan_for_exti1;
  v_exti2_used := scan_for_exti2;
  v_exti3_used := scan_for_exti3;
  v_exti4_used := scan_for_exti4;
  v_exti9_5_used := scan_for_exti9_5;
  v_exti15_10_used := scan_for_exti15_10; 
  name_all_the_pins;
  wr_main_h;
  if not code_integrity then begin
    L_DesignerMess.Caption := 'Error! Code integrity problem! Code generation unsuccessful!';
    exit;
  end;
  wr_main_c;
  if not code_integrity then begin
    L_DesignerMess.Caption := 'Error! Code integrity problem! Code generation unsuccessful!';
    exit;
  end;
  wr_makefile;
  L_DesignerMess.Caption := 'Code was generated! Close the project and open it in the Visual Studio Code.';
  // every time you generate the code, project will be saved as it contains the the last changes...
  create_jsons;
  prepare_labels_for_xml;
  save_project;
  clean_labels_from_xml;
  end;
end;

procedure TForm1.r_osfreeClick(Sender: TObject);
begin
  if (r_osfree.Checked) then begin
    v_r_osfree := TRUE;
    v_r_oslin:= FALSE;
    image3.Show;
    image2.Hide;
  end
  else begin
    //
    v_r_osfree := FALSE;
    v_r_oslin:= TRUE;
    image3.Hide;
    image2.Show;
  end;
end;

procedure TForm1.r_oslinClick(Sender: TObject);
begin
  if (r_oslin.Checked) then begin
    v_r_oslin := TRUE;
    v_r_osfree:= FALSE;
    image2.show;
    image3.hide;
  end
  else begin
    //
    v_r_oslin := FALSE;
    v_r_osfree:= TRUE;
    image2.Hide;
    image3.show;
  end;
end;

procedure TForm1.TB_CLOSEClick(Sender: TObject);
begin
  CloseProject;
  TGroup1.Enabled := FALSE;
end;

procedure TForm1.TB_ENVClick(Sender: TObject);
begin
  {Environment configuration}
  Form8.ShowModal;
  EnvironmentOK;
end;

procedure TForm1.TB_EXIT1Click(Sender: TObject);
begin
  close;
end;

procedure TForm1.TB_INFOClick(Sender: TObject);
begin
  Form4.ShowModal;
end;

procedure TForm1.TB_GEN_CODEClick(Sender: TObject);
begin
  GenerateCode;
end;

procedure TForm1.TB_NEWClick(Sender: TObject);
begin
  Form6.ShowModal;
  if_new_prj_created;
end;

procedure TForm1.TB_OPENClick(Sender: TObject);
begin
  {"New project" menu option}
  Form7.ShowModal;
  if(v_project_status = 'OPEN') then begin
    TB_NEW.Enabled:=FALSE;
    TB_OPEN.Enabled:=FALSE;
    if calculation_error then begin
      TB_SAVE.Enabled:=FALSE;
      TB_SAVEAS.Enabled:=FALSE;
      TB_SAVE.Hint := 'Clock configuration error!';
      TB_SAVEAS.Hint := 'Clock configuration error!';
      TB_GEN_CODE.Enabled:=FALSE;
      TB_GEN_CODE.Hint := 'Clock configuration error!';
    end 
    else begin
      TB_SAVE.Enabled:=TRUE;
      TB_SAVEAS.Enabled:=TRUE;      
      TB_SAVE.Hint := 'Save project';
      TB_SAVEAS.Hint := 'Save project as...';
      TB_GEN_CODE.Enabled:=TRUE;
      TB_GEN_CODE.Hint := 'Generate the code';
    end;

    TB_CLOSE.Enabled:=TRUE;
    TB_REGEN_VSFILES.Enabled:=TRUE;
    TB_PRJ_SET.Enabled:=TRUE;
    TB_ENV.Enabled:=FALSE;
    TGroup1.Enabled := TRUE;
  end;
end;

procedure TForm1.TB_PRJ_SETClick(Sender: TObject);
begin
  Form22.ShowModal;
end;

procedure TForm1.TB_REGEN_VSFILESClick(Sender: TObject);
begin
  create_jsons;
  wr_makefile;
end;

procedure TForm1.TB_SAVEASClick(Sender: TObject);
begin
  Form24.ShowModal;
end;

procedure TForm1.TB_SAVEClick(Sender: TObject);
begin
  name_all_the_pins;
  prepare_labels_for_xml;
  save_project;
  clean_labels_from_xml;
  L_DesignerMess.Caption := 'Project saved.';
end;


procedure TForm1.FormShow(Sender: TObject);
var
  PassNode: TDOMNode;
  Doc: TXMLDocument;
begin
  //v_license_agreed := TRUE;
  Form5.ShowModal;
  Caption := 'Visual Pin Configurator for Nucleo L152RE - ' + v_app_version;
  if (v_license_agreed <> FALSE) then begin
    {check if .vpc.xml file exist }
    {$I-}
    chdir(getenvironmentvariable('HOME'));
    if(FileExists('.vpc.xml'))then begin
      {load the .vpc.xml and then ...}
      //try
        ReadXMLFile(Doc, getenvironmentvariable('HOME')+'/.vpc.xml');
        PassNode := Doc.DocumentElement.FindNode('toolchain');
        v_toolchain_folder := AnsiString(PassNode.FirstChild.NodeValue);
        PassNode := Doc.DocumentElement.FindNode('toolchainsub');
        sf := AnsiString(PassNode.FirstChild.NodeValue);
        PassNode := Doc.DocumentElement.FindNode('workspace');
        v_workspace := AnsiString(PassNode.FirstChild.NodeValue);
        //PassNode := Doc.DocumentElement.FindNode('arduinofolder');
        //v_arduino_path := PassNode.FirstChild.NodeValue;
      //finally
        Doc.Free;
      //end;
    end;
    {$I+}
    {... check the data}
    v_toolchain_ok := check_toolchain_folder;
    v_toolchain_subf_ok := check_toolchain_subfolder;
    v_workspace_ok := check_workspace_folder;
    v_driver_ok    := check_driver_folder;
    v_app_ok       := check_app_folder;
    {end check}
    if(not ((v_workspace_ok) and (v_toolchain_ok) and (v_toolchain_subf_ok) and (v_driver_ok) and (v_app_ok))) then begin
      {deactivate project menu options}
      TB_NEW.Enabled:=FALSE;
      TB_OPEN.Enabled:=FALSE;
      {activate configuration menu}
      {well, it is activated at design time, it does not need activation explicitly}
      {show an error message}
      L_DesignerMess.Caption := 'ERROR! Environment broken, go to <Environment Settings>!';
    end
    else
      L_DesignerMess.Caption := 'All good! The environment is ok!';
  end
  else begin
    L_DesignerMess.Caption := 'If you don''t accept the license, then don''t bother...';
    TB_NEW.Enabled:=FALSE;
    TB_OPEN.Enabled:=FALSE;
    TB_ENV.Enabled:=FALSE;
  end;
end;

end.

