unit about_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons, data_unit;

type

  { TForm4 }

  TForm4 = class(TForm)
    BitBtn1: TBitBtn;
    Image1: TImage;
    L_APP_VERSION: TLabel;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.lfm}

{ TForm4 }



procedure TForm4.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm4.FormShow(Sender: TObject);
begin
  L_APP_VERSION.Caption := 'version ' + v_app_version;
end;






end.

