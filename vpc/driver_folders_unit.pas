unit driver_folders_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, data_unit, DOM, XMLRead;

{v_workspace_ok := FALSE; v_prj_ok := FALSE;}
function check_workspace_folder:boolean;
function check_project_folder:boolean;
function check_toolchain_folder:boolean;
function check_toolchain_subfolder:boolean;
function check_driver_folder:boolean;
function check_app_folder:boolean;

procedure create_envfile;
procedure create_jsons;
procedure save_project;
procedure create_project;
procedure open_project(fl:string);
procedure close_project;

// Thank you, @PascalDragon from Lazarus forums!
function StrToInt(const aStr: UnicodeString): Integer; inline; overload;

implementation

// Thank you, @PascalDragon from Lazarus forums!
function StrToInt(const aStr: UnicodeString): Integer;
begin
  Result := StrToInt(AnsiString(aStr));
end;

function check_workspace_folder:boolean;
begin
  {$I-}
  chdir(getenvironmentvariable('HOME'));
  result := DirectoryExists(v_workspace);
  {$I+}
end;

function check_project_folder:boolean;
begin
  {$I-}
  chdir(getenvironmentvariable('HOME'));
  chdir(v_workspace);
  result := DirectoryExists(v_prj_name);
  {$I+}
end;

function check_toolchain_folder:boolean;
var b1 : boolean; Info : TSearchRec;
begin
  {$I-}
  chdir(getenvironmentvariable('HOME'));
  b1 := DirectoryExists(v_toolchain_folder);
  if (b1) then begin
    //
    chdir(getenvironmentvariable('HOME'));
    chdir(v_toolchain_folder+'/lib/gcc/arm-none-eabi');
    if(IOresult = 0) then begin
      if(FindFirst('*',faDirectory, Info) <> 0) then
        result := FALSE
      else begin
        FindClose(Info);
        result := TRUE;
      end;
    end
    else
      result := FALSE;
  end
  else result := b1;
  {$I+}
end;

function check_toolchain_subfolder:boolean;
var b1:boolean;
begin
  {$I-}
  chdir(getenvironmentvariable('HOME'));
  b1 := DirectoryExists(v_toolchain_folder);
  if (b1) then begin
    //
    chdir(getenvironmentvariable('HOME'));
    chdir(v_toolchain_folder+'/lib/gcc/arm-none-eabi');
    if(IOresult = 0) then begin
      if DirectoryExists(sf) then
        result := TRUE
      else
        result := FALSE;
    end
    else
      result := FALSE;
  end
  else result := b1;
  {$I+}
end;

function check_driver_folder:boolean;
begin
  {$I-}
  chdir(getenvironmentvariable('HOME'));
  chdir(v_workspace);
  result := DirectoryExists(v_spl_driver);
  {$I+}
end;

function check_app_folder:boolean;
begin
  {$I-}
  chdir(getenvironmentvariable('HOME'));
  chdir(v_workspace);
  result := DirectoryExists(v_designer_folder);
  {TO DO: continue with checking if debug file is there... }
  {$I+}
end;

{ }

procedure create_envfile;
var F:Text;
begin
  {$I-}
  assign(F, getenvironmentvariable('HOME')+'/.vpc.xml');
  rewrite(F);
  writeln(F, '<?xml version="1.0"?>');
  writeln(F, '<environment>');
  writeln(F, '  <toolchain>'+v_toolchain_folder+'</toolchain>');
  writeln(F, '  <toolchainsub>'+sf+'</toolchainsub>');
  writeln(F, '  <workspace>'+v_workspace+'</workspace>');
  //if v_arduino_path = '' then v_arduino_path := 'arduino-1.8.8';
  //writeln(F, '  <arduinofolder>'+v_arduino_path+'</arduinofolder>');
  writeln(F, '</environment>');
  close(F);
  {$I+}
end;

procedure create_folders;
begin
  {$I-}
  if ((v_workspace_ok) and (v_project_status = 'NEW')) then begin
    chdir(getenvironmentvariable('HOME'));
    chdir(v_workspace);
    mkdir(v_prj_name);
    chdir(v_prj_name);
    mkdir('.vscode');
    mkdir('Inc');
    mkdir('Src');
    mkdir('Drivers');
    mkdir('Drivers/STM32L1xx_StdPeriph_Driver');
    mkdir('Drivers/STM32L1xx_StdPeriph_Driver/Inc');
    mkdir('Drivers/STM32L1xx_StdPeriph_Driver/Src');
    mkdir('Drivers/CMSIS');
    mkdir('Drivers/CMSIS/Device');
    mkdir('Drivers/CMSIS/Device/Include');
    mkdir('Drivers/CMSIS/Device/Source');
    mkdir('Drivers/CMSIS/Device/Source/Templates');
    mkdir('Drivers/CMSIS/Include');
    {ok, project folder is created}
    v_project_status := 'CREATED';
    v_prj_ok := TRUE;
  end;
  {$I+}
end;

//procedure create_json_arduino;
//var F:Text;
//begin
//  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/.vscode/'+v_vsc_arduino_file);
//  rewrite(F);
//  writeln(F,  '{');
//  writeln(F,  '  "board": "STM32:stm32:Nucleo_64",');
//  writeln(F,  '  "configuration": "pnum=NUCLEO_L152RE,upload_method=STLink,xserial=generic,usb=none,opt=osstd,rtlib=nano",');
//  if v_r_osfree then writeln(F,  '  "port": "/dev/cuaU0"')
//  else writeln(F,  '  "port": "/dev/ttyACM0"');
//  writeln(F,  '}');  
//  writeln(F,  ' ');
//  close(F);  
//end;

procedure create_json_settings;
var F:Text;
begin
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/.vscode/'+v_vsc_settings_file);
  rewrite(F);
  writeln(F,  '{');
  writeln(F,  '    "C_Cpp.intelliSenseEngineFallback": "Enabled",');
  writeln(F,  '    "C_Cpp.errorSquiggles": "Disabled",');
  writeln(F,  '    "C_Cpp.formatting": "Disabled",');
  writeln(F,  '    "astyle.cpp.enable": true,');
  writeln(F,  '    "astyle.cmd_options": ["-A3", "-s2"]');
  //writeln(F,  '    "arduino.commandPath": "arduino",');
  //writeln(F,  '    "arduino.defaultBaudRate": 19200,');
  //writeln(F,  '    "arduino.path": "${workspaceFolder}/../../'+v_arduino_path+'"');
  writeln(F,  '}');
  writeln(F,  ' ');
  close(F);
end;

procedure create_json_launch;
var F:Text;
begin
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/.vscode/'+v_vsc_launch_file);
  rewrite(F);
  writeln(F,  '{');
  writeln(F,  '    "version": "0.2.0",');
  writeln(F,  '    "configurations": [');
  writeln(F,  '        {');
  writeln(F,  '            "cwd": "${workspaceRoot}",');
  writeln(F,  '            "executable": "./build/'+v_prj_name+'.elf",');
  writeln(F,  '            "name": "Debug (OpenOCD)",');
  writeln(F,  '            "request": "launch",');
  writeln(F,  '            "type": "cortex-debug",');
  writeln(F,  '            "servertype": "openocd",');
  writeln(F,  '            "device": "STM32L152RE",');
  writeln(F,  '            "configFiles": [');
  writeln(F,  '                "board/st_nucleo_l1.cfg"');
  writeln(F,  '            ],');
  writeln(F,  '            "svdFile": "${workspaceFolder}/../STM32L151.svd"');
  writeln(F,  '        }');
  writeln(F,  '    ]');
  writeln(F,  '}');
  writeln(F,  ' ');
  close(F);
end;

procedure create_json_tasks;
var F:Text;
begin
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/.vscode/'+v_vsc_tasks_file);
  rewrite(F);
  writeln(F,  '{');
  writeln(F,  '    "version": "2.0.0",');
  writeln(F,  '    "tasks": [');
  writeln(F,  '        {');
  writeln(F,  '            "label": "make",');
  writeln(F,  '            "type": "shell",');
  if v_r_osfree then writeln(F,  '            "command": "gmake",')
  else writeln(F,  '            "command": "make",');
  writeln(F,  '            "group": {');
  writeln(F,  '                "kind": "build",');
  writeln(F,  '                "isDefault": true');
  writeln(F,  '            },');
  writeln(F,  '            "problemMatcher": []');
  writeln(F,  '        },');
  writeln(F,  '        {');
  writeln(F,  '            "label": "upload",');
  writeln(F,  '            "type": "shell",');
  if v_r_osfree then writeln(F,  '            "command": "gmake upload",')
  else writeln(F,  '            "command": "make upload",');
  writeln(F,  '            "group": {');
  writeln(F,  '                "kind": "build",');
  writeln(F,  '                "isDefault": true');
  writeln(F,  '            },');
  writeln(F,  '            "problemMatcher": []');
  writeln(F,  '        },');
  writeln(F,  '        {');
  writeln(F,  '            "label": "make clean",');
  writeln(F,  '            "type": "shell",');
  if v_r_osfree then writeln(F,  '            "command": "gmake clean",')
  else writeln(F,  '            "command": "make clean",');
  writeln(F,  '            "group": {');
  writeln(F,  '                "kind": "build",');
  writeln(F,  '                "isDefault": true');
  writeln(F,  '            },');
  writeln(F,  '            "problemMatcher": []');
  writeln(F,  '        }');
  writeln(F,  '    ]');
  writeln(F,  '}');
  writeln(F,  ' ');
  close(F);
end;

procedure create_json_intellisense;
var F:Text;
begin
  //writeln('Ok, the gcc-arm subfolder is: '+sf);
  chdir(getenvironmentvariable('HOME'));
  chdir(v_workspace+'/'+v_prj_name);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/.vscode/'+v_vsc_c_prop_file);
  rewrite(F);
  writeln(F,  '{');
  writeln(F,  '    "configurations": [');
  writeln(F,  '        {');
  writeln(F,  '            "name": "Linux",');
  writeln(F,  '            "intelliSenseMode": "linux-gcc-x64",');
  writeln(F,  '            "defines":[');
  writeln(F,  '                "STM32L1XX_XL",');
  writeln(F,  '                "USE_STDPERIPH_DRIVER"');
  writeln(F,  '            ],');
  writeln(F,  '            "includePath": [');
  writeln(F,  '                "${workspaceFolder}/Inc",');
  writeln(F,  '                "${workspaceFolder}/Drivers/CMSIS/Include",');
  writeln(F,  '                "${workspaceFolder}/Drivers/CMSIS/Device/Include",');
  writeln(F,  '                "${workspaceFolder}/Drivers/STM32L1xx_StdPeriph_Driver/Inc",');
  writeln(F,  '                "${workspaceFolder}/../../'+v_toolchain_folder+'/arm-none-eabi/include",');
  writeln(F,  '                "${workspaceFolder}/../../'+v_toolchain_folder+'/lib/gcc/arm-none-eabi/'+sf+'/include",');
  writeln(F,  '                "${workspaceFolder}/../../'+v_toolchain_folder+'/lib/gcc/arm-none-eabi/'+sf+'/include-fixed",');
  writeln(F,  '                "${workspaceFolder}/../../'+v_workspace+'/'+v_user_lib_folder+'"');
  writeln(F,  '            ],');
  writeln(F,  '            "browse": {');
  writeln(F,  '                "path": [');
  writeln(F,  '                    "${workspaceFolder}/Inc",');
  writeln(F,  '                    "${workspaceFolder}/Drivers/CMSIS/Include",');
  writeln(F,  '                    "${workspaceFolder}/Drivers/CMSIS/Device/Include",');
  writeln(F,  '                    "${workspaceFolder}/Drivers/STM32L1xx_StdPeriph_Driver/Inc",');
  writeln(F,  '                    "${workspaceFolder}/../../'+v_toolchain_folder+'/arm-none-eabi/include",');
  writeln(F,  '                    "${workspaceFolder}/../../'+v_toolchain_folder+'/lib/gcc/arm-none-eabi/'+sf+'/include",');
  writeln(F,  '                    "${workspaceFolder}/../../'+v_toolchain_folder+'/lib/gcc/arm-none-eabi/'+sf+'/include-fixed",');
  writeln(F,  '                    "${workspaceFolder}/../../'+v_workspace+'/'+v_user_lib_folder+'"');
  writeln(F,  '                ],');
  writeln(F,  '                "limitSymbolsToIncludedHeaders": false,');
  writeln(F,  '                "databaseFilename": ""');
  writeln(F,  '            }');
  writeln(F,  '        }');
  writeln(F,  '    ],');
  writeln(F,  '    "version": 4');
  writeln(F,  '}');
  writeln(F,  ' ');
  close(F);
end;

procedure create_jsons;
begin
  {$I-}
  if(((v_project_status = 'CREATED') or (v_project_status = 'OPEN')) and (v_toolchain_ok)) then begin
    //create_json_arduino;
    create_json_settings;
    create_json_launch;
    create_json_tasks;
    create_json_intellisense;
  end;
  {$I+}
end;

procedure create_ld_file;
var F:Text;
begin
  {creating STM32L152RETx_FLASH.ld file in the root folder of the project}
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/'+v_ld_file);
  rewrite(F);
  writeln(F, 'ENTRY(Reset_Handler)');
  writeln(F, '_estack = 0x20014000;');
  writeln(F, '_Min_Heap_Size = 0x200;');
  writeln(F, '_Min_Stack_Size = 0x400;');
  writeln(F, 'MEMORY');
  writeln(F, '{');
  writeln(F, 'RAM (xrw)      : ORIGIN = 0x20000000, LENGTH = 80K');
  writeln(F, 'FLASH (rx)      : ORIGIN = 0x8000000, LENGTH = 512K');
  writeln(F, '}');
  writeln(F, 'SECTIONS');
  writeln(F, '{');
  writeln(F, '  .isr_vector :');
  writeln(F, '  {');
  writeln(F, '    . = ALIGN(4);');
  writeln(F, '    KEEP(*(.isr_vector)) ');
  writeln(F, '    . = ALIGN(4);');
  writeln(F, '  } >FLASH');
  writeln(F, ' ');
  writeln(F, '  .text :');
  writeln(F, '  {');
  writeln(F, '    . = ALIGN(4);');
  writeln(F, '    *(.text)');
  writeln(F, '    *(.text*)');
  writeln(F, '    *(.glue_7)');
  writeln(F, '    *(.glue_7t)');
  writeln(F, '    *(.eh_frame)');
  writeln(F, ' ');
  writeln(F, '    KEEP (*(.init))');
  writeln(F, '    KEEP (*(.fini))');
  writeln(F, ' ');
  writeln(F, '    . = ALIGN(4);');
  writeln(F, '    _etext = .;');
  writeln(F, '  } >FLASH');
  writeln(F, ' ');
  writeln(F, '  .rodata :');
  writeln(F, '  {');
  writeln(F, '    . = ALIGN(4);');
  writeln(F, '    *(.rodata)');
  writeln(F, '    *(.rodata*)');
  writeln(F, '    . = ALIGN(4);');
  writeln(F, '  } >FLASH');
  writeln(F, ' ');
  writeln(F, '  .ARM.extab   : { *(.ARM.extab* .gnu.linkonce.armextab.*) } >FLASH');
  writeln(F, '  .ARM : {');
  writeln(F, '    __exidx_start = .;');
  writeln(F, '    *(.ARM.exidx*)');
  writeln(F, '    __exidx_end = .;');
  writeln(F, '  } >FLASH');
  writeln(F, ' ');
  writeln(F, '  .preinit_array     :');
  writeln(F, '  {');
  writeln(F, '    PROVIDE_HIDDEN (__preinit_array_start = .);');
  writeln(F, '    KEEP (*(.preinit_array*))');
  writeln(F, '    PROVIDE_HIDDEN (__preinit_array_end = .);');
  writeln(F, '  } >FLASH');
  writeln(F, '  .init_array :');
  writeln(F, '  {');
  writeln(F, '    PROVIDE_HIDDEN (__init_array_start = .);');
  writeln(F, '    KEEP (*(SORT(.init_array.*)))');
  writeln(F, '    KEEP (*(.init_array*))');
  writeln(F, '    PROVIDE_HIDDEN (__init_array_end = .);');
  writeln(F, '  } >FLASH');
  writeln(F, '  .fini_array :');
  writeln(F, '  {');
  writeln(F, '    PROVIDE_HIDDEN (__fini_array_start = .);');
  writeln(F, '    KEEP (*(SORT(.fini_array.*)))');
  writeln(F, '    KEEP (*(.fini_array*))');
  writeln(F, '    PROVIDE_HIDDEN (__fini_array_end = .);');
  writeln(F, '  } >FLASH');
  writeln(F, ' ');
  writeln(F, '  _sidata = LOADADDR(.data);');
  writeln(F, ' ');
  writeln(F, '  .data : ');
  writeln(F, '  {');
  writeln(F, '    . = ALIGN(4);');
  writeln(F, '    _sdata = .;');
  writeln(F, '    *(.data)');
  writeln(F, '    *(.data*)');
  writeln(F, ' ');
  writeln(F, '    . = ALIGN(4);');
  writeln(F, '    _edata = .;');
  writeln(F, '  } >RAM AT> FLASH');
  writeln(F, ' ');
  writeln(F, '  . = ALIGN(4);');
  writeln(F, '  .bss :');
  writeln(F, '  {');
  writeln(F, '    _sbss = .;');
  writeln(F, '    __bss_start__ = _sbss;');
  writeln(F, '    *(.bss)');
  writeln(F, '    *(.bss*)');
  writeln(F, '    *(COMMON)');
  writeln(F, ' ');
  writeln(F, '    . = ALIGN(4);');
  writeln(F, '    _ebss = .;');
  writeln(F, '    __bss_end__ = _ebss;');
  writeln(F, '  } >RAM');
  writeln(F, ' ');
  writeln(F, '  ._user_heap_stack :');
  writeln(F, '  {');
  writeln(F, '    . = ALIGN(8);');
  writeln(F, '    PROVIDE ( end = . );');
  writeln(F, '    PROVIDE ( _end = . );');
  writeln(F, '    . = . + _Min_Heap_Size;');
  writeln(F, '    . = . + _Min_Stack_Size;');
  writeln(F, '    . = ALIGN(8);');
  writeln(F, '  } >RAM');
  writeln(F, ' ');
  writeln(F, '  /DISCARD/ :');
  writeln(F, '  {');
  writeln(F, '    libc.a ( * )');
  writeln(F, '    libm.a ( * )');
  writeln(F, '    libgcc.a ( * )');
  writeln(F, '  }');
  writeln(F, ' ');
  writeln(F, '  .ARM.attributes 0 : { *(.ARM.attributes) }');
  writeln(F, '}');
  writeln(F, ' ');
  writeln(F, ' ');
  writeln(F, ' ');
  close(F);
end;

procedure create_startup_file;
begin
  {============================}
  { CMSIS DEVICE - STARTUP FILE}
  {----------------------------}
  {1. startup_stm32l1xx_xl.s}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Device/ST/STM32L1xx/Source/Templates/TrueSTUDIO/startup_stm32l1xx_xl.s');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/'+v_startup_file);
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);
end;

procedure create_cmsis_device;
begin
  {=======================}
  { CMSIS DEVICE - 3 files}
  {-----------------------}
  {1. stm32l1xx.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Device/ST/STM32L1xx/Include/stm32l1xx.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Device/Include/stm32l1xx.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {2. system_stm32l1xx.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Device/ST/STM32L1xx/Include/system_stm32l1xx.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Device/Include/system_stm32l1xx.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {3. stm32l1xx_conf.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Device/ST/STM32L1xx/Include/stm32l1xx_conf.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Device/Include/stm32l1xx_conf.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);
end;

procedure create_cmsis_headers;
begin
  {=========================}
  { CMSIS INCLUDE - 12 files}
  {-------------------------}
  {1. arm_common_tables.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/arm_common_tables.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/arm_common_tables.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {2. arm_const_structs.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/arm_const_structs.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/arm_const_structs.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {3. arm_math.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/arm_math.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/arm_math.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {4. core_cm0.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/core_cm0.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/core_cm0.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {5. core_cm0plus.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/core_cm0plus.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/core_cm0plus.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {6. core_cm3.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/core_cm3.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/core_cm3.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {7. core_cm4.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/core_cm4.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/core_cm4.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {8. core_cm4_simd.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/core_cm4_simd.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/core_cm4_simd.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {9. core_cmFunc.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/core_cmFunc.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/core_cmFunc.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {10. core_cmInstr.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/core_cmInstr.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/core_cmInstr.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {11. core_sc000.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/core_sc000.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/core_sc000.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {12. core_sc300.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/CMSIS/Include/core_sc300.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/CMSIS/Include/core_sc300.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);
end;

procedure create_spl_headers;
begin
  {===========================}
  { STM32L1 SPL INC - 25 files}
  {---------------------------}
  {1. misc.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/misc.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/misc.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {2. stm32l1xx_adc.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_adc.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_adc.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {3. stm32l1xx_aes.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_aes.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_aes.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {4. stm32l1xx_comp.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_comp.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_comp.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {5. stm32l1xx_crc.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_crc.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_crc.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {6. stm32l1xx_dac.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_dac.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_dac.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {7. stm32l1xx_dbgmcu.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_dbgmcu.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_dbgmcu.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {8. stm32l1xx_dma.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_dma.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_dma.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {9. stm32l1xx_exti.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_exti.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_exti.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {10. stm32l1xx_flash.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_flash.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_flash.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {11. stm32l1xx_fsmc.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_fsmc.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_fsmc.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {12. stm32l1xx_gpio.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_gpio.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_gpio.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {13. stm32l1xx_i2c.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_i2c.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_i2c.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {14. stm32l1xx_iwdg.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_iwdg.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_iwdg.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {15. stm32l1xx_lcd.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_lcd.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_lcd.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {16. stm32l1xx_opamp.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_opamp.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_opamp.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {17. stm32l1xx_pwr.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_pwr.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_pwr.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {18. stm32l1xx_rcc.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_rcc.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_rcc.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {19. stm32l1xx_rtc.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_rtc.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_rtc.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {20. stm32l1xx_sdio.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_sdio.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_sdio.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {21. stm32l1xx_spi.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_spi.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_spi.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {22. stm32l1xx_syscfg.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_syscfg.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_syscfg.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {23. stm32l1xx_tim.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_tim.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_tim.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {24. stm32l1xx_usart.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_usart.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_usart.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {25. stm32l1xx_wwdg.h}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/inc/stm32l1xx_wwdg.h');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Inc/stm32l1xx_wwdg.h');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);
end;

procedure create_spl_sources;
begin
  {==========================}
  { STM32L1 LL SRC - 27 files}
  {--------------------------}
  {1. misc.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/misc.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/misc.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {2. stm32l1xx_adc.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_adc.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_adc.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {3. stm32l1xx_aes.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_aes.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_aes.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {27. stm32l1xx_aes_util.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_aes_util.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_aes_util.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {4. stm32l1xx_comp.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_comp.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_comp.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {5. stm32l1xx_crc.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_crc.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_crc.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {6. stm32l1xx_dac.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_dac.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_dac.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {7. stm32l1xx_dbgmcu.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_dbgmcu.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_dbgmcu.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {8. stm32l1xx_dma.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_dma.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_dma.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {9. stm32l1xx_exti.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_exti.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_exti.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {10. stm32l1xx_flash.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_flash.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_flash.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {11. stm32l1xx_flash_ramfunc.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_flash_ramfunc.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_flash_ramfunc.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {12. stm32l1xx_fsmc.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_fsmc.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_fsmc.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {13. stm32l1xx_gpio.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_gpio.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_gpio.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {14. stm32l1xx_i2c.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_i2c.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_i2c.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {15. stm32l1xx_iwdg.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_iwdg.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_iwdg.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {16. stm32l1xx_lcd.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_lcd.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_lcd.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {17. stm32l1xx_opamp.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_opamp.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_opamp.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {18. stm32l1xx_pwr.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_pwr.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_pwr.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {19. stm32l1xx_rcc.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_rcc.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_rcc.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {20. stm32l1xx_rtc.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_rtc.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_rtc.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {21. stm32l1xx_sdio.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_sdio.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_sdio.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {22. stm32l1xx_spi.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_spi.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_spi.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {23. stm32l1xx_syscfg.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_syscfg.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_syscfg.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {24. stm32l1xx_tim.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_tim.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_tim.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {25. stm32l1xx_usart.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_usart.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_usart.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);

  {26. stm32l1xx_wwdg.c}
  assign(G, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_spl_driver+'/Libraries/STM32L1xx_StdPeriph_Driver/src/stm32l1xx_wwdg.c');
  reset(G);
  assign(F, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/Drivers/STM32L1xx_StdPeriph_Driver/Src/stm32l1xx_wwdg.c');
  rewrite(F);
  repeat
    readln(G, s);
    writeln(F, s);
  until eof(G);
  close(F);
  close(G);
end;

procedure copy_driver;
begin
  {$I-}
  if(((v_project_status = 'CREATED') or (v_project_status = 'OPEN')) and (v_driver_ok)) then begin
    create_ld_file;
    create_startup_file;
    create_cmsis_device;
    create_cmsis_headers;
    create_spl_headers;
    create_spl_sources;
  end;
  {$I+}
end;

function prepbool(val:boolean):string;
begin
  if(val = TRUE) then result:='TRUE'
  else result:='FALSE';  
end;

function setbool(val:string):boolean;
begin
  if (val = 'TRUE') then result:=TRUE
  else result:=FALSE;
end;

procedure save_project;
begin
  if((v_project_status = 'CREATED') or (v_project_status = 'OPEN'))then begin
    {save project file}
    {$I-}
    assign(J, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+v_prj_name+'/'+v_prj_name+'.vpc');
    rewrite(J);
    writeln(J, '<?xml version="1.0"?>');
    writeln(J, '<project>');

    {****************************
     ****************************
     ===== FILE  VERSION 1 ======
     ****************************
     ****************************}

    {============}
    {PROJECT DATA}
    {------------}  
    writeln(J, '  <file_version>3</file_version>');
    writeln(J, '  <v_prj_name>'+v_prj_name+'</v_prj_name>'); 
    writeln(J, '  <v_year>'+v_year+'</v_year>'); 
    writeln(J, '  <v_author>'+v_author+'</v_author>'); 
    writeln(J, '  <v_copyright_idx>'+inttostr(v_copyright_idx)+'</v_copyright_idx>'); 
    writeln(J, '  <v_debug>'+prepbool(v_debug)+'</v_debug>'); 
    writeln(J, '  <v_optimization_index>'+inttostr(v_optimization_index)+'</v_optimization_index>');
    writeln(J, '  <v_group_priority>'+inttostr(v_group_priority)+'</v_group_priority>');
    writeln(J, '  <v_exti0_priority>'+inttostr(v_exti0_priority)+'</v_exti0_priority>');
    writeln(J, '  <v_exti0_subpriority>'+inttostr(v_exti0_subpriority)+'</v_exti0_subpriority>');
    writeln(J, '  <v_exti1_priority>'+inttostr(v_exti1_priority)+'</v_exti1_priority>');
    writeln(J, '  <v_exti1_subpriority>'+inttostr(v_exti1_subpriority)+'</v_exti1_subpriority>');
    writeln(J, '  <v_exti2_priority>'+inttostr(v_exti2_priority)+'</v_exti2_priority>');
    writeln(J, '  <v_exti2_subpriority>'+inttostr(v_exti2_subpriority)+'</v_exti2_subpriority>');
    writeln(J, '  <v_exti3_priority>'+inttostr(v_exti3_priority)+'</v_exti3_priority>');
    writeln(J, '  <v_exti3_subpriority>'+inttostr(v_exti3_subpriority)+'</v_exti3_subpriority>');
    writeln(J, '  <v_exti4_priority>'+inttostr(v_exti4_priority)+'</v_exti4_priority>');
    writeln(J, '  <v_exti4_subpriority>'+inttostr(v_exti4_subpriority)+'</v_exti4_subpriority>');
    writeln(J, '  <v_exti9_5_priority>'+inttostr(v_exti9_5_priority)+'</v_exti9_5_priority>');
    writeln(J, '  <v_exti9_5_subpriority>'+inttostr(v_exti9_5_subpriority)+'</v_exti9_5_subpriority>');
    writeln(J, '  <v_exti15_10_priority>'+inttostr(v_exti15_10_priority)+'</v_exti15_10_priority>');
    writeln(J, '  <v_exti15_10_subpriority>'+inttostr(v_exti15_10_subpriority)+'</v_exti15_10_subpriority>');
    writeln(J, '  <v_efp>'+prepbool(v_efp)+'</v_efp>');
    writeln(J, '  <v_printf_efp>'+prepbool(v_printf_efp)+'</v_printf_efp>');
    writeln(J, '  <v_use_adc>'+prepbool(v_use_adc)+'</v_use_adc>');
    writeln(J, '  <v_use_adin>'+prepbool(v_use_adin)+'</v_use_adin>');

{==========================
 |||| EXTI interrupts |||||
 --------------------------}
    writeln(J, '  <v_exti0_used>'+prepbool(v_exti0_used)+'</v_exti0_used>');
    writeln(J, '  <v_exti1_used>'+prepbool(v_exti1_used)+'</v_exti1_used>');
    writeln(J, '  <v_exti2_used>'+prepbool(v_exti2_used)+'</v_exti2_used>');
    writeln(J, '  <v_exti3_used>'+prepbool(v_exti3_used)+'</v_exti3_used>');
    writeln(J, '  <v_exti4_used>'+prepbool(v_exti4_used)+'</v_exti4_used>');
    writeln(J, '  <v_exti9_5_used>'+prepbool(v_exti9_5_used)+'</v_exti9_5_used>');
    writeln(J, '  <v_exti15_10_used>'+prepbool(v_exti15_10_used)+'</v_exti15_10_used>');

{========================
       PORTS USED
 ------------------------}
    writeln(J, '  <v_porta_used>'+prepbool(v_porta_used)+'</v_porta_used>');
    writeln(J, '  <v_portb_used>'+prepbool(v_portb_used)+'</v_portb_used>');
    writeln(J, '  <v_portc_used>'+prepbool(v_portc_used)+'</v_portc_used>');
    writeln(J, '  <v_portd_used>'+prepbool(v_portd_used)+'</v_portd_used>');
    writeln(J, '  <v_porth_used>'+prepbool(v_porth_used)+'</v_porth_used>');

{==============
 USB PERIPHERAL
 --------------}
    writeln(J, '  <v_c_usb>'+prepbool(v_c_usb)+'</v_c_usb>');
    writeln(J, '  <v_cb_usb_maxpacksize_idx>'+inttostr(v_cb_usb_maxpacksize_idx)+'</v_cb_usb_maxpacksize_idx>');
    writeln(J, '  <v_cb_usb_lowpower_idx>'+inttostr(v_cb_usb_lowpower_idx)+'</v_cb_usb_lowpower_idx>');
    writeln(J, '  <v_cb_usb_charging_idx>'+inttostr(v_cb_usb_charging_idx)+'</v_cb_usb_charging_idx>');
    writeln(J, '  <v_c_usb_hint>'+prepbool(v_c_usb_hint)+'</v_c_usb_hint>');
    writeln(J, '  <v_c_usb_lint>'+prepbool(v_c_usb_lint)+'</v_c_usb_lint>');

{===============
 I2C1 PERIPHERAL
 ---------------}
    writeln(J, '  <v_c_i2c1>'+prepbool(v_c_i2c1)+'</v_c_i2c1>');
    writeln(J, '  <v_cb_i2c1_speedmode_idx>'+inttostr(v_cb_i2c1_speedmode_idx)+'</v_cb_i2c1_speedmode_idx>');
    writeln(J, '  <v_cb_i2c1_clockspeed_idx>'+inttostr(v_cb_i2c1_clockspeed_idx)+'</v_cb_i2c1_clockspeed_idx>');
    writeln(J, '  <v_cb_i2c1_fm_dc_idx>'+inttostr(v_cb_i2c1_fm_dc_idx)+'</v_cb_i2c1_fm_dc_idx>');
    writeln(J, '  <v_cb_i2c1_s_cns_idx>'+inttostr(v_cb_i2c1_s_cns_idx)+'</v_cb_i2c1_s_cns_idx>');
    writeln(J, '  <v_cb_i2c1_s_pal_idx>'+inttostr(v_cb_i2c1_s_pal_idx)+'</v_cb_i2c1_s_pal_idx>');
    writeln(J, '  <v_cb_i2c1_s_daa_idx>'+inttostr(v_cb_i2c1_s_daa_idx)+'</v_cb_i2c1_s_daa_idx>');
    writeln(J, '  <v_se_i2c1_s_psa>'+inttostr(v_se_i2c1_s_psa)+'</v_se_i2c1_s_psa>');
    writeln(J, '  <v_cb_i2c1_s_gcad_idx>'+inttostr(v_cb_i2c1_s_gcad_idx)+'</v_cb_i2c1_s_gcad_idx>');
    writeln(J, '  <v_c_i2c1_event_int>'+prepbool(v_c_i2c1_event_int)+'</v_c_i2c1_event_int>');
    writeln(J, '  <v_c_i2c1_error_int>'+prepbool(v_c_i2c1_error_int)+'</v_c_i2c1_error_int>');
    writeln(J, '  <v_i2c1_int_priority>'+inttostr(v_i2c1_int_priority)+'</v_i2c1_int_priority>');
    writeln(J, '  <v_i2c1_int_subpriority>'+inttostr(v_i2c1_int_subpriority)+'</v_i2c1_int_subpriority>');


{===============
 I2C2 PERIPHERAL
 ---------------}
    writeln(J, '  <v_c_i2c2>'+prepbool(v_c_i2c2)+'</v_c_i2c2>');
    writeln(J, '  <v_cb_i2c2_speedmode_idx>'+inttostr(v_cb_i2c2_speedmode_idx)+'</v_cb_i2c2_speedmode_idx>');
    writeln(J, '  <v_cb_i2c2_clockspeed_idx>'+inttostr(v_cb_i2c2_clockspeed_idx)+'</v_cb_i2c2_clockspeed_idx>');
    writeln(J, '  <v_cb_i2c2_fm_dc_idx>'+inttostr(v_cb_i2c2_fm_dc_idx)+'</v_cb_i2c2_fm_dc_idx>');
    writeln(J, '  <v_cb_i2c2_s_cns_idx>'+inttostr(v_cb_i2c2_s_cns_idx)+'</v_cb_i2c2_s_cns_idx>');
    writeln(J, '  <v_cb_i2c2_s_pal_idx>'+inttostr(v_cb_i2c2_s_pal_idx)+'</v_cb_i2c2_s_pal_idx>');
    writeln(J, '  <v_cb_i2c2_s_daa_idx>'+inttostr(v_cb_i2c2_s_daa_idx)+'</v_cb_i2c2_s_daa_idx>');
    writeln(J, '  <v_se_i2c2_s_psa>'+inttostr(v_se_i2c2_s_psa)+'</v_se_i2c2_s_psa>');
    writeln(J, '  <v_cb_i2c2_s_gcad_idx>'+inttostr(v_cb_i2c2_s_gcad_idx)+'</v_cb_i2c2_s_gcad_idx>');
    writeln(J, '  <v_c_i2c2_event_int>'+prepbool(v_c_i2c2_event_int)+'</v_c_i2c2_event_int>');
    writeln(J, '  <v_c_i2c2_error_int>'+prepbool(v_c_i2c2_error_int)+'</v_c_i2c2_error_int>');
    writeln(J, '  <v_i2c2_int_priority>'+inttostr(v_i2c2_int_priority)+'</v_i2c2_int_priority>');
    writeln(J, '  <v_i2c2_int_subpriority>'+inttostr(v_i2c2_int_subpriority)+'</v_i2c2_int_subpriority>');

{===================
 LCD4 pinout setting
 -------------------}
    writeln(J, '  <v_c_lcd4>'+prepbool(v_c_lcd4)+'</v_c_lcd4>');
    writeln(J, '  <v_r1_lcd4>'+prepbool(v_r1_lcd4)+'</v_r1_lcd4>');
    writeln(J, '  <v_r2_lcd4>'+prepbool(v_r2_lcd4)+'</v_r2_lcd4>');
    writeln(J, '  <v_cb_idx_lcd4>'+inttostr(v_cb_idx_lcd4)+'</v_cb_idx_lcd4>');
    writeln(J, '  <v_cb_idx_lcd4_chipset>'+inttostr(v_cb_idx_lcd4_chipset)+'</v_cb_idx_lcd4_chipset>');

{==================
 SPI1 CONFIGURATION
 ------------------}
    writeln(J, '  <v_c_spi1>'+prepbool(v_c_spi1)+'</v_c_spi1>');
    writeln(J, '  <v_cb_direction_idx_spi1>'+inttostr(v_cb_direction_idx_spi1)+'</v_cb_direction_idx_spi1>');
    writeln(J, '  <v_cb_mode_idx_spi1>'+inttostr(v_cb_mode_idx_spi1)+'</v_cb_mode_idx_spi1>');
    writeln(J, '  <v_cb_datasize_idx_spi1>'+inttostr(v_cb_datasize_idx_spi1)+'</v_cb_datasize_idx_spi1>');
    writeln(J, '  <v_cb_cpol_idx_spi1>'+inttostr(v_cb_cpol_idx_spi1)+'</v_cb_cpol_idx_spi1>');
    writeln(J, '  <v_cb_cpha_idx_spi1>'+inttostr(v_cb_cpha_idx_spi1)+'</v_cb_cpha_idx_spi1>');
    writeln(J, '  <v_cb_nss_idx_spi1>'+inttostr(v_cb_nss_idx_spi1)+'</v_cb_nss_idx_spi1>');
    writeln(J, '  <v_cb_prescaler_idx_spi1>'+inttostr(v_cb_prescaler_idx_spi1)+'</v_cb_prescaler_idx_spi1>');
    writeln(J, '  <v_cb_firstbit_idx_spi1>'+inttostr(v_cb_firstbit_idx_spi1)+'</v_cb_firstbit_idx_spi1>');
    writeln(J, '  <v_cb_crcpoly_idx_spi1>'+inttostr(v_cb_crcpoly_idx_spi1)+'</v_cb_crcpoly_idx_spi1>');
    writeln(J, '  <v_c_gb_int_spi1>'+prepbool(v_c_gb_int_spi1)+'</v_c_gb_int_spi1>');
    writeln(J, '  <v_spi1_int_priority>'+inttostr(v_spi1_int_priority)+'</v_spi1_int_priority>');
    writeln(J, '  <v_spi1_int_subpriority>'+inttostr(v_spi1_int_subpriority)+'</v_spi1_int_subpriority>');


{==================
 SPI2 CONFIGURATION
 ------------------}
    writeln(J, '  <v_c_spi2>'+prepbool(v_c_spi2)+'</v_c_spi2>');
    writeln(J, '  <v_cb_direction_idx_spi2>'+inttostr(v_cb_direction_idx_spi2)+'</v_cb_direction_idx_spi2>');
    writeln(J, '  <v_cb_mode_idx_spi2>'+inttostr(v_cb_mode_idx_spi2)+'</v_cb_mode_idx_spi2>');
    writeln(J, '  <v_cb_datasize_idx_spi2>'+inttostr(v_cb_datasize_idx_spi2)+'</v_cb_datasize_idx_spi2>');
    writeln(J, '  <v_cb_cpol_idx_spi2>'+inttostr(v_cb_cpol_idx_spi2)+'</v_cb_cpol_idx_spi2>');
    writeln(J, '  <v_cb_cpha_idx_spi2>'+inttostr(v_cb_cpha_idx_spi2)+'</v_cb_cpha_idx_spi2>');
    writeln(J, '  <v_cb_nss_idx_spi2>'+inttostr(v_cb_nss_idx_spi2)+'</v_cb_nss_idx_spi2>');
    writeln(J, '  <v_cb_prescaler_idx_spi2>'+inttostr(v_cb_prescaler_idx_spi2)+'</v_cb_prescaler_idx_spi2>');
    writeln(J, '  <v_cb_firstbit_idx_spi2>'+inttostr(v_cb_firstbit_idx_spi2)+'</v_cb_firstbit_idx_spi2>');
    writeln(J, '  <v_cb_crcpoly_idx_spi2>'+inttostr(v_cb_crcpoly_idx_spi2)+'</v_cb_crcpoly_idx_spi2>');
    writeln(J, '  <v_c_gb_int_spi2>'+prepbool(v_c_gb_int_spi2)+'</v_c_gb_int_spi2>');
    writeln(J, '  <v_spi2_int_priority>'+inttostr(v_spi2_int_priority)+'</v_spi2_int_priority>');
    writeln(J, '  <v_spi2_int_subpriority>'+inttostr(v_spi2_int_subpriority)+'</v_spi2_int_subpriority>');

{==================
 SPI3 CONFIGURATION
 ------------------}
    writeln(J, '  <v_c_spi3>'+prepbool(v_c_spi3)+'</v_c_spi3>');
    writeln(J, '  <v_cb_direction_idx_spi3>'+inttostr(v_cb_direction_idx_spi3)+'</v_cb_direction_idx_spi3>');
    writeln(J, '  <v_cb_mode_idx_spi3>'+inttostr(v_cb_mode_idx_spi3)+'</v_cb_mode_idx_spi3>');
    writeln(J, '  <v_cb_datasize_idx_spi3>'+inttostr(v_cb_datasize_idx_spi3)+'</v_cb_datasize_idx_spi3>');
    writeln(J, '  <v_cb_cpol_idx_spi3>'+inttostr(v_cb_cpol_idx_spi3)+'</v_cb_cpol_idx_spi3>');
    writeln(J, '  <v_cb_cpha_idx_spi3>'+inttostr(v_cb_cpha_idx_spi3)+'</v_cb_cpha_idx_spi3>');
    writeln(J, '  <v_cb_nss_idx_spi3>'+inttostr(v_cb_nss_idx_spi3)+'</v_cb_nss_idx_spi3>');
    writeln(J, '  <v_cb_prescaler_idx_spi3>'+inttostr(v_cb_prescaler_idx_spi3)+'</v_cb_prescaler_idx_spi3>');
    writeln(J, '  <v_cb_firstbit_idx_spi3>'+inttostr(v_cb_firstbit_idx_spi3)+'</v_cb_firstbit_idx_spi3>');
    writeln(J, '  <v_cb_crcpoly_idx_spi3>'+inttostr(v_cb_crcpoly_idx_spi3)+'</v_cb_crcpoly_idx_spi3>');
    writeln(J, '  <v_c_gb_int_spi3>'+prepbool(v_c_gb_int_spi3)+'</v_c_gb_int_spi3>');
    writeln(J, '  <v_spi3_int_priority>'+inttostr(v_spi3_int_priority)+'</v_spi3_int_priority>');
    writeln(J, '  <v_spi3_int_subpriority>'+inttostr(v_spi3_int_subpriority)+'</v_spi3_int_subpriority>');

{===================
 UART4 CONFIGURATION
 -------------------}
    writeln(J, '  <v_c_uart4>'+prepbool(v_c_uart4)+'</v_c_uart4>');
    writeln(J, '  <v_cb_baud_idx_uart4>'+inttostr(v_cb_baud_idx_uart4)+'</v_cb_baud_idx_uart4>');
    writeln(J, '  <v_cb_wordlng_idx_uart4>'+inttostr(v_cb_wordlng_idx_uart4)+'</v_cb_wordlng_idx_uart4>');
    writeln(J, '  <v_cb_parity_idx_uart4>'+inttostr(v_cb_parity_idx_uart4)+'</v_cb_parity_idx_uart4>');
    writeln(J, '  <v_cb_stop_idx_uart4>'+inttostr(v_cb_stop_idx_uart4)+'</v_cb_stop_idx_uart4>');
    writeln(J, '  <v_cb_datadir_idx_uart4>'+inttostr(v_cb_datadir_idx_uart4)+'</v_cb_datadir_idx_uart4>');
    writeln(J, '  <v_cb_sampling_idx_uart4>'+inttostr(v_cb_sampling_idx_uart4)+'</v_cb_sampling_idx_uart4>');
    writeln(J, '  <v_c_gb_int_uart4>'+prepbool(v_c_gb_int_uart4)+'</v_c_gb_int_uart4>');
    writeln(J, '  <v_uart4_int_priority>'+inttostr(v_uart4_int_priority)+'</v_uart4_int_priority>');
    writeln(J, '  <v_uart4_int_subpriority>'+inttostr(v_uart4_int_subpriority)+'</v_uart4_int_subpriority>');


{===================
 UART5 CONFIGURATION
 -------------------}
    writeln(J, '  <v_c_uart5>'+prepbool(v_c_uart5)+'</v_c_uart5>');
    writeln(J, '  <v_cb_baud_idx_uart5>'+inttostr(v_cb_baud_idx_uart5)+'</v_cb_baud_idx_uart5>');
    writeln(J, '  <v_cb_wordlng_idx_uart5>'+inttostr(v_cb_wordlng_idx_uart5)+'</v_cb_wordlng_idx_uart5>');
    writeln(J, '  <v_cb_parity_idx_uart5>'+inttostr(v_cb_parity_idx_uart5)+'</v_cb_parity_idx_uart5>');
    writeln(J, '  <v_cb_stop_idx_uart5>'+inttostr(v_cb_stop_idx_uart5)+'</v_cb_stop_idx_uart5>');
    writeln(J, '  <v_cb_datadir_idx_uart5>'+inttostr(v_cb_datadir_idx_uart5)+'</v_cb_datadir_idx_uart5>');
    writeln(J, '  <v_cb_sampling_idx_uart5>'+inttostr(v_cb_sampling_idx_uart5)+'</v_cb_sampling_idx_uart5>');
    writeln(J, '  <v_c_gb_int_uart5>'+prepbool(v_c_gb_int_uart5)+'</v_c_gb_int_uart5>');
    writeln(J, '  <v_uart5_int_priority>'+inttostr(v_uart5_int_priority)+'</v_uart5_int_priority>');
    writeln(J, '  <v_uart5_int_subpriority>'+inttostr(v_uart5_int_subpriority)+'</v_uart5_int_subpriority>');

{====================
 USART2 CONFIGURATION
 --------------------}
    writeln(J, '  <v_cb_baud_idx_usart2>'+inttostr(v_cb_baud_idx_usart2)+'</v_cb_baud_idx_usart2>');
    writeln(J, '  <v_cb_wordlng_idx_usart2>'+inttostr(v_cb_wordlng_idx_usart2)+'</v_cb_wordlng_idx_usart2>');
    writeln(J, '  <v_cb_parity_idx_usart2>'+inttostr(v_cb_parity_idx_usart2)+'</v_cb_parity_idx_usart2>');
    writeln(J, '  <v_cb_stop_idx_usart2>'+inttostr(v_cb_stop_idx_usart2)+'</v_cb_stop_idx_usart2>');
    writeln(J, '  <v_cb_datadir_idx_usart2>'+inttostr(v_cb_datadir_idx_usart2)+'</v_cb_datadir_idx_usart2>');
    writeln(J, '  <v_cb_sampling_idx_usart2>'+inttostr(v_cb_sampling_idx_usart2)+'</v_cb_sampling_idx_usart2>');
    writeln(J, '  <v_c_gb_int_usart2>'+prepbool(v_c_gb_int_usart2)+'</v_c_gb_int_usart2>');
    writeln(J, '  <v_usart2_int_priority>'+inttostr(v_usart2_int_priority)+'</v_usart2_int_priority>');
    writeln(J, '  <v_usart2_int_subpriority>'+inttostr(v_usart2_int_subpriority)+'</v_usart2_int_subpriority>');

{====================
 USART1 CONFIGURATION
 --------------------}
    writeln(J, '  <v_c_usart1>'+prepbool(v_c_usart1)+'</v_c_usart1>');
    writeln(J, '  <v_cb_baud_idx_usart1>'+inttostr(v_cb_baud_idx_usart1)+'</v_cb_baud_idx_usart1>');
    writeln(J, '  <v_cb_wordlng_idx_usart1>'+inttostr(v_cb_wordlng_idx_usart1)+'</v_cb_wordlng_idx_usart1>');
    writeln(J, '  <v_cb_parity_idx_usart1>'+inttostr(v_cb_parity_idx_usart1)+'</v_cb_parity_idx_usart1>');
    writeln(J, '  <v_cb_stop_idx_usart1>'+inttostr(v_cb_stop_idx_usart1)+'</v_cb_stop_idx_usart1>');
    writeln(J, '  <v_cb_datadir_idx_usart1>'+inttostr(v_cb_datadir_idx_usart1)+'</v_cb_datadir_idx_usart1>');
    writeln(J, '  <v_cb_sampling_idx_usart1>'+inttostr(v_cb_sampling_idx_usart1)+'</v_cb_sampling_idx_usart1>');
    writeln(J, '  <v_c_gb_int_usart1>'+prepbool(v_c_gb_int_usart1)+'</v_c_gb_int_usart1>');
    writeln(J, '  <v_usart1_int_priority>'+inttostr(v_usart1_int_priority)+'</v_usart1_int_priority>');
    writeln(J, '  <v_usart1_int_subpriority>'+inttostr(v_usart1_int_subpriority)+'</v_usart1_int_subpriority>');

{===============
 ADC1 PERIPHERAL
 ---------------}
    writeln(J, '  <v_adc1_res_idx>'+inttostr(v_adc1_res_idx)+'</v_adc1_res_idx>');
    writeln(J, '  <v_adc1_scan_idx>'+inttostr(v_adc1_scan_idx)+'</v_adc1_scan_idx>');
    writeln(J, '  <v_adc1_continuous_idx>'+inttostr(v_adc1_continuous_idx)+'</v_adc1_continuous_idx>');
    writeln(J, '  <v_adc1_tce_idx>'+inttostr(v_adc1_tce_idx)+'</v_adc1_tce_idx>');
    writeln(J, '  <v_adc1_tc_idx>'+inttostr(v_adc1_tc_idx)+'</v_adc1_tc_idx>');
    writeln(J, '  <v_adc1_align_idx>'+inttostr(v_adc1_align_idx)+'</v_adc1_align_idx>');
    writeln(J, '  <v_adc1_prescaler_idx>'+inttostr(v_adc1_prescaler_idx)+'</v_adc1_prescaler_idx>');
    writeln(J, '  <v_adc1_nrconv_idx>'+inttostr(v_adc1_nrconv_idx)+'</v_adc1_nrconv_idx>');
    writeln(J, '  <v_c_gb_int_adc1>'+prepbool(v_c_gb_int_adc1)+'</v_c_gb_int_adc1>');
    writeln(J, '  <v_adc1_int_priority>'+inttostr(v_adc1_int_priority)+'</v_adc1_int_priority>');
    writeln(J, '  <v_adc1_int_subpriority>'+inttostr(v_adc1_int_subpriority)+'</v_adc1_int_subpriority>');


{================}
{MORPHO CONNECTOR}
{----------------}
{CN7 connector, the non-paired pins row - 1,3,5, etc.}
{PC10}
    writeln(J, '  <v_cb_pc10_idx>'+inttostr(v_cb_pc10_idx)+'</v_cb_pc10_idx>');
    writeln(J, '  <v_e_pc10_txt>'+v_e_pc10_txt+'</v_e_pc10_txt>');
    writeln(J, '  <v_pc10_id>'+v_pc10_id+'</v_pc10_id>');
    writeln(J, '  <v_pc10_func>'+v_pc10_func+'</v_pc10_func>');
    writeln(J, '  <v_pc10_o_lvl_idx>'+inttostr(v_pc10_o_lvl_idx)+'</v_pc10_o_lvl_idx>');
    writeln(J, '  <v_pc10_o_mode_idx>'+inttostr(v_pc10_o_mode_idx)+'</v_pc10_o_mode_idx>');
    writeln(J, '  <v_pc10_i_mode_idx>'+inttostr(v_pc10_i_mode_idx)+'</v_pc10_i_mode_idx>');
    writeln(J, '  <v_pc10_a_mode_idx>'+inttostr(v_pc10_a_mode_idx)+'</v_pc10_a_mode_idx>');
    writeln(J, '  <v_pc10_i2c_mode_idx>'+inttostr(v_pc10_i2c_mode_idx)+'</v_pc10_i2c_mode_idx>');
    writeln(J, '  <v_pc10_spi_mode_idx>'+inttostr(v_pc10_spi_mode_idx)+'</v_pc10_spi_mode_idx>');
    writeln(J, '  <v_pc10_int_mode_idx>'+inttostr(v_pc10_int_mode_idx)+'</v_pc10_int_mode_idx>');
    writeln(J, '  <v_pc10_pull_updown_idx>'+inttostr(v_pc10_pull_updown_idx)+'</v_pc10_pull_updown_idx>');
    writeln(J, '  <v_pc10_speed_idx>'+inttostr(v_pc10_speed_idx)+'</v_pc10_speed_idx>');
    writeln(J, '  <v_pc10_name>'+v_pc10_name+'</v_pc10_name>');

{PC12}
    writeln(J, '  <v_cb_pc12_idx>'+inttostr(v_cb_pc12_idx)+'</v_cb_pc12_idx>');
    writeln(J, '  <v_e_pc12_txt>'+v_e_pc12_txt+'</v_e_pc12_txt>');
    writeln(J, '  <v_pc12_id>'+v_pc12_id+'</v_pc12_id>');
    writeln(J, '  <v_pc12_func>'+v_pc12_func+'</v_pc12_func>');
    writeln(J, '  <v_pc12_o_lvl_idx>'+inttostr(v_pc12_o_lvl_idx)+'</v_pc12_o_lvl_idx>');
    writeln(J, '  <v_pc12_o_mode_idx>'+inttostr(v_pc12_o_mode_idx)+'</v_pc12_o_mode_idx>');
    writeln(J, '  <v_pc12_i_mode_idx>'+inttostr(v_pc12_i_mode_idx)+'</v_pc12_i_mode_idx>');
    writeln(J, '  <v_pc12_a_mode_idx>'+inttostr(v_pc12_a_mode_idx)+'</v_pc12_a_mode_idx>');
    writeln(J, '  <v_pc12_i2c_mode_idx>'+inttostr(v_pc12_i2c_mode_idx)+'</v_pc12_i2c_mode_idx>');
    writeln(J, '  <v_pc12_spi_mode_idx>'+inttostr(v_pc12_spi_mode_idx)+'</v_pc12_spi_mode_idx>');
    writeln(J, '  <v_pc12_int_mode_idx>'+inttostr(v_pc12_int_mode_idx)+'</v_pc12_int_mode_idx>');
    writeln(J, '  <v_pc12_pull_updown_idx>'+inttostr(v_pc12_pull_updown_idx)+'</v_pc12_pull_updown_idx>');
    writeln(J, '  <v_pc12_speed_idx>'+inttostr(v_pc12_speed_idx)+'</v_pc12_speed_idx>');
    writeln(J, '  <v_pc12_name>'+v_pc12_name+'</v_pc12_name>');


{PA15}
    writeln(J, '  <v_cb_pa15_idx>'+inttostr(v_cb_pa15_idx)+'</v_cb_pa15_idx>');
    writeln(J, '  <v_e_pa15_txt>'+v_e_pa15_txt+'</v_e_pa15_txt>');
    writeln(J, '  <v_pa15_id>'+v_pa15_id+'</v_pa15_id>');
    writeln(J, '  <v_pa15_func>'+v_pa15_func+'</v_pa15_func>');
    writeln(J, '  <v_pa15_o_lvl_idx>'+inttostr(v_pa15_o_lvl_idx)+'</v_pa15_o_lvl_idx>');
    writeln(J, '  <v_pa15_o_mode_idx>'+inttostr(v_pa15_o_mode_idx)+'</v_pa15_o_mode_idx>');
    writeln(J, '  <v_pa15_i_mode_idx>'+inttostr(v_pa15_i_mode_idx)+'</v_pa15_i_mode_idx>');
    writeln(J, '  <v_pa15_a_mode_idx>'+inttostr(v_pa15_a_mode_idx)+'</v_pa15_a_mode_idx>');
    writeln(J, '  <v_pa15_i2c_mode_idx>'+inttostr(v_pa15_i2c_mode_idx)+'</v_pa15_i2c_mode_idx>');
    writeln(J, '  <v_pa15_spi_mode_idx>'+inttostr(v_pa15_spi_mode_idx)+'</v_pa15_spi_mode_idx>');
    writeln(J, '  <v_pa15_int_mode_idx>'+inttostr(v_pa15_int_mode_idx)+'</v_pa15_int_mode_idx>');
    writeln(J, '  <v_pa15_pull_updown_idx>'+inttostr(v_pa15_pull_updown_idx)+'</v_pa15_pull_updown_idx>');
    writeln(J, '  <v_pa15_speed_idx>'+inttostr(v_pa15_speed_idx)+'</v_pa15_speed_idx>');
    writeln(J, '  <v_pa15_name>'+v_pa15_name+'</v_pa15_name>');

{PB7}
    writeln(J, '  <v_cb_pb7_idx>'+inttostr(v_cb_pb7_idx)+'</v_cb_pb7_idx>');
    writeln(J, '  <v_e_pb7_txt>'+v_e_pb7_txt+'</v_e_pb7_txt>');
    writeln(J, '  <v_pb7_id>'+v_pb7_id+'</v_pb7_id>');
    writeln(J, '  <v_pb7_func>'+v_pb7_func+'</v_pb7_func>');
    writeln(J, '  <v_pb7_o_lvl_idx>'+inttostr(v_pb7_o_lvl_idx)+'</v_pb7_o_lvl_idx>');
    writeln(J, '  <v_pb7_o_mode_idx>'+inttostr(v_pb7_o_mode_idx)+'</v_pb7_o_mode_idx>');
    writeln(J, '  <v_pb7_i_mode_idx>'+inttostr(v_pb7_i_mode_idx)+'</v_pb7_i_mode_idx>');
    writeln(J, '  <v_pb7_a_mode_idx>'+inttostr(v_pb7_a_mode_idx)+'</v_pb7_a_mode_idx>');
    writeln(J, '  <v_pb7_i2c_mode_idx>'+inttostr(v_pb7_i2c_mode_idx)+'</v_pb7_i2c_mode_idx>');
    writeln(J, '  <v_pb7_spi_mode_idx>'+inttostr(v_pb7_spi_mode_idx)+'</v_pb7_spi_mode_idx>');
    writeln(J, '  <v_pb7_int_mode_idx>'+inttostr(v_pb7_int_mode_idx)+'</v_pb7_int_mode_idx>');
    writeln(J, '  <v_pb7_pull_updown_idx>'+inttostr(v_pb7_pull_updown_idx)+'</v_pb7_pull_updown_idx>');
    writeln(J, '  <v_pb7_speed_idx>'+inttostr(v_pb7_speed_idx)+'</v_pb7_speed_idx>');
    writeln(J, '  <v_pb7_name>'+v_pb7_name+'</v_pb7_name>');

{PC13} {- the user button on Nucleo}
    writeln(J, '  <v_cb_pc13_idx>'+inttostr(v_cb_pc13_idx)+'</v_cb_pc13_idx>');
    writeln(J, '  <v_e_pc13_txt>'+v_e_pc13_txt+'</v_e_pc13_txt>');
    writeln(J, '  <v_pc13_id>'+v_pc13_id+'</v_pc13_id>');
    writeln(J, '  <v_pc13_func>'+v_pc13_func+'</v_pc13_func>');
    writeln(J, '  <v_pc13_o_lvl_idx>'+inttostr(v_pc13_o_lvl_idx)+'</v_pc13_o_lvl_idx>');
    writeln(J, '  <v_pc13_o_mode_idx>'+inttostr(v_pc13_o_mode_idx)+'</v_pc13_o_mode_idx>');
    writeln(J, '  <v_pc13_i_mode_idx>'+inttostr(v_pc13_i_mode_idx)+'</v_pc13_i_mode_idx>');
    writeln(J, '  <v_pc13_a_mode_idx>'+inttostr(v_pc13_a_mode_idx)+'</v_pc13_a_mode_idx>');
    writeln(J, '  <v_pc13_i2c_mode_idx>'+inttostr(v_pc13_i2c_mode_idx)+'</v_pc13_i2c_mode_idx>');
    writeln(J, '  <v_pc13_spi_mode_idx>'+inttostr(v_pc13_spi_mode_idx)+'</v_pc13_spi_mode_idx>');
    writeln(J, '  <v_pc13_int_mode_idx>'+inttostr(v_pc13_int_mode_idx)+'</v_pc13_int_mode_idx>');
    writeln(J, '  <v_pc13_pull_updown_idx>'+inttostr(v_pc13_pull_updown_idx)+'</v_pc13_pull_updown_idx>');
    writeln(J, '  <v_pc13_speed_idx>'+inttostr(v_pc13_speed_idx)+'</v_pc13_speed_idx>');
    writeln(J, '  <v_pc13_name>'+v_pc13_name+'</v_pc13_name>');

{PC2}
    writeln(J, '  <v_cb_pc2_idx>'+inttostr(v_cb_pc2_idx)+'</v_cb_pc2_idx>');
    writeln(J, '  <v_e_pc2_txt>'+v_e_pc2_txt+'</v_e_pc2_txt>');
    writeln(J, '  <v_pc2_id>'+v_pc2_id+'</v_pc2_id>');
    writeln(J, '  <v_pc2_func>'+v_pc2_func+'</v_pc2_func>');
    writeln(J, '  <v_pc2_o_lvl_idx>'+inttostr(v_pc2_o_lvl_idx)+'</v_pc2_o_lvl_idx>');
    writeln(J, '  <v_pc2_o_mode_idx>'+inttostr(v_pc2_o_mode_idx)+'</v_pc2_o_mode_idx>');
    writeln(J, '  <v_pc2_i_mode_idx>'+inttostr(v_pc2_i_mode_idx)+'</v_pc2_i_mode_idx>');
    writeln(J, '  <v_pc2_a_mode_idx>'+inttostr(v_pc2_a_mode_idx)+'</v_pc2_a_mode_idx>');
    writeln(J, '  <v_pc2_i2c_mode_idx>'+inttostr(v_pc2_i2c_mode_idx)+'</v_pc2_i2c_mode_idx>');
    writeln(J, '  <v_pc2_spi_mode_idx>'+inttostr(v_pc2_spi_mode_idx)+'</v_pc2_spi_mode_idx>');
    writeln(J, '  <v_pc2_int_mode_idx>'+inttostr(v_pc2_int_mode_idx)+'</v_pc2_int_mode_idx>');
    writeln(J, '  <v_pc2_pull_updown_idx>'+inttostr(v_pc2_pull_updown_idx)+'</v_pc2_pull_updown_idx>');
    writeln(J, '  <v_pc2_speed_idx>'+inttostr(v_pc2_speed_idx)+'</v_pc2_speed_idx>');
    writeln(J, '  <v_pc2_name>'+v_pc2_name+'</v_pc2_name>');

{PC3}
    writeln(J, '  <v_cb_pc3_idx>'+inttostr(v_cb_pc3_idx)+'</v_cb_pc3_idx>');
    writeln(J, '  <v_e_pc3_txt>'+v_e_pc3_txt+'</v_e_pc3_txt>');
    writeln(J, '  <v_pc3_id>'+v_pc3_id+'</v_pc3_id>');
    writeln(J, '  <v_pc3_func>'+v_pc3_func+'</v_pc3_func>');
    writeln(J, '  <v_pc3_o_lvl_idx>'+inttostr(v_pc3_o_lvl_idx)+'</v_pc3_o_lvl_idx>');
    writeln(J, '  <v_pc3_o_mode_idx>'+inttostr(v_pc3_o_mode_idx)+'</v_pc3_o_mode_idx>');
    writeln(J, '  <v_pc3_i_mode_idx>'+inttostr(v_pc3_i_mode_idx)+'</v_pc3_i_mode_idx>');
    writeln(J, '  <v_pc3_a_mode_idx>'+inttostr(v_pc3_a_mode_idx)+'</v_pc3_a_mode_idx>');
    writeln(J, '  <v_pc3_i2c_mode_idx>'+inttostr(v_pc3_i2c_mode_idx)+'</v_pc3_i2c_mode_idx>');
    writeln(J, '  <v_pc3_spi_mode_idx>'+inttostr(v_pc3_spi_mode_idx)+'</v_pc3_spi_mode_idx>');
    writeln(J, '  <v_pc3_int_mode_idx>'+inttostr(v_pc3_int_mode_idx)+'</v_pc3_int_mode_idx>');
    writeln(J, '  <v_pc3_pull_updown_idx>'+inttostr(v_pc3_pull_updown_idx)+'</v_pc3_pull_updown_idx>');
    writeln(J, '  <v_pc3_speed_idx>'+inttostr(v_pc3_speed_idx)+'</v_pc3_speed_idx>');
    writeln(J, '  <v_pc3_name>'+v_pc3_name+'</v_pc3_name>');

{CN7 connector, the paired pins row - 2,4,6, etc.}
{PC11}
    writeln(J, '  <v_cb_pc11_idx>'+inttostr(v_cb_pc11_idx)+'</v_cb_pc11_idx>');
    writeln(J, '  <v_e_pc11_txt>'+v_e_pc11_txt+'</v_e_pc11_txt>');
    writeln(J, '  <v_pc11_id>'+v_pc11_id+'</v_pc11_id>');
    writeln(J, '  <v_pc11_func>'+v_pc11_func+'</v_pc11_func>');
    writeln(J, '  <v_pc11_o_lvl_idx>'+inttostr(v_pc11_o_lvl_idx)+'</v_pc11_o_lvl_idx>');
    writeln(J, '  <v_pc11_o_mode_idx>'+inttostr(v_pc11_o_mode_idx)+'</v_pc11_o_mode_idx>');
    writeln(J, '  <v_pc11_i_mode_idx>'+inttostr(v_pc11_i_mode_idx)+'</v_pc11_i_mode_idx>');
    writeln(J, '  <v_pc11_a_mode_idx>'+inttostr(v_pc11_a_mode_idx)+'</v_pc11_a_mode_idx>');
    writeln(J, '  <v_pc11_i2c_mode_idx>'+inttostr(v_pc11_i2c_mode_idx)+'</v_pc11_i2c_mode_idx>');
    writeln(J, '  <v_pc11_spi_mode_idx>'+inttostr(v_pc11_spi_mode_idx)+'</v_pc11_spi_mode_idx>');
    writeln(J, '  <v_pc11_int_mode_idx>'+inttostr(v_pc11_int_mode_idx)+'</v_pc11_int_mode_idx>');
    writeln(J, '  <v_pc11_pull_updown_idx>'+inttostr(v_pc11_pull_updown_idx)+'</v_pc11_pull_updown_idx>');
    writeln(J, '  <v_pc11_speed_idx>'+inttostr(v_pc11_speed_idx)+'</v_pc11_speed_idx>');
    writeln(J, '  <v_pc11_name>'+v_pc11_name+'</v_pc11_name>');

{PD2}
    writeln(J, '  <v_cb_pd2_idx>'+inttostr(v_cb_pd2_idx)+'</v_cb_pd2_idx>');
    writeln(J, '  <v_e_pd2_txt>'+v_e_pd2_txt+'</v_e_pd2_txt>');
    writeln(J, '  <v_pd2_id>'+v_pd2_id+'</v_pd2_id>');
    writeln(J, '  <v_pd2_func>'+v_pd2_func+'</v_pd2_func>');
    writeln(J, '  <v_pd2_o_lvl_idx>'+inttostr(v_pd2_o_lvl_idx)+'</v_pd2_o_lvl_idx>');
    writeln(J, '  <v_pd2_o_mode_idx>'+inttostr(v_pd2_o_mode_idx)+'</v_pd2_o_mode_idx>');
    writeln(J, '  <v_pd2_i_mode_idx>'+inttostr(v_pd2_i_mode_idx)+'</v_pd2_i_mode_idx>');
    writeln(J, '  <v_pd2_a_mode_idx>'+inttostr(v_pd2_a_mode_idx)+'</v_pd2_a_mode_idx>');
    writeln(J, '  <v_pd2_i2c_mode_idx>'+inttostr(v_pd2_i2c_mode_idx)+'</v_pd2_i2c_mode_idx>');
    writeln(J, '  <v_pd2_spi_mode_idx>'+inttostr(v_pd2_spi_mode_idx)+'</v_pd2_spi_mode_idx>');
    writeln(J, '  <v_pd2_int_mode_idx>'+inttostr(v_pd2_int_mode_idx)+'</v_pd2_int_mode_idx>');
    writeln(J, '  <v_pd2_pull_updown_idx>'+inttostr(v_pd2_pull_updown_idx)+'</v_pd2_pull_updown_idx>');
    writeln(J, '  <v_pd2_speed_idx>'+inttostr(v_pd2_speed_idx)+'</v_pd2_speed_idx>');
    writeln(J, '  <v_pd2_name>'+v_pd2_name+'</v_pd2_name>');

{PA0}
    writeln(J, '  <v_cb_pa0_idx>'+inttostr(v_cb_pa0_idx)+'</v_cb_pa0_idx>');
    writeln(J, '  <v_e_pa0_txt>'+v_e_pa0_txt+'</v_e_pa0_txt>');
    writeln(J, '  <v_pa0_id>'+v_pa0_id+'</v_pa0_id>');
    writeln(J, '  <v_pa0_func>'+v_pa0_func+'</v_pa0_func>');
    writeln(J, '  <v_pa0_o_lvl_idx>'+inttostr(v_pa0_o_lvl_idx)+'</v_pa0_o_lvl_idx>');
    writeln(J, '  <v_pa0_o_mode_idx>'+inttostr(v_pa0_o_mode_idx)+'</v_pa0_o_mode_idx>');
    writeln(J, '  <v_pa0_i_mode_idx>'+inttostr(v_pa0_i_mode_idx)+'</v_pa0_i_mode_idx>');
    writeln(J, '  <v_pa0_a_mode_idx>'+inttostr(v_pa0_a_mode_idx)+'</v_pa0_a_mode_idx>');
    writeln(J, '  <v_pa0_i2c_mode_idx>'+inttostr(v_pa0_i2c_mode_idx)+'</v_pa0_i2c_mode_idx>');
    writeln(J, '  <v_pa0_spi_mode_idx>'+inttostr(v_pa0_spi_mode_idx)+'</v_pa0_spi_mode_idx>');
    writeln(J, '  <v_pa0_int_mode_idx>'+inttostr(v_pa0_int_mode_idx)+'</v_pa0_int_mode_idx>');
    writeln(J, '  <v_pa0_pull_updown_idx>'+inttostr(v_pa0_pull_updown_idx)+'</v_pa0_pull_updown_idx>');
    writeln(J, '  <v_pa0_speed_idx>'+inttostr(v_pa0_speed_idx)+'</v_pa0_speed_idx>');
    writeln(J, '  <v_pa0_name>'+v_pa0_name+'</v_pa0_name>');

{PA1}
    writeln(J, '  <v_cb_pa1_idx>'+inttostr(v_cb_pa1_idx)+'</v_cb_pa1_idx>');
    writeln(J, '  <v_e_pa1_txt>'+v_e_pa1_txt+'</v_e_pa1_txt>');
    writeln(J, '  <v_pa1_id>'+v_pa1_id+'</v_pa1_id>');
    writeln(J, '  <v_pa1_func>'+v_pa1_func+'</v_pa1_func>');
    writeln(J, '  <v_pa1_o_lvl_idx>'+inttostr(v_pa1_o_lvl_idx)+'</v_pa1_o_lvl_idx>');
    writeln(J, '  <v_pa1_o_mode_idx>'+inttostr(v_pa1_o_mode_idx)+'</v_pa1_o_mode_idx>');
    writeln(J, '  <v_pa1_i_mode_idx>'+inttostr(v_pa1_i_mode_idx)+'</v_pa1_i_mode_idx>');
    writeln(J, '  <v_pa1_a_mode_idx>'+inttostr(v_pa1_a_mode_idx)+'</v_pa1_a_mode_idx>');
    writeln(J, '  <v_pa1_i2c_mode_idx>'+inttostr(v_pa1_i2c_mode_idx)+'</v_pa1_i2c_mode_idx>');
    writeln(J, '  <v_pa1_spi_mode_idx>'+inttostr(v_pa1_spi_mode_idx)+'</v_pa1_spi_mode_idx>');
    writeln(J, '  <v_pa1_int_mode_idx>'+inttostr(v_pa1_int_mode_idx)+'</v_pa1_int_mode_idx>');
    writeln(J, '  <v_pa1_pull_updown_idx>'+inttostr(v_pa1_pull_updown_idx)+'</v_pa1_pull_updown_idx>');
    writeln(J, '  <v_pa1_speed_idx>'+inttostr(v_pa1_speed_idx)+'</v_pa1_speed_idx>');
    writeln(J, '  <v_pa1_name>'+v_pa1_name+'</v_pa1_name>');

{PA4}
    writeln(J, '  <v_cb_pa4_idx>'+inttostr(v_cb_pa4_idx)+'</v_cb_pa4_idx>');
    writeln(J, '  <v_e_pa4_txt>'+v_e_pa4_txt+'</v_e_pa4_txt>');
    writeln(J, '  <v_pa4_id>'+v_pa4_id+'</v_pa4_id>');
    writeln(J, '  <v_pa4_func>'+v_pa4_func+'</v_pa4_func>');
    writeln(J, '  <v_pa4_o_lvl_idx>'+inttostr(v_pa4_o_lvl_idx)+'</v_pa4_o_lvl_idx>');
    writeln(J, '  <v_pa4_o_mode_idx>'+inttostr(v_pa4_o_mode_idx)+'</v_pa4_o_mode_idx>');
    writeln(J, '  <v_pa4_i_mode_idx>'+inttostr(v_pa4_i_mode_idx)+'</v_pa4_i_mode_idx>');
    writeln(J, '  <v_pa4_a_mode_idx>'+inttostr(v_pa4_a_mode_idx)+'</v_pa4_a_mode_idx>');
    writeln(J, '  <v_pa4_i2c_mode_idx>'+inttostr(v_pa4_i2c_mode_idx)+'</v_pa4_i2c_mode_idx>');
    writeln(J, '  <v_pa4_spi_mode_idx>'+inttostr(v_pa4_spi_mode_idx)+'</v_pa4_spi_mode_idx>');
    writeln(J, '  <v_pa4_int_mode_idx>'+inttostr(v_pa4_int_mode_idx)+'</v_pa4_int_mode_idx>');
    writeln(J, '  <v_pa4_pull_updown_idx>'+inttostr(v_pa4_pull_updown_idx)+'</v_pa4_pull_updown_idx>');
    writeln(J, '  <v_pa4_speed_idx>'+inttostr(v_pa4_speed_idx)+'</v_pa4_speed_idx>');
    writeln(J, '  <v_pa4_name>'+v_pa4_name+'</v_pa4_name>');

{PB0}
    writeln(J, '  <v_cb_pb0_idx>'+inttostr(v_cb_pb0_idx)+'</v_cb_pb0_idx>');
    writeln(J, '  <v_e_pb0_txt>'+v_e_pb0_txt+'</v_e_pb0_txt>');
    writeln(J, '  <v_pb0_id>'+v_pb0_id+'</v_pb0_id>');
    writeln(J, '  <v_pb0_func>'+v_pb0_func+'</v_pb0_func>');
    writeln(J, '  <v_pb0_o_lvl_idx>'+inttostr(v_pb0_o_lvl_idx)+'</v_pb0_o_lvl_idx>');
    writeln(J, '  <v_pb0_o_mode_idx>'+inttostr(v_pb0_o_mode_idx)+'</v_pb0_o_mode_idx>');
    writeln(J, '  <v_pb0_i_mode_idx>'+inttostr(v_pb0_i_mode_idx)+'</v_pb0_i_mode_idx>');
    writeln(J, '  <v_pb0_a_mode_idx>'+inttostr(v_pb0_a_mode_idx)+'</v_pb0_a_mode_idx>');
    writeln(J, '  <v_pb0_i2c_mode_idx>'+inttostr(v_pb0_i2c_mode_idx)+'</v_pb0_i2c_mode_idx>');
    writeln(J, '  <v_pb0_spi_mode_idx>'+inttostr(v_pb0_spi_mode_idx)+'</v_pb0_spi_mode_idx>');
    writeln(J, '  <v_pb0_int_mode_idx>'+inttostr(v_pb0_int_mode_idx)+'</v_pb0_int_mode_idx>');
    writeln(J, '  <v_pb0_pull_updown_idx>'+inttostr(v_pb0_pull_updown_idx)+'</v_pb0_pull_updown_idx>');
    writeln(J, '  <v_pb0_speed_idx>'+inttostr(v_pb0_speed_idx)+'</v_pb0_speed_idx>');
    writeln(J, '  <v_pb0_name>'+v_pb0_name+'</v_pb0_name>');

{PC1}
    writeln(J, '  <v_cb_pc1_idx>'+inttostr(v_cb_pc1_idx)+'</v_cb_pc1_idx>');
    writeln(J, '  <v_e_pc1_txt>'+v_e_pc1_txt+'</v_e_pc1_txt>');
    writeln(J, '  <v_pc1_id>'+v_pc1_id+'</v_pc1_id>');
    writeln(J, '  <v_pc1_func>'+v_pc1_func+'</v_pc1_func>');
    writeln(J, '  <v_pc1_o_lvl_idx>'+inttostr(v_pc1_o_lvl_idx)+'</v_pc1_o_lvl_idx>');
    writeln(J, '  <v_pc1_o_mode_idx>'+inttostr(v_pc1_o_mode_idx)+'</v_pc1_o_mode_idx>');
    writeln(J, '  <v_pc1_i_mode_idx>'+inttostr(v_pc1_i_mode_idx)+'</v_pc1_i_mode_idx>');
    writeln(J, '  <v_pc1_a_mode_idx>'+inttostr(v_pc1_a_mode_idx)+'</v_pc1_a_mode_idx>');
    writeln(J, '  <v_pc1_i2c_mode_idx>'+inttostr(v_pc1_i2c_mode_idx)+'</v_pc1_i2c_mode_idx>');
    writeln(J, '  <v_pc1_spi_mode_idx>'+inttostr(v_pc1_spi_mode_idx)+'</v_pc1_spi_mode_idx>');
    writeln(J, '  <v_pc1_int_mode_idx>'+inttostr(v_pc1_int_mode_idx)+'</v_pc1_int_mode_idx>');
    writeln(J, '  <v_pc1_pull_updown_idx>'+inttostr(v_pc1_pull_updown_idx)+'</v_pc1_pull_updown_idx>');
    writeln(J, '  <v_pc1_speed_idx>'+inttostr(v_pc1_speed_idx)+'</v_pc1_speed_idx>');
    writeln(J, '  <v_pc1_name>'+v_pc1_name+'</v_pc1_name>');

{PC0}
    writeln(J, '  <v_cb_pc0_idx>'+inttostr(v_cb_pc0_idx)+'</v_cb_pc0_idx>');
    writeln(J, '  <v_e_pc0_txt>'+v_e_pc0_txt+'</v_e_pc0_txt>');
    writeln(J, '  <v_pc0_id>'+v_pc0_id+'</v_pc0_id>');
    writeln(J, '  <v_pc0_func>'+v_pc0_func+'</v_pc0_func>');
    writeln(J, '  <v_pc0_o_lvl_idx>'+inttostr(v_pc0_o_lvl_idx)+'</v_pc0_o_lvl_idx>');
    writeln(J, '  <v_pc0_o_mode_idx>'+inttostr(v_pc0_o_mode_idx)+'</v_pc0_o_mode_idx>');
    writeln(J, '  <v_pc0_i_mode_idx>'+inttostr(v_pc0_i_mode_idx)+'</v_pc0_i_mode_idx>');
    writeln(J, '  <v_pc0_a_mode_idx>'+inttostr(v_pc0_a_mode_idx)+'</v_pc0_a_mode_idx>');
    writeln(J, '  <v_pc0_i2c_mode_idx>'+inttostr(v_pc0_i2c_mode_idx)+'</v_pc0_i2c_mode_idx>');
    writeln(J, '  <v_pc0_spi_mode_idx>'+inttostr(v_pc0_spi_mode_idx)+'</v_pc0_spi_mode_idx>');
    writeln(J, '  <v_pc0_int_mode_idx>'+inttostr(v_pc0_int_mode_idx)+'</v_pc0_int_mode_idx>');
    writeln(J, '  <v_pc0_pull_updown_idx>'+inttostr(v_pc0_pull_updown_idx)+'</v_pc0_pull_updown_idx>');
    writeln(J, '  <v_pc0_speed_idx>'+inttostr(v_pc0_speed_idx)+'</v_pc0_speed_idx>');
    writeln(J, '  <v_pc0_name>'+v_pc0_name+'</v_pc0_name>');

{CN10 connector, the non-paired pins row - 1,3,5, etc.}
{PC9}
    writeln(J, '  <v_cb_pc9_idx>'+inttostr(v_cb_pc9_idx)+'</v_cb_pc9_idx>');
    writeln(J, '  <v_e_pc9_txt>'+v_e_pc9_txt+'</v_e_pc9_txt>');
    writeln(J, '  <v_pc9_id>'+v_pc9_id+'</v_pc9_id>');
    writeln(J, '  <v_pc9_func>'+v_pc9_func+'</v_pc9_func>');
    writeln(J, '  <v_pc9_o_lvl_idx>'+inttostr(v_pc9_o_lvl_idx)+'</v_pc9_o_lvl_idx>');
    writeln(J, '  <v_pc9_o_mode_idx>'+inttostr(v_pc9_o_mode_idx)+'</v_pc9_o_mode_idx>');
    writeln(J, '  <v_pc9_i_mode_idx>'+inttostr(v_pc9_i_mode_idx)+'</v_pc9_i_mode_idx>');
    writeln(J, '  <v_pc9_a_mode_idx>'+inttostr(v_pc9_a_mode_idx)+'</v_pc9_a_mode_idx>');
    writeln(J, '  <v_pc9_i2c_mode_idx>'+inttostr(v_pc9_i2c_mode_idx)+'</v_pc9_i2c_mode_idx>');
    writeln(J, '  <v_pc9_spi_mode_idx>'+inttostr(v_pc9_spi_mode_idx)+'</v_pc9_spi_mode_idx>');
    writeln(J, '  <v_pc9_int_mode_idx>'+inttostr(v_pc9_int_mode_idx)+'</v_pc9_int_mode_idx>');
    writeln(J, '  <v_pc9_pull_updown_idx>'+inttostr(v_pc9_pull_updown_idx)+'</v_pc9_pull_updown_idx>');
    writeln(J, '  <v_pc9_speed_idx>'+inttostr(v_pc9_speed_idx)+'</v_pc9_speed_idx>');
    writeln(J, '  <v_pc9_name>'+v_pc9_name+'</v_pc9_name>');

{PB8}
    writeln(J, '  <v_cb_pb8_idx>'+inttostr(v_cb_pb8_idx)+'</v_cb_pb8_idx>');
    writeln(J, '  <v_e_pb8_txt>'+v_e_pb8_txt+'</v_e_pb8_txt>');
    writeln(J, '  <v_pb8_id>'+v_pb8_id+'</v_pb8_id>');
    writeln(J, '  <v_pb8_func>'+v_pb8_func+'</v_pb8_func>');
    writeln(J, '  <v_pb8_o_lvl_idx>'+inttostr(v_pb8_o_lvl_idx)+'</v_pb8_o_lvl_idx>');
    writeln(J, '  <v_pb8_o_mode_idx>'+inttostr(v_pb8_o_mode_idx)+'</v_pb8_o_mode_idx>');
    writeln(J, '  <v_pb8_i_mode_idx>'+inttostr(v_pb8_i_mode_idx)+'</v_pb8_i_mode_idx>');
    writeln(J, '  <v_pb8_a_mode_idx>'+inttostr(v_pb8_a_mode_idx)+'</v_pb8_a_mode_idx>');
    writeln(J, '  <v_pb8_i2c_mode_idx>'+inttostr(v_pb8_i2c_mode_idx)+'</v_pb8_i2c_mode_idx>');
    writeln(J, '  <v_pb8_spi_mode_idx>'+inttostr(v_pb8_spi_mode_idx)+'</v_pb8_spi_mode_idx>');
    writeln(J, '  <v_pb8_int_mode_idx>'+inttostr(v_pb8_int_mode_idx)+'</v_pb8_int_mode_idx>');
    writeln(J, '  <v_pb8_pull_updown_idx>'+inttostr(v_pb8_pull_updown_idx)+'</v_pb8_pull_updown_idx>');
    writeln(J, '  <v_pb8_speed_idx>'+inttostr(v_pb8_speed_idx)+'</v_pb8_speed_idx>');
    writeln(J, '  <v_pb8_name>'+v_pb8_name+'</v_pb8_name>');

{PB9}
    writeln(J, '  <v_cb_pb9_idx>'+inttostr(v_cb_pb9_idx)+'</v_cb_pb9_idx>');
    writeln(J, '  <v_e_pb9_txt>'+v_e_pb9_txt+'</v_e_pb9_txt>');
    writeln(J, '  <v_pb9_id>'+v_pb9_id+'</v_pb9_id>');
    writeln(J, '  <v_pb9_func>'+v_pb9_func+'</v_pb9_func>');
    writeln(J, '  <v_pb9_o_lvl_idx>'+inttostr(v_pb9_o_lvl_idx)+'</v_pb9_o_lvl_idx>');
    writeln(J, '  <v_pb9_o_mode_idx>'+inttostr(v_pb9_o_mode_idx)+'</v_pb9_o_mode_idx>');
    writeln(J, '  <v_pb9_i_mode_idx>'+inttostr(v_pb9_i_mode_idx)+'</v_pb9_i_mode_idx>');
    writeln(J, '  <v_pb9_a_mode_idx>'+inttostr(v_pb9_a_mode_idx)+'</v_pb9_a_mode_idx>');
    writeln(J, '  <v_pb9_i2c_mode_idx>'+inttostr(v_pb9_i2c_mode_idx)+'</v_pb9_i2c_mode_idx>');
    writeln(J, '  <v_pb9_spi_mode_idx>'+inttostr(v_pb9_spi_mode_idx)+'</v_pb9_spi_mode_idx>');
    writeln(J, '  <v_pb9_int_mode_idx>'+inttostr(v_pb9_int_mode_idx)+'</v_pb9_int_mode_idx>');
    writeln(J, '  <v_pb9_pull_updown_idx>'+inttostr(v_pb9_pull_updown_idx)+'</v_pb9_pull_updown_idx>');
    writeln(J, '  <v_pb9_speed_idx>'+inttostr(v_pb9_speed_idx)+'</v_pb9_speed_idx>');
    writeln(J, '  <v_pb9_name>'+v_pb9_name+'</v_pb9_name>');

{PA5} {- the LD2 user LED}
    writeln(J, '  <v_cb_pa5_idx>'+inttostr(v_cb_pa5_idx)+'</v_cb_pa5_idx>');
    writeln(J, '  <v_e_pa5_txt>'+v_e_pa5_txt+'</v_e_pa5_txt>');
    writeln(J, '  <v_pa5_id>'+v_pa5_id+'</v_pa5_id>');
    writeln(J, '  <v_pa5_func>'+v_pa5_func+'</v_pa5_func>');
    writeln(J, '  <v_pa5_o_lvl_idx>'+inttostr(v_pa5_o_lvl_idx)+'</v_pa5_o_lvl_idx>');
    writeln(J, '  <v_pa5_o_mode_idx>'+inttostr(v_pa5_o_mode_idx)+'</v_pa5_o_mode_idx>');
    writeln(J, '  <v_pa5_i_mode_idx>'+inttostr(v_pa5_i_mode_idx)+'</v_pa5_i_mode_idx>');
    writeln(J, '  <v_pa5_a_mode_idx>'+inttostr(v_pa5_a_mode_idx)+'</v_pa5_a_mode_idx>');
    writeln(J, '  <v_pa5_i2c_mode_idx>'+inttostr(v_pa5_i2c_mode_idx)+'</v_pa5_i2c_mode_idx>');
    writeln(J, '  <v_pa5_spi_mode_idx>'+inttostr(v_pa5_spi_mode_idx)+'</v_pa5_spi_mode_idx>');
    writeln(J, '  <v_pa5_int_mode_idx>'+inttostr(v_pa5_int_mode_idx)+'</v_pa5_int_mode_idx>');
    writeln(J, '  <v_pa5_pull_updown_idx>'+inttostr(v_pa5_pull_updown_idx)+'</v_pa5_pull_updown_idx>');
    writeln(J, '  <v_pa5_speed_idx>'+inttostr(v_pa5_speed_idx)+'</v_pa5_speed_idx>');
    writeln(J, '  <v_pa5_name>'+v_pa5_name+'</v_pa5_name>');

{PA6}
    writeln(J, '  <v_cb_pa6_idx>'+inttostr(v_cb_pa6_idx)+'</v_cb_pa6_idx>');
    writeln(J, '  <v_e_pa6_txt>'+v_e_pa6_txt+'</v_e_pa6_txt>');
    writeln(J, '  <v_pa6_id>'+v_pa6_id+'</v_pa6_id>');
    writeln(J, '  <v_pa6_func>'+v_pa6_func+'</v_pa6_func>');
    writeln(J, '  <v_pa6_o_lvl_idx>'+inttostr(v_pa6_o_lvl_idx)+'</v_pa6_o_lvl_idx>');
    writeln(J, '  <v_pa6_o_mode_idx>'+inttostr(v_pa6_o_mode_idx)+'</v_pa6_o_mode_idx>');
    writeln(J, '  <v_pa6_i_mode_idx>'+inttostr(v_pa6_i_mode_idx)+'</v_pa6_i_mode_idx>');
    writeln(J, '  <v_pa6_a_mode_idx>'+inttostr(v_pa6_a_mode_idx)+'</v_pa6_a_mode_idx>');
    writeln(J, '  <v_pa6_i2c_mode_idx>'+inttostr(v_pa6_i2c_mode_idx)+'</v_pa6_i2c_mode_idx>');
    writeln(J, '  <v_pa6_spi_mode_idx>'+inttostr(v_pa6_spi_mode_idx)+'</v_pa6_spi_mode_idx>');
    writeln(J, '  <v_pa6_int_mode_idx>'+inttostr(v_pa6_int_mode_idx)+'</v_pa6_int_mode_idx>');
    writeln(J, '  <v_pa6_pull_updown_idx>'+inttostr(v_pa6_pull_updown_idx)+'</v_pa6_pull_updown_idx>');
    writeln(J, '  <v_pa6_speed_idx>'+inttostr(v_pa6_speed_idx)+'</v_pa6_speed_idx>');
    writeln(J, '  <v_pa6_name>'+v_pa6_name+'</v_pa6_name>');

{PA7}
    writeln(J, '  <v_cb_pa7_idx>'+inttostr(v_cb_pa7_idx)+'</v_cb_pa7_idx>');
    writeln(J, '  <v_e_pa7_txt>'+v_e_pa7_txt+'</v_e_pa7_txt>');
    writeln(J, '  <v_pa7_id>'+v_pa7_id+'</v_pa7_id>');
    writeln(J, '  <v_pa7_func>'+v_pa7_func+'</v_pa7_func>');
    writeln(J, '  <v_pa7_o_lvl_idx>'+inttostr(v_pa7_o_lvl_idx)+'</v_pa7_o_lvl_idx>');
    writeln(J, '  <v_pa7_o_mode_idx>'+inttostr(v_pa7_o_mode_idx)+'</v_pa7_o_mode_idx>');
    writeln(J, '  <v_pa7_i_mode_idx>'+inttostr(v_pa7_i_mode_idx)+'</v_pa7_i_mode_idx>');
    writeln(J, '  <v_pa7_a_mode_idx>'+inttostr(v_pa7_a_mode_idx)+'</v_pa7_a_mode_idx>');
    writeln(J, '  <v_pa7_i2c_mode_idx>'+inttostr(v_pa7_i2c_mode_idx)+'</v_pa7_i2c_mode_idx>');
    writeln(J, '  <v_pa7_spi_mode_idx>'+inttostr(v_pa7_spi_mode_idx)+'</v_pa7_spi_mode_idx>');
    writeln(J, '  <v_pa7_int_mode_idx>'+inttostr(v_pa7_int_mode_idx)+'</v_pa7_int_mode_idx>');
    writeln(J, '  <v_pa7_pull_updown_idx>'+inttostr(v_pa7_pull_updown_idx)+'</v_pa7_pull_updown_idx>');
    writeln(J, '  <v_pa7_speed_idx>'+inttostr(v_pa7_speed_idx)+'</v_pa7_speed_idx>');
    writeln(J, '  <v_pa7_name>'+v_pa7_name+'</v_pa7_name>');

{PB6}
    writeln(J, '  <v_cb_pb6_idx>'+inttostr(v_cb_pb6_idx)+'</v_cb_pb6_idx>');
    writeln(J, '  <v_e_pb6_txt>'+v_e_pb6_txt+'</v_e_pb6_txt>');
    writeln(J, '  <v_pb6_id>'+v_pb6_id+'</v_pb6_id>');
    writeln(J, '  <v_pb6_func>'+v_pb6_func+'</v_pb6_func>');
    writeln(J, '  <v_pb6_o_lvl_idx>'+inttostr(v_pb6_o_lvl_idx)+'</v_pb6_o_lvl_idx>');
    writeln(J, '  <v_pb6_o_mode_idx>'+inttostr(v_pb6_o_mode_idx)+'</v_pb6_o_mode_idx>');
    writeln(J, '  <v_pb6_i_mode_idx>'+inttostr(v_pb6_i_mode_idx)+'</v_pb6_i_mode_idx>');
    writeln(J, '  <v_pb6_a_mode_idx>'+inttostr(v_pb6_a_mode_idx)+'</v_pb6_a_mode_idx>');
    writeln(J, '  <v_pb6_i2c_mode_idx>'+inttostr(v_pb6_i2c_mode_idx)+'</v_pb6_i2c_mode_idx>');
    writeln(J, '  <v_pb6_spi_mode_idx>'+inttostr(v_pb6_spi_mode_idx)+'</v_pb6_spi_mode_idx>');
    writeln(J, '  <v_pb6_int_mode_idx>'+inttostr(v_pb6_int_mode_idx)+'</v_pb6_int_mode_idx>');
    writeln(J, '  <v_pb6_pull_updown_idx>'+inttostr(v_pb6_pull_updown_idx)+'</v_pb6_pull_updown_idx>');
    writeln(J, '  <v_pb6_speed_idx>'+inttostr(v_pb6_speed_idx)+'</v_pb6_speed_idx>');
    writeln(J, '  <v_pb6_name>'+v_pb6_name+'</v_pb6_name>');

{PC7}
    writeln(J, '  <v_cb_pc7_idx>'+inttostr(v_cb_pc7_idx)+'</v_cb_pc7_idx>');
    writeln(J, '  <v_e_pc7_txt>'+v_e_pc7_txt+'</v_e_pc7_txt>');
    writeln(J, '  <v_pc7_id>'+v_pc7_id+'</v_pc7_id>');
    writeln(J, '  <v_pc7_func>'+v_pc7_func+'</v_pc7_func>');
    writeln(J, '  <v_pc7_o_lvl_idx>'+inttostr(v_pc7_o_lvl_idx)+'</v_pc7_o_lvl_idx>');
    writeln(J, '  <v_pc7_o_mode_idx>'+inttostr(v_pc7_o_mode_idx)+'</v_pc7_o_mode_idx>');
    writeln(J, '  <v_pc7_i_mode_idx>'+inttostr(v_pc7_i_mode_idx)+'</v_pc7_i_mode_idx>');
    writeln(J, '  <v_pc7_a_mode_idx>'+inttostr(v_pc7_a_mode_idx)+'</v_pc7_a_mode_idx>');
    writeln(J, '  <v_pc7_i2c_mode_idx>'+inttostr(v_pc7_i2c_mode_idx)+'</v_pc7_i2c_mode_idx>');
    writeln(J, '  <v_pc7_spi_mode_idx>'+inttostr(v_pc7_spi_mode_idx)+'</v_pc7_spi_mode_idx>');
    writeln(J, '  <v_pc7_int_mode_idx>'+inttostr(v_pc7_int_mode_idx)+'</v_pc7_int_mode_idx>');
    writeln(J, '  <v_pc7_pull_updown_idx>'+inttostr(v_pc7_pull_updown_idx)+'</v_pc7_pull_updown_idx>');
    writeln(J, '  <v_pc7_speed_idx>'+inttostr(v_pc7_speed_idx)+'</v_pc7_speed_idx>');
    writeln(J, '  <v_pc7_name>'+v_pc7_name+'</v_pc7_name>');

{PA9}
    writeln(J, '  <v_cb_pa9_idx>'+inttostr(v_cb_pa9_idx)+'</v_cb_pa9_idx>');
    writeln(J, '  <v_e_pa9_txt>'+v_e_pa9_txt+'</v_e_pa9_txt>');
    writeln(J, '  <v_pa9_id>'+v_pa9_id+'</v_pa9_id>');
    writeln(J, '  <v_pa9_func>'+v_pa9_func+'</v_pa9_func>');
    writeln(J, '  <v_pa9_o_lvl_idx>'+inttostr(v_pa9_o_lvl_idx)+'</v_pa9_o_lvl_idx>');
    writeln(J, '  <v_pa9_o_mode_idx>'+inttostr(v_pa9_o_mode_idx)+'</v_pa9_o_mode_idx>');
    writeln(J, '  <v_pa9_i_mode_idx>'+inttostr(v_pa9_i_mode_idx)+'</v_pa9_i_mode_idx>');
    writeln(J, '  <v_pa9_a_mode_idx>'+inttostr(v_pa9_a_mode_idx)+'</v_pa9_a_mode_idx>');
    writeln(J, '  <v_pa9_i2c_mode_idx>'+inttostr(v_pa9_i2c_mode_idx)+'</v_pa9_i2c_mode_idx>');
    writeln(J, '  <v_pa9_spi_mode_idx>'+inttostr(v_pa9_spi_mode_idx)+'</v_pa9_spi_mode_idx>');
    writeln(J, '  <v_pa9_int_mode_idx>'+inttostr(v_pa9_int_mode_idx)+'</v_pa9_int_mode_idx>');
    writeln(J, '  <v_pa9_pull_updown_idx>'+inttostr(v_pa9_pull_updown_idx)+'</v_pa9_pull_updown_idx>');
    writeln(J, '  <v_pa9_speed_idx>'+inttostr(v_pa9_speed_idx)+'</v_pa9_speed_idx>');
    writeln(J, '  <v_pa9_name>'+v_pa9_name+'</v_pa9_name>');

{PA8}
    writeln(J, '  <v_cb_pa8_idx>'+inttostr(v_cb_pa8_idx)+'</v_cb_pa8_idx>');
    writeln(J, '  <v_e_pa8_txt>'+v_e_pa8_txt+'</v_e_pa8_txt>');
    writeln(J, '  <v_pa8_id>'+v_pa8_id+'</v_pa8_id>');
    writeln(J, '  <v_pa8_func>'+v_pa8_func+'</v_pa8_func>');
    writeln(J, '  <v_pa8_o_lvl_idx>'+inttostr(v_pa8_o_lvl_idx)+'</v_pa8_o_lvl_idx>');
    writeln(J, '  <v_pa8_o_mode_idx>'+inttostr(v_pa8_o_mode_idx)+'</v_pa8_o_mode_idx>');
    writeln(J, '  <v_pa8_i_mode_idx>'+inttostr(v_pa8_i_mode_idx)+'</v_pa8_i_mode_idx>');
    writeln(J, '  <v_pa8_a_mode_idx>'+inttostr(v_pa8_a_mode_idx)+'</v_pa8_a_mode_idx>');
    writeln(J, '  <v_pa8_i2c_mode_idx>'+inttostr(v_pa8_i2c_mode_idx)+'</v_pa8_i2c_mode_idx>');
    writeln(J, '  <v_pa8_spi_mode_idx>'+inttostr(v_pa8_spi_mode_idx)+'</v_pa8_spi_mode_idx>');
    writeln(J, '  <v_pa8_int_mode_idx>'+inttostr(v_pa8_int_mode_idx)+'</v_pa8_int_mode_idx>');
    writeln(J, '  <v_pa8_pull_updown_idx>'+inttostr(v_pa8_pull_updown_idx)+'</v_pa8_pull_updown_idx>');
    writeln(J, '  <v_pa8_speed_idx>'+inttostr(v_pa8_speed_idx)+'</v_pa8_speed_idx>');
    writeln(J, '  <v_pa8_name>'+v_pa8_name+'</v_pa8_name>');

{PB10}
    writeln(J, '  <v_cb_pb10_idx>'+inttostr(v_cb_pb10_idx)+'</v_cb_pb10_idx>');
    writeln(J, '  <v_e_pb10_txt>'+v_e_pb10_txt+'</v_e_pb10_txt>');
    writeln(J, '  <v_pb10_id>'+v_pb10_id+'</v_pb10_id>');
    writeln(J, '  <v_pb10_func>'+v_pb10_func+'</v_pb10_func>');
    writeln(J, '  <v_pb10_o_lvl_idx>'+inttostr(v_pb10_o_lvl_idx)+'</v_pb10_o_lvl_idx>');
    writeln(J, '  <v_pb10_o_mode_idx>'+inttostr(v_pb10_o_mode_idx)+'</v_pb10_o_mode_idx>');
    writeln(J, '  <v_pb10_i_mode_idx>'+inttostr(v_pb10_i_mode_idx)+'</v_pb10_i_mode_idx>');
    writeln(J, '  <v_pb10_a_mode_idx>'+inttostr(v_pb10_a_mode_idx)+'</v_pb10_a_mode_idx>');
    writeln(J, '  <v_pb10_i2c_mode_idx>'+inttostr(v_pb10_i2c_mode_idx)+'</v_pb10_i2c_mode_idx>');
    writeln(J, '  <v_pb10_spi_mode_idx>'+inttostr(v_pb10_spi_mode_idx)+'</v_pb10_spi_mode_idx>');
    writeln(J, '  <v_pb10_int_mode_idx>'+inttostr(v_pb10_int_mode_idx)+'</v_pb10_int_mode_idx>');
    writeln(J, '  <v_pb10_pull_updown_idx>'+inttostr(v_pb10_pull_updown_idx)+'</v_pb10_pull_updown_idx>');
    writeln(J, '  <v_pb10_speed_idx>'+inttostr(v_pb10_speed_idx)+'</v_pb10_speed_idx>');
    writeln(J, '  <v_pb10_name>'+v_pb10_name+'</v_pb10_name>');

{PB4}
    writeln(J, '  <v_cb_pb4_idx>'+inttostr(v_cb_pb4_idx)+'</v_cb_pb4_idx>');
    writeln(J, '  <v_e_pb4_txt>'+v_e_pb4_txt+'</v_e_pb4_txt>');
    writeln(J, '  <v_pb4_id>'+v_pb4_id+'</v_pb4_id>');
    writeln(J, '  <v_pb4_func>'+v_pb4_func+'</v_pb4_func>');
    writeln(J, '  <v_pb4_o_lvl_idx>'+inttostr(v_pb4_o_lvl_idx)+'</v_pb4_o_lvl_idx>');
    writeln(J, '  <v_pb4_o_mode_idx>'+inttostr(v_pb4_o_mode_idx)+'</v_pb4_o_mode_idx>');
    writeln(J, '  <v_pb4_i_mode_idx>'+inttostr(v_pb4_i_mode_idx)+'</v_pb4_i_mode_idx>');
    writeln(J, '  <v_pb4_a_mode_idx>'+inttostr(v_pb4_a_mode_idx)+'</v_pb4_a_mode_idx>');
    writeln(J, '  <v_pb4_i2c_mode_idx>'+inttostr(v_pb4_i2c_mode_idx)+'</v_pb4_i2c_mode_idx>');
    writeln(J, '  <v_pb4_spi_mode_idx>'+inttostr(v_pb4_spi_mode_idx)+'</v_pb4_spi_mode_idx>');
    writeln(J, '  <v_pb4_int_mode_idx>'+inttostr(v_pb4_int_mode_idx)+'</v_pb4_int_mode_idx>');
    writeln(J, '  <v_pb4_pull_updown_idx>'+inttostr(v_pb4_pull_updown_idx)+'</v_pb4_pull_updown_idx>');
    writeln(J, '  <v_pb4_speed_idx>'+inttostr(v_pb4_speed_idx)+'</v_pb4_speed_idx>');
    writeln(J, '  <v_pb4_name>'+v_pb4_name+'</v_pb4_name>');

{PB5}
    writeln(J, '  <v_cb_pb5_idx>'+inttostr(v_cb_pb5_idx)+'</v_cb_pb5_idx>');
    writeln(J, '  <v_e_pb5_txt>'+v_e_pb5_txt+'</v_e_pb5_txt>');
    writeln(J, '  <v_pb5_id>'+v_pb5_id+'</v_pb5_id>');
    writeln(J, '  <v_pb5_func>'+v_pb5_func+'</v_pb5_func>');
    writeln(J, '  <v_pb5_o_lvl_idx>'+inttostr(v_pb5_o_lvl_idx)+'</v_pb5_o_lvl_idx>');
    writeln(J, '  <v_pb5_o_mode_idx>'+inttostr(v_pb5_o_mode_idx)+'</v_pb5_o_mode_idx>');
    writeln(J, '  <v_pb5_i_mode_idx>'+inttostr(v_pb5_i_mode_idx)+'</v_pb5_i_mode_idx>');
    writeln(J, '  <v_pb5_a_mode_idx>'+inttostr(v_pb5_a_mode_idx)+'</v_pb5_a_mode_idx>');
    writeln(J, '  <v_pb5_i2c_mode_idx>'+inttostr(v_pb5_i2c_mode_idx)+'</v_pb5_i2c_mode_idx>');
    writeln(J, '  <v_pb5_spi_mode_idx>'+inttostr(v_pb5_spi_mode_idx)+'</v_pb5_spi_mode_idx>');
    writeln(J, '  <v_pb5_int_mode_idx>'+inttostr(v_pb5_int_mode_idx)+'</v_pb5_int_mode_idx>');
    writeln(J, '  <v_pb5_pull_updown_idx>'+inttostr(v_pb5_pull_updown_idx)+'</v_pb5_pull_updown_idx>');
    writeln(J, '  <v_pb5_speed_idx>'+inttostr(v_pb5_speed_idx)+'</v_pb5_speed_idx>');
    writeln(J, '  <v_pb5_name>'+v_pb5_name+'</v_pb5_name>');

{PB3}
    writeln(J, '  <v_cb_pb3_idx>'+inttostr(v_cb_pb3_idx)+'</v_cb_pb3_idx>');
    writeln(J, '  <v_e_pb3_txt>'+v_e_pb3_txt+'</v_e_pb3_txt>');
    writeln(J, '  <v_pb3_id>'+v_pb3_id+'</v_pb3_id>');
    writeln(J, '  <v_pb3_func>'+v_pb3_func+'</v_pb3_func>');
    writeln(J, '  <v_pb3_o_lvl_idx>'+inttostr(v_pb3_o_lvl_idx)+'</v_pb3_o_lvl_idx>');
    writeln(J, '  <v_pb3_o_mode_idx>'+inttostr(v_pb3_o_mode_idx)+'</v_pb3_o_mode_idx>');
    writeln(J, '  <v_pb3_i_mode_idx>'+inttostr(v_pb3_i_mode_idx)+'</v_pb3_i_mode_idx>');
    writeln(J, '  <v_pb3_a_mode_idx>'+inttostr(v_pb3_a_mode_idx)+'</v_pb3_a_mode_idx>');
    writeln(J, '  <v_pb3_i2c_mode_idx>'+inttostr(v_pb3_i2c_mode_idx)+'</v_pb3_i2c_mode_idx>');
    writeln(J, '  <v_pb3_spi_mode_idx>'+inttostr(v_pb3_spi_mode_idx)+'</v_pb3_spi_mode_idx>');
    writeln(J, '  <v_pb3_int_mode_idx>'+inttostr(v_pb3_int_mode_idx)+'</v_pb3_int_mode_idx>');
    writeln(J, '  <v_pb3_pull_updown_idx>'+inttostr(v_pb3_pull_updown_idx)+'</v_pb3_pull_updown_idx>');
    writeln(J, '  <v_pb3_speed_idx>'+inttostr(v_pb3_speed_idx)+'</v_pb3_speed_idx>');
    writeln(J, '  <v_pb3_name>'+v_pb3_name+'</v_pb3_name>');

{PA10}
    writeln(J, '  <v_cb_pa10_idx>'+inttostr(v_cb_pa10_idx)+'</v_cb_pa10_idx>');
    writeln(J, '  <v_e_pa10_txt>'+v_e_pa10_txt+'</v_e_pa10_txt>');
    writeln(J, '  <v_pa10_id>'+v_pa10_id+'</v_pa10_id>');
    writeln(J, '  <v_pa10_func>'+v_pa10_func+'</v_pa10_func>');
    writeln(J, '  <v_pa10_o_lvl_idx>'+inttostr(v_pa10_o_lvl_idx)+'</v_pa10_o_lvl_idx>');
    writeln(J, '  <v_pa10_o_mode_idx>'+inttostr(v_pa10_o_mode_idx)+'</v_pa10_o_mode_idx>');
    writeln(J, '  <v_pa10_i_mode_idx>'+inttostr(v_pa10_i_mode_idx)+'</v_pa10_i_mode_idx>');
    writeln(J, '  <v_pa10_a_mode_idx>'+inttostr(v_pa10_a_mode_idx)+'</v_pa10_a_mode_idx>');
    writeln(J, '  <v_pa10_i2c_mode_idx>'+inttostr(v_pa10_i2c_mode_idx)+'</v_pa10_i2c_mode_idx>');
    writeln(J, '  <v_pa10_spi_mode_idx>'+inttostr(v_pa10_spi_mode_idx)+'</v_pa10_spi_mode_idx>');
    writeln(J, '  <v_pa10_int_mode_idx>'+inttostr(v_pa10_int_mode_idx)+'</v_pa10_int_mode_idx>');
    writeln(J, '  <v_pa10_pull_updown_idx>'+inttostr(v_pa10_pull_updown_idx)+'</v_pa10_pull_updown_idx>');
    writeln(J, '  <v_pa10_speed_idx>'+inttostr(v_pa10_speed_idx)+'</v_pa10_speed_idx>');
    writeln(J, '  <v_pa10_name>'+v_pa10_name+'</v_pa10_name>');

{PA2}
    writeln(J, '  <v_cb_pa2_idx>'+inttostr(v_cb_pa2_idx)+'</v_cb_pa2_idx>');
    writeln(J, '  <v_e_pa2_txt>'+v_e_pa2_txt+'</v_e_pa2_txt>');
    writeln(J, '  <v_pa2_id>'+v_pa2_id+'</v_pa2_id>');
    writeln(J, '  <v_pa2_func>'+v_pa2_func+'</v_pa2_func>');
    writeln(J, '  <v_pa2_o_lvl_idx>'+inttostr(v_pa2_o_lvl_idx)+'</v_pa2_o_lvl_idx>');
    writeln(J, '  <v_pa2_o_mode_idx>'+inttostr(v_pa2_o_mode_idx)+'</v_pa2_o_mode_idx>');
    writeln(J, '  <v_pa2_i_mode_idx>'+inttostr(v_pa2_i_mode_idx)+'</v_pa2_i_mode_idx>');
    writeln(J, '  <v_pa2_a_mode_idx>'+inttostr(v_pa2_a_mode_idx)+'</v_pa2_a_mode_idx>');
    writeln(J, '  <v_pa2_i2c_mode_idx>'+inttostr(v_pa2_i2c_mode_idx)+'</v_pa2_i2c_mode_idx>');
    writeln(J, '  <v_pa2_spi_mode_idx>'+inttostr(v_pa2_spi_mode_idx)+'</v_pa2_spi_mode_idx>');
    writeln(J, '  <v_pa2_int_mode_idx>'+inttostr(v_pa2_int_mode_idx)+'</v_pa2_int_mode_idx>');
    writeln(J, '  <v_pa2_pull_updown_idx>'+inttostr(v_pa2_pull_updown_idx)+'</v_pa2_pull_updown_idx>');
    writeln(J, '  <v_pa2_speed_idx>'+inttostr(v_pa2_speed_idx)+'</v_pa2_speed_idx>');
    writeln(J, '  <v_pa2_name>'+v_pa2_name+'</v_pa2_name>');

{PA3}
    writeln(J, '  <v_cb_pa3_idx>'+inttostr(v_cb_pa3_idx)+'</v_cb_pa3_idx>');
    writeln(J, '  <v_e_pa3_txt>'+v_e_pa3_txt+'</v_e_pa3_txt>');
    writeln(J, '  <v_pa3_id>'+v_pa3_id+'</v_pa3_id>');
    writeln(J, '  <v_pa3_func>'+v_pa3_func+'</v_pa3_func>');
    writeln(J, '  <v_pa3_o_lvl_idx>'+inttostr(v_pa3_o_lvl_idx)+'</v_pa3_o_lvl_idx>');
    writeln(J, '  <v_pa3_o_mode_idx>'+inttostr(v_pa3_o_mode_idx)+'</v_pa3_o_mode_idx>');
    writeln(J, '  <v_pa3_i_mode_idx>'+inttostr(v_pa3_i_mode_idx)+'</v_pa3_i_mode_idx>');
    writeln(J, '  <v_pa3_a_mode_idx>'+inttostr(v_pa3_a_mode_idx)+'</v_pa3_a_mode_idx>');
    writeln(J, '  <v_pa3_i2c_mode_idx>'+inttostr(v_pa3_i2c_mode_idx)+'</v_pa3_i2c_mode_idx>');
    writeln(J, '  <v_pa3_spi_mode_idx>'+inttostr(v_pa3_spi_mode_idx)+'</v_pa3_spi_mode_idx>');
    writeln(J, '  <v_pa3_int_mode_idx>'+inttostr(v_pa3_int_mode_idx)+'</v_pa3_int_mode_idx>');
    writeln(J, '  <v_pa3_pull_updown_idx>'+inttostr(v_pa3_pull_updown_idx)+'</v_pa3_pull_updown_idx>');
    writeln(J, '  <v_pa3_speed_idx>'+inttostr(v_pa3_speed_idx)+'</v_pa3_speed_idx>');
    writeln(J, '  <v_pa3_name>'+v_pa3_name+'</v_pa3_name>');

{CN10 connector, the paired pins row - 2,4,6, etc.}
{PC8}
    writeln(J, '  <v_cb_pc8_idx>'+inttostr(v_cb_pc8_idx)+'</v_cb_pc8_idx>');
    writeln(J, '  <v_e_pc8_txt>'+v_e_pc8_txt+'</v_e_pc8_txt>');
    writeln(J, '  <v_pc8_id>'+v_pc8_id+'</v_pc8_id>');
    writeln(J, '  <v_pc8_func>'+v_pc8_func+'</v_pc8_func>');
    writeln(J, '  <v_pc8_o_lvl_idx>'+inttostr(v_pc8_o_lvl_idx)+'</v_pc8_o_lvl_idx>');
    writeln(J, '  <v_pc8_o_mode_idx>'+inttostr(v_pc8_o_mode_idx)+'</v_pc8_o_mode_idx>');
    writeln(J, '  <v_pc8_i_mode_idx>'+inttostr(v_pc8_i_mode_idx)+'</v_pc8_i_mode_idx>');
    writeln(J, '  <v_pc8_a_mode_idx>'+inttostr(v_pc8_a_mode_idx)+'</v_pc8_a_mode_idx>');
    writeln(J, '  <v_pc8_i2c_mode_idx>'+inttostr(v_pc8_i2c_mode_idx)+'</v_pc8_i2c_mode_idx>');
    writeln(J, '  <v_pc8_spi_mode_idx>'+inttostr(v_pc8_spi_mode_idx)+'</v_pc8_spi_mode_idx>');
    writeln(J, '  <v_pc8_int_mode_idx>'+inttostr(v_pc8_int_mode_idx)+'</v_pc8_int_mode_idx>');
    writeln(J, '  <v_pc8_pull_updown_idx>'+inttostr(v_pc8_pull_updown_idx)+'</v_pc8_pull_updown_idx>');
    writeln(J, '  <v_pc8_speed_idx>'+inttostr(v_pc8_speed_idx)+'</v_pc8_speed_idx>');
    writeln(J, '  <v_pc8_name>'+v_pc8_name+'</v_pc8_name>');

{PC6}
    writeln(J, '  <v_cb_pc6_idx>'+inttostr(v_cb_pc6_idx)+'</v_cb_pc6_idx>');
    writeln(J, '  <v_e_pc6_txt>'+v_e_pc6_txt+'</v_e_pc6_txt>');
    writeln(J, '  <v_pc6_id>'+v_pc6_id+'</v_pc6_id>');
    writeln(J, '  <v_pc6_func>'+v_pc6_func+'</v_pc6_func>');
    writeln(J, '  <v_pc6_o_lvl_idx>'+inttostr(v_pc6_o_lvl_idx)+'</v_pc6_o_lvl_idx>');
    writeln(J, '  <v_pc6_o_mode_idx>'+inttostr(v_pc6_o_mode_idx)+'</v_pc6_o_mode_idx>');
    writeln(J, '  <v_pc6_i_mode_idx>'+inttostr(v_pc6_i_mode_idx)+'</v_pc6_i_mode_idx>');
    writeln(J, '  <v_pc6_a_mode_idx>'+inttostr(v_pc6_a_mode_idx)+'</v_pc6_a_mode_idx>');
    writeln(J, '  <v_pc6_i2c_mode_idx>'+inttostr(v_pc6_i2c_mode_idx)+'</v_pc6_i2c_mode_idx>');
    writeln(J, '  <v_pc6_spi_mode_idx>'+inttostr(v_pc6_spi_mode_idx)+'</v_pc6_spi_mode_idx>');
    writeln(J, '  <v_pc6_int_mode_idx>'+inttostr(v_pc6_int_mode_idx)+'</v_pc6_int_mode_idx>');
    writeln(J, '  <v_pc6_pull_updown_idx>'+inttostr(v_pc6_pull_updown_idx)+'</v_pc6_pull_updown_idx>');
    writeln(J, '  <v_pc6_speed_idx>'+inttostr(v_pc6_speed_idx)+'</v_pc6_speed_idx>');
    writeln(J, '  <v_pc6_name>'+v_pc6_name+'</v_pc6_name>');

{PC5}
    writeln(J, '  <v_cb_pc5_idx>'+inttostr(v_cb_pc5_idx)+'</v_cb_pc5_idx>');
    writeln(J, '  <v_e_pc5_txt>'+v_e_pc5_txt+'</v_e_pc5_txt>');
    writeln(J, '  <v_pc5_id>'+v_pc5_id+'</v_pc5_id>');
    writeln(J, '  <v_pc5_func>'+v_pc5_func+'</v_pc5_func>');
    writeln(J, '  <v_pc5_o_lvl_idx>'+inttostr(v_pc5_o_lvl_idx)+'</v_pc5_o_lvl_idx>');
    writeln(J, '  <v_pc5_o_mode_idx>'+inttostr(v_pc5_o_mode_idx)+'</v_pc5_o_mode_idx>');
    writeln(J, '  <v_pc5_i_mode_idx>'+inttostr(v_pc5_i_mode_idx)+'</v_pc5_i_mode_idx>');
    writeln(J, '  <v_pc5_a_mode_idx>'+inttostr(v_pc5_a_mode_idx)+'</v_pc5_a_mode_idx>');
    writeln(J, '  <v_pc5_i2c_mode_idx>'+inttostr(v_pc5_i2c_mode_idx)+'</v_pc5_i2c_mode_idx>');
    writeln(J, '  <v_pc5_spi_mode_idx>'+inttostr(v_pc5_spi_mode_idx)+'</v_pc5_spi_mode_idx>');
    writeln(J, '  <v_pc5_int_mode_idx>'+inttostr(v_pc5_int_mode_idx)+'</v_pc5_int_mode_idx>');
    writeln(J, '  <v_pc5_pull_updown_idx>'+inttostr(v_pc5_pull_updown_idx)+'</v_pc5_pull_updown_idx>');
    writeln(J, '  <v_pc5_speed_idx>'+inttostr(v_pc5_speed_idx)+'</v_pc5_speed_idx>');
    writeln(J, '  <v_pc5_name>'+v_pc5_name+'</v_pc5_name>');

{PA12}
    writeln(J, '  <v_cb_pa12_idx>'+inttostr(v_cb_pa12_idx)+'</v_cb_pa12_idx>');
    writeln(J, '  <v_e_pa12_txt>'+v_e_pa12_txt+'</v_e_pa12_txt>');
    writeln(J, '  <v_pa12_id>'+v_pa12_id+'</v_pa12_id>');
    writeln(J, '  <v_pa12_func>'+v_pa12_func+'</v_pa12_func>');
    writeln(J, '  <v_pa12_o_lvl_idx>'+inttostr(v_pa12_o_lvl_idx)+'</v_pa12_o_lvl_idx>');
    writeln(J, '  <v_pa12_o_mode_idx>'+inttostr(v_pa12_o_mode_idx)+'</v_pa12_o_mode_idx>');
    writeln(J, '  <v_pa12_i_mode_idx>'+inttostr(v_pa12_i_mode_idx)+'</v_pa12_i_mode_idx>');
    writeln(J, '  <v_pa12_a_mode_idx>'+inttostr(v_pa12_a_mode_idx)+'</v_pa12_a_mode_idx>');
    writeln(J, '  <v_pa12_i2c_mode_idx>'+inttostr(v_pa12_i2c_mode_idx)+'</v_pa12_i2c_mode_idx>');
    writeln(J, '  <v_pa12_spi_mode_idx>'+inttostr(v_pa12_spi_mode_idx)+'</v_pa12_spi_mode_idx>');
    writeln(J, '  <v_pa12_int_mode_idx>'+inttostr(v_pa12_int_mode_idx)+'</v_pa12_int_mode_idx>');
    writeln(J, '  <v_pa12_pull_updown_idx>'+inttostr(v_pa12_pull_updown_idx)+'</v_pa12_pull_updown_idx>');
    writeln(J, '  <v_pa12_speed_idx>'+inttostr(v_pa12_speed_idx)+'</v_pa12_speed_idx>');
    writeln(J, '  <v_pa12_name>'+v_pa12_name+'</v_pa12_name>');

{PA11}
    writeln(J, '  <v_cb_pa11_idx>'+inttostr(v_cb_pa11_idx)+'</v_cb_pa11_idx>');
    writeln(J, '  <v_e_pa11_txt>'+v_e_pa11_txt+'</v_e_pa11_txt>');
    writeln(J, '  <v_pa11_id>'+v_pa11_id+'</v_pa11_id>');
    writeln(J, '  <v_pa11_func>'+v_pa11_func+'</v_pa11_func>');
    writeln(J, '  <v_pa11_o_lvl_idx>'+inttostr(v_pa11_o_lvl_idx)+'</v_pa11_o_lvl_idx>');
    writeln(J, '  <v_pa11_o_mode_idx>'+inttostr(v_pa11_o_mode_idx)+'</v_pa11_o_mode_idx>');
    writeln(J, '  <v_pa11_i_mode_idx>'+inttostr(v_pa11_i_mode_idx)+'</v_pa11_i_mode_idx>');
    writeln(J, '  <v_pa11_a_mode_idx>'+inttostr(v_pa11_a_mode_idx)+'</v_pa11_a_mode_idx>');
    writeln(J, '  <v_pa11_i2c_mode_idx>'+inttostr(v_pa11_i2c_mode_idx)+'</v_pa11_i2c_mode_idx>');
    writeln(J, '  <v_pa11_spi_mode_idx>'+inttostr(v_pa11_spi_mode_idx)+'</v_pa11_spi_mode_idx>');
    writeln(J, '  <v_pa11_int_mode_idx>'+inttostr(v_pa11_int_mode_idx)+'</v_pa11_int_mode_idx>');
    writeln(J, '  <v_pa11_pull_updown_idx>'+inttostr(v_pa11_pull_updown_idx)+'</v_pa11_pull_updown_idx>');
    writeln(J, '  <v_pa11_speed_idx>'+inttostr(v_pa11_speed_idx)+'</v_pa11_speed_idx>');
    writeln(J, '  <v_pa11_name>'+v_pa11_name+'</v_pa11_name>');

{PB12}
    writeln(J, '  <v_cb_pb12_idx>'+inttostr(v_cb_pb12_idx)+'</v_cb_pb12_idx>');
    writeln(J, '  <v_e_pb12_txt>'+v_e_pb12_txt+'</v_e_pb12_txt>');
    writeln(J, '  <v_pb12_id>'+v_pb12_id+'</v_pb12_id>');
    writeln(J, '  <v_pb12_func>'+v_pb12_func+'</v_pb12_func>');
    writeln(J, '  <v_pb12_o_lvl_idx>'+inttostr(v_pb12_o_lvl_idx)+'</v_pb12_o_lvl_idx>');
    writeln(J, '  <v_pb12_o_mode_idx>'+inttostr(v_pb12_o_mode_idx)+'</v_pb12_o_mode_idx>');
    writeln(J, '  <v_pb12_i_mode_idx>'+inttostr(v_pb12_i_mode_idx)+'</v_pb12_i_mode_idx>');
    writeln(J, '  <v_pb12_a_mode_idx>'+inttostr(v_pb12_a_mode_idx)+'</v_pb12_a_mode_idx>');
    writeln(J, '  <v_pb12_i2c_mode_idx>'+inttostr(v_pb12_i2c_mode_idx)+'</v_pb12_i2c_mode_idx>');
    writeln(J, '  <v_pb12_spi_mode_idx>'+inttostr(v_pb12_spi_mode_idx)+'</v_pb12_spi_mode_idx>');
    writeln(J, '  <v_pb12_int_mode_idx>'+inttostr(v_pb12_int_mode_idx)+'</v_pb12_int_mode_idx>');
    writeln(J, '  <v_pb12_pull_updown_idx>'+inttostr(v_pb12_pull_updown_idx)+'</v_pb12_pull_updown_idx>');
    writeln(J, '  <v_pb12_speed_idx>'+inttostr(v_pb12_speed_idx)+'</v_pb12_speed_idx>');
    writeln(J, '  <v_pb12_name>'+v_pb12_name+'</v_pb12_name>');

{PB11}
    writeln(J, '  <v_cb_pb11_idx>'+inttostr(v_cb_pb11_idx)+'</v_cb_pb11_idx>');
    writeln(J, '  <v_e_pb11_txt>'+v_e_pb11_txt+'</v_e_pb11_txt>');
    writeln(J, '  <v_pb11_id>'+v_pb11_id+'</v_pb11_id>');
    writeln(J, '  <v_pb11_func>'+v_pb11_func+'</v_pb11_func>');
    writeln(J, '  <v_pb11_o_lvl_idx>'+inttostr(v_pb11_o_lvl_idx)+'</v_pb11_o_lvl_idx>');
    writeln(J, '  <v_pb11_o_mode_idx>'+inttostr(v_pb11_o_mode_idx)+'</v_pb11_o_mode_idx>');
    writeln(J, '  <v_pb11_i_mode_idx>'+inttostr(v_pb11_i_mode_idx)+'</v_pb11_i_mode_idx>');
    writeln(J, '  <v_pb11_a_mode_idx>'+inttostr(v_pb11_a_mode_idx)+'</v_pb11_a_mode_idx>');
    writeln(J, '  <v_pb11_i2c_mode_idx>'+inttostr(v_pb11_i2c_mode_idx)+'</v_pb11_i2c_mode_idx>');
    writeln(J, '  <v_pb11_spi_mode_idx>'+inttostr(v_pb11_spi_mode_idx)+'</v_pb11_spi_mode_idx>');
    writeln(J, '  <v_pb11_int_mode_idx>'+inttostr(v_pb11_int_mode_idx)+'</v_pb11_int_mode_idx>');
    writeln(J, '  <v_pb11_pull_updown_idx>'+inttostr(v_pb11_pull_updown_idx)+'</v_pb11_pull_updown_idx>');
    writeln(J, '  <v_pb11_speed_idx>'+inttostr(v_pb11_speed_idx)+'</v_pb11_speed_idx>');
    writeln(J, '  <v_pb11_name>'+v_pb11_name+'</v_pb11_name>');

{PB2}
    writeln(J, '  <v_cb_pb2_idx>'+inttostr(v_cb_pb2_idx)+'</v_cb_pb2_idx>');
    writeln(J, '  <v_e_pb2_txt>'+v_e_pb2_txt+'</v_e_pb2_txt>');
    writeln(J, '  <v_pb2_id>'+v_pb2_id+'</v_pb2_id>');
    writeln(J, '  <v_pb2_func>'+v_pb2_func+'</v_pb2_func>');
    writeln(J, '  <v_pb2_o_lvl_idx>'+inttostr(v_pb2_o_lvl_idx)+'</v_pb2_o_lvl_idx>');
    writeln(J, '  <v_pb2_o_mode_idx>'+inttostr(v_pb2_o_mode_idx)+'</v_pb2_o_mode_idx>');
    writeln(J, '  <v_pb2_i_mode_idx>'+inttostr(v_pb2_i_mode_idx)+'</v_pb2_i_mode_idx>');
    writeln(J, '  <v_pb2_a_mode_idx>'+inttostr(v_pb2_a_mode_idx)+'</v_pb2_a_mode_idx>');
    writeln(J, '  <v_pb2_i2c_mode_idx>'+inttostr(v_pb2_i2c_mode_idx)+'</v_pb2_i2c_mode_idx>');
    writeln(J, '  <v_pb2_spi_mode_idx>'+inttostr(v_pb2_spi_mode_idx)+'</v_pb2_spi_mode_idx>');
    writeln(J, '  <v_pb2_int_mode_idx>'+inttostr(v_pb2_int_mode_idx)+'</v_pb2_int_mode_idx>');
    writeln(J, '  <v_pb2_pull_updown_idx>'+inttostr(v_pb2_pull_updown_idx)+'</v_pb2_pull_updown_idx>');
    writeln(J, '  <v_pb2_speed_idx>'+inttostr(v_pb2_speed_idx)+'</v_pb2_speed_idx>');
    writeln(J, '  <v_pb2_name>'+v_pb2_name+'</v_pb2_name>');

{PB1}
    writeln(J, '  <v_cb_pb1_idx>'+inttostr(v_cb_pb1_idx)+'</v_cb_pb1_idx>');
    writeln(J, '  <v_e_pb1_txt>'+v_e_pb1_txt+'</v_e_pb1_txt>');
    writeln(J, '  <v_pb1_id>'+v_pb1_id+'</v_pb1_id>');
    writeln(J, '  <v_pb1_func>'+v_pb1_func+'</v_pb1_func>');
    writeln(J, '  <v_pb1_o_lvl_idx>'+inttostr(v_pb1_o_lvl_idx)+'</v_pb1_o_lvl_idx>');
    writeln(J, '  <v_pb1_o_mode_idx>'+inttostr(v_pb1_o_mode_idx)+'</v_pb1_o_mode_idx>');
    writeln(J, '  <v_pb1_i_mode_idx>'+inttostr(v_pb1_i_mode_idx)+'</v_pb1_i_mode_idx>');
    writeln(J, '  <v_pb1_a_mode_idx>'+inttostr(v_pb1_a_mode_idx)+'</v_pb1_a_mode_idx>');
    writeln(J, '  <v_pb1_i2c_mode_idx>'+inttostr(v_pb1_i2c_mode_idx)+'</v_pb1_i2c_mode_idx>');
    writeln(J, '  <v_pb1_spi_mode_idx>'+inttostr(v_pb1_spi_mode_idx)+'</v_pb1_spi_mode_idx>');
    writeln(J, '  <v_pb1_int_mode_idx>'+inttostr(v_pb1_int_mode_idx)+'</v_pb1_int_mode_idx>');
    writeln(J, '  <v_pb1_pull_updown_idx>'+inttostr(v_pb1_pull_updown_idx)+'</v_pb1_pull_updown_idx>');
    writeln(J, '  <v_pb1_speed_idx>'+inttostr(v_pb1_speed_idx)+'</v_pb1_speed_idx>');
    writeln(J, '  <v_pb1_name>'+v_pb1_name+'</v_pb1_name>');

{PB15}
    writeln(J, '  <v_cb_pb15_idx>'+inttostr(v_cb_pb15_idx)+'</v_cb_pb15_idx>');
    writeln(J, '  <v_e_pb15_txt>'+v_e_pb15_txt+'</v_e_pb15_txt>');
    writeln(J, '  <v_pb15_id>'+v_pb15_id+'</v_pb15_id>');
    writeln(J, '  <v_pb15_func>'+v_pb15_func+'</v_pb15_func>');
    writeln(J, '  <v_pb15_o_lvl_idx>'+inttostr(v_pb15_o_lvl_idx)+'</v_pb15_o_lvl_idx>');
    writeln(J, '  <v_pb15_o_mode_idx>'+inttostr(v_pb15_o_mode_idx)+'</v_pb15_o_mode_idx>');
    writeln(J, '  <v_pb15_i_mode_idx>'+inttostr(v_pb15_i_mode_idx)+'</v_pb15_i_mode_idx>');
    writeln(J, '  <v_pb15_a_mode_idx>'+inttostr(v_pb15_a_mode_idx)+'</v_pb15_a_mode_idx>');
    writeln(J, '  <v_pb15_i2c_mode_idx>'+inttostr(v_pb15_i2c_mode_idx)+'</v_pb15_i2c_mode_idx>');
    writeln(J, '  <v_pb15_spi_mode_idx>'+inttostr(v_pb15_spi_mode_idx)+'</v_pb15_spi_mode_idx>');
    writeln(J, '  <v_pb15_int_mode_idx>'+inttostr(v_pb15_int_mode_idx)+'</v_pb15_int_mode_idx>');
    writeln(J, '  <v_pb15_pull_updown_idx>'+inttostr(v_pb15_pull_updown_idx)+'</v_pb15_pull_updown_idx>');
    writeln(J, '  <v_pb15_speed_idx>'+inttostr(v_pb15_speed_idx)+'</v_pb15_speed_idx>');
    writeln(J, '  <v_pb15_name>'+v_pb15_name+'</v_pb15_name>');

{PB14}
    writeln(J, '  <v_cb_pb14_idx>'+inttostr(v_cb_pb14_idx)+'</v_cb_pb14_idx>');
    writeln(J, '  <v_e_pb14_txt>'+v_e_pb14_txt+'</v_e_pb14_txt>');
    writeln(J, '  <v_pb14_id>'+v_pb14_id+'</v_pb14_id>');
    writeln(J, '  <v_pb14_func>'+v_pb14_func+'</v_pb14_func>');
    writeln(J, '  <v_pb14_o_lvl_idx>'+inttostr(v_pb14_o_lvl_idx)+'</v_pb14_o_lvl_idx>');
    writeln(J, '  <v_pb14_o_mode_idx>'+inttostr(v_pb14_o_mode_idx)+'</v_pb14_o_mode_idx>');
    writeln(J, '  <v_pb14_i_mode_idx>'+inttostr(v_pb14_i_mode_idx)+'</v_pb14_i_mode_idx>');
    writeln(J, '  <v_pb14_a_mode_idx>'+inttostr(v_pb14_a_mode_idx)+'</v_pb14_a_mode_idx>');
    writeln(J, '  <v_pb14_i2c_mode_idx>'+inttostr(v_pb14_i2c_mode_idx)+'</v_pb14_i2c_mode_idx>');
    writeln(J, '  <v_pb14_spi_mode_idx>'+inttostr(v_pb14_spi_mode_idx)+'</v_pb14_spi_mode_idx>');
    writeln(J, '  <v_pb14_int_mode_idx>'+inttostr(v_pb14_int_mode_idx)+'</v_pb14_int_mode_idx>');
    writeln(J, '  <v_pb14_pull_updown_idx>'+inttostr(v_pb14_pull_updown_idx)+'</v_pb14_pull_updown_idx>');
    writeln(J, '  <v_pb14_speed_idx>'+inttostr(v_pb14_speed_idx)+'</v_pb14_speed_idx>');
    writeln(J, '  <v_pb14_name>'+v_pb14_name+'</v_pb14_name>');

{PB13}
    writeln(J, '  <v_cb_pb13_idx>'+inttostr(v_cb_pb13_idx)+'</v_cb_pb13_idx>');
    writeln(J, '  <v_e_pb13_txt>'+v_e_pb13_txt+'</v_e_pb13_txt>');
    writeln(J, '  <v_pb13_id>'+v_pb13_id+'</v_pb13_id>');
    writeln(J, '  <v_pb13_func>'+v_pb13_func+'</v_pb13_func>');
    writeln(J, '  <v_pb13_o_lvl_idx>'+inttostr(v_pb13_o_lvl_idx)+'</v_pb13_o_lvl_idx>');
    writeln(J, '  <v_pb13_o_mode_idx>'+inttostr(v_pb13_o_mode_idx)+'</v_pb13_o_mode_idx>');
    writeln(J, '  <v_pb13_i_mode_idx>'+inttostr(v_pb13_i_mode_idx)+'</v_pb13_i_mode_idx>');
    writeln(J, '  <v_pb13_a_mode_idx>'+inttostr(v_pb13_a_mode_idx)+'</v_pb13_a_mode_idx>');
    writeln(J, '  <v_pb13_i2c_mode_idx>'+inttostr(v_pb13_i2c_mode_idx)+'</v_pb13_i2c_mode_idx>');
    writeln(J, '  <v_pb13_spi_mode_idx>'+inttostr(v_pb13_spi_mode_idx)+'</v_pb13_spi_mode_idx>');
    writeln(J, '  <v_pb13_int_mode_idx>'+inttostr(v_pb13_int_mode_idx)+'</v_pb13_int_mode_idx>');
    writeln(J, '  <v_pb13_pull_updown_idx>'+inttostr(v_pb13_pull_updown_idx)+'</v_pb13_pull_updown_idx>');
    writeln(J, '  <v_pb13_speed_idx>'+inttostr(v_pb13_speed_idx)+'</v_pb13_speed_idx>');
    writeln(J, '  <v_pb13_name>'+v_pb13_name+'</v_pb13_name>');

{PC4}
    writeln(J, '  <v_cb_pc4_idx>'+inttostr(v_cb_pc4_idx)+'</v_cb_pc4_idx>');
    writeln(J, '  <v_e_pc4_txt>'+v_e_pc4_txt+'</v_e_pc4_txt>');
    writeln(J, '  <v_pc4_id>'+v_pc4_id+'</v_pc4_id>');
    writeln(J, '  <v_pc4_func>'+v_pc4_func+'</v_pc4_func>');
    writeln(J, '  <v_pc4_o_lvl_idx>'+inttostr(v_pc4_o_lvl_idx)+'</v_pc4_o_lvl_idx>');
    writeln(J, '  <v_pc4_o_mode_idx>'+inttostr(v_pc4_o_mode_idx)+'</v_pc4_o_mode_idx>');
    writeln(J, '  <v_pc4_i_mode_idx>'+inttostr(v_pc4_i_mode_idx)+'</v_pc4_i_mode_idx>');
    writeln(J, '  <v_pc4_a_mode_idx>'+inttostr(v_pc4_a_mode_idx)+'</v_pc4_a_mode_idx>');
    writeln(J, '  <v_pc4_i2c_mode_idx>'+inttostr(v_pc4_i2c_mode_idx)+'</v_pc4_i2c_mode_idx>');
    writeln(J, '  <v_pc4_spi_mode_idx>'+inttostr(v_pc4_spi_mode_idx)+'</v_pc4_spi_mode_idx>');
    writeln(J, '  <v_pc4_int_mode_idx>'+inttostr(v_pc4_int_mode_idx)+'</v_pc4_int_mode_idx>');
    writeln(J, '  <v_pc4_pull_updown_idx>'+inttostr(v_pc4_pull_updown_idx)+'</v_pc4_pull_updown_idx>');
    writeln(J, '  <v_pc4_speed_idx>'+inttostr(v_pc4_speed_idx)+'</v_pc4_speed_idx>');
    writeln(J, '  <v_pc4_name>'+v_pc4_name+'</v_pc4_name>');

    //writeln(J, '  <v_c_rcc_mco>'+prepbool(v_c_rcc_mco)+'</v_c_rcc_mco>'); {Master Clock Output}
    //writeln(J, '  <v_mco_divider_idx>'+inttostr(v_mco_divider_idx)+'</v_mco_divider_idx>');
    //writeln(J, '  <v_mco_source>'+inttostr(v_mco_source)+'</v_mco_source>');
    //writeln(J, '  <v_clock_source>'+inttostr(v_clock_source)+'</v_clock_source>');
    //writeln(J, '  <v_hse_clk_source_idx>'+inttostr(v_hse_clk_source_idx)+'</v_hse_clk_source_idx>');
    //writeln(J, '  <v_tick_manager>'+inttostr(v_tick_manager)+'</v_tick_manager>');

{==================
 TIM6 CONFIGURATION
 ------------------}
    writeln(J, '  <v_tim6_check>'+prepbool(v_tim6_check)+'</v_tim6_check>');
    writeln(J, '  <v_tim6_prescaler>'+inttostr(v_tim6_prescaler)+'</v_tim6_prescaler>');
    writeln(J, '  <v_tim6_period>'+inttostr(v_tim6_period)+'</v_tim6_period>');
    writeln(J, '  <v_tim6_event>'+inttostr(v_tim6_event)+'</v_tim6_event>');
    writeln(J, '  <v_tim6_global_int>'+prepbool(v_tim6_global_int)+'</v_tim6_global_int>');
    writeln(J, '  <v_tim6_int_priority>'+inttostr(v_tim6_int_priority)+'</v_tim6_int_priority>');
    writeln(J, '  <v_tim6_int_subpriority>'+inttostr(v_tim6_int_subpriority)+'</v_tim6_int_subpriority>');

{==================
 TIM7 CONFIGURATION
 ------------------}
    writeln(J, '  <v_tim7_check>'+prepbool(v_tim7_check)+'</v_tim7_check>');
    writeln(J, '  <v_tim7_prescaler>'+inttostr(v_tim7_prescaler)+'</v_tim7_prescaler>');
    writeln(J, '  <v_tim7_period>'+inttostr(v_tim7_period)+'</v_tim7_period>');
    writeln(J, '  <v_tim7_event>'+inttostr(v_tim7_event)+'</v_tim7_event>');
    writeln(J, '  <v_tim7_global_int>'+prepbool(v_tim7_global_int)+'</v_tim7_global_int>');
    writeln(J, '  <v_tim7_int_priority>'+inttostr(v_tim7_int_priority)+'</v_tim7_int_priority>');
    writeln(J, '  <v_tim7_int_subpriority>'+inttostr(v_tim7_int_subpriority)+'</v_tim7_int_subpriority>');

    {now we add the elements needed by the version 2}
    writeln(J, '  <elements_of_version_2>StartHere</elements_of_version_2>');
    writeln(J, '  <v_c_i2c1_a>'+prepbool(v_c_i2c1_a)+'</v_c_i2c1_a>');
    writeln(J, '  <v_c_i2c1_b>'+prepbool(v_c_i2c1_b)+'</v_c_i2c1_b>');

    {PA2} {- PA2-PA3 USART2 on Nucleo}
    writeln(J, '  <v_cb_pa2_idx>'+inttostr(v_cb_pa2_idx)+'</v_cb_pa2_idx>');
    writeln(J, '  <v_e_pa2_txt>'+v_e_pa2_txt+'</v_e_pa2_txt>');
    writeln(J, '  <v_pa2_id>'+v_pa2_id+'</v_pa2_id>');
    writeln(J, '  <v_pa2_func>'+v_pa2_func+'</v_pa2_func>');
    writeln(J, '  <v_pa2_o_lvl_idx>'+inttostr(v_pa2_o_lvl_idx)+'</v_pa2_o_lvl_idx>');
    writeln(J, '  <v_pa2_o_mode_idx>'+inttostr(v_pa2_o_mode_idx)+'</v_pa2_o_mode_idx>');
    writeln(J, '  <v_pa2_i_mode_idx>'+inttostr(v_pa2_i_mode_idx)+'</v_pa2_i_mode_idx>');
    writeln(J, '  <v_pa2_a_mode_idx>'+inttostr(v_pa2_a_mode_idx)+'</v_pa2_a_mode_idx>');
    writeln(J, '  <v_pa2_i2c_mode_idx>'+inttostr(v_pa2_i2c_mode_idx)+'</v_pa2_i2c_mode_idx>');
    writeln(J, '  <v_pa2_spi_mode_idx>'+inttostr(v_pa2_spi_mode_idx)+'</v_pa2_spi_mode_idx>');
    writeln(J, '  <v_pa2_int_mode_idx>'+inttostr(v_pa2_int_mode_idx)+'</v_pa2_int_mode_idx>');
    writeln(J, '  <v_pa2_pull_updown_idx>'+inttostr(v_pa2_pull_updown_idx)+'</v_pa2_pull_updown_idx>');
    writeln(J, '  <v_pa2_speed_idx>'+inttostr(v_pa2_speed_idx)+'</v_pa2_speed_idx>');
    writeln(J, '  <v_pa2_name>'+v_pa2_name+'</v_pa2_name>');

    {PA3}
    writeln(J, '  <v_cb_pa3_idx>'+inttostr(v_cb_pa3_idx)+'</v_cb_pa3_idx>');
    writeln(J, '  <v_e_pa3_txt>'+v_e_pa3_txt+'</v_e_pa3_txt>');
    writeln(J, '  <v_pa3_id>'+v_pa3_id+'</v_pa3_id>');
    writeln(J, '  <v_pa3_func>'+v_pa3_func+'</v_pa3_func>');
    writeln(J, '  <v_pa3_o_lvl_idx>'+inttostr(v_pa3_o_lvl_idx)+'</v_pa3_o_lvl_idx>');
    writeln(J, '  <v_pa3_o_mode_idx>'+inttostr(v_pa3_o_mode_idx)+'</v_pa3_o_mode_idx>');
    writeln(J, '  <v_pa3_i_mode_idx>'+inttostr(v_pa3_i_mode_idx)+'</v_pa3_i_mode_idx>');
    writeln(J, '  <v_pa3_a_mode_idx>'+inttostr(v_pa3_a_mode_idx)+'</v_pa3_a_mode_idx>');
    writeln(J, '  <v_pa3_i2c_mode_idx>'+inttostr(v_pa3_i2c_mode_idx)+'</v_pa3_i2c_mode_idx>');
    writeln(J, '  <v_pa3_spi_mode_idx>'+inttostr(v_pa3_spi_mode_idx)+'</v_pa3_spi_mode_idx>');
    writeln(J, '  <v_pa3_int_mode_idx>'+inttostr(v_pa3_int_mode_idx)+'</v_pa3_int_mode_idx>');
    writeln(J, '  <v_pa3_pull_updown_idx>'+inttostr(v_pa3_pull_updown_idx)+'</v_pa3_pull_updown_idx>');
    writeln(J, '  <v_pa3_speed_idx>'+inttostr(v_pa3_speed_idx)+'</v_pa3_speed_idx>');
    writeln(J, '  <v_pa3_name>'+v_pa3_name+'</v_pa3_name>');

    {PC13}
    writeln(J, '  <v_cb_pc13_idx>'+inttostr(v_cb_pc13_idx)+'</v_cb_pc13_idx>');
    writeln(J, '  <v_e_pc13_txt>'+v_e_pc13_txt+'</v_e_pc13_txt>');
    writeln(J, '  <v_pc13_id>'+v_pc13_id+'</v_pc13_id>');
    writeln(J, '  <v_pc13_func>'+v_pc13_func+'</v_pc13_func>');
    writeln(J, '  <v_pc13_o_lvl_idx>'+inttostr(v_pc13_o_lvl_idx)+'</v_pc13_o_lvl_idx>');
    writeln(J, '  <v_pc13_o_mode_idx>'+inttostr(v_pc13_o_mode_idx)+'</v_pc13_o_mode_idx>');
    writeln(J, '  <v_pc13_i_mode_idx>'+inttostr(v_pc13_i_mode_idx)+'</v_pc13_i_mode_idx>');
    writeln(J, '  <v_pc13_a_mode_idx>'+inttostr(v_pc13_a_mode_idx)+'</v_pc13_a_mode_idx>');
    writeln(J, '  <v_pc13_i2c_mode_idx>'+inttostr(v_pc13_i2c_mode_idx)+'</v_pc13_i2c_mode_idx>');
    writeln(J, '  <v_pc13_spi_mode_idx>'+inttostr(v_pc13_spi_mode_idx)+'</v_pc13_spi_mode_idx>');
    writeln(J, '  <v_pc13_int_mode_idx>'+inttostr(v_pc13_int_mode_idx)+'</v_pc13_int_mode_idx>');
    writeln(J, '  <v_pc13_pull_updown_idx>'+inttostr(v_pc13_pull_updown_idx)+'</v_pc13_pull_updown_idx>');
    writeln(J, '  <v_pc13_speed_idx>'+inttostr(v_pc13_speed_idx)+'</v_pc13_speed_idx>');
    writeln(J, '  <v_pc13_name>'+v_pc13_name+'</v_pc13_name>');

    {lcd4}
    writeln(J, '  <v_lcd4_defaults>'+prepbool(v_lcd4_defaults)+'</v_lcd4_defaults>');

    {now we add the elements needed by the version 3}
    writeln(J, '  <elements_of_version_3>StartHere</elements_of_version_3>');

{==========================}
{SYSTEM CLOCK CONFIGURATION}
{--------------------------}
    writeln(J, '  <v_se_hse_value>'+inttostr(v_se_hse_value)+'</v_se_hse_value>');
    writeln(J, '  <v_cb_msi_idx>'+inttostr(v_cb_msi_idx)+'</v_cb_msi_idx>');
    writeln(J, '  <v_rg_vco_idx>'+inttostr(v_rg_vco_idx)+'</v_rg_vco_idx>');
    writeln(J, '  <v_cb_pllmul_idx>'+inttostr(v_cb_pllmul_idx)+'</v_cb_pllmul_idx>');
    writeln(J, '  <v_cb_plldiv_idx>'+inttostr(v_cb_plldiv_idx)+'</v_cb_plldiv_idx>');
    writeln(J, '  <v_rg_clock_hub_idx>'+inttostr(v_rg_clock_hub_idx)+'</v_rg_clock_hub_idx>');
    writeln(J, '  <v_cb_ahb_pr_idx>'+inttostr(v_cb_ahb_pr_idx)+'</v_cb_ahb_pr_idx>');
    writeln(J, '  <v_cb_systick_pr_idx>'+inttostr(v_cb_systick_pr_idx)+'</v_cb_systick_pr_idx>');
    writeln(J, '  <v_cb_apb1_pr_idx>'+inttostr(v_cb_apb1_pr_idx)+'</v_cb_apb1_pr_idx>');
    writeln(J, '  <v_cb_apb2_pr_idx>'+inttostr(v_cb_apb2_pr_idx)+'</v_cb_apb2_pr_idx>');
    writeln(J, '  <v_c_en_css_hse>'+prepbool(v_c_en_css_hse)+'</v_c_en_css_hse>');
    //
    writeln(J, '  <v_cb_rcc_hse_idx>'+inttostr(v_cb_rcc_hse_idx)+'</v_cb_rcc_hse_idx>');
    //writeln(J, '  <v_cb_rcc_lse_idx>'+inttostr(v_cb_rcc_lse_idx)+'</v_cb_rcc_lse_idx>');
    writeln(J, '  <v_c_rcc_mco>'+prepbool(v_c_rcc_mco)+'</v_c_rcc_mco>');
    writeln(J, '  <v_cb_mco_div_idx>'+inttostr(v_cb_mco_div_idx)+'</v_cb_mco_div_idx>');
    writeln(J, '  <v_rg_mco_src_idx>'+inttostr(v_rg_mco_src_idx)+'</v_rg_mco_src_idx>');
    //
    writeln(J, '  <v_rg_osc32_idx>'+inttostr(v_rg_osc32_idx)+'</v_rg_osc32_idx>');
    writeln(J, '  <v_cb_hse_div_idx>'+inttostr(v_cb_hse_div_idx)+'</v_cb_hse_div_idx>');
    writeln(J, '  <v_c_en_css_lse>'+prepbool(v_c_en_css_lse)+'</v_c_en_css_lse>');


    writeln(J, '</project>');
    close(J);
    {$I+}
  end;
end;


procedure create_project;
begin
  create_folders;
  //create_jsons;
  copy_driver;
  {$I-}
  save_project;
  {$I+}
end;


procedure open_project(fl:string);
var
  PassNode: TDOMNode;
  Doc: TXMLDocument;
begin
  {open project}
  {$I-}
  try
  ReadXMLFile(Doc, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+fl+'/'+fl+'.vpc');
  {============}
  {PROJECT DATA}
  {------------}

  {NOTE: The lines are intentionally staggered - easy to follow}
  {============================================================}
  PassNode := Doc.DocumentElement.FindNode('file_version');
v_file_version := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_prj_name');
v_prj_name := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_year');
v_year := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_author');
v_author := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_copyright_idx');
v_copyright_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_debug');
v_debug := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_optimization_index');
v_optimization_index := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_group_priority');
v_group_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti0_priority');
v_exti0_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti0_subpriority');
v_exti0_subpriority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti1_priority');
v_exti1_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti1_subpriority');
v_exti1_subpriority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti2_priority');
v_exti2_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti2_subpriority');
v_exti2_subpriority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti3_priority');
v_exti3_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti3_subpriority');
v_exti3_subpriority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti4_priority');
v_exti4_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti4_subpriority');
v_exti4_subpriority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti9_5_priority');
v_exti9_5_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti9_5_subpriority');
v_exti9_5_subpriority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti15_10_priority');
v_exti15_10_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_exti15_10_subpriority');
v_exti15_10_subpriority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_efp');
v_efp := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_printf_efp');
v_printf_efp := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_use_adc');
v_use_adc := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_use_adin');
v_use_adin := setbool(AnsiString(PassNode.FirstChild.NodeValue));

{==========================
 |||| EXTI interrupts |||||
 --------------------------}
  PassNode := Doc.DocumentElement.FindNode('v_exti0_used');
v_exti0_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_exti1_used');
v_exti1_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_exti2_used');
v_exti2_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_exti3_used');
v_exti3_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_exti4_used');
v_exti4_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_exti9_5_used');
v_exti9_5_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_exti15_10_used');
v_exti15_10_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));

{========================
       PORTS USED
 ------------------------}
  PassNode := Doc.DocumentElement.FindNode('v_porta_used');
v_porta_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_portb_used');
v_portb_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_portc_used');
v_portc_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_portd_used');
v_portd_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_porth_used');
v_porth_used := setbool(AnsiString(PassNode.FirstChild.NodeValue));

{==============
 USB PERIPHERAL
 --------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_usb');
v_c_usb := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_usb_maxpacksize_idx');
v_cb_usb_maxpacksize_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_usb_lowpower_idx');
v_cb_usb_lowpower_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_usb_charging_idx');
v_cb_usb_charging_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_usb_hint');
v_c_usb_hint := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_c_usb_lint');
v_c_usb_lint := setbool(AnsiString(PassNode.FirstChild.NodeValue));

{===============
 I2C1 PERIPHERAL
 ---------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_i2c1');
v_c_i2c1 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c1_speedmode_idx');
v_cb_i2c1_speedmode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c1_clockspeed_idx');
v_cb_i2c1_clockspeed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c1_fm_dc_idx');
v_cb_i2c1_fm_dc_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c1_s_cns_idx');
v_cb_i2c1_s_cns_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c1_s_pal_idx');
v_cb_i2c1_s_pal_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c1_s_daa_idx');
v_cb_i2c1_s_daa_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_se_i2c1_s_psa');
v_se_i2c1_s_psa := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c1_s_gcad_idx');
v_cb_i2c1_s_gcad_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_i2c1_event_int');
v_c_i2c1_event_int := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_c_i2c1_error_int');
v_c_i2c1_error_int := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_i2c1_int_priority');
v_i2c1_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_i2c1_int_subpriority');
v_i2c1_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);


{===============
 I2C2 PERIPHERAL
 ---------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_i2c2');
v_c_i2c2 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c2_speedmode_idx');
v_cb_i2c2_speedmode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c2_clockspeed_idx');
v_cb_i2c2_clockspeed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c2_fm_dc_idx');
v_cb_i2c2_fm_dc_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c2_s_cns_idx');
v_cb_i2c2_s_cns_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c2_s_pal_idx');
v_cb_i2c2_s_pal_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c2_s_daa_idx');
v_cb_i2c2_s_daa_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_se_i2c2_s_psa');
v_se_i2c2_s_psa := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_i2c2_s_gcad_idx');
v_cb_i2c2_s_gcad_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_i2c2_event_int');
v_c_i2c2_event_int := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_c_i2c2_error_int');
v_c_i2c2_error_int := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_i2c2_int_priority');
v_i2c2_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_i2c2_int_subpriority');
v_i2c2_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);

{===================
 LCD4 pinout setting
 -------------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_lcd4');
v_c_lcd4 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_r1_lcd4');
v_r1_lcd4 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_r2_lcd4');
v_r2_lcd4 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_idx_lcd4');
v_cb_idx_lcd4 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_idx_lcd4_chipset');
v_cb_idx_lcd4_chipset := strtoint(PassNode.FirstChild.NodeValue);

{==================
 SPI1 CONFIGURATION
 ------------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_spi1');
v_c_spi1 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_direction_idx_spi1');
v_cb_direction_idx_spi1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_mode_idx_spi1');
v_cb_mode_idx_spi1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_datasize_idx_spi1');
v_cb_datasize_idx_spi1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_cpol_idx_spi1');
v_cb_cpol_idx_spi1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_cpha_idx_spi1');
v_cb_cpha_idx_spi1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_nss_idx_spi1');
v_cb_nss_idx_spi1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_prescaler_idx_spi1');
v_cb_prescaler_idx_spi1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_firstbit_idx_spi1');
v_cb_firstbit_idx_spi1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_crcpoly_idx_spi1');
v_cb_crcpoly_idx_spi1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_gb_int_spi1');
v_c_gb_int_spi1 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_spi1_int_priority');
v_spi1_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_spi1_int_subpriority');
v_spi1_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);


{==================
 SPI2 CONFIGURATION
 ------------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_spi2');
v_c_spi2 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_direction_idx_spi2');
v_cb_direction_idx_spi2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_mode_idx_spi2');
v_cb_mode_idx_spi2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_datasize_idx_spi2');
v_cb_datasize_idx_spi2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_cpol_idx_spi2');
v_cb_cpol_idx_spi2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_cpha_idx_spi2');
v_cb_cpha_idx_spi2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_nss_idx_spi2');
v_cb_nss_idx_spi2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_prescaler_idx_spi2');
v_cb_prescaler_idx_spi2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_firstbit_idx_spi2');
v_cb_firstbit_idx_spi2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_crcpoly_idx_spi2');
v_cb_crcpoly_idx_spi2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_gb_int_spi2');
v_c_gb_int_spi2 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_spi2_int_priority');
v_spi2_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_spi2_int_subpriority');
v_spi2_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);

{==================
 SPI3 CONFIGURATION
 ------------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_spi3');
v_c_spi3 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_direction_idx_spi3');
v_cb_direction_idx_spi3 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_mode_idx_spi3');
v_cb_mode_idx_spi3 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_datasize_idx_spi3');
v_cb_datasize_idx_spi3 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_cpol_idx_spi3');
v_cb_cpol_idx_spi3 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_cpha_idx_spi3');
v_cb_cpha_idx_spi3 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_nss_idx_spi3');
v_cb_nss_idx_spi3 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_prescaler_idx_spi3');
v_cb_prescaler_idx_spi3 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_firstbit_idx_spi3');
v_cb_firstbit_idx_spi3 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_crcpoly_idx_spi3');
v_cb_crcpoly_idx_spi3 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_gb_int_spi3');
v_c_gb_int_spi3 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_spi3_int_priority');
v_spi3_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_spi3_int_subpriority');
v_spi3_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);

{===================
 UART4 CONFIGURATION
 -------------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_uart4');
v_c_uart4 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_baud_idx_uart4');
v_cb_baud_idx_uart4 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_wordlng_idx_uart4');
v_cb_wordlng_idx_uart4 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_parity_idx_uart4');
v_cb_parity_idx_uart4 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_stop_idx_uart4');
v_cb_stop_idx_uart4 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_datadir_idx_uart4');
v_cb_datadir_idx_uart4 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_sampling_idx_uart4');
v_cb_sampling_idx_uart4 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_gb_int_uart4');
v_c_gb_int_uart4 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_uart4_int_priority');
v_uart4_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_uart4_int_subpriority');
v_uart4_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);


{===================
 UART5 CONFIGURATION
 -------------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_uart5');
v_c_uart5 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_baud_idx_uart5');
v_cb_baud_idx_uart5 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_wordlng_idx_uart5');
v_cb_wordlng_idx_uart5 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_parity_idx_uart5');
v_cb_parity_idx_uart5 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_stop_idx_uart5');
v_cb_stop_idx_uart5 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_datadir_idx_uart5');
v_cb_datadir_idx_uart5 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_sampling_idx_uart5');
v_cb_sampling_idx_uart5 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_gb_int_uart5');
v_c_gb_int_uart5 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_uart5_int_priority');
v_uart5_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_uart5_int_subpriority');
v_uart5_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);

{====================
 USART2 CONFIGURATION
 --------------------}
  PassNode := Doc.DocumentElement.FindNode('v_cb_baud_idx_usart2');
v_cb_baud_idx_usart2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_wordlng_idx_usart2');
v_cb_wordlng_idx_usart2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_parity_idx_usart2');
v_cb_parity_idx_usart2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_stop_idx_usart2');
v_cb_stop_idx_usart2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_datadir_idx_usart2');
v_cb_datadir_idx_usart2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_sampling_idx_usart2');
v_cb_sampling_idx_usart2 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_gb_int_usart2');
v_c_gb_int_usart2 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_usart2_int_priority');
v_usart2_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_usart2_int_subpriority');
v_usart2_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);

{====================
 USART1 CONFIGURATION
 --------------------}
  PassNode := Doc.DocumentElement.FindNode('v_c_usart1');
v_c_usart1 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_baud_idx_usart1');
v_cb_baud_idx_usart1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_wordlng_idx_usart1');
v_cb_wordlng_idx_usart1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_parity_idx_usart1');
v_cb_parity_idx_usart1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_stop_idx_usart1');
v_cb_stop_idx_usart1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_datadir_idx_usart1');
v_cb_datadir_idx_usart1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_sampling_idx_usart1');
v_cb_sampling_idx_usart1 := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_gb_int_usart1');
v_c_gb_int_usart1 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_usart1_int_priority');
v_usart1_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_usart1_int_subpriority');
v_usart1_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);

{===============
 ADC1 PERIPHERAL
 ---------------}
  PassNode := Doc.DocumentElement.FindNode('v_adc1_res_idx');
v_adc1_res_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_adc1_scan_idx');
v_adc1_scan_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_adc1_continuous_idx');
v_adc1_continuous_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_adc1_tce_idx');
v_adc1_tce_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_adc1_tc_idx');
v_adc1_tc_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_adc1_align_idx');
v_adc1_align_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_adc1_prescaler_idx');
v_adc1_prescaler_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_adc1_nrconv_idx');
v_adc1_nrconv_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_gb_int_adc1');
v_c_gb_int_adc1 := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_adc1_int_priority');
v_adc1_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_adc1_int_subpriority');
v_adc1_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);


{================}
{MORPHO CONNECTOR}
{----------------}
{CN7 connector, the non-paired pins row - 1,3,5, etc.}
{PC10}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc10_idx');
v_cb_pc10_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc10_txt');
v_e_pc10_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_id');
v_pc10_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_func');
v_pc10_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_o_lvl_idx');
v_pc10_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_o_mode_idx');
v_pc10_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_i_mode_idx');
v_pc10_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_a_mode_idx');
v_pc10_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_i2c_mode_idx');
v_pc10_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_spi_mode_idx');
v_pc10_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_int_mode_idx');
v_pc10_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_pull_updown_idx');
v_pc10_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_speed_idx');
v_pc10_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc10_name');
v_pc10_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC12}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc12_idx');
v_cb_pc12_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc12_txt');
v_e_pc12_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_id');
v_pc12_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_func');
v_pc12_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_o_lvl_idx');
v_pc12_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_o_mode_idx');
v_pc12_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_i_mode_idx');
v_pc12_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_a_mode_idx');
v_pc12_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_i2c_mode_idx');
v_pc12_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_spi_mode_idx');
v_pc12_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_int_mode_idx');
v_pc12_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_pull_updown_idx');
v_pc12_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_speed_idx');
v_pc12_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc12_name');
v_pc12_name := AnsiString(PassNode.FirstChild.NodeValue);


{PA15}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa15_idx');
v_cb_pa15_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa15_txt');
v_e_pa15_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_id');
v_pa15_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_func');
v_pa15_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_o_lvl_idx');
v_pa15_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_o_mode_idx');
v_pa15_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_i_mode_idx');
v_pa15_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_a_mode_idx');
v_pa15_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_i2c_mode_idx');
v_pa15_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_spi_mode_idx');
v_pa15_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_int_mode_idx');
v_pa15_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_pull_updown_idx');
v_pa15_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_speed_idx');
v_pa15_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa15_name');
v_pa15_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB7}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb7_idx');
v_cb_pb7_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb7_txt');
v_e_pb7_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_id');
v_pb7_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_func');
v_pb7_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_o_lvl_idx');
v_pb7_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_o_mode_idx');
v_pb7_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_i_mode_idx');
v_pb7_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_a_mode_idx');
v_pb7_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_i2c_mode_idx');
v_pb7_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_spi_mode_idx');
v_pb7_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_int_mode_idx');
v_pb7_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_pull_updown_idx');
v_pb7_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_speed_idx');
v_pb7_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb7_name');
v_pb7_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC13} {- the user button on Nucleo}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc13_idx');
v_cb_pc13_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc13_txt');
v_e_pc13_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_id');
v_pc13_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_func');
v_pc13_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_o_lvl_idx');
v_pc13_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_o_mode_idx');
v_pc13_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_i_mode_idx');
v_pc13_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_a_mode_idx');
v_pc13_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_i2c_mode_idx');
v_pc13_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_spi_mode_idx');
v_pc13_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_int_mode_idx');
v_pc13_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_pull_updown_idx');
v_pc13_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_speed_idx');
v_pc13_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_name');
v_pc13_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC2}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc2_idx');
v_cb_pc2_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc2_txt');
v_e_pc2_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_id');
v_pc2_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_func');
v_pc2_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_o_lvl_idx');
v_pc2_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_o_mode_idx');
v_pc2_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_i_mode_idx');
v_pc2_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_a_mode_idx');
v_pc2_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_i2c_mode_idx');
v_pc2_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_spi_mode_idx');
v_pc2_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_int_mode_idx');
v_pc2_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_pull_updown_idx');
v_pc2_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_speed_idx');
v_pc2_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc2_name');
v_pc2_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC3}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc3_idx');
v_cb_pc3_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc3_txt');
v_e_pc3_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_id');
v_pc3_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_func');
v_pc3_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_o_lvl_idx');
v_pc3_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_o_mode_idx');
v_pc3_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_i_mode_idx');
v_pc3_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_a_mode_idx');
v_pc3_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_i2c_mode_idx');
v_pc3_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_spi_mode_idx');
v_pc3_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_int_mode_idx');
v_pc3_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_pull_updown_idx');
v_pc3_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_speed_idx');
v_pc3_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc3_name');
v_pc3_name := AnsiString(PassNode.FirstChild.NodeValue);

{CN7 connector, the paired pins row - 2,4,6, etc.}
{PC11}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc11_idx');
v_cb_pc11_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc11_txt');
v_e_pc11_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_id');
v_pc11_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_func');
v_pc11_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_o_lvl_idx');
v_pc11_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_o_mode_idx');
v_pc11_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_i_mode_idx');
v_pc11_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_a_mode_idx');
v_pc11_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_i2c_mode_idx');
v_pc11_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_spi_mode_idx');
v_pc11_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_int_mode_idx');
v_pc11_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_pull_updown_idx');
v_pc11_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_speed_idx');
v_pc11_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc11_name');
v_pc11_name := AnsiString(PassNode.FirstChild.NodeValue);

{PD2}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pd2_idx');
v_cb_pd2_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pd2_txt');
v_e_pd2_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_id');
v_pd2_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_func');
v_pd2_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_o_lvl_idx');
v_pd2_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_o_mode_idx');
v_pd2_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_i_mode_idx');
v_pd2_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_a_mode_idx');
v_pd2_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_i2c_mode_idx');
v_pd2_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_spi_mode_idx');
v_pd2_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_int_mode_idx');
v_pd2_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_pull_updown_idx');
v_pd2_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_speed_idx');
v_pd2_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pd2_name');
v_pd2_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA0}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa0_idx');
v_cb_pa0_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa0_txt');
v_e_pa0_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_id');
v_pa0_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_func');
v_pa0_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_o_lvl_idx');
v_pa0_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_o_mode_idx');
v_pa0_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_i_mode_idx');
v_pa0_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_a_mode_idx');
v_pa0_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_i2c_mode_idx');
v_pa0_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_spi_mode_idx');
v_pa0_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_int_mode_idx');
v_pa0_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_pull_updown_idx');
v_pa0_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_speed_idx');
v_pa0_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa0_name');
v_pa0_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA1}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa1_idx');
v_cb_pa1_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa1_txt');
v_e_pa1_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_id');
v_pa1_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_func');
v_pa1_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_o_lvl_idx');
v_pa1_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_o_mode_idx');
v_pa1_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_i_mode_idx');
v_pa1_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_a_mode_idx');
v_pa1_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_i2c_mode_idx');
v_pa1_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_spi_mode_idx');
v_pa1_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_int_mode_idx');
v_pa1_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_pull_updown_idx');
v_pa1_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_speed_idx');
v_pa1_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa1_name');
v_pa1_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA4}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa4_idx');
v_cb_pa4_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa4_txt');
v_e_pa4_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_id');
v_pa4_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_func');
v_pa4_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_o_lvl_idx');
v_pa4_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_o_mode_idx');
v_pa4_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_i_mode_idx');
v_pa4_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_a_mode_idx');
v_pa4_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_i2c_mode_idx');
v_pa4_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_spi_mode_idx');
v_pa4_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_int_mode_idx');
v_pa4_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_pull_updown_idx');
v_pa4_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_speed_idx');
v_pa4_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa4_name');
v_pa4_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB0}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb0_idx');
v_cb_pb0_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb0_txt');
v_e_pb0_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_id');
v_pb0_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_func');
v_pb0_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_o_lvl_idx');
v_pb0_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_o_mode_idx');
v_pb0_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_i_mode_idx');
v_pb0_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_a_mode_idx');
v_pb0_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_i2c_mode_idx');
v_pb0_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_spi_mode_idx');
v_pb0_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_int_mode_idx');
v_pb0_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_pull_updown_idx');
v_pb0_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_speed_idx');
v_pb0_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb0_name');
v_pb0_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC1}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc1_idx');
v_cb_pc1_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc1_txt');
v_e_pc1_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_id');
v_pc1_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_func');
v_pc1_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_o_lvl_idx');
v_pc1_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_o_mode_idx');
v_pc1_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_i_mode_idx');
v_pc1_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_a_mode_idx');
v_pc1_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_i2c_mode_idx');
v_pc1_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_spi_mode_idx');
v_pc1_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_int_mode_idx');
v_pc1_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_pull_updown_idx');
v_pc1_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_speed_idx');
v_pc1_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc1_name');
v_pc1_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC0}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc0_idx');
v_cb_pc0_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc0_txt');
v_e_pc0_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_id');
v_pc0_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_func');
v_pc0_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_o_lvl_idx');
v_pc0_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_o_mode_idx');
v_pc0_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_i_mode_idx');
v_pc0_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_a_mode_idx');
v_pc0_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_i2c_mode_idx');
v_pc0_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_spi_mode_idx');
v_pc0_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_int_mode_idx');
v_pc0_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_pull_updown_idx');
v_pc0_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_speed_idx');
v_pc0_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc0_name');
v_pc0_name := AnsiString(PassNode.FirstChild.NodeValue);

{CN10 connector, the non-paired pins row - 1,3,5, etc.}
{PC9}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc9_idx');
v_cb_pc9_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc9_txt');
v_e_pc9_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_id');
v_pc9_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_func');
v_pc9_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_o_lvl_idx');
v_pc9_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_o_mode_idx');
v_pc9_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_i_mode_idx');
v_pc9_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_a_mode_idx');
v_pc9_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_i2c_mode_idx');
v_pc9_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_spi_mode_idx');
v_pc9_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_int_mode_idx');
v_pc9_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_pull_updown_idx');
v_pc9_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_speed_idx');
v_pc9_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc9_name');
v_pc9_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB8}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb8_idx');
v_cb_pb8_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb8_txt');
v_e_pb8_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_id');
v_pb8_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_func');
v_pb8_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_o_lvl_idx');
v_pb8_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_o_mode_idx');
v_pb8_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_i_mode_idx');
v_pb8_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_a_mode_idx');
v_pb8_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_i2c_mode_idx');
v_pb8_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_spi_mode_idx');
v_pb8_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_int_mode_idx');
v_pb8_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_pull_updown_idx');
v_pb8_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_speed_idx');
v_pb8_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb8_name');
v_pb8_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB9}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb9_idx');
v_cb_pb9_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb9_txt');
v_e_pb9_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_id');
v_pb9_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_func');
v_pb9_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_o_lvl_idx');
v_pb9_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_o_mode_idx');
v_pb9_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_i_mode_idx');
v_pb9_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_a_mode_idx');
v_pb9_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_i2c_mode_idx');
v_pb9_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_spi_mode_idx');
v_pb9_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_int_mode_idx');
v_pb9_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_pull_updown_idx');
v_pb9_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_speed_idx');
v_pb9_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb9_name');
v_pb9_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA5} {- the LD2 user LED}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa5_idx');
v_cb_pa5_idx := strtoint(PassNode.FirstChild.NodeValue);
  if v_file_version = 1 then v_cb_pa5_idx := v_cb_pa5_idx - 1;
  PassNode := Doc.DocumentElement.FindNode('v_e_pa5_txt');
v_e_pa5_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_id');
v_pa5_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_func');
v_pa5_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_o_lvl_idx');
v_pa5_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_o_mode_idx');
v_pa5_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_i_mode_idx');
v_pa5_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_a_mode_idx');
v_pa5_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_i2c_mode_idx');
v_pa5_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_spi_mode_idx');
v_pa5_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_int_mode_idx');
v_pa5_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_pull_updown_idx');
v_pa5_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_speed_idx');
v_pa5_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa5_name');
v_pa5_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA6}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa6_idx');
v_cb_pa6_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa6_txt');
v_e_pa6_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_id');
v_pa6_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_func');
v_pa6_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_o_lvl_idx');
v_pa6_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_o_mode_idx');
v_pa6_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_i_mode_idx');
v_pa6_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_a_mode_idx');
v_pa6_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_i2c_mode_idx');
v_pa6_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_spi_mode_idx');
v_pa6_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_int_mode_idx');
v_pa6_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_pull_updown_idx');
v_pa6_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_speed_idx');
v_pa6_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa6_name');
v_pa6_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA7}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa7_idx');
v_cb_pa7_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa7_txt');
v_e_pa7_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_id');
v_pa7_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_func');
v_pa7_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_o_lvl_idx');
v_pa7_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_o_mode_idx');
v_pa7_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_i_mode_idx');
v_pa7_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_a_mode_idx');
v_pa7_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_i2c_mode_idx');
v_pa7_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_spi_mode_idx');
v_pa7_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_int_mode_idx');
v_pa7_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_pull_updown_idx');
v_pa7_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_speed_idx');
v_pa7_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa7_name');
v_pa7_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB6}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb6_idx');
v_cb_pb6_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb6_txt');
v_e_pb6_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_id');
v_pb6_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_func');
v_pb6_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_o_lvl_idx');
v_pb6_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_o_mode_idx');
v_pb6_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_i_mode_idx');
v_pb6_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_a_mode_idx');
v_pb6_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_i2c_mode_idx');
v_pb6_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_spi_mode_idx');
v_pb6_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_int_mode_idx');
v_pb6_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_pull_updown_idx');
v_pb6_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_speed_idx');
v_pb6_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb6_name');
v_pb6_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC7}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc7_idx');
v_cb_pc7_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc7_txt');
v_e_pc7_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_id');
v_pc7_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_func');
v_pc7_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_o_lvl_idx');
v_pc7_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_o_mode_idx');
v_pc7_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_i_mode_idx');
v_pc7_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_a_mode_idx');
v_pc7_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_i2c_mode_idx');
v_pc7_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_spi_mode_idx');
v_pc7_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_int_mode_idx');
v_pc7_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_pull_updown_idx');
v_pc7_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_speed_idx');
v_pc7_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc7_name');
v_pc7_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA9}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa9_idx');
v_cb_pa9_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa9_txt');
v_e_pa9_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_id');
v_pa9_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_func');
v_pa9_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_o_lvl_idx');
v_pa9_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_o_mode_idx');
v_pa9_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_i_mode_idx');
v_pa9_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_a_mode_idx');
v_pa9_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_i2c_mode_idx');
v_pa9_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_spi_mode_idx');
v_pa9_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_int_mode_idx');
v_pa9_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_pull_updown_idx');
v_pa9_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_speed_idx');
v_pa9_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa9_name');
v_pa9_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA8}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa8_idx');
v_cb_pa8_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa8_txt');
v_e_pa8_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_id');
v_pa8_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_func');
v_pa8_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_o_lvl_idx');
v_pa8_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_o_mode_idx');
v_pa8_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_i_mode_idx');
v_pa8_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_a_mode_idx');
v_pa8_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_i2c_mode_idx');
v_pa8_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_spi_mode_idx');
v_pa8_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_int_mode_idx');
v_pa8_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_pull_updown_idx');
v_pa8_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_speed_idx');
v_pa8_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa8_name');
v_pa8_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB10}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb10_idx');
v_cb_pb10_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb10_txt');
v_e_pb10_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_id');
v_pb10_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_func');
v_pb10_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_o_lvl_idx');
v_pb10_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_o_mode_idx');
v_pb10_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_i_mode_idx');
v_pb10_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_a_mode_idx');
v_pb10_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_i2c_mode_idx');
v_pb10_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_spi_mode_idx');
v_pb10_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_int_mode_idx');
v_pb10_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_pull_updown_idx');
v_pb10_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_speed_idx');
v_pb10_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb10_name');
v_pb10_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB4}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb4_idx');
v_cb_pb4_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb4_txt');
v_e_pb4_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_id');
v_pb4_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_func');
v_pb4_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_o_lvl_idx');
v_pb4_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_o_mode_idx');
v_pb4_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_i_mode_idx');
v_pb4_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_a_mode_idx');
v_pb4_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_i2c_mode_idx');
v_pb4_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_spi_mode_idx');
v_pb4_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_int_mode_idx');
v_pb4_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_pull_updown_idx');
v_pb4_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_speed_idx');
v_pb4_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb4_name');
v_pb4_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB5}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb5_idx');
v_cb_pb5_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb5_txt');
v_e_pb5_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_id');
v_pb5_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_func');
v_pb5_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_o_lvl_idx');
v_pb5_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_o_mode_idx');
v_pb5_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_i_mode_idx');
v_pb5_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_a_mode_idx');
v_pb5_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_i2c_mode_idx');
v_pb5_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_spi_mode_idx');
v_pb5_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_int_mode_idx');
v_pb5_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_pull_updown_idx');
v_pb5_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_speed_idx');
v_pb5_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb5_name');
v_pb5_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB3}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb3_idx');
v_cb_pb3_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb3_txt');
v_e_pb3_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_id');
v_pb3_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_func');
v_pb3_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_o_lvl_idx');
v_pb3_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_o_mode_idx');
v_pb3_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_i_mode_idx');
v_pb3_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_a_mode_idx');
v_pb3_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_i2c_mode_idx');
v_pb3_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_spi_mode_idx');
v_pb3_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_int_mode_idx');
v_pb3_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_pull_updown_idx');
v_pb3_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_speed_idx');
v_pb3_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb3_name');
v_pb3_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA10}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa10_idx');
v_cb_pa10_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa10_txt');
v_e_pa10_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_id');
v_pa10_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_func');
v_pa10_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_o_lvl_idx');
v_pa10_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_o_mode_idx');
v_pa10_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_i_mode_idx');
v_pa10_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_a_mode_idx');
v_pa10_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_i2c_mode_idx');
v_pa10_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_spi_mode_idx');
v_pa10_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_int_mode_idx');
v_pa10_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_pull_updown_idx');
v_pa10_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_speed_idx');
v_pa10_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa10_name');
v_pa10_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA2}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa2_idx');
v_cb_pa2_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa2_txt');
v_e_pa2_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_id');
v_pa2_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_func');
v_pa2_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_o_lvl_idx');
v_pa2_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_o_mode_idx');
v_pa2_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_i_mode_idx');
v_pa2_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_a_mode_idx');
v_pa2_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_i2c_mode_idx');
v_pa2_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_spi_mode_idx');
v_pa2_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_int_mode_idx');
v_pa2_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_pull_updown_idx');
v_pa2_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_speed_idx');
v_pa2_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_name');
v_pa2_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA3}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa3_idx');
v_cb_pa3_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa3_txt');
v_e_pa3_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_id');
v_pa3_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_func');
v_pa3_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_o_lvl_idx');
v_pa3_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_o_mode_idx');
v_pa3_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_i_mode_idx');
v_pa3_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_a_mode_idx');
v_pa3_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_i2c_mode_idx');
v_pa3_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_spi_mode_idx');
v_pa3_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_int_mode_idx');
v_pa3_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_pull_updown_idx');
v_pa3_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_speed_idx');
v_pa3_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_name');
v_pa3_name := AnsiString(PassNode.FirstChild.NodeValue);

{CN10 connector, the paired pins row - 2,4,6, etc.}
{PC8}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc8_idx');
v_cb_pc8_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc8_txt');
v_e_pc8_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_id');
v_pc8_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_func');
v_pc8_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_o_lvl_idx');
v_pc8_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_o_mode_idx');
v_pc8_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_i_mode_idx');
v_pc8_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_a_mode_idx');
v_pc8_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_i2c_mode_idx');
v_pc8_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_spi_mode_idx');
v_pc8_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_int_mode_idx');
v_pc8_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_pull_updown_idx');
v_pc8_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_speed_idx');
v_pc8_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc8_name');
v_pc8_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC6}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc6_idx');
v_cb_pc6_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc6_txt');
v_e_pc6_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_id');
v_pc6_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_func');
v_pc6_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_o_lvl_idx');
v_pc6_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_o_mode_idx');
v_pc6_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_i_mode_idx');
v_pc6_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_a_mode_idx');
v_pc6_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_i2c_mode_idx');
v_pc6_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_spi_mode_idx');
v_pc6_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_int_mode_idx');
v_pc6_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_pull_updown_idx');
v_pc6_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_speed_idx');
v_pc6_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc6_name');
v_pc6_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC5}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc5_idx');
v_cb_pc5_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc5_txt');
v_e_pc5_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_id');
v_pc5_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_func');
v_pc5_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_o_lvl_idx');
v_pc5_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_o_mode_idx');
v_pc5_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_i_mode_idx');
v_pc5_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_a_mode_idx');
v_pc5_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_i2c_mode_idx');
v_pc5_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_spi_mode_idx');
v_pc5_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_int_mode_idx');
v_pc5_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_pull_updown_idx');
v_pc5_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_speed_idx');
v_pc5_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc5_name');
v_pc5_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA12}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa12_idx');
v_cb_pa12_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa12_txt');
v_e_pa12_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_id');
v_pa12_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_func');
v_pa12_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_o_lvl_idx');
v_pa12_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_o_mode_idx');
v_pa12_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_i_mode_idx');
v_pa12_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_a_mode_idx');
v_pa12_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_i2c_mode_idx');
v_pa12_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_spi_mode_idx');
v_pa12_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_int_mode_idx');
v_pa12_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_pull_updown_idx');
v_pa12_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_speed_idx');
v_pa12_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa12_name');
v_pa12_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA11}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa11_idx');
v_cb_pa11_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa11_txt');
v_e_pa11_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_id');
v_pa11_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_func');
v_pa11_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_o_lvl_idx');
v_pa11_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_o_mode_idx');
v_pa11_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_i_mode_idx');
v_pa11_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_a_mode_idx');
v_pa11_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_i2c_mode_idx');
v_pa11_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_spi_mode_idx');
v_pa11_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_int_mode_idx');
v_pa11_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_pull_updown_idx');
v_pa11_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_speed_idx');
v_pa11_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa11_name');
v_pa11_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB12}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb12_idx');
v_cb_pb12_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb12_txt');
v_e_pb12_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_id');
v_pb12_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_func');
v_pb12_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_o_lvl_idx');
v_pb12_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_o_mode_idx');
v_pb12_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_i_mode_idx');
v_pb12_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_a_mode_idx');
v_pb12_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_i2c_mode_idx');
v_pb12_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_spi_mode_idx');
v_pb12_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_int_mode_idx');
v_pb12_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_pull_updown_idx');
v_pb12_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_speed_idx');
v_pb12_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb12_name');
v_pb12_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB11}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb11_idx');
v_cb_pb11_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb11_txt');
v_e_pb11_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_id');
v_pb11_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_func');
v_pb11_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_o_lvl_idx');
v_pb11_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_o_mode_idx');
v_pb11_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_i_mode_idx');
v_pb11_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_a_mode_idx');
v_pb11_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_i2c_mode_idx');
v_pb11_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_spi_mode_idx');
v_pb11_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_int_mode_idx');
v_pb11_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_pull_updown_idx');
v_pb11_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_speed_idx');
v_pb11_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb11_name');
v_pb11_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB2}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb2_idx');
v_cb_pb2_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb2_txt');
v_e_pb2_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_id');
v_pb2_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_func');
v_pb2_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_o_lvl_idx');
v_pb2_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_o_mode_idx');
v_pb2_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_i_mode_idx');
v_pb2_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_a_mode_idx');
v_pb2_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_i2c_mode_idx');
v_pb2_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_spi_mode_idx');
v_pb2_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_int_mode_idx');
v_pb2_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_pull_updown_idx');
v_pb2_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_speed_idx');
v_pb2_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb2_name');
v_pb2_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB1}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb1_idx');
v_cb_pb1_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb1_txt');
v_e_pb1_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_id');
v_pb1_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_func');
v_pb1_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_o_lvl_idx');
v_pb1_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_o_mode_idx');
v_pb1_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_i_mode_idx');
v_pb1_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_a_mode_idx');
v_pb1_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_i2c_mode_idx');
v_pb1_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_spi_mode_idx');
v_pb1_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_int_mode_idx');
v_pb1_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_pull_updown_idx');
v_pb1_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_speed_idx');
v_pb1_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb1_name');
v_pb1_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB15}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb15_idx');
v_cb_pb15_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb15_txt');
v_e_pb15_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_id');
v_pb15_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_func');
v_pb15_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_o_lvl_idx');
v_pb15_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_o_mode_idx');
v_pb15_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_i_mode_idx');
v_pb15_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_a_mode_idx');
v_pb15_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_i2c_mode_idx');
v_pb15_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_spi_mode_idx');
v_pb15_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_int_mode_idx');
v_pb15_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_pull_updown_idx');
v_pb15_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_speed_idx');
v_pb15_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb15_name');
v_pb15_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB14}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb14_idx');
v_cb_pb14_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb14_txt');
v_e_pb14_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_id');
v_pb14_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_func');
v_pb14_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_o_lvl_idx');
v_pb14_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_o_mode_idx');
v_pb14_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_i_mode_idx');
v_pb14_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_a_mode_idx');
v_pb14_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_i2c_mode_idx');
v_pb14_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_spi_mode_idx');
v_pb14_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_int_mode_idx');
v_pb14_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_pull_updown_idx');
v_pb14_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_speed_idx');
v_pb14_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb14_name');
v_pb14_name := AnsiString(PassNode.FirstChild.NodeValue);

{PB13}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pb13_idx');
v_cb_pb13_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pb13_txt');
v_e_pb13_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_id');
v_pb13_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_func');
v_pb13_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_o_lvl_idx');
v_pb13_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_o_mode_idx');
v_pb13_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_i_mode_idx');
v_pb13_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_a_mode_idx');
v_pb13_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_i2c_mode_idx');
v_pb13_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_spi_mode_idx');
v_pb13_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_int_mode_idx');
v_pb13_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_pull_updown_idx');
v_pb13_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_speed_idx');
v_pb13_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pb13_name');
v_pb13_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC4}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc4_idx');
v_cb_pc4_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc4_txt');
v_e_pc4_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_id');
v_pc4_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_func');
v_pc4_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_o_lvl_idx');
v_pc4_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_o_mode_idx');
v_pc4_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_i_mode_idx');
v_pc4_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_a_mode_idx');
v_pc4_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_i2c_mode_idx');
v_pc4_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_spi_mode_idx');
v_pc4_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_int_mode_idx');
v_pc4_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_pull_updown_idx');
v_pc4_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_speed_idx');
v_pc4_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc4_name');
v_pc4_name := AnsiString(PassNode.FirstChild.NodeValue);

{==========================}
{SYSTEM CLOCK CONFIGURATION}
{--------------------------}
//  PassNode := Doc.DocumentElement.FindNode('v_c_rcc_mco');
//v_c_rcc_mco := setbool(AnsiString(PassNode.FirstChild.NodeValue)); {Master Clock Output}
//  PassNode := Doc.DocumentElement.FindNode('v_mco_divider_idx');
//v_mco_divider_idx := strtoint(PassNode.FirstChild.NodeValue);
//  PassNode := Doc.DocumentElement.FindNode('v_mco_source');
//v_mco_source := strtoint(PassNode.FirstChild.NodeValue);
//  PassNode := Doc.DocumentElement.FindNode('v_clock_source');
//v_clock_source := strtoint(PassNode.FirstChild.NodeValue);
//  PassNode := Doc.DocumentElement.FindNode('v_hse_clk_source_idx');
//v_hse_clk_source_idx := strtoint(PassNode.FirstChild.NodeValue);
  //PassNode := Doc.DocumentElement.FindNode('v_tick_manager');
//v_tick_manager := strtoint(PassNode.FirstChild.NodeValue);

{==================
 TIM6 CONFIGURATION
 ------------------}
  PassNode := Doc.DocumentElement.FindNode('v_tim6_check');
v_tim6_check := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_tim6_prescaler');
v_tim6_prescaler := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_tim6_period');
v_tim6_period := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_tim6_event');
v_tim6_event := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_tim6_global_int');
v_tim6_global_int := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_tim6_int_priority');
v_tim6_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_tim6_int_subpriority');
v_tim6_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);

{==================
 TIM7 CONFIGURATION
 ------------------}
  PassNode := Doc.DocumentElement.FindNode('v_tim7_check');
v_tim7_check := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_tim7_prescaler');
v_tim7_prescaler := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_tim7_period');
v_tim7_period := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_tim7_event');
v_tim7_event := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_tim7_global_int');
v_tim7_global_int := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_tim7_int_priority');
v_tim7_int_priority := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_tim7_int_subpriority');
v_tim7_int_subpriority := strtoint(PassNode.FirstChild.NodeValue);

{now read the elements specific to version 2}

  PassNode := Doc.DocumentElement.FindNode('v_c_i2c1_a');
v_c_i2c1_a := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_c_i2c1_b');
v_c_i2c1_b := setbool(AnsiString(PassNode.FirstChild.NodeValue));

{PA2 - PA2-PA3 are permanent USART2 on Nucleo} 
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa2_idx');
v_cb_pa2_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa2_txt');
v_e_pa2_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_id');
v_pa2_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_func');
v_pa2_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_o_lvl_idx');
v_pa2_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_o_mode_idx');
v_pa2_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_i_mode_idx');
v_pa2_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_a_mode_idx');
v_pa2_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_i2c_mode_idx');
v_pa2_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_spi_mode_idx');
v_pa2_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_int_mode_idx');
v_pa2_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_pull_updown_idx');
v_pa2_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_speed_idx');
v_pa2_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa2_name');
v_pa2_name := AnsiString(PassNode.FirstChild.NodeValue);

{PA6}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pa3_idx');
v_cb_pa3_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pa3_txt');
v_e_pa3_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_id');
v_pa3_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_func');
v_pa3_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_o_lvl_idx');
v_pa3_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_o_mode_idx');
v_pa3_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_i_mode_idx');
v_pa3_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_a_mode_idx');
v_pa3_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_i2c_mode_idx');
v_pa3_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_spi_mode_idx');
v_pa3_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_int_mode_idx');
v_pa3_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_pull_updown_idx');
v_pa3_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_speed_idx');
v_pa3_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pa3_name');
v_pa3_name := AnsiString(PassNode.FirstChild.NodeValue);

{PC13}
  PassNode := Doc.DocumentElement.FindNode('v_cb_pc13_idx');
v_cb_pc13_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_e_pc13_txt');
v_e_pc13_txt := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_id');
v_pc13_id := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_func');
v_pc13_func := AnsiString(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_o_lvl_idx');
v_pc13_o_lvl_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_o_mode_idx');
v_pc13_o_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_i_mode_idx');
v_pc13_i_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_a_mode_idx');
v_pc13_a_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_i2c_mode_idx');
v_pc13_i2c_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_spi_mode_idx');
v_pc13_spi_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_int_mode_idx');
v_pc13_int_mode_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_pull_updown_idx');
v_pc13_pull_updown_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_speed_idx');
v_pc13_speed_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_pc13_name');
v_pc13_name := AnsiString(PassNode.FirstChild.NodeValue);

{lcd4}
  PassNode := Doc.DocumentElement.FindNode('v_lcd4_defaults');
v_lcd4_defaults := setbool(AnsiString(PassNode.FirstChild.NodeValue));

{==========================}
{SYSTEM CLOCK CONFIGURATION}
{--------------------------}
  PassNode := Doc.DocumentElement.FindNode('v_se_hse_value');
v_se_hse_value := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_msi_idx');
v_cb_msi_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_rg_vco_idx');
v_rg_vco_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_pllmul_idx');
v_cb_pllmul_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_plldiv_idx');
v_cb_plldiv_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_rg_clock_hub_idx');
v_rg_clock_hub_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_ahb_pr_idx');
v_cb_ahb_pr_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_systick_pr_idx');
v_cb_systick_pr_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_apb1_pr_idx');
v_cb_apb1_pr_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_apb2_pr_idx');
v_cb_apb2_pr_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_en_css_hse');
v_c_en_css_hse := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  //
  PassNode := Doc.DocumentElement.FindNode('v_cb_rcc_hse_idx');
v_cb_rcc_hse_idx := strtoint(PassNode.FirstChild.NodeValue);
//  PassNode := Doc.DocumentElement.FindNode('v_cb_rcc_lse_idx');
//v_cb_rcc_lse_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_rcc_mco');
v_c_rcc_mco := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  PassNode := Doc.DocumentElement.FindNode('v_cb_mco_div_idx');
v_cb_mco_div_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_rg_mco_src_idx');
v_rg_mco_src_idx := strtoint(PassNode.FirstChild.NodeValue);
  //
  PassNode := Doc.DocumentElement.FindNode('v_rg_osc32_idx');
v_rg_osc32_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_cb_hse_div_idx');
v_cb_hse_div_idx := strtoint(PassNode.FirstChild.NodeValue);
  PassNode := Doc.DocumentElement.FindNode('v_c_en_css_lse');
v_c_en_css_lse := setbool(AnsiString(PassNode.FirstChild.NodeValue));
  
  

  // DONE!!!
  finally
  Doc.Free;
  end;
  v_project_status := 'OPEN';
  {$I+}
end;

procedure close_project;
begin
  //save_project; {lets behave in a good maner ;) }
  v_project_status := 'NEW';
  v_prj_ok := FALSE;
  v_prj_name := '';
  {and so on...}
end;

end.


