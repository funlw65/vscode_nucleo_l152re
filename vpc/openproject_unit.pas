unit openproject_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics,
  Dialogs, FileCtrl, StdCtrls, Buttons, ExtCtrls, data_unit,
  driver_folders_unit, code_unit, DOM, XMLRead;

type

  { TForm7 }

  TForm7 = class(TForm)
    B_OpenPrjOk: TBitBtn;
    E_PrjName2: TEdit;
    LST_PrjFolder2: TFileListBox;
    Image1: TImage;
    Label1: TLabel;
    L_OpenPrjMess: TLabel;
    procedure B_OpenPrjOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LST_PrjFolder2Click(Sender: TObject);
  private

  public

  end;

var
  Form7: TForm7;

implementation

{$R *.lfm}

{ TForm7 }

procedure TForm7.B_OpenPrjOkClick(Sender: TObject);
var
  PassNode: TDOMNode;
  Doc: TXMLDocument;
begin
  {$I-}
  if((E_PrjName2.Text<>'')and(E_PrjName2.Text<>'.')and(E_PrjName2.Text<>'..')) then begin
    chdir(getenvironmentvariable('HOME') + '/' + v_workspace + '/' + E_PrjName2.Text);
    if(FileExists(E_PrjName2.Text+'.vpc')) then begin
      ReadXMLFile(Doc, getenvironmentvariable('HOME')+'/'+v_workspace+'/'+E_PrjName2.Text+'/'+E_PrjName2.Text+'.vpc');
      PassNode := Doc.DocumentElement.FindNode('file_version');
      v_file_version := strtoint(PassNode.FirstChild.NodeValue);
      Doc.Free;
      if v_file_version = 3 then begin
        reset_to_nucleo_hardware;
        open_project(E_PrjName2.Text);
        clean_labels_from_xml;
        calculate; // protection against bad/malicious clock configuration
        Close;
      end 
      else begin
        chdir(getenvironmentvariable('HOME') + '/' + v_workspace);
        L_OpenPrjMess.Caption := 'Error! Not a valid .vpc project file version!';        
      end;  
    end
    else begin
      chdir(getenvironmentvariable('HOME') + '/' + v_workspace);
      L_OpenPrjMess.Caption := 'Error! Not a valid .vpc project file inside!';
    end;
  end
  else L_OpenPrjMess.Caption := 'Error! Not a valid project selection!';
  {$I+}
end;

procedure TForm7.FormShow(Sender: TObject);
begin
  LST_PrjFolder2.Directory := getenvironmentvariable('HOME') + '/' + v_workspace;
  L_OpenPrjMess.Caption := 'Select a project (folder) from the list.';
  E_PrjName2.Text := '';
end;

procedure TForm7.LST_PrjFolder2Click(Sender: TObject);
begin
  E_PrjName2.Text := ExtractFileName(LST_PrjFolder2.FileName);
end;



end.

