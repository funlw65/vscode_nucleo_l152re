unit license;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, data_unit;

type

  { TForm5 }

  TForm5 = class(TForm)
    BitBtn1: TBitBtn;
    C_AGREE: TCheckBox;
    StaticText1: TStaticText;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject);
  private

  public

  end;

var
  Form5: TForm5;

implementation

{$R *.lfm}

{ TForm5 }



procedure TForm5.BitBtn1Click(Sender: TObject);
begin
  close;
end;

procedure TForm5.FormClose(Sender: TObject);
begin
  if(C_AGREE.Checked)then v_license_agreed := TRUE
  else v_license_agreed := FALSE;
end;



end.

