unit warning_generating_code_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, data_unit;

type

  { TForm27 }

  TForm27 = class(TForm)
    b_cancel: TBitBtn;
    b_gen: TBitBtn;
    Image1: TImage;
    Label1: TLabel;
    StaticText1: TStaticText;
    procedure b_cancelClick(Sender: TObject);
    procedure b_genClick(Sender: TObject);
  private

  public

  end;

var
  Form27: TForm27;

implementation

{$R *.lfm}

{ TForm27 }

procedure TForm27.b_genClick(Sender: TObject);
begin
  v_generating_code_is_ok := TRUE;
  close;
end;

procedure TForm27.b_cancelClick(Sender: TObject);
begin
  close;
end;

end.

