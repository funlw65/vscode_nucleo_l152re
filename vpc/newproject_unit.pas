unit newproject_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, EditBtn, FileCtrl, ExtCtrls, utils_unit, data_unit,
  driver_folders_unit, code_unit;

type

  { TForm6 }

  TForm6 = class(TForm)
    B_NEWPRJ_OK: TBitBtn;
    CB_COPYRIGHT: TComboBox;
    CB_OPTIMIZATION: TComboBox;
    C_EFP: TCheckBox;
    C_PRINTF_EFP: TCheckBox;
    C_DEBUG: TCheckBox;
    D_copyrightYear: TDateEdit;
    E_AUTH_NAME: TEdit;
    E_PRJNAME: TEdit;
    Image1: TImage;
    Label2: TLabel;
    LST_PrjList: TFileListBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    l_newprj_mess: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure B_NEWPRJ_OKClick(Sender: TObject);
    procedure CB_OPTIMIZATIONSelect(Sender: TObject);
    procedure C_DEBUGClick(Sender: TObject);
    procedure C_EFPClick(Sender: TObject);
    procedure C_PRINTF_EFPClick(Sender: TObject);
    procedure E_PRJNAMEEditingDone(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form6: TForm6;

implementation

{$R *.lfm}

{ TForm6 }


procedure TForm6.CB_OPTIMIZATIONSelect(Sender: TObject);
begin
  if (CB_OPTIMIZATION.ItemIndex = 0) then C_DEBUG.Checked:= TRUE
  else C_DEBUG.Checked:= FALSE;
end;

procedure TForm6.B_NEWPRJ_OKClick(Sender: TObject);
begin
  if(v_prj_ok) then begin
    v_prj_name := E_PRJNAME.Text;
    v_year :=  D_copyrightYear.Text;
    v_author := E_AUTH_NAME.Text;
    v_debug := C_DEBUG.Checked;
    v_efp := C_EFP.Checked;
    v_printf_efp := C_PRINTF_EFP.Checked;
    v_optimization_index := CB_OPTIMIZATION.ItemIndex;
    v_copyright_idx := CB_COPYRIGHT.ItemIndex;
    name_all_the_pins;
    prepare_labels_for_xml;
    create_project;
    clean_labels_from_xml;
    v_project_status := 'CREATED';
    Close;
  end
  else l_newprj_mess.Caption := 'Error! The project name can not be created! Try a different name.';
end;

procedure TForm6.C_DEBUGClick(Sender: TObject);
begin
  if C_DEBUG.Checked then CB_OPTIMIZATION.ItemIndex := 0;
end;

procedure TForm6.C_EFPClick(Sender: TObject);
begin
  if not C_EFP.Checked then C_PRINTF_EFP.Checked := FALSE;
end;

procedure TForm6.C_PRINTF_EFPClick(Sender: TObject);
begin
  if C_PRINTF_EFP.Checked then C_EFP.Checked := TRUE;
end;

procedure TForm6.E_PRJNAMEEditingDone(Sender: TObject);
begin
  E_PRJNAME.Text := StrNoWhite(E_PRJNAME.Text);
  {$I-}
  if(E_PRJNAME.Text <> '')then begin
    //
    chdir(getenvironmentvariable('HOME'));
    chdir(v_workspace);
    if(not DirectoryExists(E_PRJNAME.Text)) then begin
      l_newprj_mess.Caption := 'The name can be used as project name and folder! Press OK button.';
      v_prj_ok := TRUE;
    end
    else begin
      l_newprj_mess.Caption := 'Error! The name is in use, try another.';
      v_prj_ok := FALSE;
    end;
  end
  else begin
    l_newprj_mess.Caption := 'Error! The project name field can not be empty!';
    v_prj_ok := FALSE;
  end;
  {$I+}
end;

procedure TForm6.FormShow(Sender: TObject);
begin
  {v_prj_ok := FALSE;}
  C_DEBUG.Checked := v_debug;
  C_EFP.Checked := v_efp;
  C_PRINTF_EFP.Checked := v_printf_efp;
  CB_OPTIMIZATION.ItemIndex := v_optimization_index;
  CB_COPYRIGHT.ItemIndex := v_copyright_idx;
  {populate the project list}
  LST_PrjList.Directory := getenvironmentvariable('HOME') + '/' + v_workspace;
  E_PRJNAME.Text := '';
  l_newprj_mess.Caption := 'Type a project name in the project field that must be unique.';
end;

end.

