unit prjsettings_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  EditBtn, Buttons, ExtCtrls, data_unit, code_unit;

type

  { TForm22 }

  TForm22 = class(TForm)
    B_NEWPRJ_OK: TBitBtn;
    CB_COPYRIGHT: TComboBox;
    cb_exti0_priority: TComboBox;
    cb_exti1_priority: TComboBox;
    cb_exti2_priority: TComboBox;
    cb_exti3_priority: TComboBox;
    cb_exti4_priority: TComboBox;
    cb_exti9_priority: TComboBox;
    cb_exti15_priority: TComboBox;
    cb_exti0_subpriority: TComboBox;
    cb_exti1_subpriority: TComboBox;
    cb_exti2_subpriority: TComboBox;
    cb_exti3_subpriority: TComboBox;
    cb_exti4_subpriority: TComboBox;
    cb_exti9_subpriority: TComboBox;
    cb_exti15_subpriority: TComboBox;
    CB_OPTIMIZATION: TComboBox;
    cb_nvic_priority_group: TComboBox;
    C_DEBUG: TCheckBox;
    C_EFP: TCheckBox;
    C_PRINTF_EFP: TCheckBox;
    D_copyrightYear: TDateEdit;
    E_AUTH_NAME: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    L_priority_message: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    priority: TLabel;
    priority1: TLabel;
    priority10: TLabel;
    priority11: TLabel;
    priority12: TLabel;
    priority13: TLabel;
    priority2: TLabel;
    priority3: TLabel;
    priority4: TLabel;
    priority5: TLabel;
    priority6: TLabel;
    priority7: TLabel;
    priority8: TLabel;
    priority9: TLabel;
    procedure B_NEWPRJ_OKClick(Sender: TObject);
    procedure cb_nvic_priority_groupSelect(Sender: TObject);
    procedure CB_OPTIMIZATIONSelect(Sender: TObject);
    procedure C_DEBUGChange(Sender: TObject);
    procedure C_EFPChange(Sender: TObject);
    procedure C_PRINTF_EFPChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form22: TForm22;

implementation

{$R *.lfm}

{ TForm22 }

procedure TForm22.B_NEWPRJ_OKClick(Sender: TObject);
begin
  v_year :=  D_copyrightYear.Text;
  v_author := E_AUTH_NAME.Text;
  v_debug := C_DEBUG.Checked;
  v_optimization_index := CB_OPTIMIZATION.ItemIndex;
  v_efp := C_EFP.Checked;
  v_printf_efp := C_PRINTF_EFP.Checked;
  v_copyright_idx := CB_COPYRIGHT.ItemIndex;
  v_group_priority := cb_nvic_priority_group.ItemIndex;
  v_exti0_priority := cb_exti0_priority.ItemIndex;
  v_exti1_priority := cb_exti1_priority.ItemIndex;
  v_exti2_priority := cb_exti2_priority.ItemIndex;
  v_exti3_priority := cb_exti3_priority.ItemIndex;
  v_exti4_priority := cb_exti4_priority.ItemIndex;
  v_exti9_5_priority := cb_exti9_priority.ItemIndex;
  v_exti15_10_priority := cb_exti15_priority.ItemIndex;
  v_exti0_subpriority := cb_exti0_subpriority.ItemIndex;
  v_exti1_subpriority := cb_exti1_subpriority.ItemIndex;
  v_exti2_subpriority := cb_exti2_subpriority.ItemIndex;
  v_exti3_subpriority := cb_exti3_subpriority.ItemIndex;
  v_exti4_subpriority := cb_exti4_subpriority.ItemIndex;
  v_exti9_5_subpriority := cb_exti9_subpriority.ItemIndex;
  v_exti15_10_subpriority := cb_exti15_subpriority.ItemIndex;
  validate_exti_priorities;
  close;
end;

procedure TForm22.cb_nvic_priority_groupSelect(Sender: TObject);
begin
  case cb_nvic_priority_group.ItemIndex of
    0:
      begin
        v_group_priority := 0;
        set_priority_message;
        L_priority_message.Caption := v_priority_message;
      end;
    1:
      begin
        v_group_priority := 1;
        set_priority_message;
        L_priority_message.Caption := v_priority_message;
      end;
    2:
      begin
        v_group_priority := 2;
        set_priority_message;
        L_priority_message.Caption := v_priority_message;
      end;
    3:
      begin
        v_group_priority := 3;
        set_priority_message;
        L_priority_message.Caption := v_priority_message;
      end;
    4:
      begin
        v_group_priority := 4;
        set_priority_message;
        L_priority_message.Caption := v_priority_message;
      end;
  end;
end;

procedure TForm22.CB_OPTIMIZATIONSelect(Sender: TObject);
begin
  if (CB_OPTIMIZATION.ItemIndex = 0) then C_DEBUG.Checked:= TRUE
  else C_DEBUG.Checked:= FALSE;
end;

procedure TForm22.C_DEBUGChange(Sender: TObject);
begin
  if C_DEBUG.Checked then CB_OPTIMIZATION.ItemIndex := 0;
end;

procedure TForm22.C_EFPChange(Sender: TObject);
begin
  if not C_EFP.Checked then C_PRINTF_EFP.Checked := FALSE;
end;

procedure TForm22.C_PRINTF_EFPChange(Sender: TObject);
begin
  if C_PRINTF_EFP.Checked then C_EFP.Checked := TRUE;
end;

procedure TForm22.FormShow(Sender: TObject);
begin
  D_copyrightYear.Text := v_year;
  E_AUTH_NAME.Text := v_author;
  C_DEBUG.Checked := v_debug;
  CB_OPTIMIZATION.ItemIndex := v_optimization_index;
  CB_COPYRIGHT.ItemIndex := v_copyright_idx;
  C_EFP.Checked := v_efp;
  C_PRINTF_EFP.Checked := v_printf_efp;
  cb_nvic_priority_group.ItemIndex := v_group_priority;
  validate_exti_priorities;
  cb_exti0_priority.ItemIndex:= v_exti0_priority;
  cb_exti1_priority.ItemIndex:= v_exti1_priority;
  cb_exti2_priority.ItemIndex:= v_exti2_priority;
  cb_exti3_priority.ItemIndex:= v_exti3_priority;
  cb_exti4_priority.ItemIndex:= v_exti4_priority;
  cb_exti9_priority.ItemIndex:= v_exti9_5_priority;
  cb_exti15_priority.ItemIndex:= v_exti15_10_priority;
  cb_exti0_subpriority.ItemIndex:= v_exti0_subpriority;
  cb_exti1_subpriority.ItemIndex:= v_exti1_subpriority;
  cb_exti2_subpriority.ItemIndex:= v_exti2_subpriority;
  cb_exti3_subpriority.ItemIndex:= v_exti3_subpriority;
  cb_exti4_subpriority.ItemIndex:= v_exti4_subpriority;
  cb_exti9_subpriority.ItemIndex:= v_exti9_5_subpriority;
  cb_exti15_subpriority.ItemIndex:= v_exti15_10_subpriority;

end;

end.

