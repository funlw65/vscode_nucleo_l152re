unit utils_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

function StrNoWhite(src:string):string;
function StrOnlyNumbers(src:string):string;

implementation

function StrNoWhite(src:string):string;
var n:Integer;
const white=[' ',#9,#13,#10];
begin
 result:='';
 for n:=1 to Length(src) do if not (src[n] in white) then result:=result+src[n];
end;

function StrOnlyNumbers(src:string):string;
var n:Integer;
const white=['0','1','2','3','4','5','6','7','8','9','-'];
begin
 result:='';
 for n:=1 to Length(src) do if (src[n] in white) then result:=result+src[n];
end;


end.

