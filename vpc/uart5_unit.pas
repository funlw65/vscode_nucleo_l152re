unit uart5_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons, data_unit, code_unit;

type

  { TForm16 }

  TForm16 = class(TForm)
    BitBtn1: TBitBtn;
    cb_uart5_subpriority: TComboBox;
    cb_uart5_priority: TComboBox;
    CheckBox2: TCheckBox;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    ComboBox5: TComboBox;
    ComboBox6: TComboBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label11: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    l_uart5_mess: TLabel;
    priority: TLabel;
    priority1: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form16: TForm16;

implementation

{$R *.lfm}

{ TForm16 }


procedure TForm16.BitBtn1Click(Sender: TObject);
begin
  Close;
end;


procedure TForm16.FormClose(Sender: TObject);
begin
  v_cb_baud_idx_uart5 := ComboBox1.ItemIndex;
  v_cb_wordlng_idx_uart5 := ComboBox2.ItemIndex;
  v_cb_parity_idx_uart5 := ComboBox3.ItemIndex;
  v_cb_stop_idx_uart5 := ComboBox4.ItemIndex;
  v_cb_datadir_idx_uart5 := ComboBox5.ItemIndex;
  v_cb_sampling_idx_uart5 := ComboBox6.ItemIndex;
  v_c_gb_int_uart5 := CheckBox2.Checked;
  v_uart5_int_priority:=validate_priority_value(cb_uart5_priority.ItemIndex);
  v_uart5_int_subpriority:=validate_subpriority_value(cb_uart5_subpriority.ItemIndex);

end;

procedure TForm16.FormShow(Sender: TObject);
begin
  ComboBox1.ItemIndex := v_cb_baud_idx_uart5;
  ComboBox2.ItemIndex := v_cb_wordlng_idx_uart5;
  ComboBox3.ItemIndex := v_cb_parity_idx_uart5;
  ComboBox4.ItemIndex := v_cb_stop_idx_uart5;
  ComboBox5.ItemIndex := v_cb_datadir_idx_uart5;
  ComboBox6.ItemIndex := v_cb_sampling_idx_uart5;
  CheckBox2.Checked := v_c_gb_int_uart5;
  cb_uart5_priority.ItemIndex:=validate_priority_value(v_uart5_int_priority);
  cb_uart5_subpriority.ItemIndex:=validate_subpriority_value(v_uart5_int_subpriority);

  if v_c_uart5 then begin
    GroupBox1.Enabled := TRUE;
    l_uart5_mess.Caption:='UART5 peripheral activated.';
  end
  else begin
    GroupBox1.Enabled := FALSE;
    l_uart5_mess.Caption:='Set PC12-PD2 pair as UART5 in Nucleo or LQFP64 window.';
  end;
end;

end.

