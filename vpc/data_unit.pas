unit data_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  v_spl_driver = 'STM32L1xx_StdPeriph_Lib_V1.3.1';
  v_ld_file = 'STM32L152RE.ld';
  v_startup_file = 'STM32L152RE.s';
  v_vsc_tasks_file = 'tasks.json';
  v_vsc_c_prop_file = 'c_cpp_properties.json';
  v_vsc_settings_file = 'settings.json';
  v_vsc_launch_file = 'launch.json';
  //v_vsc_arduino_file = 'arduino.json';
  v_designer_folder = 'vpc';
  v_app_version = '3.8.1.1071 Beta';
  v_c_usart2 = TRUE;
  {clock}
  v_hsi_value = 16; {MHz}
  w_hsi = v_hsi_value * 1000000;
  w_lse = 32768;
  w_lsi = 37000;


var

_fsearch: TSearchRec;
s : AnsiString;
F, G, H, J: Text;
sf:string;
v_file_version : integer;

v_current_lqfp_pad : string; 

v_r_oslin, v_r_osfree : boolean;
//v_application_force_quit : boolean;
v_use_adc, v_use_adin : boolean;

v_generating_code_is_ok : boolean;

{============}
{PROJECT DATA}
{------------}
v_workspace_ok, v_prj_ok, v_toolchain_ok, v_toolchain_subf_ok, v_driver_ok, v_app_ok: boolean;
v_project_status : string;
v_license_agreed : boolean;
v_workspace : string;
v_prj_name : string;
v_toolchain_folder: string;
v_user_lib_folder: string; {relative to workspace}
v_debug : boolean;
v_optimization_index : byte;
v_group_priority : byte;
v_priority_message : string;
v_exti0_priority,
v_exti0_subpriority:byte;
v_exti1_priority,
v_exti1_subpriority:byte;
v_exti2_priority,
v_exti2_subpriority:byte;
v_exti3_priority,
v_exti3_subpriority:byte;
v_exti4_priority,
v_exti4_subpriority:byte;
v_exti9_5_priority,
v_exti9_5_subpriority:byte;
v_exti15_10_priority,
v_exti15_10_subpriority:byte;
v_year, v_author: string;
v_copyright_idx : byte;
v_efp : boolean;
v_printf_efp : boolean;
v_arduino_path : string;



{==========================
 |||| EXTI interrupts |||||
 --------------------------}
v_exti0_used,
v_exti1_used,
v_exti2_used,
v_exti3_used,
v_exti4_used,
v_exti9_5_used,
v_exti15_10_used : boolean;

{========================
       PORTS USED
 ------------------------}
v_porta_used,
v_portb_used,
v_portc_used,
v_portd_used,
v_porth_used: boolean;

{===========================
 BUFFER DATA FOR GPIO TUNING
 ---------------------------}
b_pin_id,
b_pin_func : string;
b_pin_label : string;
b_pin_o_lvl_idx,
b_pin_o_mode_idx,
b_pin_i_mode_idx,
b_pin_a_mode_idx,
b_pin_i2c_mode_idx,
b_pin_spi_mode_idx,
b_pin_int_mode_idx,
b_pin_pull_updown_idx,
b_pin_speed_idx : byte;
b_pin_name : string;

{==============
 USB PERIPHERAL
 --------------}
v_c_usb: boolean;
v_cb_usb_maxpacksize_idx,
v_cb_usb_lowpower_idx,
v_cb_usb_charging_idx: byte;
v_c_usb_hint,
v_c_usb_lint: boolean;

{===============
 I2C1 PERIPHERAL
 ---------------}
v_c_i2c1, v_c_i2c1_a, v_c_i2c1_b  : boolean;
v_cb_i2c1_speedmode_idx,
v_cb_i2c1_clockspeed_idx,
v_cb_i2c1_fm_dc_idx : byte;
v_cb_i2c1_s_cns_idx,
v_cb_i2c1_s_pal_idx,
v_cb_i2c1_s_daa_idx : byte;
v_se_i2c1_s_psa : integer;
v_cb_i2c1_s_gcad_idx : byte;
v_c_i2c1_event_int,
v_c_i2c1_error_int : boolean;
v_i2c1_int_priority,
v_i2c1_int_subpriority:byte;


{===============
 I2C2 PERIPHERAL
 ---------------}
v_c_i2c2 : boolean;
v_cb_i2c2_speedmode_idx,
v_cb_i2c2_clockspeed_idx,
v_cb_i2c2_fm_dc_idx : byte;
v_cb_i2c2_s_cns_idx,
v_cb_i2c2_s_pal_idx,
v_cb_i2c2_s_daa_idx : byte;
v_se_i2c2_s_psa : integer;
v_cb_i2c2_s_gcad_idx : byte;
v_c_i2c2_event_int,
v_c_i2c2_error_int : boolean;
v_i2c2_int_priority,
v_i2c2_int_subpriority:byte;

{===================
 LCD4 pinout setting
 -------------------}
v_c_lcd4 : boolean;
v_lcd4_defaults : boolean;
v_r1_lcd4: boolean;
v_r2_lcd4: boolean;
v_cb_idx_lcd4: byte;
v_cb_idx_lcd4_chipset: byte;

{==================
 SPI1 CONFIGURATION
 ------------------}
v_c_spi1 : boolean;
v_cb_direction_idx_spi1,
v_cb_mode_idx_spi1,
v_cb_datasize_idx_spi1,
v_cb_cpol_idx_spi1,
v_cb_cpha_idx_spi1,
v_cb_nss_idx_spi1, 
v_cb_prescaler_idx_spi1,
v_cb_firstbit_idx_spi1:byte;
v_cb_crcpoly_idx_spi1:integer;
v_c_gb_int_spi1:boolean;
v_spi1_int_priority,
v_spi1_int_subpriority:byte;


{==================
 SPI2 CONFIGURATION
 ------------------}
v_c_spi2 : boolean;
v_cb_direction_idx_spi2,
v_cb_mode_idx_spi2,
v_cb_datasize_idx_spi2,
v_cb_cpol_idx_spi2,
v_cb_cpha_idx_spi2,
v_cb_nss_idx_spi2, 
v_cb_prescaler_idx_spi2,
v_cb_firstbit_idx_spi2:byte;
v_cb_crcpoly_idx_spi2:integer;
v_c_gb_int_spi2:boolean;
v_spi2_int_priority,
v_spi2_int_subpriority:byte;

{==================
 SPI3 CONFIGURATION
 ------------------}
v_c_spi3 : boolean;
v_cb_direction_idx_spi3,
v_cb_mode_idx_spi3,
v_cb_datasize_idx_spi3,
v_cb_cpol_idx_spi3,
v_cb_cpha_idx_spi3,
v_cb_nss_idx_spi3, 
v_cb_prescaler_idx_spi3,
v_cb_firstbit_idx_spi3:byte;
v_cb_crcpoly_idx_spi3:integer;
v_c_gb_int_spi3:boolean;
v_spi3_int_priority,
v_spi3_int_subpriority:byte;

{===================
 UART4 CONFIGURATION
 -------------------}
v_c_uart4 : boolean;
v_cb_baud_idx_uart4,
v_cb_wordlng_idx_uart4,
v_cb_parity_idx_uart4,
v_cb_stop_idx_uart4,
v_cb_datadir_idx_uart4,
v_cb_sampling_idx_uart4: byte;
v_c_gb_int_uart4: boolean;
v_uart4_int_priority,
v_uart4_int_subpriority:byte;


{===================
 UART5 CONFIGURATION
 -------------------}
v_c_uart5 : boolean;
v_cb_baud_idx_uart5,
v_cb_wordlng_idx_uart5,
v_cb_parity_idx_uart5,
v_cb_stop_idx_uart5,
v_cb_datadir_idx_uart5,
v_cb_sampling_idx_uart5: byte;
v_c_gb_int_uart5: boolean;
v_uart5_int_priority,
v_uart5_int_subpriority:byte;

{====================
 USART2 CONFIGURATION
 --------------------}
v_cb_baud_idx_usart2,
v_cb_wordlng_idx_usart2,
v_cb_parity_idx_usart2,
v_cb_stop_idx_usart2,
v_cb_datadir_idx_usart2,
v_cb_sampling_idx_usart2: byte;
v_c_gb_int_usart2: boolean;
v_usart2_int_priority,
v_usart2_int_subpriority:byte;

{====================
 USART1 CONFIGURATION
 --------------------}
v_c_usart1 : boolean;
v_cb_baud_idx_usart1,
v_cb_wordlng_idx_usart1,
v_cb_parity_idx_usart1,
v_cb_stop_idx_usart1,
v_cb_datadir_idx_usart1,
v_cb_sampling_idx_usart1: byte;
v_c_gb_int_usart1: boolean;
v_usart1_int_priority,
v_usart1_int_subpriority:byte;

{===============
 ADC1 PERIPHERAL
 ---------------}
v_adc1_res_idx,
v_adc1_scan_idx,
v_adc1_continuous_idx,
v_adc1_tce_idx,
v_adc1_tc_idx,
v_adc1_align_idx,
v_adc1_prescaler_idx: byte;
v_adc1_nrconv_idx: integer;
v_c_gb_int_adc1: boolean;
v_adc1_int_priority,
v_adc1_int_subpriority:byte;


{================}
{MORPHO CONNECTOR}
{----------------}
{CN7 connector, the non-paired pins row - 1,3,5, etc.}
{PC10}
v_cb_pc10_idx: byte;
v_e_pc10_txt : string;
v_pc10_id,
v_pc10_func : string;
v_pc10_o_lvl_idx,
v_pc10_o_mode_idx,
v_pc10_i_mode_idx,
v_pc10_a_mode_idx,
v_pc10_i2c_mode_idx,
v_pc10_spi_mode_idx,
v_pc10_int_mode_idx,
v_pc10_pull_updown_idx,
v_pc10_speed_idx : byte;
v_pc10_name : string;

{PC12}
v_cb_pc12_idx: byte;
v_e_pc12_txt : string;
v_pc12_id,
v_pc12_func : string;
v_pc12_o_lvl_idx,
v_pc12_o_mode_idx,
v_pc12_i_mode_idx,
v_pc12_a_mode_idx,
v_pc12_i2c_mode_idx,
v_pc12_spi_mode_idx,
v_pc12_int_mode_idx,
v_pc12_pull_updown_idx,
v_pc12_speed_idx : byte;
v_pc12_name : string;


{PA15}
v_cb_pa15_idx: byte;
v_e_pa15_txt : string;
v_pa15_id,
v_pa15_func : string;
v_pa15_o_lvl_idx,
v_pa15_o_mode_idx,
v_pa15_i_mode_idx,
v_pa15_a_mode_idx,
v_pa15_i2c_mode_idx,
v_pa15_spi_mode_idx,
v_pa15_int_mode_idx,
v_pa15_pull_updown_idx,
v_pa15_speed_idx : byte;
v_pa15_name : string;

{PB7}
v_cb_pb7_idx: byte;
v_e_pb7_txt : string;
v_pb7_id,
v_pb7_func : string;
v_pb7_o_lvl_idx,
v_pb7_o_mode_idx,
v_pb7_i_mode_idx,
v_pb7_a_mode_idx,
v_pb7_i2c_mode_idx,
v_pb7_spi_mode_idx,
v_pb7_int_mode_idx,
v_pb7_pull_updown_idx,
v_pb7_speed_idx : byte;
v_pb7_name : string;

{PC13} {- the user button on Nucleo}
v_cb_pc13_idx: byte;
v_e_pc13_txt : string;
v_pc13_id,
v_pc13_func : string;
v_pc13_o_lvl_idx,
v_pc13_o_mode_idx,
v_pc13_i_mode_idx,
v_pc13_a_mode_idx,
v_pc13_i2c_mode_idx,
v_pc13_spi_mode_idx,
v_pc13_int_mode_idx,
v_pc13_pull_updown_idx,
v_pc13_speed_idx : byte;
v_pc13_name : string;

{PC2}
v_cb_pc2_idx: byte;
v_e_pc2_txt : string;
v_pc2_id,
v_pc2_func : string;
v_pc2_o_lvl_idx,
v_pc2_o_mode_idx,
v_pc2_i_mode_idx,
v_pc2_a_mode_idx,
v_pc2_i2c_mode_idx,
v_pc2_spi_mode_idx,
v_pc2_int_mode_idx,
v_pc2_pull_updown_idx,
v_pc2_speed_idx : byte;
v_pc2_name : string;

{PC3}
v_cb_pc3_idx: byte;
v_e_pc3_txt : string;
v_pc3_id,
v_pc3_func : string;
v_pc3_o_lvl_idx,
v_pc3_o_mode_idx,
v_pc3_i_mode_idx,
v_pc3_a_mode_idx,
v_pc3_i2c_mode_idx,
v_pc3_spi_mode_idx,
v_pc3_int_mode_idx,
v_pc3_pull_updown_idx,
v_pc3_speed_idx : byte;
v_pc3_name : string;

{CN7 connector, the paired pins row - 2,4,6, etc.}
{PC11}
v_cb_pc11_idx: byte;
v_e_pc11_txt : string;
v_pc11_id,
v_pc11_func : string;
v_pc11_o_lvl_idx,
v_pc11_o_mode_idx,
v_pc11_i_mode_idx,
v_pc11_a_mode_idx,
v_pc11_i2c_mode_idx,
v_pc11_spi_mode_idx,
v_pc11_int_mode_idx,
v_pc11_pull_updown_idx,
v_pc11_speed_idx : byte;
v_pc11_name : string;

{PD2}
v_cb_pd2_idx: byte;
v_e_pd2_txt : string;
v_pd2_id,
v_pd2_func : string;
v_pd2_o_lvl_idx,
v_pd2_o_mode_idx,
v_pd2_i_mode_idx,
v_pd2_a_mode_idx,
v_pd2_i2c_mode_idx,
v_pd2_spi_mode_idx,
v_pd2_int_mode_idx,
v_pd2_pull_updown_idx,
v_pd2_speed_idx : byte;
v_pd2_name : string;

{PA0}
v_cb_pa0_idx: byte;
v_e_pa0_txt : string;
v_pa0_id,
v_pa0_func : string;
v_pa0_o_lvl_idx,
v_pa0_o_mode_idx,
v_pa0_i_mode_idx,
v_pa0_a_mode_idx,
v_pa0_i2c_mode_idx,
v_pa0_spi_mode_idx,
v_pa0_int_mode_idx,
v_pa0_pull_updown_idx,
v_pa0_speed_idx : byte;
v_pa0_name : string;

{PA1}
v_cb_pa1_idx: byte;
v_e_pa1_txt : string;
v_pa1_id,
v_pa1_func : string;
v_pa1_o_lvl_idx,
v_pa1_o_mode_idx,
v_pa1_i_mode_idx,
v_pa1_a_mode_idx,
v_pa1_i2c_mode_idx,
v_pa1_spi_mode_idx,
v_pa1_int_mode_idx,
v_pa1_pull_updown_idx,
v_pa1_speed_idx : byte;
v_pa1_name : string;

{PA4}
v_cb_pa4_idx: byte;
v_e_pa4_txt : string;
v_pa4_id,
v_pa4_func : string;
v_pa4_o_lvl_idx,
v_pa4_o_mode_idx,
v_pa4_i_mode_idx,
v_pa4_a_mode_idx,
v_pa4_i2c_mode_idx,
v_pa4_spi_mode_idx,
v_pa4_int_mode_idx,
v_pa4_pull_updown_idx,
v_pa4_speed_idx : byte;
v_pa4_name : string;

{PB0}
v_cb_pb0_idx: byte;
v_e_pb0_txt : string;
v_pb0_id,
v_pb0_func : string;
v_pb0_o_lvl_idx,
v_pb0_o_mode_idx,
v_pb0_i_mode_idx,
v_pb0_a_mode_idx,
v_pb0_i2c_mode_idx,
v_pb0_spi_mode_idx,
v_pb0_int_mode_idx,
v_pb0_pull_updown_idx,
v_pb0_speed_idx : byte;
v_pb0_name : string;

{PC1}
v_cb_pc1_idx: byte;
v_e_pc1_txt : string;
v_pc1_id,
v_pc1_func : string;
v_pc1_o_lvl_idx,
v_pc1_o_mode_idx,
v_pc1_i_mode_idx,
v_pc1_a_mode_idx,
v_pc1_i2c_mode_idx,
v_pc1_spi_mode_idx,
v_pc1_int_mode_idx,
v_pc1_pull_updown_idx,
v_pc1_speed_idx : byte;
v_pc1_name : string;

{PC0}
v_cb_pc0_idx: byte;
v_e_pc0_txt : string;
v_pc0_id,
v_pc0_func : string;
v_pc0_o_lvl_idx,
v_pc0_o_mode_idx,
v_pc0_i_mode_idx,
v_pc0_a_mode_idx,
v_pc0_i2c_mode_idx,
v_pc0_spi_mode_idx,
v_pc0_int_mode_idx,
v_pc0_pull_updown_idx,
v_pc0_speed_idx : byte;
v_pc0_name : string;

{CN10 connector, the non-paired pins row - 1,3,5, etc.}
{PC9}
v_cb_pc9_idx: byte;
v_e_pc9_txt : string;
v_pc9_id,
v_pc9_func : string;
v_pc9_o_lvl_idx,
v_pc9_o_mode_idx,
v_pc9_i_mode_idx,
v_pc9_a_mode_idx,
v_pc9_i2c_mode_idx,
v_pc9_spi_mode_idx,
v_pc9_int_mode_idx,
v_pc9_pull_updown_idx,
v_pc9_speed_idx : byte;
v_pc9_name : string;

{PB8}
v_cb_pb8_idx: byte;
v_e_pb8_txt : string;
v_pb8_id,
v_pb8_func : string;
v_pb8_o_lvl_idx,
v_pb8_o_mode_idx,
v_pb8_i_mode_idx,
v_pb8_a_mode_idx,
v_pb8_i2c_mode_idx,
v_pb8_spi_mode_idx,
v_pb8_int_mode_idx,
v_pb8_pull_updown_idx,
v_pb8_speed_idx : byte;
v_pb8_name : string;

{PB9}
v_cb_pb9_idx: byte;
v_e_pb9_txt : string;
v_pb9_id,
v_pb9_func : string;
v_pb9_o_lvl_idx,
v_pb9_o_mode_idx,
v_pb9_i_mode_idx,
v_pb9_a_mode_idx,
v_pb9_i2c_mode_idx,
v_pb9_spi_mode_idx,
v_pb9_int_mode_idx,
v_pb9_pull_updown_idx,
v_pb9_speed_idx : byte;
v_pb9_name : string;

{PA5} {- the LD2 user LED}
v_cb_pa5_idx: byte;
v_e_pa5_txt : string;
v_pa5_id,
v_pa5_func : string;
v_pa5_o_lvl_idx,
v_pa5_o_mode_idx,
v_pa5_i_mode_idx,
v_pa5_a_mode_idx,
v_pa5_i2c_mode_idx,
v_pa5_spi_mode_idx,
v_pa5_int_mode_idx,
v_pa5_pull_updown_idx,
v_pa5_speed_idx : byte;
v_pa5_name : string;

{PA6}
v_cb_pa6_idx: byte;
v_e_pa6_txt : string;
v_pa6_id,
v_pa6_func : string;
v_pa6_o_lvl_idx,
v_pa6_o_mode_idx,
v_pa6_i_mode_idx,
v_pa6_a_mode_idx,
v_pa6_i2c_mode_idx,
v_pa6_spi_mode_idx,
v_pa6_int_mode_idx,
v_pa6_pull_updown_idx,
v_pa6_speed_idx : byte;
v_pa6_name : string;

{PA7}
v_cb_pa7_idx: byte;
v_e_pa7_txt : string;
v_pa7_id,
v_pa7_func : string;
v_pa7_o_lvl_idx,
v_pa7_o_mode_idx,
v_pa7_i_mode_idx,
v_pa7_a_mode_idx,
v_pa7_i2c_mode_idx,
v_pa7_spi_mode_idx,
v_pa7_int_mode_idx,
v_pa7_pull_updown_idx,
v_pa7_speed_idx : byte;
v_pa7_name : string;

{PB6}
v_cb_pb6_idx: byte;
v_e_pb6_txt : string;
v_pb6_id,
v_pb6_func : string;
v_pb6_o_lvl_idx,
v_pb6_o_mode_idx,
v_pb6_i_mode_idx,
v_pb6_a_mode_idx,
v_pb6_i2c_mode_idx,
v_pb6_spi_mode_idx,
v_pb6_int_mode_idx,
v_pb6_pull_updown_idx,
v_pb6_speed_idx : byte;
v_pb6_name : string;

{PC7}
v_cb_pc7_idx: byte;
v_e_pc7_txt : string;
v_pc7_id,
v_pc7_func : string;
v_pc7_o_lvl_idx,
v_pc7_o_mode_idx,
v_pc7_i_mode_idx,
v_pc7_a_mode_idx,
v_pc7_i2c_mode_idx,
v_pc7_spi_mode_idx,
v_pc7_int_mode_idx,
v_pc7_pull_updown_idx,
v_pc7_speed_idx : byte;
v_pc7_name : string;

{PA9}
v_cb_pa9_idx: byte;
v_e_pa9_txt : string;
v_pa9_id,
v_pa9_func : string;
v_pa9_o_lvl_idx,
v_pa9_o_mode_idx,
v_pa9_i_mode_idx,
v_pa9_a_mode_idx,
v_pa9_i2c_mode_idx,
v_pa9_spi_mode_idx,
v_pa9_int_mode_idx,
v_pa9_pull_updown_idx,
v_pa9_speed_idx : byte;
v_pa9_name : string;

{PA8}
v_cb_pa8_idx: byte;
v_e_pa8_txt : string;
v_pa8_id,
v_pa8_func : string;
v_pa8_o_lvl_idx,
v_pa8_o_mode_idx,
v_pa8_i_mode_idx,
v_pa8_a_mode_idx,
v_pa8_i2c_mode_idx,
v_pa8_spi_mode_idx,
v_pa8_int_mode_idx,
v_pa8_pull_updown_idx,
v_pa8_speed_idx : byte;
v_pa8_name : string;

{PB10}
v_cb_pb10_idx: byte;
v_e_pb10_txt : string;
v_pb10_id,
v_pb10_func : string;
v_pb10_o_lvl_idx,
v_pb10_o_mode_idx,
v_pb10_i_mode_idx,
v_pb10_a_mode_idx,
v_pb10_i2c_mode_idx,
v_pb10_spi_mode_idx,
v_pb10_int_mode_idx,
v_pb10_pull_updown_idx,
v_pb10_speed_idx : byte;
v_pb10_name : string;

{PB4}
v_cb_pb4_idx: byte;
v_e_pb4_txt : string;
v_pb4_id,
v_pb4_func : string;
v_pb4_o_lvl_idx,
v_pb4_o_mode_idx,
v_pb4_i_mode_idx,
v_pb4_a_mode_idx,
v_pb4_i2c_mode_idx,
v_pb4_spi_mode_idx,
v_pb4_int_mode_idx,
v_pb4_pull_updown_idx,
v_pb4_speed_idx : byte;
v_pb4_name : string;

{PB5}
v_cb_pb5_idx: byte;
v_e_pb5_txt : string;
v_pb5_id,
v_pb5_func : string;
v_pb5_o_lvl_idx,
v_pb5_o_mode_idx,
v_pb5_i_mode_idx,
v_pb5_a_mode_idx,
v_pb5_i2c_mode_idx,
v_pb5_spi_mode_idx,
v_pb5_int_mode_idx,
v_pb5_pull_updown_idx,
v_pb5_speed_idx : byte;
v_pb5_name : string;

{PB3}
v_cb_pb3_idx: byte;
v_e_pb3_txt : string;
v_pb3_id,
v_pb3_func : string;
v_pb3_o_lvl_idx,
v_pb3_o_mode_idx,
v_pb3_i_mode_idx,
v_pb3_a_mode_idx,
v_pb3_i2c_mode_idx,
v_pb3_spi_mode_idx,
v_pb3_int_mode_idx,
v_pb3_pull_updown_idx,
v_pb3_speed_idx : byte;
v_pb3_name : string;

{PA10}
v_cb_pa10_idx: byte;
v_e_pa10_txt : string;
v_pa10_id,
v_pa10_func : string;
v_pa10_o_lvl_idx,
v_pa10_o_mode_idx,
v_pa10_i_mode_idx,
v_pa10_a_mode_idx,
v_pa10_i2c_mode_idx,
v_pa10_spi_mode_idx,
v_pa10_int_mode_idx,
v_pa10_pull_updown_idx,
v_pa10_speed_idx : byte;
v_pa10_name : string;

{PA2}
v_cb_pa2_idx: byte;
v_e_pa2_txt : string;
v_pa2_id,
v_pa2_func : string;
v_pa2_o_lvl_idx,
v_pa2_o_mode_idx,
v_pa2_i_mode_idx,
v_pa2_a_mode_idx,
v_pa2_i2c_mode_idx,
v_pa2_spi_mode_idx,
v_pa2_int_mode_idx,
v_pa2_pull_updown_idx,
v_pa2_speed_idx : byte;
v_pa2_name : string;

{PA3}
v_cb_pa3_idx: byte;
v_e_pa3_txt : string;
v_pa3_id,
v_pa3_func : string;
v_pa3_o_lvl_idx,
v_pa3_o_mode_idx,
v_pa3_i_mode_idx,
v_pa3_a_mode_idx,
v_pa3_i2c_mode_idx,
v_pa3_spi_mode_idx,
v_pa3_int_mode_idx,
v_pa3_pull_updown_idx,
v_pa3_speed_idx : byte;
v_pa3_name : string;

{CN10 connector, the paired pins row - 2,4,6, etc.}
{PC8}
v_cb_pc8_idx: byte;
v_e_pc8_txt : string;
v_pc8_id,
v_pc8_func : string;
v_pc8_o_lvl_idx,
v_pc8_o_mode_idx,
v_pc8_i_mode_idx,
v_pc8_a_mode_idx,
v_pc8_i2c_mode_idx,
v_pc8_spi_mode_idx,
v_pc8_int_mode_idx,
v_pc8_pull_updown_idx,
v_pc8_speed_idx : byte;
v_pc8_name : string;

{PC6}
v_cb_pc6_idx: byte;
v_e_pc6_txt : string;
v_pc6_id,
v_pc6_func : string;
v_pc6_o_lvl_idx,
v_pc6_o_mode_idx,
v_pc6_i_mode_idx,
v_pc6_a_mode_idx,
v_pc6_i2c_mode_idx,
v_pc6_spi_mode_idx,
v_pc6_int_mode_idx,
v_pc6_pull_updown_idx,
v_pc6_speed_idx : byte;
v_pc6_name : string;

{PC5}
v_cb_pc5_idx: byte;
v_e_pc5_txt : string;
v_pc5_id,
v_pc5_func : string;
v_pc5_o_lvl_idx,
v_pc5_o_mode_idx,
v_pc5_i_mode_idx,
v_pc5_a_mode_idx,
v_pc5_i2c_mode_idx,
v_pc5_spi_mode_idx,
v_pc5_int_mode_idx,
v_pc5_pull_updown_idx,
v_pc5_speed_idx : byte;
v_pc5_name : string;

{PA12}
v_cb_pa12_idx: byte;
v_e_pa12_txt : string;
v_pa12_id,
v_pa12_func : string;
v_pa12_o_lvl_idx,
v_pa12_o_mode_idx,
v_pa12_i_mode_idx,
v_pa12_a_mode_idx,
v_pa12_i2c_mode_idx,
v_pa12_spi_mode_idx,
v_pa12_int_mode_idx,
v_pa12_pull_updown_idx,
v_pa12_speed_idx : byte;
v_pa12_name : string;

{PA11}
v_cb_pa11_idx: byte;
v_e_pa11_txt : string;
v_pa11_id,
v_pa11_func : string;
v_pa11_o_lvl_idx,
v_pa11_o_mode_idx,
v_pa11_i_mode_idx,
v_pa11_a_mode_idx,
v_pa11_i2c_mode_idx,
v_pa11_spi_mode_idx,
v_pa11_int_mode_idx,
v_pa11_pull_updown_idx,
v_pa11_speed_idx : byte;
v_pa11_name : string;

{PB12}
v_cb_pb12_idx: byte;
v_e_pb12_txt : string;
v_pb12_id,
v_pb12_func : string;
v_pb12_o_lvl_idx,
v_pb12_o_mode_idx,
v_pb12_i_mode_idx,
v_pb12_a_mode_idx,
v_pb12_i2c_mode_idx,
v_pb12_spi_mode_idx,
v_pb12_int_mode_idx,
v_pb12_pull_updown_idx,
v_pb12_speed_idx : byte;
v_pb12_name : string;

{PB11}
v_cb_pb11_idx: byte;
v_e_pb11_txt : string;
v_pb11_id,
v_pb11_func : string;
v_pb11_o_lvl_idx,
v_pb11_o_mode_idx,
v_pb11_i_mode_idx,
v_pb11_a_mode_idx,
v_pb11_i2c_mode_idx,
v_pb11_spi_mode_idx,
v_pb11_int_mode_idx,
v_pb11_pull_updown_idx,
v_pb11_speed_idx : byte;
v_pb11_name : string;

{PB2}
v_cb_pb2_idx: byte;
v_e_pb2_txt : string;
v_pb2_id,
v_pb2_func : string;
v_pb2_o_lvl_idx,
v_pb2_o_mode_idx,
v_pb2_i_mode_idx,
v_pb2_a_mode_idx,
v_pb2_i2c_mode_idx,
v_pb2_spi_mode_idx,
v_pb2_int_mode_idx,
v_pb2_pull_updown_idx,
v_pb2_speed_idx : byte;
v_pb2_name : string;

{PB1}
v_cb_pb1_idx: byte;
v_e_pb1_txt : string;
v_pb1_id,
v_pb1_func : string;
v_pb1_o_lvl_idx,
v_pb1_o_mode_idx,
v_pb1_i_mode_idx,
v_pb1_a_mode_idx,
v_pb1_i2c_mode_idx,
v_pb1_spi_mode_idx,
v_pb1_int_mode_idx,
v_pb1_pull_updown_idx,
v_pb1_speed_idx : byte;
v_pb1_name : string;

{PB15}
v_cb_pb15_idx: byte;
v_e_pb15_txt : string;
v_pb15_id,
v_pb15_func : string;
v_pb15_o_lvl_idx,
v_pb15_o_mode_idx,
v_pb15_i_mode_idx,
v_pb15_a_mode_idx,
v_pb15_i2c_mode_idx,
v_pb15_spi_mode_idx,
v_pb15_int_mode_idx,
v_pb15_pull_updown_idx,
v_pb15_speed_idx : byte;
v_pb15_name : string;

{PB14}
v_cb_pb14_idx: byte;
v_e_pb14_txt : string;
v_pb14_id,
v_pb14_func : string;
v_pb14_o_lvl_idx,
v_pb14_o_mode_idx,
v_pb14_i_mode_idx,
v_pb14_a_mode_idx,
v_pb14_i2c_mode_idx,
v_pb14_spi_mode_idx,
v_pb14_int_mode_idx,
v_pb14_pull_updown_idx,
v_pb14_speed_idx : byte;
v_pb14_name : string;

{PB13}
v_cb_pb13_idx: byte;
v_e_pb13_txt : string;
v_pb13_id,
v_pb13_func : string;
v_pb13_o_lvl_idx,
v_pb13_o_mode_idx,
v_pb13_i_mode_idx,
v_pb13_a_mode_idx,
v_pb13_i2c_mode_idx,
v_pb13_spi_mode_idx,
v_pb13_int_mode_idx,
v_pb13_pull_updown_idx,
v_pb13_speed_idx : byte;
v_pb13_name : string;

{PC4}
v_cb_pc4_idx: byte;
v_e_pc4_txt : string;
v_pc4_id,
v_pc4_func : string;
v_pc4_o_lvl_idx,
v_pc4_o_mode_idx,
v_pc4_i_mode_idx,
v_pc4_a_mode_idx,
v_pc4_i2c_mode_idx,
v_pc4_spi_mode_idx,
v_pc4_int_mode_idx,
v_pc4_pull_updown_idx,
v_pc4_speed_idx : byte;
v_pc4_name : string;

{==========================}
{SYSTEM CLOCK CONFIGURATION}
{--------------------------}
v_c_rcc_mco : boolean; {Master Clock Output}
v_mco_divider_idx: integer;
v_mco_source : integer;
//v_clock_source : integer;
v_hse_clk_source_idx : integer;
//v_tick_manager : integer;
{==========================
*||| CLOCK Values       |||
*--------------------------}
v_se_hse_value : byte; {the external clock 1..32 MHz}
v_cb_msi_idx,
v_rg_vco_idx,
v_cb_pllmul_idx,
v_cb_plldiv_idx,
v_rg_clock_hub_idx,
v_cb_ahb_pr_idx,
v_cb_systick_pr_idx,
v_cb_apb1_pr_idx,
v_cb_apb2_pr_idx: byte;
v_c_en_css_hse : boolean;
//
v_cb_rcc_hse_idx,
//v_cb_rcc_lse_idx,
v_cb_mco_div_idx,
v_rg_mco_src_idx : byte;
//
v_rg_osc32_idx,
v_cb_hse_div_idx : byte;
v_c_en_css_lse : boolean;

//////////////
w_hse,
w_vco,
w_pllmul,
w_usb,
w_plldiv,
w_msi,
w_sysclk,
w_hclk,
w_tick,
w_apb1_p,
w_apb1_t,
w_apb2_p,
w_apb2_t,
w_mco,
w_hse_rtc: LongWord;
w_rtc
: LongInt;

calculation_error : boolean;
                          



{==================
 TIM6 CONFIGURATION
 ------------------}
v_tim6_check : boolean;
v_tim6_prescaler : integer;
v_tim6_period : integer;
v_tim6_event : byte;
v_tim6_global_int : boolean;
v_tim6_int_priority,
v_tim6_int_subpriority: byte;

{==================
 TIM7 CONFIGURATION
 ------------------}
v_tim7_check : boolean;
v_tim7_prescaler : integer;
v_tim7_period : integer;
v_tim7_event : byte;
v_tim7_global_int : boolean;
v_tim7_int_priority,
v_tim7_int_subpriority: byte;

{reset pin procedures}
{1,3,5,7...}{CN7}
procedure calculate;
procedure reset_pc10_pin;
procedure reset_pc12_pin;
procedure reset_pa15_pin;
procedure reset_pb7_pin;
{pc13 is connected to the user button so it won't be configurable}
{so it will be initialized at startup only}
procedure reset_pc2_pin;
procedure reset_pc3_pin;
{2,4,6,8...}{CN7}
procedure reset_pc11_pin;
procedure reset_pd2_pin;
procedure reset_pa0_pin;
procedure reset_pa1_pin;
procedure reset_pa4_pin;
procedure reset_pb0_pin;
procedure reset_pc1_pin;
procedure reset_pc0_pin;

{1,3,5,7...}{CN10}
procedure reset_pc9_pin;
procedure reset_pb8_pin;
procedure reset_pb9_pin;
procedure reset_pa5_pin;
procedure reset_pa6_pin;
procedure reset_pa7_pin;
procedure reset_pb6_pin;
procedure reset_pc7_pin;
procedure reset_pa9_pin;
procedure reset_pa8_pin;
procedure reset_pb10_pin;
procedure reset_pb4_pin;
procedure reset_pb5_pin;
procedure reset_pb3_pin;
procedure reset_pa10_pin;
{the last two pins on serial will be initialized at startup only}
{2,4,6,8...}{CN10} 
procedure reset_pc8_pin;
procedure reset_pc6_pin;
procedure reset_pc5_pin;
procedure reset_pa12_pin;
procedure reset_pa11_pin;
procedure reset_pb12_pin;
procedure reset_pb11_pin;
procedure reset_pb2_pin;
procedure reset_pb1_pin;
procedure reset_pb15_pin;
procedure reset_pb14_pin;
procedure reset_pb13_pin;
procedure reset_pc4_pin;
{}
procedure reset_i2c1_a_pins;
procedure reset_i2c1_b_pins;
procedure reset_i2c2_pins;
procedure reset_uart4_pins;
procedure reset_uart5_pins;
procedure reset_usart1_pins;
procedure reset_spi1_pins;
procedure reset_spi2_pins;
procedure reset_spi3_pins;
procedure reset_default_lcd4_pins;
{}
procedure reset_to_nucleo_hardware;

procedure clean_buffer;

implementation

procedure calculate;
var tmp: LongWord;  
begin
  //SYSTEM
  calculation_error := FALSE;
  w_hse := v_se_hse_value * 1000000;
  case v_rg_vco_idx of
    0: w_vco := w_hse;
    1: w_vco := w_hsi;
  end;
  if v_rg_clock_hub_idx = 3 then
    if ((w_vco < 2000000) or (w_vco > 24000000)) then calculation_error := TRUE; 
  case v_cb_pllmul_idx of
    0: w_pllmul := w_vco * 3;
    1: w_pllmul := w_vco * 4;
    2: w_pllmul := w_vco * 6;
    3: w_pllmul := w_vco * 8;
    4: w_pllmul := w_vco * 12;
    5: w_pllmul := w_vco * 16;
    6: w_pllmul := w_vco * 24;
    7: w_pllmul := w_vco * 32;  
  end;
  if v_rg_clock_hub_idx = 3 then
    if w_pllmul > 96000000 then calculation_error := TRUE;
  w_usb := w_pllmul div 2;
  if ((v_rg_vco_idx=0) and (v_rg_clock_hub_idx=3) and (v_c_usb)) then begin
    if w_usb <> 48000000 then calculation_error := TRUE;
  end;
  case v_cb_plldiv_idx of 
    0: w_plldiv := w_pllmul div 2;
    1: w_plldiv := w_pllmul div 3;
    2: w_plldiv := w_pllmul div 4;
  end; 
  if v_rg_clock_hub_idx = 3 then
    if ((w_plldiv < 2000000) or (w_plldiv > 32000000)) then calculation_error := TRUE;
  case v_cb_msi_idx of
    0: w_msi := 65536;
    1: w_msi := 131072;
    2: w_msi := 262144;
    3: w_msi := 524288;
    4: w_msi := 1048000;
    5: w_msi := 2097000;
    6: w_msi := 4194000;
  end;  
  case v_rg_clock_hub_idx of
    0: w_sysclk := w_msi;
    1: w_sysclk := w_hse;
    2: w_sysclk := w_hsi;
    3: w_sysclk := w_plldiv;
  end; 
  if w_sysclk > 32000000 then calculation_error := TRUE;
  case v_cb_ahb_pr_idx of
    0: w_hclk := w_sysclk div 1;
    1: w_hclk := w_sysclk div 2;
    2: w_hclk := w_sysclk div 4;
    3: w_hclk := w_sysclk div 8;
    4: w_hclk := w_sysclk div 16;
    5: w_hclk := w_sysclk div 64;
    6: w_hclk := w_sysclk div 128;
    7: w_hclk := w_sysclk div 256;
    8: w_hclk := w_sysclk div 512;
  end;
  if w_hclk > 32000000 then calculation_error := TRUE;
  case v_cb_systick_pr_idx of
    0: w_tick := w_hclk div 1;
    1: w_tick := w_hclk div 8;
  end;
  case v_cb_apb1_pr_idx of
    0: begin w_apb1_p := w_hclk div 1; w_apb1_t := w_apb1_p; end;
    1: begin w_apb1_p := w_hclk div 2; w_apb1_t := w_apb1_p * 2; end;
    2: begin w_apb1_p := w_hclk div 4; w_apb1_t := w_apb1_p * 2; end;
    3: begin w_apb1_p := w_hclk div 8; w_apb1_t := w_apb1_p * 2; end;
    4: begin w_apb1_p := w_hclk div 16; w_apb1_t := w_apb1_p * 2; end;
  end;
  if w_apb1_p > 32000000 then calculation_error := TRUE;
  case v_cb_apb2_pr_idx of
    0: begin w_apb2_p := w_hclk div 1; w_apb2_t := w_apb2_p; end;
    1: begin w_apb2_p := w_hclk div 2; w_apb2_t := w_apb2_p * 2; end;
    2: begin w_apb2_p := w_hclk div 4; w_apb2_t := w_apb2_p * 2; end;
    3: begin w_apb2_p := w_hclk div 8; w_apb2_t := w_apb2_p * 2; end;
    4: begin w_apb2_p := w_hclk div 16; w_apb2_t := w_apb2_p * 2; end;
  end;
  if w_apb2_p > 32000000 then calculation_error := TRUE;
  // RTC
  case v_rg_osc32_idx of
    0: w_rtc := w_lse;
    1: w_rtc := w_lsi;
    2: 
      begin
        //
        if v_cb_hse_div_idx = 0 then w_hse_rtc := w_hse div 2;
        if v_cb_hse_div_idx = 1 then w_hse_rtc := w_hse div 4;
        if v_cb_hse_div_idx = 2 then w_hse_rtc := w_hse div 8;
        if v_cb_hse_div_idx = 3 then w_hse_rtc := w_hse div 16;
        w_rtc := w_hse_rtc;
      end;
  end;
  if ((w_rtc < 0) or (w_rtc > 1000000)) then calculation_error := TRUE;
  // MCO
  case v_rg_mco_src_idx of
    0: tmp := w_sysclk;
    1: tmp := w_hsi;
    2: tmp := w_msi;
    3: tmp := w_hse;
    4: tmp := w_plldiv;
    5: tmp := w_lsi;
    6: tmp := w_lse;
  end;
  case v_cb_mco_div_idx of
    0: w_mco := tmp div 1;
    1: w_mco := tmp div 2;
    2: w_mco := tmp div 4;
    3: w_mco := tmp div 8;
    4: w_mco := tmp div 16;
  end;
end;


procedure clean_buffer;
begin
  b_pin_id := '';
  b_pin_func := '';
  b_pin_o_lvl_idx := 0;
  b_pin_o_mode_idx := 0;
  b_pin_i_mode_idx := 0;
  b_pin_a_mode_idx := 0;
  b_pin_i2c_mode_idx := 0;
  b_pin_spi_mode_idx := 0;
  b_pin_int_mode_idx := 0;
  b_pin_pull_updown_idx := 0;
  b_pin_speed_idx := 0;
  b_pin_name := '';
end;

procedure reset_pc10_pin;
begin
  v_cb_pc10_idx:= 0;
  v_e_pc10_txt := '';
  v_pc10_id := 'PC10';
  v_pc10_func := 'N/A';
  v_pc10_o_lvl_idx := 0;
  v_pc10_o_mode_idx := 0;
  v_pc10_i_mode_idx := 0;
  v_pc10_a_mode_idx := 0;
  v_pc10_i2c_mode_idx := 0;
  v_pc10_spi_mode_idx := 0;
  v_pc10_int_mode_idx := 0;
  v_pc10_pull_updown_idx := 0;
  v_pc10_speed_idx := 0;
  v_pc10_name := '';
end;

procedure reset_pc12_pin;
begin
  v_cb_pc12_idx:= 0;
  v_e_pc12_txt := '';
  v_pc12_id := 'PC12';
  v_pc12_func := 'N/A';
  v_pc12_o_lvl_idx := 0;
  v_pc12_o_mode_idx := 0;
  v_pc12_i_mode_idx := 0;
  v_pc12_a_mode_idx := 0;
  v_pc12_i2c_mode_idx := 0;
  v_pc12_spi_mode_idx := 0;
  v_pc12_int_mode_idx := 0;
  v_pc12_pull_updown_idx := 0;
  v_pc12_speed_idx := 0;
  v_pc12_name := '';
end;

procedure reset_pa15_pin;
begin  
  v_cb_pa15_idx:= 0;
  v_e_pa15_txt := '';
  v_pa15_id := 'PA15';
  v_pa15_func := 'N/A';
  v_pa15_o_lvl_idx := 0;
  v_pa15_o_mode_idx := 0;
  v_pa15_i_mode_idx := 0;
  v_pa15_a_mode_idx := 0;
  v_pa15_i2c_mode_idx := 0;
  v_pa15_spi_mode_idx := 0;
  v_pa15_int_mode_idx := 0;
  v_pa15_pull_updown_idx := 0;
  v_pa15_speed_idx := 0;
  v_pa15_name := '';
end;  

procedure reset_pb7_pin;
begin 
  v_cb_pb7_idx:= 0;
  v_e_pb7_txt := '';
  v_pb7_id := 'PB7';
  v_pb7_func := 'N/A';
  v_pb7_o_lvl_idx := 0;
  v_pb7_o_mode_idx := 0;
  v_pb7_i_mode_idx := 0;
  v_pb7_a_mode_idx := 0;
  v_pb7_i2c_mode_idx := 0;
  v_pb7_spi_mode_idx := 0;
  v_pb7_int_mode_idx := 0;
  v_pb7_pull_updown_idx := 0;
  v_pb7_speed_idx := 0;
  v_pb7_name := '';
end;  

procedure reset_pc2_pin;  
begin
  v_cb_pc2_idx:= 0;
  v_e_pc2_txt := '';
  v_pc2_id := 'PC2';
  v_pc2_func := 'N/A';
  v_pc2_o_lvl_idx := 0;
  v_pc2_o_mode_idx := 0;
  v_pc2_i_mode_idx := 0;
  v_pc2_a_mode_idx := 0;
  v_pc2_i2c_mode_idx := 0;
  v_pc2_spi_mode_idx := 0;
  v_pc2_int_mode_idx := 0;
  v_pc2_pull_updown_idx := 0;
  v_pc2_speed_idx := 0;
  v_pc2_name := '';
end;  

procedure reset_pc3_pin;
begin  
  v_cb_pc3_idx:= 0;
  v_e_pc3_txt := '';
  v_pc3_id := 'PC3';
  v_pc3_func := 'N/A';
  v_pc3_o_lvl_idx := 0;
  v_pc3_o_mode_idx := 0;
  v_pc3_i_mode_idx := 0;
  v_pc3_a_mode_idx := 0;
  v_pc3_i2c_mode_idx := 0;
  v_pc3_spi_mode_idx := 0;
  v_pc3_int_mode_idx := 0;
  v_pc3_pull_updown_idx := 0;
  v_pc3_speed_idx := 0;
  v_pc3_name := '';
end;  

procedure reset_pc11_pin;
begin  
  v_cb_pc11_idx:= 0;
  v_e_pc11_txt := '';
  v_pc11_id := 'PC11';
  v_pc11_func := 'N/A';
  v_pc11_o_lvl_idx := 0;
  v_pc11_o_mode_idx := 0;
  v_pc11_i_mode_idx := 0;
  v_pc11_a_mode_idx := 0;
  v_pc11_i2c_mode_idx := 0;
  v_pc11_spi_mode_idx := 0;
  v_pc11_int_mode_idx := 0;
  v_pc11_pull_updown_idx := 0;
  v_pc11_speed_idx := 0;
  v_pc11_name := '';
end;  

procedure reset_pd2_pin;
begin
  v_cb_pd2_idx:= 0;
  v_e_pd2_txt := '';
  v_pd2_id := 'PD2';
  v_pd2_func := 'N/A';
  v_pd2_o_lvl_idx := 0;
  v_pd2_o_mode_idx := 0;
  v_pd2_i_mode_idx := 0;
  v_pd2_a_mode_idx := 0;
  v_pd2_i2c_mode_idx := 0;
  v_pd2_spi_mode_idx := 0;
  v_pd2_int_mode_idx := 0;
  v_pd2_pull_updown_idx := 0;
  v_pd2_speed_idx := 0;
  v_pd2_name := '';
end;

procedure reset_pa0_pin;
begin
  v_cb_pa0_idx:= 0;
  v_e_pa0_txt := '';
  v_pa0_id := 'PA0';
  v_pa0_func := 'N/A';
  v_pa0_o_lvl_idx := 0;
  v_pa0_o_mode_idx := 0;
  v_pa0_i_mode_idx := 0;
  v_pa0_a_mode_idx := 0;
  v_pa0_i2c_mode_idx := 0;
  v_pa0_spi_mode_idx := 0;
  v_pa0_int_mode_idx := 0;
  v_pa0_pull_updown_idx := 0;
  v_pa0_speed_idx := 0;
  v_pa0_name := '';
end;

procedure reset_pa1_pin;  
begin
  v_cb_pa1_idx:= 0;
  v_e_pa1_txt := '';
  v_pa1_id := 'PA1';
  v_pa1_func := 'N/A';
  v_pa1_o_lvl_idx := 0;
  v_pa1_o_mode_idx := 0;
  v_pa1_i_mode_idx := 0;
  v_pa1_a_mode_idx := 0;
  v_pa1_i2c_mode_idx := 0;
  v_pa1_spi_mode_idx := 0;
  v_pa1_int_mode_idx := 0;
  v_pa1_pull_updown_idx := 0;
  v_pa1_speed_idx := 0;
  v_pa1_name := '';
end;

procedure reset_pa4_pin;
begin  
  v_cb_pa4_idx:= 0;
  v_e_pa4_txt := '';
  v_pa4_id := 'PA4';
  v_pa4_func := 'N/A';
  v_pa4_o_lvl_idx := 0;
  v_pa4_o_mode_idx := 0;
  v_pa4_i_mode_idx := 0;
  v_pa4_a_mode_idx := 0;
  v_pa4_i2c_mode_idx := 0;
  v_pa4_spi_mode_idx := 0;
  v_pa4_int_mode_idx := 0;
  v_pa4_pull_updown_idx := 0;
  v_pa4_speed_idx := 0;
  v_pa4_name := '';
end;

procedure reset_pb0_pin;  
begin
  v_cb_pb0_idx:= 0;
  v_e_pb0_txt := '';
  v_pb0_id := 'PB0';
  v_pb0_func := 'N/A';
  v_pb0_o_lvl_idx := 0;
  v_pb0_o_mode_idx := 0;
  v_pb0_i_mode_idx := 0;
  v_pb0_a_mode_idx := 0;
  v_pb0_i2c_mode_idx := 0;
  v_pb0_spi_mode_idx := 0;
  v_pb0_int_mode_idx := 0;
  v_pb0_pull_updown_idx := 0;
  v_pb0_speed_idx := 0;
  v_pb0_name := '';
end;

procedure reset_pc1_pin;  
begin
  v_cb_pc1_idx:= 0;
  v_e_pc1_txt := '';
  v_pc1_id := 'PC1';
  v_pc1_func := 'N/A';
  v_pc1_o_lvl_idx := 0;
  v_pc1_o_mode_idx := 0;
  v_pc1_i_mode_idx := 0;
  v_pc1_a_mode_idx := 0;
  v_pc1_i2c_mode_idx := 0;
  v_pc1_spi_mode_idx := 0;
  v_pc1_int_mode_idx := 0;
  v_pc1_pull_updown_idx := 0;
  v_pc1_speed_idx := 0;
  v_pc1_name := '';
end;

procedure reset_pc0_pin;
begin
  v_cb_pc0_idx:= 0;
  v_e_pc0_txt := '';
  v_pc0_id := 'PC0';
  v_pc0_func := 'N/A';
  v_pc0_o_lvl_idx := 0;
  v_pc0_o_mode_idx := 0;
  v_pc0_i_mode_idx := 0;
  v_pc0_a_mode_idx := 0;
  v_pc0_i2c_mode_idx := 0;
  v_pc0_spi_mode_idx := 0;
  v_pc0_int_mode_idx := 0;
  v_pc0_pull_updown_idx := 0;
  v_pc0_speed_idx := 0;
  v_pc0_name := '';
end;

procedure reset_pc9_pin;  
begin
  v_cb_pc9_idx:= 0;
  v_e_pc9_txt := '';
  v_pc9_id := 'PC9';
  v_pc9_func := 'N/A';
  v_pc9_o_lvl_idx := 0;
  v_pc9_o_mode_idx := 0;
  v_pc9_i_mode_idx := 0;
  v_pc9_a_mode_idx := 0;
  v_pc9_i2c_mode_idx := 0;
  v_pc9_spi_mode_idx := 0;
  v_pc9_int_mode_idx := 0;
  v_pc9_pull_updown_idx := 0;
  v_pc9_speed_idx := 0;
  v_pc9_name := '';
end;

procedure reset_pb8_pin;  
begin
  v_cb_pb8_idx:= 0;
  v_e_pb8_txt := '';
  v_pb8_id := 'PB8';
  v_pb8_func := 'N/A';
  v_pb8_o_lvl_idx := 0;
  v_pb8_o_mode_idx := 0;
  v_pb8_i_mode_idx := 0;
  v_pb8_a_mode_idx := 0;
  v_pb8_i2c_mode_idx := 0;
  v_pb8_spi_mode_idx := 0;
  v_pb8_int_mode_idx := 0;
  v_pb8_pull_updown_idx := 0;
  v_pb8_speed_idx := 0;
  v_pb8_name := '';
end;

procedure reset_pb9_pin;
begin  
  v_cb_pb9_idx:= 0;
  v_e_pb9_txt := '';
  v_pb9_id := 'PB9';
  v_pb9_func := 'N/A';
  v_pb9_o_lvl_idx := 0;
  v_pb9_o_mode_idx := 0;
  v_pb9_i_mode_idx := 0;
  v_pb9_a_mode_idx := 0;
  v_pb9_i2c_mode_idx := 0;
  v_pb9_spi_mode_idx := 0;
  v_pb9_int_mode_idx := 0;
  v_pb9_pull_updown_idx := 0;
  v_pb9_speed_idx := 0;
  v_pb9_name := '';
end;

procedure reset_pa5_pin;  
begin
  v_cb_pa5_idx:= 0;
  v_e_pa5_txt := 'LD2';
  v_pa5_id := 'PA5';
  v_pa5_func := 'OUTPUT';
  v_pa5_o_lvl_idx := 1;
  v_pa5_o_mode_idx := 1;
  v_pa5_i_mode_idx := 0;
  v_pa5_a_mode_idx := 0;
  v_pa5_i2c_mode_idx := 0;
  v_pa5_spi_mode_idx := 0;
  v_pa5_int_mode_idx := 0;
  v_pa5_pull_updown_idx := 1;
  v_pa5_speed_idx := 2;
  v_pa5_name := '';
end;

procedure reset_pa6_pin;  
begin
  v_cb_pa6_idx:= 0;
  v_e_pa6_txt := '';
  v_pa6_id := 'PA6';
  v_pa6_func := 'N/A';
  v_pa6_o_lvl_idx := 0;
  v_pa6_o_mode_idx := 0;
  v_pa6_i_mode_idx := 0;
  v_pa6_a_mode_idx := 0;
  v_pa6_i2c_mode_idx := 0;
  v_pa6_spi_mode_idx := 0;
  v_pa6_int_mode_idx := 0;
  v_pa6_pull_updown_idx := 0;
  v_pa6_speed_idx := 0;
  v_pa6_name := '';
end;

procedure reset_pa7_pin;
begin  
  v_cb_pa7_idx:= 0;
  v_e_pa7_txt := '';
  v_pa7_id := 'PA7';
  v_pa7_func := 'N/A';
  v_pa7_o_lvl_idx := 0;
  v_pa7_o_mode_idx := 0;
  v_pa7_i_mode_idx := 0;
  v_pa7_a_mode_idx := 0;
  v_pa7_i2c_mode_idx := 0;
  v_pa7_spi_mode_idx := 0;
  v_pa7_int_mode_idx := 0;
  v_pa7_pull_updown_idx := 0;
  v_pa7_speed_idx := 0;
  v_pa7_name := '';
end;

procedure reset_pb6_pin;  
begin
  v_cb_pb6_idx:= 0;
  v_e_pb6_txt := '';
  v_pb6_id := 'PB6';
  v_pb6_func := 'N/A';
  v_pb6_o_lvl_idx := 0;
  v_pb6_o_mode_idx := 0;
  v_pb6_i_mode_idx := 0;
  v_pb6_a_mode_idx := 0;
  v_pb6_i2c_mode_idx := 0;
  v_pb6_spi_mode_idx := 0;
  v_pb6_int_mode_idx := 0;
  v_pb6_pull_updown_idx := 0;
  v_pb6_speed_idx := 0;
  v_pb6_name := '';
end;

procedure reset_pc7_pin;
begin  
  v_cb_pc7_idx:= 0;
  v_e_pc7_txt := '';
  v_pc7_id := 'PC7';
  v_pc7_func := 'N/A';
  v_pc7_o_lvl_idx := 0;
  v_pc7_o_mode_idx := 0;
  v_pc7_i_mode_idx := 0;
  v_pc7_a_mode_idx := 0;
  v_pc7_i2c_mode_idx := 0;
  v_pc7_spi_mode_idx := 0;
  v_pc7_int_mode_idx := 0;
  v_pc7_pull_updown_idx := 0;
  v_pc7_speed_idx := 0;
  v_pc7_name := '';
end;

procedure reset_pa9_pin;
begin
  v_cb_pa9_idx:= 0;
  v_e_pa9_txt := '';
  v_pa9_id := 'PA9';
  v_pa9_func := 'N/A';
  v_pa9_o_lvl_idx := 0;
  v_pa9_o_mode_idx := 0;
  v_pa9_i_mode_idx := 0;
  v_pa9_a_mode_idx := 0;
  v_pa9_i2c_mode_idx := 0;
  v_pa9_spi_mode_idx := 0;
  v_pa9_int_mode_idx := 0;
  v_pa9_pull_updown_idx := 0;
  v_pa9_speed_idx := 0;
  v_pa9_name := '';
end;  

procedure reset_pa8_pin;  
begin
  v_cb_pa8_idx:= 0; //set to 1 if RCC_MCO
  v_e_pa8_txt := '';
  v_pa8_id := 'PA8';
  v_pa8_func := 'N/A';
  v_pa8_o_lvl_idx := 0;
  v_pa8_o_mode_idx := 0;
  v_pa8_i_mode_idx := 0;
  v_pa8_a_mode_idx := 0;
  v_pa8_i2c_mode_idx := 0;
  v_pa8_spi_mode_idx := 0;
  v_pa8_int_mode_idx := 0;
  v_pa8_pull_updown_idx := 0;
  v_pa8_speed_idx := 0;
  v_pa8_name := '';
end;

procedure reset_pb10_pin;
begin  
  v_cb_pb10_idx:= 0;
  v_e_pb10_txt := '';
  v_pb10_id := 'PB10';
  v_pb10_func := 'N/A';
  v_pb10_o_lvl_idx := 0;
  v_pb10_o_mode_idx := 0;
  v_pb10_i_mode_idx := 0;
  v_pb10_a_mode_idx := 0;
  v_pb10_i2c_mode_idx := 0;
  v_pb10_spi_mode_idx := 0;
  v_pb10_int_mode_idx := 0;
  v_pb10_pull_updown_idx := 0;
  v_pb10_speed_idx := 0;
  v_pb10_name := '';
end;

procedure reset_pb4_pin;
begin  
  v_cb_pb4_idx:= 0;
  v_e_pb4_txt := '';
  v_pb4_id := 'PB4';
  v_pb4_func := 'N/A';
  v_pb4_o_lvl_idx := 0;
  v_pb4_o_mode_idx := 0;
  v_pb4_i_mode_idx := 0;
  v_pb4_a_mode_idx := 0;
  v_pb4_i2c_mode_idx := 0;
  v_pb4_spi_mode_idx := 0;
  v_pb4_int_mode_idx := 0;
  v_pb4_pull_updown_idx := 0;
  v_pb4_speed_idx := 0;
  v_pb4_name := '';
end;

procedure reset_pb5_pin;
begin  
  v_cb_pb5_idx:= 0;
  v_e_pb5_txt := '';
  v_pb5_id := 'PB5';
  v_pb5_func := 'N/A';
  v_pb5_o_lvl_idx := 0;
  v_pb5_o_mode_idx := 0;
  v_pb5_i_mode_idx := 0;
  v_pb5_a_mode_idx := 0;
  v_pb5_i2c_mode_idx := 0;
  v_pb5_spi_mode_idx := 0;
  v_pb5_int_mode_idx := 0;
  v_pb5_pull_updown_idx := 0;
  v_pb5_speed_idx := 0;
  v_pb5_name := '';
end;

procedure reset_pb3_pin;
begin  
  v_cb_pb3_idx:= 5;
  v_e_pb3_txt := 'SWO';
  v_pb3_id := 'PB3';
  v_pb3_func := 'N/A';
  v_pb3_o_lvl_idx := 0;
  v_pb3_o_mode_idx := 0;
  v_pb3_i_mode_idx := 0;
  v_pb3_a_mode_idx := 0;
  v_pb3_i2c_mode_idx := 0;
  v_pb3_spi_mode_idx := 0;
  v_pb3_int_mode_idx := 0;
  v_pb3_pull_updown_idx := 0;
  v_pb3_speed_idx := 0;
  v_pb3_name := '';
end;

procedure reset_pa10_pin;
begin  
  v_cb_pa10_idx:= 0;
  v_e_pa10_txt := '';
  v_pa10_id := 'PA10';
  v_pa10_func := 'N/A';
  v_pa10_o_lvl_idx := 0;
  v_pa10_o_mode_idx := 0;
  v_pa10_i_mode_idx := 0;
  v_pa10_a_mode_idx := 0;
  v_pa10_i2c_mode_idx := 0;
  v_pa10_spi_mode_idx := 0;
  v_pa10_int_mode_idx := 0;
  v_pa10_pull_updown_idx := 0;
  v_pa10_speed_idx := 0;
  v_pa10_name := '';
end;

procedure reset_pc8_pin;  
begin
  v_cb_pc8_idx:= 0;
  v_e_pc8_txt := '';
  v_pc8_id := 'PC8';
  v_pc8_func := 'N/A';
  v_pc8_o_lvl_idx := 0;
  v_pc8_o_mode_idx := 0;
  v_pc8_i_mode_idx := 0;
  v_pc8_a_mode_idx := 0;
  v_pc8_i2c_mode_idx := 0;
  v_pc8_spi_mode_idx := 0;
  v_pc8_int_mode_idx := 0;
  v_pc8_pull_updown_idx := 0;
  v_pc8_speed_idx := 0;
  v_pc8_name := '';
end;

procedure reset_pc6_pin;
begin  
  v_cb_pc6_idx:= 0;
  v_e_pc6_txt := '';
  v_pc6_id := 'PC6';
  v_pc6_func := 'N/A';
  v_pc6_o_lvl_idx := 0;
  v_pc6_o_mode_idx := 0;
  v_pc6_i_mode_idx := 0;
  v_pc6_a_mode_idx := 0;
  v_pc6_i2c_mode_idx := 0;
  v_pc6_spi_mode_idx := 0;
  v_pc6_int_mode_idx := 0;
  v_pc6_pull_updown_idx := 0;
  v_pc6_speed_idx := 0;
  v_pc6_name := '';
end;

procedure reset_pc5_pin;
begin  
  v_cb_pc5_idx:= 0;
  v_e_pc5_txt := '';
  v_pc5_id := 'PC5';
  v_pc5_func := 'N/A';
  v_pc5_o_lvl_idx := 0;
  v_pc5_o_mode_idx := 0;
  v_pc5_i_mode_idx := 0;
  v_pc5_a_mode_idx := 0;
  v_pc5_i2c_mode_idx := 0;
  v_pc5_spi_mode_idx := 0;
  v_pc5_int_mode_idx := 0;
  v_pc5_pull_updown_idx := 0;
  v_pc5_speed_idx := 0;
  v_pc5_name := '';
end;

procedure reset_pa12_pin;
begin  
  v_cb_pa12_idx:= 0;
  v_e_pa12_txt := '';
  v_pa12_id := 'PA12';
  v_pa12_func := 'N/A';
  v_pa12_o_lvl_idx := 0;
  v_pa12_o_mode_idx := 0;
  v_pa12_i_mode_idx := 0;
  v_pa12_a_mode_idx := 0;
  v_pa12_i2c_mode_idx := 0;
  v_pa12_spi_mode_idx := 0;
  v_pa12_int_mode_idx := 0;
  v_pa12_pull_updown_idx := 0;
  v_pa12_speed_idx := 0;
  v_pa12_name := '';
end;

procedure reset_pa11_pin;
begin  
  v_cb_pa11_idx:= 0;
  v_e_pa11_txt := '';
  v_pa11_id := 'PA11';
  v_pa11_func := 'N/A';
  v_pa11_o_lvl_idx := 0;
  v_pa11_o_mode_idx := 0;
  v_pa11_i_mode_idx := 0;
  v_pa11_a_mode_idx := 0;
  v_pa11_i2c_mode_idx := 0;
  v_pa11_spi_mode_idx := 0;
  v_pa11_int_mode_idx := 0;
  v_pa11_pull_updown_idx := 0;
  v_pa11_speed_idx := 0;
  v_pa11_name := '';
end;

procedure reset_pb12_pin;
begin  
  v_cb_pb12_idx:= 0;
  v_e_pb12_txt := '';
  v_pb12_id := 'PB12';
  v_pb12_func := 'N/A';
  v_pb12_o_lvl_idx := 0;
  v_pb12_o_mode_idx := 0;
  v_pb12_i_mode_idx := 0;
  v_pb12_a_mode_idx := 0;
  v_pb12_i2c_mode_idx := 0;
  v_pb12_spi_mode_idx := 0;
  v_pb12_int_mode_idx := 0;
  v_pb12_pull_updown_idx := 0;
  v_pb12_speed_idx := 0;
  v_pb12_name := '';
end;

procedure reset_pb11_pin;
begin  
  v_cb_pb11_idx:= 0;
  v_e_pb11_txt := '';
  v_pb11_id := 'PB11';
  v_pb11_func := 'N/A';
  v_pb11_o_lvl_idx := 0;
  v_pb11_o_mode_idx := 0;
  v_pb11_i_mode_idx := 0;
  v_pb11_a_mode_idx := 0;
  v_pb11_i2c_mode_idx := 0;
  v_pb11_spi_mode_idx := 0;
  v_pb11_int_mode_idx := 0;
  v_pb11_pull_updown_idx := 0;
  v_pb11_speed_idx := 0;
  v_pb11_name := '';
end;

procedure reset_pb2_pin;
begin  
  v_cb_pb2_idx:= 0;
  v_e_pb2_txt := '';
  v_pb2_id := 'PB2';
  v_pb2_func := 'N/A';
  v_pb2_o_lvl_idx := 0;
  v_pb2_o_mode_idx := 0;
  v_pb2_i_mode_idx := 0;
  v_pb2_a_mode_idx := 0;
  v_pb2_i2c_mode_idx := 0;
  v_pb2_spi_mode_idx := 0;
  v_pb2_int_mode_idx := 0;
  v_pb2_pull_updown_idx := 0;
  v_pb2_speed_idx := 0;
  v_pb2_name := '';
end;

procedure reset_pb1_pin;
begin
  v_cb_pb1_idx:= 0;
  v_e_pb1_txt := '';
  v_pb1_id := 'PB1';
  v_pb1_func := 'N/A';
  v_pb1_o_lvl_idx := 0;
  v_pb1_o_mode_idx := 0;
  v_pb1_i_mode_idx := 0;
  v_pb1_a_mode_idx := 0;
  v_pb1_i2c_mode_idx := 0;
  v_pb1_spi_mode_idx := 0;
  v_pb1_int_mode_idx := 0;
  v_pb1_pull_updown_idx := 0;
  v_pb1_speed_idx := 0;
  v_pb1_name := '';
end;

procedure reset_pb15_pin;
begin  
  v_cb_pb15_idx:= 0;
  v_e_pb15_txt := '';
  v_pb15_id := 'PB15';
  v_pb15_func := 'N/A';
  v_pb15_o_lvl_idx := 0;
  v_pb15_o_mode_idx := 0;
  v_pb15_i_mode_idx := 0;
  v_pb15_a_mode_idx := 0;
  v_pb15_i2c_mode_idx := 0;
  v_pb15_spi_mode_idx := 0;
  v_pb15_int_mode_idx := 0;
  v_pb15_pull_updown_idx := 0;
  v_pb15_speed_idx := 0;
  v_pb15_name := '';
end;

procedure reset_pb14_pin;
begin  
  v_cb_pb14_idx:= 0;
  v_e_pb14_txt := '';
  v_pb14_id := 'PB14';
  v_pb14_func := 'N/A';
  v_pb14_o_lvl_idx := 0;
  v_pb14_o_mode_idx := 0;
  v_pb14_i_mode_idx := 0;
  v_pb14_a_mode_idx := 0;
  v_pb14_i2c_mode_idx := 0;
  v_pb14_spi_mode_idx := 0;
  v_pb14_int_mode_idx := 0;
  v_pb14_pull_updown_idx := 0;
  v_pb14_speed_idx := 0;
  v_pb14_name := '';
end;

procedure reset_pb13_pin;
begin  
  v_cb_pb13_idx:= 0;
  v_e_pb13_txt := '';
  v_pb13_id := 'PB13';
  v_pb13_func := 'N/A';
  v_pb13_o_lvl_idx := 0;
  v_pb13_o_mode_idx := 0;
  v_pb13_i_mode_idx := 0;
  v_pb13_a_mode_idx := 0;
  v_pb13_i2c_mode_idx := 0;
  v_pb13_spi_mode_idx := 0;
  v_pb13_int_mode_idx := 0;
  v_pb13_pull_updown_idx := 0;
  v_pb13_speed_idx := 0;
  v_pb13_name := '';
end;

procedure reset_pc4_pin;
begin  
  v_cb_pc4_idx:= 0;
  v_e_pc4_txt := '';
  v_pc4_id := 'PC4';
  v_pc4_func := 'N/A';
  v_pc4_o_lvl_idx := 0;
  v_pc4_o_mode_idx := 0;
  v_pc4_i_mode_idx := 0;
  v_pc4_a_mode_idx := 0;
  v_pc4_i2c_mode_idx := 0;
  v_pc4_spi_mode_idx := 0;
  v_pc4_int_mode_idx := 0;
  v_pc4_pull_updown_idx := 0;
  v_pc4_speed_idx := 0;
  v_pc4_name := '';
end;

///
procedure reset_i2c1_a_pins;
begin
  v_c_i2c1_a := FALSE;
  {PB8}
  reset_pb8_pin;

  {PB9}
  reset_pb9_pin;
  if((v_c_i2c1_a = FALSE) AND (v_c_i2c1_b = FALSE)) then v_c_i2c1 := FALSE;
end;

procedure reset_i2c1_b_pins;
begin
  v_c_i2c1_b := FALSE;
  {PB7}
  reset_pb7_pin;

  {PB6}
  reset_pb6_pin;
  if((v_c_i2c1_a = FALSE) AND (v_c_i2c1_b = FALSE)) then v_c_i2c1 := FALSE;
end;


procedure reset_i2c2_pins;
begin
  v_c_i2c2 := FALSE;
  {PB10}
  reset_pb10_pin;

  {PB11}
  reset_pb11_pin;
end;

procedure reset_uart4_pins;
begin
  v_c_uart4 := FALSE;
  {PC10}
  reset_pc10_pin;

  {PC11}
  reset_pc11_pin;
end;

procedure reset_uart5_pins;
begin
  v_c_uart5 := FALSE;
  {PC12}
  reset_pc12_pin;

  {PD2}
  reset_pd2_pin;
end;

procedure reset_usart1_pins;
begin
  //
end;

procedure reset_spi1_pins;
begin
  v_c_spi1 := FALSE;
  {PA5} {- the LD2 user LED}
  reset_pa5_pin;

  {PA6}
  reset_pa6_pin;

  {PA7}
  reset_pa7_pin;
end;

procedure reset_spi2_pins;
begin
  v_c_spi2 := FALSE;
  {PB15}
  reset_pb15_pin;

  {PB14}
  reset_pb14_pin;

  {PB13}
  reset_pb13_pin;
end;

procedure reset_spi3_pins;
begin
  v_c_spi3 := FALSE;
  {PC10}
  reset_pc10_pin;

  {PC11}
  reset_pc11_pin;

  {PC12}
  reset_pc12_pin;
end;


procedure reset_default_lcd4_pins;
begin
  //
  v_c_lcd4 := FALSE;
  v_lcd4_defaults := FALSE;
  v_r1_lcd4 := TRUE;
  v_r2_lcd4 := FALSE;
  {PA10}
  reset_pa10_pin;

  {PB3}
  reset_pb3_pin;

  {PB5}
  reset_pb5_pin;

  {PB4}
  reset_pb4_pin;

  {PB10}
  reset_pb10_pin;

  {PA8}
  reset_pa8_pin;
end;


procedure reset_to_nucleo_hardware;
begin
  v_project_status := 'NEW';
  v_prj_name := '';
  v_debug := TRUE;
  v_optimization_index := 0;
  v_efp := FALSE;
  v_printf_efp := FALSE;
  v_group_priority := 0;
  v_priority_message := '';
  v_exti0_priority := 0;
  v_exti1_priority := 0;
  v_exti2_priority := 0;
  v_exti3_priority := 0;
  v_exti4_priority := 0;
  v_exti9_5_priority := 0;
  v_exti15_10_priority := 0;
  v_exti0_subpriority := 4;
  v_exti1_subpriority := 4;
  v_exti2_subpriority := 4;
  v_exti3_subpriority := 4;
  v_exti4_subpriority := 4;
  v_exti9_5_subpriority := 4;
  v_exti15_10_subpriority := 4;
  { }
  v_use_adc := FALSE;
  v_use_adin := FALSE;

  {========================
   EXTI interrupts
   ------------------------}
  v_exti0_used := FALSE;
  v_exti1_used := FALSE;
  v_exti2_used := FALSE;
  v_exti3_used := FALSE;
  v_exti4_used := FALSE;
  v_exti9_5_used := FALSE;
  v_exti15_10_used := TRUE; // hardware connected... but not mandatory...


  {========================
   DRIVERS AND PORTS USED
   ------------------------}

  v_porta_used := TRUE;
  v_portb_used := FALSE;
  v_portc_used := TRUE;
  v_portd_used := FALSE;
  v_porth_used := TRUE;


  {Initialization conform to Nucleo L152RE board}
  {CN7 connector, the non-paired pins row - 1,3,5, etc.}
  {PC10}
  reset_pc10_pin;

  {PC12}
  reset_pc12_pin;
  

  {PA15}
  reset_pa15_pin;

  {PB7}
  reset_pb7_pin;

  {PC13} {- user button on Nucleo, this is read-only so never changes}

  {PC2}
  reset_pc2_pin;

  {PC3}
  reset_pc3_pin;

  {CN7 connector, the paired pins row - 2,4,6, etc.}
  {PC11}
  reset_pc11_pin;

  {PD2}
  reset_pd2_pin;  

  {PA0}
  reset_pa0_pin;  

  {PA1}
  reset_pa1_pin;  

  {PA4}
  reset_pa4_pin;  

  {PB0}
  reset_pb0_pin;  

  {PC1}
  reset_pc1_pin;  

  {PC0}
  reset_pc0_pin;  

  {CN10 connector, the non-paired pins row - 1,3,5, etc.}
  {PC9}
  reset_pc9_pin;  

  {PB8}
  reset_pb8_pin;  

  {PB9}
  reset_pb9_pin;  

  {PA5} {- the LD2 user LED}
  reset_pa5_pin;  

  {PA6}
  reset_pa6_pin;  

  {PA7}
  reset_pa7_pin;  

  {PB6}
  reset_pb6_pin;  

  {PC7}
  reset_pc7_pin;  

  {PA9}
  reset_pa9_pin;  

  {PA8}
  reset_pa8_pin;  

  {PB10}
  reset_pb10_pin;  

  {PB4}
  reset_pb4_pin;  

  {PB5}
  reset_pb5_pin;  

  {PB3}
  reset_pb3_pin;  

  {PA10}
  reset_pa10_pin;  

  {PA2 PA3 are hardware connected to Serial so are initialized at startup}

  {CN10 connector, the paired pins row - 2,4,6, etc.}
  {PC8}
  reset_pc8_pin;  

  {PC6}
  reset_pc6_pin;  

  {PC5}
  reset_pc5_pin;  

  {PA12}
  reset_pa12_pin;  

  {PA11}
  reset_pa11_pin;  

  {PB12}
  reset_pb12_pin;  

  {PB11}
  reset_pb11_pin;  

  {PB2}
  reset_pb2_pin;  

  {PB1}
  reset_pb1_pin;  

  {PB15}
  reset_pb15_pin;  

  {PB14}
  reset_pb14_pin;  

  {PB13}
  reset_pb13_pin;  

  {PC4}
  reset_pc4_pin;  

  {==========================}
  {SYSTEM CLOCK CONFIGURATION}
  {--------------------------}
  //v_c_rcc_mco := FALSE; {Master Clock Output}
  //v_mco_divider_idx := 0;
  //v_mco_source := 0;
  //v_clock_source := 1;
  //v_hse_clk_source_idx := 0;
  //v_tick_manager := 0;
  v_se_hse_value := 8;
  v_cb_msi_idx := 5;
  v_rg_vco_idx := 1;
  v_cb_pllmul_idx := 2;
  v_cb_plldiv_idx := 1;
  v_rg_clock_hub_idx := 3;
  v_cb_ahb_pr_idx := 0;
  v_cb_systick_pr_idx := 0;
  v_cb_apb1_pr_idx := 0;
  v_cb_apb2_pr_idx := 0;
  v_c_en_css_hse := FALSE;
  //
  v_cb_rcc_hse_idx := 0;
  //v_cb_rcc_lse_idx := 1;
  v_c_rcc_mco := FALSE;
  v_cb_mco_div_idx := 0;
  v_rg_mco_src_idx := 0;
  //
  v_rg_osc32_idx := 0;
  v_cb_hse_div_idx := 3;
  v_c_en_css_lse := TRUE;
  calculate;
  {==============
   USB PERIPHERAL
   --------------}
  v_c_usb := FALSE;
  v_cb_usb_maxpacksize_idx := 3;
  v_cb_usb_lowpower_idx := 0;
  v_cb_usb_charging_idx := 0;
  v_c_usb_hint := FALSE;
  v_c_usb_lint := FALSE;

  {=========
   TIM6 INIT
   ---------}
  v_tim6_check := FALSE;
  v_tim6_prescaler := 0;
  v_tim6_period := 0;
  v_tim6_event := 2;
  v_tim6_global_int := FALSE;
  v_tim6_int_priority := 0;
  v_tim6_int_subpriority := 1;

  {=========
   TIM7 INIT
   ---------}
  v_tim7_check := FALSE;
  v_tim7_prescaler := 0;
  v_tim7_period := 0;
  v_tim7_event := 2;
  v_tim7_global_int := FALSE;
  v_tim7_int_priority := 0;
  v_tim7_int_subpriority := 1;

  {==================
   SPI1 CONFIGURATION
   ------------------}
  v_c_spi1 := FALSE;
  v_cb_direction_idx_spi1 := 0;
  v_cb_mode_idx_spi1 := 0;
  v_cb_datasize_idx_spi1 := 0;
  v_cb_cpol_idx_spi1 := 1;
  v_cb_cpha_idx_spi1 := 1;
  v_cb_nss_idx_spi1 := 0;
  v_cb_prescaler_idx_spi1 := 1;
  v_cb_firstbit_idx_spi1 := 0;
  v_cb_crcpoly_idx_spi1 := 7;
  v_c_gb_int_spi1 := FALSE;
  v_spi1_int_priority := 0;
  v_spi1_int_subpriority := 3;

  {==================
   SPI2 CONFIGURATION
   ------------------}
  v_c_spi2 := FALSE;
  v_cb_direction_idx_spi2 := 0;
  v_cb_mode_idx_spi2 := 0;
  v_cb_datasize_idx_spi2 := 0;
  v_cb_cpol_idx_spi2 := 1;
  v_cb_cpha_idx_spi2 := 1;
  v_cb_nss_idx_spi2 := 0;
  v_cb_prescaler_idx_spi2 := 1;
  v_cb_firstbit_idx_spi2 := 0;
  v_cb_crcpoly_idx_spi2 := 7;
  v_c_gb_int_spi2 := FALSE;
  v_spi2_int_priority := 0;
  v_spi2_int_subpriority := 3;

  {==================
   SPI3 CONFIGURATION
   ------------------}
  v_c_spi3 := FALSE;
  v_cb_direction_idx_spi3 := 0;
  v_cb_mode_idx_spi3 := 0;
  v_cb_datasize_idx_spi3 := 0;
  v_cb_cpol_idx_spi3 := 1;
  v_cb_cpha_idx_spi3 := 1;
  v_cb_nss_idx_spi3 := 0;
  v_cb_prescaler_idx_spi3 := 1;
  v_cb_firstbit_idx_spi3 := 0;
  v_cb_crcpoly_idx_spi3 := 7;
  v_c_gb_int_spi3 := FALSE;
  v_spi3_int_priority := 0;
  v_spi3_int_subpriority := 3;

  {===============
   I2C1 PERIPHERAL
   ---------------}
  v_c_i2c1 := FALSE;
  v_c_i2c1_a := FALSE; v_c_i2c1_b := FALSE;
  v_cb_i2c1_speedmode_idx := 0;
  v_cb_i2c1_clockspeed_idx := 0;
  v_cb_i2c1_fm_dc_idx := 0;
  v_cb_i2c1_s_cns_idx := 0;
  v_cb_i2c1_s_pal_idx := 0;
  v_cb_i2c1_s_daa_idx := 0;
  v_se_i2c1_s_psa := 0;
  v_cb_i2c1_s_gcad_idx := 0;
  v_c_i2c1_event_int := FALSE;
  v_c_i2c1_error_int := FALSE;
  v_i2c1_int_priority := 0;
  v_i2c1_int_subpriority := 3;

  {===============
   I2C2 PERIPHERAL
   ---------------}
  v_c_i2c2 := FALSE;
  v_cb_i2c2_speedmode_idx := 0;
  v_cb_i2c2_clockspeed_idx := 0;
  v_cb_i2c2_fm_dc_idx := 0;
  v_cb_i2c2_s_cns_idx := 0;
  v_cb_i2c2_s_pal_idx := 0;
  v_cb_i2c2_s_daa_idx := 0;
  v_se_i2c2_s_psa := 0;
  v_cb_i2c2_s_gcad_idx := 0;
  v_c_i2c2_event_int := FALSE;
  v_c_i2c2_error_int := FALSE;
  v_i2c2_int_priority := 0;
  v_i2c2_int_subpriority := 3;

  {===================
   LCD4 pinout setting
   -------------------}
  v_c_lcd4 := FALSE;
  v_lcd4_defaults := FALSE;
  v_r1_lcd4:= TRUE;
  v_r2_lcd4:= FALSE;
  v_cb_idx_lcd4:= 0;
  v_cb_idx_lcd4_chipset:= 0;
  {===================
   UART4 CONFIGURATION
   -------------------}
  v_c_uart4 := FALSE;
  v_cb_baud_idx_uart4 := 4;
  v_cb_wordlng_idx_uart4 := 0;
  v_cb_parity_idx_uart4 := 0;
  v_cb_stop_idx_uart4 := 0;
  v_cb_datadir_idx_uart4 := 0;
  v_cb_sampling_idx_uart4 := 0;
  v_c_gb_int_uart4 := FALSE;
  v_uart4_int_priority := 0;
  v_uart4_int_subpriority := 2;

  {===================
   UART5 CONFIGURATION
   -------------------}
  v_c_uart5 := FALSE;
  v_cb_baud_idx_uart5 := 4;
  v_cb_wordlng_idx_uart5 := 0;
  v_cb_parity_idx_uart5 := 0;
  v_cb_stop_idx_uart5 := 0;
  v_cb_datadir_idx_uart5 := 0;
  v_cb_sampling_idx_uart5 := 0;
  v_c_gb_int_uart5 := FALSE;
  v_uart5_int_priority := 0;
  v_uart5_int_subpriority := 2;

  {====================
   USART2 CONFIGURATION
   --------------------}
  v_cb_baud_idx_usart2 := 4;
  v_cb_wordlng_idx_usart2 := 0;
  v_cb_parity_idx_usart2 := 0;
  v_cb_stop_idx_usart2 := 0;
  v_cb_datadir_idx_usart2 := 0;
  v_cb_sampling_idx_usart2 := 0;
  v_c_gb_int_usart2 := FALSE;
  v_usart2_int_priority := 0;
  v_usart2_int_subpriority := 2;

  {====================
   USART1 CONFIGURATION
   --------------------}
  v_c_usart1 := FALSE;
  v_cb_baud_idx_usart1 := 4;
  v_cb_wordlng_idx_usart1 := 0;
  v_cb_parity_idx_usart1 := 0;
  v_cb_stop_idx_usart1 := 0;
  v_cb_datadir_idx_usart1 := 0;
  v_cb_sampling_idx_usart1 := 0;
  v_c_gb_int_usart1 := FALSE;
  v_usart1_int_priority := 0;
  v_usart1_int_subpriority := 2;

  {===============
   ADC1 PERIPHERAL
   ---------------}
  v_adc1_res_idx := 0;
  v_adc1_scan_idx := 1;
  v_adc1_continuous_idx := 1;
  v_adc1_tce_idx := 0;
  v_adc1_tc_idx := 0;
  v_adc1_align_idx := 0;
  v_adc1_prescaler_idx := 0;
  v_adc1_nrconv_idx := 1;
  v_c_gb_int_adc1 := FALSE;
  v_adc1_int_priority := 0;
  v_adc1_int_subpriority := 4;
  

end;

begin

  v_generating_code_is_ok := FALSE;

  b_pin_id := '';
  b_pin_func := '';
  b_pin_o_lvl_idx := 0;
  b_pin_o_mode_idx := 0;
  b_pin_i_mode_idx := 0;
  b_pin_a_mode_idx := 0;
  b_pin_i2c_mode_idx := 0;
  b_pin_spi_mode_idx := 0;
  b_pin_int_mode_idx := 0;
  b_pin_pull_updown_idx := 0;
  b_pin_speed_idx := 0;
  b_pin_name := '';

  v_file_version := 1;
  v_workspace_ok := FALSE; v_prj_ok := FALSE; v_toolchain_ok := FALSE;
  v_toolchain_subf_ok := FALSE;
  v_app_ok := FALSE; v_driver_ok := FALSE;
  sf := 'change-me';
  v_workspace := 'vsc_stm32l1_projects';
  v_user_lib_folder := 'spl_library';
  //v_arduino_path := 'arduino-1.8.8';
  v_toolchain_folder := 'gcc-arm';
  v_license_agreed := FALSE;

  v_r_oslin := TRUE;
  v_r_osfree := FALSE;

  {PC13} // user button
  v_cb_pc13_idx:= 11;
  v_e_pc13_txt := 'B1';
  v_pc13_id := 'PC13';
  v_pc13_func := 'INTERRUPT';
  v_pc13_o_lvl_idx := 0;
  v_pc13_o_mode_idx := 0;
  v_pc13_i_mode_idx := 0;
  v_pc13_a_mode_idx := 0;
  v_pc13_i2c_mode_idx := 0;
  v_pc13_spi_mode_idx := 0;
  v_pc13_int_mode_idx := 1;
  v_pc13_pull_updown_idx := 1;
  v_pc13_speed_idx := 0;
  v_pc13_name := '';

  {PA2  TX}
  v_cb_pa2_idx:= 9;
  v_e_pa2_txt := 'U2_TX';
  v_pa2_id := 'PA2';
  v_pa2_func := 'SERIAL';
  v_pa2_o_lvl_idx := 0;
  v_pa2_o_mode_idx := 0;
  v_pa2_i_mode_idx := 0;
  v_pa2_a_mode_idx := 0;
  v_pa2_i2c_mode_idx := 0;
  v_pa2_spi_mode_idx := 1;
  v_pa2_int_mode_idx := 0;
  v_pa2_pull_updown_idx := 2;
  v_pa2_speed_idx := 4;
  v_pa2_name := '';

  {PA3  RX}
  v_cb_pa3_idx:= 9;
  v_e_pa3_txt := 'U2_RX';
  v_pa3_id := 'PA3';
  v_pa3_func := 'SERIAL';
  v_pa3_o_lvl_idx := 0;
  v_pa3_o_mode_idx := 0;
  v_pa3_i_mode_idx := 0;
  v_pa3_a_mode_idx := 0;
  v_pa3_i2c_mode_idx := 0;
  v_pa3_spi_mode_idx := 1;
  v_pa3_int_mode_idx := 0;
  v_pa3_pull_updown_idx := 2;
  v_pa3_speed_idx := 4;
  v_pa3_name := '';


  reset_to_nucleo_hardware;

end.

