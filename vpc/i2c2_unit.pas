unit i2c2_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, Buttons, data_unit, (* pinout_unit,*) code_unit;

type

  { TForm13 }

  TForm13 = class(TForm)
    BitBtn1: TBitBtn;
    CB_I2C2_CLOCKSPEED: TComboBox;
    CB_I2C2_FM_DUTYCYCLE: TComboBox;
    CB_I2C2_GCAD: TComboBox;
    CB_I2C2_SPEEDMODE: TComboBox;
    CB_I2C2_S_ClockNoStretch: TComboBox;
    CB_I2C2_S_DualAddACK: TComboBox;
    CB_I2C2_S_PAL: TComboBox;
    cb_i2c2_subpriority: TComboBox;
    cb_i2c2_priority: TComboBox;
    C_I2C2_Error_INT: TCheckBox;
    C_i2C2_Event_INT: TCheckBox;
    GB_I2C2: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    l_i2c2_mess: TLabel;
    priority: TLabel;
    priority1: TLabel;
    SE_I2C2_PSA: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure CB_I2C2_SPEEDMODESelect(Sender: TObject);
    procedure FormClose(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SetInterfaceElements;
  private

  public

  end;

var
  Form13: TForm13;

implementation

{$R *.lfm}

{ TForm13 }

procedure TForm13.SetInterfaceElements;
begin
  if v_c_i2c2 then GB_I2C2.Enabled := TRUE
  else GB_I2C2.Enabled := FALSE;
  if (CB_I2C2_SPEEDMODE.ItemIndex = 0) then begin
    CB_I2C2_CLOCKSPEED.ItemIndex := 0;
    CB_I2C2_CLOCKSPEED.Enabled := FALSE;
    CB_I2C2_FM_DUTYCYCLE.Enabled := FALSE;
    CB_I2C2_FM_DUTYCYCLE.ItemIndex := 0;
  end
  else begin
    if CB_I2C2_CLOCKSPEED.ItemIndex = 0 then CB_I2C2_CLOCKSPEED.ItemIndex := 1;
    CB_I2C2_CLOCKSPEED.Enabled := TRUE;
    CB_I2C2_FM_DUTYCYCLE.Enabled := TRUE;
    if CB_I2C2_FM_DUTYCYCLE.ItemIndex = 0 then CB_I2C2_FM_DUTYCYCLE.ItemIndex := 1;
  end;
end;

procedure TForm13.BitBtn1Click(Sender: TObject);
begin
  close;
end;

procedure TForm13.CB_I2C2_SPEEDMODESelect(Sender: TObject);
begin
  SetInterfaceElements;
end;



procedure TForm13.FormClose(Sender: TObject);
begin
  {put data back in global variables}
  if v_c_i2c2 then begin
    v_cb_i2c2_speedmode_idx := CB_I2C2_SPEEDMODE.ItemIndex;
    v_cb_i2c2_clockspeed_idx := CB_I2C2_CLOCKSPEED.ItemIndex;
    v_cb_i2c2_fm_dc_idx := CB_I2C2_FM_DUTYCYCLE.ItemIndex;
    v_cb_i2c2_s_cns_idx := CB_I2C2_S_ClockNoStretch.ItemIndex;
    v_cb_i2c2_s_pal_idx := CB_I2C2_S_PAL.ItemIndex;
    v_cb_i2c2_s_daa_idx := CB_I2C2_S_DualAddACK.ItemIndex;
    v_se_i2c2_s_psa := SE_I2C2_PSA.Value;
    v_cb_i2c2_s_gcad_idx := CB_I2C2_GCAD.ItemIndex;
    v_c_i2c2_event_int := C_i2C2_Event_INT.Checked;
    v_c_i2c2_error_int := C_I2C2_Error_INT.Checked;
    v_i2c2_int_priority:=validate_priority_value(cb_i2c2_priority.ItemIndex);
    v_i2c2_int_subpriority:=validate_subpriority_value(cb_i2c2_subpriority.ItemIndex);
  end;
  {}
end;

procedure TForm13.FormShow(Sender: TObject);
begin
  {get data from the global variables}
  if v_c_i2c2 then l_i2c2_mess.Caption:='I2C2 peripheral activated.'
  else l_i2c2_mess.Caption:='Set the PB10-PB11 pair as I2C2 in Nucleo or LQFP64 window.';
  CB_I2C2_SPEEDMODE.ItemIndex := v_cb_i2c2_speedmode_idx;
  CB_I2C2_CLOCKSPEED.ItemIndex := v_cb_i2c2_clockspeed_idx;
  CB_I2C2_FM_DUTYCYCLE.ItemIndex := v_cb_i2c2_fm_dc_idx;
  CB_I2C2_S_ClockNoStretch.ItemIndex := v_cb_i2c2_s_cns_idx;
  CB_I2C2_S_PAL.ItemIndex := v_cb_i2c2_s_pal_idx;
  CB_I2C2_S_DualAddACK.ItemIndex := v_cb_i2c2_s_daa_idx;
  SE_I2C2_PSA.Value := v_se_i2c2_s_psa;
  CB_I2C2_GCAD.ItemIndex := v_cb_i2c2_s_gcad_idx;
  C_i2C2_Event_INT.Checked := v_c_i2c2_event_int;
  C_I2C2_Error_INT.Checked := v_c_i2c2_error_int;
  cb_i2c2_priority.ItemIndex:=validate_priority_value(v_i2c2_int_priority);
  cb_i2c2_subpriority.ItemIndex:=validate_subpriority_value(v_i2c2_int_subpriority);
  {}
  SetInterfaceElements;
end;

end.

