unit gpio_unit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons, data_unit, code_unit;

type

  { TForm9 }

  TForm9 = class(TForm)
    BitBtn1: TBitBtn;
    CB_GPIO_O_LVL: TComboBox;
    CB_GPIO_O_MODE: TComboBox;
    CB_GPIO_I_MODE: TComboBox;
    CB_GPIO_A_MODE: TComboBox;
    CB_GPIO_I2C_MODE: TComboBox;
    CB_GPIO_SPI_MODE: TComboBox;
    CB_GPIO_INT_MODE: TComboBox;
    CB_GPIO_PULL_UPDOWN: TComboBox;
    CB_GPIO_SPEED: TComboBox;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    L_GPIO_MSG: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private

  public

  end;

var
  Form9: TForm9;

//procedure pin_preliminary_set(f:string);

implementation

{$R *.lfm}

{ TForm9 }


procedure TForm9.BitBtn1Click(Sender: TObject);
begin
  if b_pin_func <> 'N/A' then begin
    b_pin_o_lvl_idx := CB_GPIO_O_LVL.ItemIndex;
    b_pin_o_mode_idx := CB_GPIO_O_MODE.ItemIndex;
    b_pin_i_mode_idx := CB_GPIO_I_MODE.ItemIndex;
    b_pin_a_mode_idx := CB_GPIO_A_MODE.ItemIndex;
    b_pin_i2c_mode_idx := CB_GPIO_I2C_MODE.ItemIndex;
    b_pin_spi_mode_idx := CB_GPIO_SPI_MODE.ItemIndex;
    b_pin_int_mode_idx := CB_GPIO_INT_MODE.ItemIndex;
    b_pin_pull_updown_idx := CB_GPIO_PULL_UPDOWN.ItemIndex;
    b_pin_speed_idx := CB_GPIO_SPEED.ItemIndex;
    {now start checking and enforcing the data according to the pin function}
    validate_buffer(b_pin_func);
  end
  else begin
    b_pin_o_lvl_idx := 0;
    b_pin_o_mode_idx := 0;
    b_pin_i_mode_idx := 0;
    b_pin_a_mode_idx := 0;
    b_pin_i2c_mode_idx := 0;
    b_pin_spi_mode_idx := 0;
    b_pin_int_mode_idx := 0;
    b_pin_pull_updown_idx := 0;
    b_pin_speed_idx := 0;
  end;
  CB_GPIO_O_LVL.ItemIndex := 0;
  CB_GPIO_O_MODE.ItemIndex := 0;
  CB_GPIO_I_MODE.ItemIndex := 0;
  CB_GPIO_A_MODE.ItemIndex := 0;
  CB_GPIO_I2C_MODE.ItemIndex := 0;
  CB_GPIO_SPI_MODE.ItemIndex := 0;
  CB_GPIO_INT_MODE.ItemIndex := 0;
  CB_GPIO_PULL_UPDOWN.ItemIndex := 0;
  CB_GPIO_SPEED.ItemIndex := 0;
  close;
end;

procedure TForm9.FormShow(Sender: TObject);
var
  current_function : String;
begin
  Caption := b_pin_id + ' ' + b_pin_label + ' pin tuning';
  current_function := b_pin_func;
  if current_function = 'N/A' then begin
    L_GPIO_MSG.Caption := 'This function is NOT AVAILABLE, choose another!!!';
    {make all comboboxes on index zero and disabled}
    CB_GPIO_O_LVL.ItemIndex := 0;
    CB_GPIO_O_MODE.ItemIndex := 0;
    CB_GPIO_I_MODE.ItemIndex := 0;
    CB_GPIO_A_MODE.ItemIndex := 0;
    CB_GPIO_I2C_MODE.ItemIndex := 0;
    CB_GPIO_SPI_MODE.ItemIndex := 0;
    CB_GPIO_INT_MODE.ItemIndex := 0;
    CB_GPIO_PULL_UPDOWN.ItemIndex := 0;
    CB_GPIO_SPEED.ItemIndex := 0;
    { }
    CB_GPIO_O_LVL.Enabled := FALSE;
    CB_GPIO_O_MODE.Enabled := FALSE;
    CB_GPIO_I_MODE.Enabled := FALSE;
    CB_GPIO_A_MODE.Enabled := FALSE;
    CB_GPIO_I2C_MODE.Enabled := FALSE;
    CB_GPIO_SPI_MODE.Enabled := FALSE;
    CB_GPIO_INT_MODE.Enabled := FALSE;
    CB_GPIO_PULL_UPDOWN.Enabled := FALSE;
    CB_GPIO_SPEED.Enabled := FALSE;
  end
  else begin
    {load pin data into the buffer}
    case b_pin_id of
      {PA}
      'PA0': pin_PA0_to_buffer;
      'PA10': pin_PA10_to_buffer;
      'PA11': pin_PA11_to_buffer;
      'PA12': pin_PA12_to_buffer;
      'PA15': pin_PA15_to_buffer;
      'PA1': pin_PA1_to_buffer;
      'PA4': pin_PA4_to_buffer;
      'PA5': pin_PA5_to_buffer;
      'PA6': pin_PA6_to_buffer;
      'PA7': pin_PA7_to_buffer;
      'PA8': pin_PA8_to_buffer;
      'PA9': pin_PA9_to_buffer;
      {PB}
      'PB0': pin_PB0_to_buffer;
      'PB10': pin_PB10_to_buffer;
      'PB11': pin_PB11_to_buffer;
      'PB12': pin_PB12_to_buffer;
      'PB13': pin_PB13_to_buffer;
      'PB14': pin_PB14_to_buffer;
      'PB15': pin_PB15_to_buffer;
      'PB1': pin_PB1_to_buffer;
      'PB2': pin_PB2_to_buffer;
      'PB3': pin_PB3_to_buffer;
      'PB4': pin_PB4_to_buffer;
      'PB5': pin_PB5_to_buffer;
      'PB6': pin_PB6_to_buffer;
      'PB7': pin_PB7_to_buffer;
      'PB8': pin_PB8_to_buffer;
      'PB9': pin_PB9_to_buffer;
      {PC}
      'PC0': pin_PC0_to_buffer;
      'PC10': pin_PC10_to_buffer;
      'PC11': pin_PC11_to_buffer;
      'PC12': pin_PC12_to_buffer;
      'PC1': pin_PC1_to_buffer;
      'PC2': pin_PC2_to_buffer;
      'PC3': pin_PC3_to_buffer;
      'PC4': pin_PC4_to_buffer;
      'PC5': pin_PC5_to_buffer;
      'PC6': pin_PC6_to_buffer;
      'PC7': pin_PC7_to_buffer;
      'PC8': pin_PC8_to_buffer;
      'PC9': pin_PC9_to_buffer;
      {PD}
      'PD2': pin_PD2_to_buffer;
    end;
    if current_function <> b_pin_func then begin
      pin_preliminary_set(current_function);
      b_pin_func := current_function;
    end;
    //validate buffer data
    validate_buffer(current_function);
    { }
    if current_function = 'USB' then L_GPIO_MSG.Caption := 'Good, the pin is set as USB with all controls disabled.'
    else  L_GPIO_MSG.Caption := 'Good, the pin is set as ' + current_function + '.';
    {load the values from buffer into the form}
    if b_pin_o_lvl_idx = 0 then begin
      CB_GPIO_O_LVL.Enabled := FALSE;
      CB_GPIO_O_LVL.ItemIndex := 0;
    end
    else begin
      CB_GPIO_O_LVL.ItemIndex := b_pin_o_lvl_idx;
      CB_GPIO_O_LVL.Enabled := TRUE;
    end;
    //
    if b_pin_o_mode_idx = 0 then begin
      CB_GPIO_O_MODE.Enabled := FALSE;
      CB_GPIO_O_MODE.ItemIndex := 0;
    end
    else begin
      CB_GPIO_O_MODE.ItemIndex := b_pin_o_mode_idx;
      CB_GPIO_O_MODE.Enabled := TRUE;
    end;
    //
    if b_pin_i_mode_idx = 0 then begin
      CB_GPIO_I_MODE.Enabled := FALSE;
      CB_GPIO_I_MODE.ItemIndex := 0;
    end
    else begin
      CB_GPIO_I_MODE.ItemIndex := b_pin_i_mode_idx;
      CB_GPIO_I_MODE.Enabled := TRUE;
    end;
    //
    if b_pin_a_mode_idx = 0 then begin
      CB_GPIO_A_MODE.Enabled := FALSE;
      CB_GPIO_A_MODE.ItemIndex := 0;
    end
    else begin
      CB_GPIO_A_MODE.ItemIndex := b_pin_a_mode_idx;
      CB_GPIO_A_MODE.Enabled := TRUE;
    end;
    //
    if b_pin_i2c_mode_idx = 0 then begin
      CB_GPIO_I2C_MODE.Enabled := FALSE;
      CB_GPIO_I2C_MODE.ItemIndex := 9;
    end
    else begin
      CB_GPIO_I2C_MODE.Enabled := TRUE;
      CB_GPIO_I2C_MODE.ItemIndex := b_pin_i2c_mode_idx;
    end;
    //
    if b_pin_spi_mode_idx = 0 then begin
      CB_GPIO_SPI_MODE.Enabled := FALSE;
      CB_GPIO_SPI_MODE.ItemIndex := 0;
    end
    else begin
      CB_GPIO_SPI_MODE.Enabled := TRUE;
      CB_GPIO_SPI_MODE.ItemIndex := b_pin_spi_mode_idx;
    end;
    //
    if b_pin_int_mode_idx = 0 then begin
      CB_GPIO_INT_MODE.ItemIndex := 0;
      CB_GPIO_INT_MODE.Enabled := FALSE;
    end
    else begin
      CB_GPIO_INT_MODE.ItemIndex := b_pin_int_mode_idx;
      CB_GPIO_INT_MODE.Enabled := TRUE;
    end;
    //
    if b_pin_pull_updown_idx = 0 then begin
      CB_GPIO_PULL_UPDOWN.ItemIndex := 0;
      CB_GPIO_PULL_UPDOWN.Enabled := FALSE;
    end
    else begin
      CB_GPIO_PULL_UPDOWN.ItemIndex := b_pin_pull_updown_idx;
      CB_GPIO_PULL_UPDOWN.Enabled := TRUE;
    end;
    //
    if b_pin_speed_idx = 0 then begin
      CB_GPIO_SPEED.ItemIndex := 0;
      CB_GPIO_SPEED.Enabled := FALSE;
    end
    else begin
      CB_GPIO_SPEED.ItemIndex := b_pin_speed_idx;
      CB_GPIO_SPEED.Enabled := TRUE;
    end;
  end;
end;

end.

