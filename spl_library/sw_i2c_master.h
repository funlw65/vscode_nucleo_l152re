/*
 * Author: Vasile Guta-Ciucur
 *
 * Description: Software I2C on STM32
 * Setup: Inside VPC, chose two pins (that must be 5V tolerant), set
 *        them as INPUTS (yeah, exactly that), and label them as SW_SDA and SW_SCL.
 *        THE LIBRARY WILL USE THESE NAMES!!!
 *
 *        If those are not defined, the library will set some defaults.
 *        On these defaults, GPIO PINS 9 and 8 are chosen. Hardware wise, don't
 *        forget the external pull-up resistors.
 *
 * COPYRIGHT(c) 2017 Vasile Guta-Ciucur
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of Author nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 */

#ifndef  SW_I2C_MASTER_H
#define  SW_I2C_MASTER_H

#include "stm32l1xx.h"
#include "stm32l1xx_gpio.h"
#include "my_delay_us.h"

/*
 * If you forgot to define and correctly name the software i2c pins, then these
 * are the defaults:
 */

#undef SCL_PIN_LOCALLY_DEFINED
#ifndef SW_SCL_Pin
#define SW_SCL_Pin GPIO_Pin_8
#define SCL_PIN_LOCALLY_DEFINED 1
#endif

#ifndef SW_SCL_Port
#define SW_SCL_Port GPIOB
#endif

#undef SDA_PIN_LOCALLY_DEFINED
#ifndef SW_SDA_Pin
#define SW_SDA_Pin GPIO_Pin_9
#define SDA_PIN_LOCALLY_DEFINED 1
#endif

#ifndef SW_SDA_Port
#define SW_SDA_Port GPIOB
#endif

void I2C_SDA_WR(void); // SDA pin set as Output
void I2C_SDA_RD(void); // SDA pin set as Input
void I2C_SCL_WR(void); // SCL pin set as Output

void I2C_SDA_H(void); // SDA pin set HIGH
void I2C_SDA_L(void); // SDA pin set LOW
void I2C_SCL_H(void); // SCL pin set HIGH
void I2C_SCL_L(void); // SCL pin set LOW

/*
 * This function is used in case you forgot to correctly name the
 * I2C pins in the STM32CubeMX software. But you have to call it anyway
 * just for exercise (discipline yourself).
 *
 */
void sw_i2c_init(void)
{
#ifdef SCL_PIN_LOCALLY_DEFINED
  GPIO_InitTypeDef GPIO_InitStruct;
#define GPIO_InitStructUSED
#endif
  //
#ifdef SDA_PIN_LOCALLY_DEFINED
#ifndef GPIO_InitStructUSED
  GPIO_InitTypeDef GPIO_InitStruct;
#endif
#endif
  //
#ifdef SCL_PIN_LOCALLY_DEFINED
  GPIO_InitStruct.GPIO_Pin = SW_SCL_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(SW_SCL_Port, &GPIO_InitStruct);
#endif
#ifdef SDA_PIN_LOCALLY_DEFINED
  GPIO_InitStruct.GPIO_Pin = SW_SDA_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(SW_SDA_Port, &GPIO_InitStruct);
#endif
}

void sw_i2c_start(void)
{
  //
  I2C_SCL_H();
  I2C_SDA_H();
  I2C_SDA_WR();
  I2C_SCL_WR();
  my_delay_us(10);
  I2C_SDA_L();
  my_delay_us(10);
  I2C_SCL_L();
  my_delay_us(10);
}

void sw_i2c_stop(void)
{
  //
  I2C_SDA_WR();
  I2C_SCL_H();
  my_delay_us(10);
  I2C_SDA_H();
  my_delay_us(10);
}

uint8_t sw_i2c_write(uint8_t b)
{
  uint8_t i;
  I2C_SDA_WR();
  for (i = 0; i < 8; i++) {
    if (b & 0x80)
      I2C_SDA_H();
    else
      I2C_SDA_L();
    my_delay_us(10);
    I2C_SCL_H();
    my_delay_us(10);
    I2C_SCL_L();
    b <<= 1;
  }
  I2C_SDA_RD();
  I2C_SDA_H();
  my_delay_us(10);
  I2C_SCL_H();
  my_delay_us(10);
  i = 0xFF;
  do {
    if (GPIO_ReadInputDataBit(SW_SDA_Port, SW_SDA_Pin) == 0)
      break;
    my_delay_us(10);
  } while (--i > 0);
  I2C_SCL_L();
  my_delay_us(10);
  return i;
}

uint8_t sw_i2c_read(uint8_t ack)
{
  uint8_t i;
  uint8_t b = 0;
  I2C_SDA_RD();
  I2C_SDA_H();
  my_delay_us(10);
  for (i = 0; i < 8; i++) {
    I2C_SCL_H();
    my_delay_us(10);
    b <<= 1;
    if (GPIO_ReadInputDataBit(SW_SDA_Port, SW_SDA_Pin))
      b |= 1;
    I2C_SCL_L();
    my_delay_us(10);
  }
  I2C_SDA_WR();
  if (ack == 0)
    I2C_SDA_L();
  else
    I2C_SDA_H();
  my_delay_us(10);
  I2C_SCL_H();
  my_delay_us(10);
  I2C_SCL_L();
  my_delay_us(10);
  I2C_SDA_L();
  my_delay_us(10);
  return (b);
}

void I2C_SDA_RD(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  //
  GPIO_InitStruct.GPIO_Pin = SW_SDA_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(SW_SDA_Port, &GPIO_InitStruct);
}

void I2C_SDA_WR(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  //
  GPIO_InitStruct.GPIO_Pin = SW_SDA_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(SW_SDA_Port, &GPIO_InitStruct);
}

void I2C_SCL_WR(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  //
  GPIO_InitStruct.GPIO_Pin = SW_SCL_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(SW_SCL_Port, &GPIO_InitStruct);
}

//

void I2C_SDA_H(void)
{
  //
  GPIO_SetBits(SW_SDA_Port, SW_SDA_Pin);
}

void I2C_SDA_L(void)
{
  //
  GPIO_ResetBits(SW_SDA_Port, SW_SDA_Pin);
}

void I2C_SCL_H(void)
{
  //
  GPIO_SetBits(SW_SCL_Port, SW_SCL_Pin);
}

void I2C_SCL_L(void)
{
  //
  GPIO_ResetBits(SW_SCL_Port, SW_SCL_Pin);
}

#endif
