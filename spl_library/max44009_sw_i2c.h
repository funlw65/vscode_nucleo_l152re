/*
 * ll_max44009.h
 *
 *  Created on: October 5, 2017
 *  Author:
 *  Adapted to C and STM32 Low Layer by: Vasile Guta-Ciucur
 *
 *
 * COPYRIGHT(c) 2017 Vasile Guta-Ciucur
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of Author nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef MAX44009_H
#define MAX44009_H

#include "typedefs.h"
#include "sw_i2c_master.h"

#define MAX44009_BASE_ADDR 0x00
#define MAX44009_WR_ADDR   0x94 // if AO is coupled to GND (default on CJMCU MAX44009)
//#define MAX44009_WR_ADDR   0x96 // if AO is coupled to Vcc
#define MAX44009_RD_ADDR   MAX44009_WR_ADDR+1 // reading address, redundant on Tilen i2c implementation

// begin definition of slave addresses for MAX44009
#define MAX44009_REG_INT_STATUS      0x00
#define MAX44009_REG_INT_ENABLE      0x01
#define MAX44009_REG_CONFIG          0x02
#define MAX44009_REG_HIGH_BYTE       0x03
#define MAX44009_REG_LOW_BYTE        0x04
#define MAX44009_REG_THRESH_HIGH     0x05
#define MAX44009_REG_THRESH_LOW      0x06
#define MAX44009_REG_THRESH_TIMER    0x07
// end definition of slave addresses for MAX44009

#define MAX44009_ACK  0
#define MAX44009_NACK 1

/* global variables */
volatile uint16_t _max44009_reg0;
volatile uint16_t _max44009_reg1;

void max44009_write_reg(uint8_t reg, uint8_t value)
{
  uint8_t a, b;
  //
  b = MAX44009_BASE_ADDR;
  a = (b << 1) | MAX44009_WR_ADDR;
  sw_i2c_start();
  sw_i2c_write(a);
  sw_i2c_write(reg);
  sw_i2c_write(value);
  sw_i2c_stop();
}

void max44009_read_reg(uint8_t reg)
{
  uint8_t a, b;
  //
  b = MAX44009_BASE_ADDR;
  a = (b << 1) | MAX44009_WR_ADDR;
  sw_i2c_start();
  sw_i2c_write(a);
  sw_i2c_write(reg);
  sw_i2c_start();
  sw_i2c_write(a | 1);
  _max44009_reg0 = sw_i2c_read(MAX44009_NACK);
  sw_i2c_stop();
}

void max44009_read_2regs(uint8_t reg)
{
  uint8_t a, b;
  //
  b = MAX44009_BASE_ADDR;
  a = (b << 1) | MAX44009_WR_ADDR;
  sw_i2c_start();
  sw_i2c_write(a);
  sw_i2c_write(reg);
  sw_i2c_start();
  sw_i2c_write(a | 1);
  _max44009_reg0 = sw_i2c_read(MAX44009_NACK);
  sw_i2c_start();
  sw_i2c_write(a);
  sw_i2c_write(reg + 1);
  sw_i2c_start();
  sw_i2c_write(a | 1);
  _max44009_reg1 = sw_i2c_read(MAX44009_NACK);
  sw_i2c_stop();
}

void max44009_setup_auto_noint(uint8_t modus_operandi)
{
  //
  max44009_write_reg(MAX44009_REG_INT_ENABLE, 0); // no interrupt
  if (modus_operandi)
    max44009_write_reg(MAX44009_REG_CONFIG, 0x80); // continuous mode
  else
    max44009_write_reg(MAX44009_REG_CONFIG, 0); // cycling at every 800ms
}

/* Calculate LUX (pseudocode)
 * ==========================
 *
 * max44009_read_2regs(MAX44009_REG_HIGH_BYTE);
 * uint8_t exponent = (_max44009_reg0 & 0xf0) >> 4;
 * if(exponent < 0x0F){
 *   uint8_t mant = (_max44009_reg0 & 0x0f) << 4 | _max44009_reg1;
 *   float lux = (float)(((0x00000001 << exponent) * (float)mant) * 0.045);
 * }
 * else error;
 *
 */

#endif // MAX44009_H

