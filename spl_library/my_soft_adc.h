#ifndef __MY_SOFT_ADC_H
#define __MY_SOFT_ADC_H
#include "stm32l1xx_adc.h"

uint16_t sw_read_adc1(uint8_t channel)
{
  /* Add the following line if you use more than one channel. Otherwise,
     move the line in the init function, after enabling the ADC1. */
  ADC_RegularChannelConfig(ADC1, channel, 1, ADC_SampleTime_16Cycles);
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
  ADC_ClearFlag(ADC1, ADC_FLAG_EOC);
  return ADC_GetConversionValue(ADC1);
}

#endif /* __MY_SOFT_ADC_H */
