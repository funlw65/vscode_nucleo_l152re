/* FILE: my_uart.h
 */

#ifndef __MY_UART_H
#define __MY_UART_H
#include "stm32l1xx_usart.h"

uint8_t uart_receive_byte(USART_TypeDef *USARTx)
{
  return (uint8_t)(READ_BIT(USARTx->DR, USART_DR_DR));
}

void uart_transmit_byte(USART_TypeDef *USARTx, uint8_t Value)
{
  USARTx->DR = Value;
}

void uart_putchar(USART_TypeDef* USARTx, uint8_t c)
{
  /**/
  while(!USART_GetFlagStatus(USARTx, USART_FLAG_TXE)) {}
  USART_ClearFlag(USARTx, USART_FLAG_TC);
  uart_transmit_byte(USARTx, c);
  while (!USART_GetFlagStatus(USARTx, USART_FLAG_TC)) {}
}

void uart_putstr(USART_TypeDef* USARTx, uint8_t * s)
{
  uint8_t c;
  while ((c = *s++))
    uart_putchar(USARTx, c);
}

void uart_putstrF(USART_TypeDef* USARTx, const uint8_t * s)
{
  uint8_t c;
  while ((c = *s++))
    uart_putchar(USARTx, c);
}

#endif /*__MY_UART_H*/
