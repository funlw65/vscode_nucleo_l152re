/*
 * ds18b20.h
 *
 *  Created on: Mar 12, 2017
 *      Author: vasi
 *
 * COPYRIGHT(c) 2017 Vasile Guta-Ciucur
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of Author nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef DS18B20_H_
#define DS18B20_H_

#include "typedefs.h"
#include "conversion.h"
#include "onewire1.h"

#ifndef DS18B20_IGNORE_CRC
#define DS18B20_IGNORE_CRC 0
#endif

#ifndef DS18B20_MAX_RETRIES
#define DS18B20_MAX_RETRIES 0
#endif

#define DS18B20_CONVERT_T         0x44
#define DS18B20_READ              0xBE
#define DS18B20_READ_ID           0x33
#define DS18B20_WRITE             0x4E
#define DS18B20_EE_WRITE          0x48
#define DS18B20_EE_RECALL         0xB8
/*#define DS18B20_READ_POWER_SUPPLY 0xB4*/

#define DS18B20_OK         0 /* everything is OK */
#define DS18B20_NO_SENSOR  1 /* no response on the bus */
#define DS18B20_BUS_ERROR  2 /* CRC error */

typedef enum {
  RAW = 0, CELSIUS, FARENHEIT
} temp_type_t;

uint8_t ds18b20_scratchpad[9];
uint8_t ds18b20_formated_temp[7];

void ds18b20_clear_scratchpad(void)
{
  /**/
}

void _ds18b20_match_rom(const uint8_t * id)
{
  /**/
  ow_reset();
  ow_byte_wr(OW_MATCH_ROM);

  /* send the sensor ID */
  ow_byte_wr(id[0]);
  ow_byte_wr(id[1]);
  ow_byte_wr(id[2]);
  ow_byte_wr(id[3]);
  ow_byte_wr(id[4]);
  ow_byte_wr(id[5]);
  ow_byte_wr(id[6]);
  ow_byte_wr(id[7]);
}

void _ds18b20_skip_rom(void)
{
  /**/
  ow_reset();
  ow_byte_wr(OW_SKIP_ROM);
}

bool_t _ds18b20_read(uint8_t nr_bytes)
{
  /**/
  uint8_bits_t crcbyte;
  uint8_bits_t bb;
  bool_t crcbit;
  uint8_t i, j;

  bb.val = 0;
  crcbyte.val = 0;

  for (i = 0; i < nr_bytes; i++) {
    /**/
    bb.val = ow_byte_rd();
    ds18b20_scratchpad[i] = bb.val;
    /**/
    if (DS18B20_IGNORE_CRC == 0) {
      /**/
      for (j = 0; j < 8; j++) {
        /**/
        crcbit = crcbyte.bits.b0 ^ bb.bits.b0;
        crcbyte.val = crcbyte.val >> 1;
        crcbyte.bits.b7 = crcbit;
        crcbyte.bits.b2 = crcbyte.bits.b2 ^ crcbit;
        crcbyte.bits.b3 = crcbyte.bits.b3 ^ crcbit;
        bb.val = bb.val >> 1;
      }
    }
  }

  if (nr_bytes == 9) {
    /**/
    if ((ds18b20_scratchpad[4] == 0xFF) && (ds18b20_scratchpad[5] == 0xFF)
        && (ds18b20_scratchpad[6] == 0xFF)
        && (ds18b20_scratchpad[7] == 0xFF)
        && (ds18b20_scratchpad[8] == 0xFF)) {
      /**/
      ds18b20_scratchpad[0] = 198;
      return FALSE;
    }
  }

  if (DS18B20_IGNORE_CRC == 0)
    return (crcbyte.val == 0);
  return TRUE;
}

void ds18b20_set(int8_t th, int8_t tl, uint8_t resolution)
{
  /**/
  uint8_t reg[3];

  reg[2] = 0x7F; /* default resolution is 12 bits */

  if (resolution == 9)
    reg[2] = 0x1F;
  else if (resolution == 10)
    reg[2] = 0x3F;
  else if (resolution == 11)
    reg[2] = 0x5F;

  reg[0] = (uint8_t) th;
  reg[1] = (uint8_t) tl;

  ow_byte_wr(DS18B20_WRITE);
  ow_byte_wr(reg[0]);
  ow_byte_wr(reg[1]);
  ow_byte_wr(reg[2]);
}

void ds18b20_convert()
{
  /**/
  _ds18b20_skip_rom();
  ow_byte_wr(DS18B20_CONVERT_T);
}

void ds18b20_m_convert(const uint8_t *id)
{
  /**/
  _ds18b20_match_rom(id);
  ow_byte_wr(DS18B20_CONVERT_T);
}

uint8_t ds18b20_s_read_rom()
{
  /**/
  bool_t success_flag;
  uint8_t i;

  if (ow_reset())
    return DS18B20_NO_SENSOR;
  i = 0;
  do {
    /**/
    ow_reset();
    ow_byte_wr(DS18B20_READ_ID);
    success_flag = _ds18b20_read(8);
    i = i + 1;
  } while ((success_flag == FALSE) && (i <= DS18B20_MAX_RETRIES));

  if (success_flag == FALSE)
    return DS18B20_BUS_ERROR;

  return DS18B20_OK;
}

uint8_t ds18b20_s_temp(int16_t *temperature, temp_type_t type_value)
{
  /**/
  int16_bits_t b;
  bool_t success_flag;
  uint8_t i;

  if (ow_reset())
    return DS18B20_NO_SENSOR;

  if (DS18B20_IGNORE_CRC != 0) {
    /**/
    _ds18b20_skip_rom();
    ow_byte_wr(DS18B20_READ);
    b.ch[0] = ow_byte_rd();
    b.ch[1] = ow_byte_rd();
    ow_reset();
    *temperature = b.val;
    success_flag = TRUE;
  } else {
    /**/
    i = 0;
    do {
      /**/
      _ds18b20_skip_rom();
      ow_byte_wr(DS18B20_READ);
      success_flag = _ds18b20_read(9);
      i = i + 1;
    } while ((success_flag == FALSE) && (i <= DS18B20_MAX_RETRIES));
    if (success_flag == FALSE)
      return DS18B20_BUS_ERROR;
    b.ch[0] = ds18b20_scratchpad[0];
    b.ch[1] = ds18b20_scratchpad[1];
  }
  *temperature = b.val; // the RAW value is here

  if (type_value == CELSIUS)
    *temperature = (*temperature * 10) >> 4;

  if (type_value == FARENHEIT)
    *temperature = ((*temperature * 9) >> 3) + 320;

  return DS18B20_OK;
}

uint8_t ds18b20_m_temp(const uint8_t *id, int16_t *temperature,
                       temp_type_t type_value)
{
  /**/
  int16_val_t b;
  bool_t success_flag;
  uint8_t i;

  if (ow_reset())
    return DS18B20_NO_SENSOR;

  if (DS18B20_IGNORE_CRC != 0) {
    /**/
    _ds18b20_match_rom(id);
    ow_byte_wr(DS18B20_READ);
    b.ch[0] = ow_byte_rd();
    b.ch[1] = ow_byte_rd();
    if (ow_reset())
      return DS18B20_NO_SENSOR;
    *temperature = b.val;
    if (*temperature != -1)
      success_flag = TRUE; /* we received something, so probably okay */
  } else {
    /**/
    i = 0;
    do {
      /**/
      _ds18b20_match_rom(id);
      ow_byte_wr(DS18B20_READ);
      success_flag = _ds18b20_read(9);
      i = i + 1;
    } while ((success_flag == FALSE) && (i <= DS18B20_MAX_RETRIES));
    if (success_flag == FALSE)
      return DS18B20_BUS_ERROR;
    b.ch[0] = ds18b20_scratchpad[0];
    b.ch[1] = ds18b20_scratchpad[1];
  }
  *temperature = b.val; // the RAW value is here

  if (type_value == CELSIUS)
    *temperature = (*temperature * 10) >> 4;

  if (type_value == FARENHEIT)
    *temperature = ((*temperature * 9) >> 3) + 320;

  return DS18B20_OK;
}

void ds18b20_temp_format(int16_t temperature)
{
  /**/
  int16_bits_t nibble;
  uint8_t str_temp[6];
  nibble.val = temperature;

  if (nibble.bits.b15) { /* negative value */
    ds18b20_formated_temp[0] = '-';
    nibble.val *= -1; /* make it negative */
  } else
    ds18b20_formated_temp[0] = '+';

  word2dec(nibble.val, str_temp);
  ds18b20_formated_temp[6] = 0;
  ds18b20_formated_temp[5] = str_temp[4];
  ds18b20_formated_temp[4] = '.';
  ds18b20_formated_temp[3] = str_temp[3];
  ds18b20_formated_temp[2] = str_temp[2];
  ds18b20_formated_temp[1] = str_temp[1];
}

#endif /* UTILITIES_STM32L1XX_NUCLEO_DS18B20_H_ */
