/**
 * @author  Tilen Majerle
 * @email   tilen@majerle.eu
 * @website http://stm32f4-discovery.net
 * @link    http://stm32f4-discovery.net/2014/05/library-09-i2c-for-stm32f4xx/
 * @version v1.6.1
 * @ide     Keil uVision
 * @license GNU GPL v3
 * @brief   I2C library for STM32F4xx
 *
 @ ve*rbatim
 ----------------------------------------------------------------------
 Copyright (C) Tilen Majerle, 2015

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ----------------------------------------------------------------------
 @endverbatim
 */

#ifndef I2C_MASTER_H
#define I2C_MASTER_H
#include "stm32l1xx_i2c.h"
#include "typedefs.h"

//#define  I2C_EVENT_MASTER_BYTE_RECEIVED                    ((uint32_t)0x00030040)  /* BUSY, MSL and RXNE flags */
//#define  I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED        ((uint32_t)0x00070082)  /* BUSY, MSL, ADDR, TXE and TRA flags */
//#define  I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED           ((uint32_t)0x00030002)  /* BUSY, MSL and ADDR flags */

//#define CR1_CLEAR_MASK          ((uint16_t)0xFBF5)      /*<! I2C registers Masks */
#define FLAG_MASK               ((uint32_t)0x00FFFFFF)  /*<! I2C FLAG mask */
//#define ITEN_MASK               ((uint32_t)0x07000000)  /*<! I2C Interrupt Enable mask */

#define I2C_TRANSMITTER_MODE   0
#define I2C_RECEIVER_MODE      1
#define I2C_MY_ACK_ENABLE      1
#define I2C_MY_ACK_DISABLE     0

#define I2C_TIMEOUT 20000


ErrorStatus i2c_check_ev(I2C_TypeDef* I2Cx, uint32_t I2C_EVENT)
{
  uint32_t lastevent = 0;
  uint32_t flag1 = 0, flag2 = 0;
  ErrorStatus status = ERROR;

  /* Read the I2Cx status register */
  flag1 = I2Cx->SR1;
  flag2 = I2Cx->SR2;
  flag2 = flag2 << 16;

  /* Get the last event value from I2C status register */
  lastevent = (flag1 | flag2) & FLAG_MASK;

  /* Check whether the last event contains the I2C_EVENT */
  if ((lastevent & I2C_EVENT) == I2C_EVENT)
    status = SUCCESS;
  else
    status = ERROR;
  /* Return status */
  return status;
}

int16_t i2c_start(I2C_TypeDef* I2Cx, uint8_t address, uint8_t direction,
                  uint8_t ack)
{
  /* Generate I2C start pulse */
  I2Cx->CR1 |= I2C_CR1_START;

  /* Wait till I2C is busy */
  uint16_t I2C_Timeout = I2C_TIMEOUT;
  while (!(I2Cx->SR1 & I2C_SR1_SB)) {
    if (--I2C_Timeout == 0x00) {
      return 1;
    }
  }

  /* Enable ack if we select it */
  if (ack) {
    I2Cx->CR1 |= I2C_CR1_ACK;
  }

  /* Send write/read bit */
  if (direction == I2C_TRANSMITTER_MODE) {
    /* Send address with zero last bit */
    I2Cx->DR = address & ~I2C_OAR1_ADD0;

    /* Wait till finished */
    I2C_Timeout = I2C_TIMEOUT;
    while (!(I2Cx->SR1 & I2C_SR1_ADDR)) {
      if (--I2C_Timeout == 0x00) {
        return 1;
      }
    }
  }
  if (direction == I2C_RECEIVER_MODE) {
    /* Send address with 1 last bit */
    I2Cx->DR = address | I2C_OAR1_ADD0;

    /* Wait till finished */
    I2C_Timeout = I2C_TIMEOUT;
    while (!i2c_check_ev(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) {
      if (--I2C_Timeout == 0x00) {
        return 1;
      }
    }
  }

  /* Read status register to clear ADDR flag */
  I2Cx->SR2;

  /* Return 0, everything ok */
  return 0;
}

void i2c_write(I2C_TypeDef* I2Cx, uint8_t data)
{
  /* Wait till I2C is not busy anymore */
  uint16_t I2C_Timeout = I2C_TIMEOUT;
  while (!(I2Cx->SR1 & I2C_SR1_TXE) && I2C_Timeout) {
    I2C_Timeout--;
  }

  /* Send I2C data */
  I2Cx->DR = data;
}

uint8_t i2c_read(I2C_TypeDef* I2Cx, bool_t ack)
{
  uint8_t data;

  if (ack)
    /* Enable ACK */
    I2Cx->CR1 |= I2C_CR1_ACK;
  else {
    /* Disable ACK */
    I2Cx->CR1 &= ~I2C_CR1_ACK;

    /* Generate stop */
    I2Cx->CR1 |= I2C_CR1_STOP;

  }

  /* Wait till not received */
  uint16_t I2C_Timeout = I2C_TIMEOUT;
  while (!i2c_check_ev(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED)) {
    if (--I2C_Timeout == 0x00) {
      return 1;
    }
  }

  /* Read data */
  data = I2Cx->DR;

  /* Return data */
  return data;
}

uint8_t i2c_stop(I2C_TypeDef* I2Cx)
{
  /* Wait till transmitter not empty */
  uint16_t I2C_Timeout = I2C_TIMEOUT;
  while (((!(I2Cx->SR1 & I2C_SR1_TXE)) || (!(I2Cx->SR1 & I2C_SR1_BTF)))) {
    if (--I2C_Timeout == 0x00) {
      return 1;
    }
  }

  /* Generate stop */
  I2Cx->CR1 |= I2C_CR1_STOP;

  /* Return 0, everything ok */
  return 0;
}

#endif /* I2C_MASTER_H */
