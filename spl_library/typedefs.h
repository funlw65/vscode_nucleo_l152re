/*
 * typedefs.h
 *
 *  Created on: Jan 3, 2017
 *      Author: Vasile Guta-Ciucur
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of Author nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 */

#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

typedef enum {
  FALSE = 0, TRUE
} bool_t; /* Undefined size */
typedef enum {
  OFF = 0, ON
} bit_t;

#define LOW  0
#define HIGH  1

#include <stdint.h>

typedef union {
  uint8_t val;
  struct {
    uint8_t b0 :1;
    uint8_t b1 :1;
    uint8_t b2 :1;
    uint8_t b3 :1;
    uint8_t b4 :1;
    uint8_t b5 :1;
    uint8_t b6 :1;
    uint8_t b7 :1;
  } bits;
} uint8_val_t, uint8_bits_t;

typedef union {
  uint16_t val;
  struct {
    uint8_t lb;
    uint8_t hb;
  } bytes;
  struct {
    uint8_t b0 :1;
    uint8_t b1 :1;
    uint8_t b2 :1;
    uint8_t b3 :1;
    uint8_t b4 :1;
    uint8_t b5 :1;
    uint8_t b6 :1;
    uint8_t b7 :1;
    uint8_t b8 :1;
    uint8_t b9 :1;
    uint8_t b10 :1;
    uint8_t b11 :1;
    uint8_t b12 :1;
    uint8_t b13 :1;
    uint8_t b14 :1;
    uint8_t b15 :1;
  } bits;
} uint16_val_t, uint16_bytes_t, uint16_bits_t;

typedef union {
  int16_t val;
  int8_t ch[2];
  struct {
    uint8_t b0 :1;
    uint8_t b1 :1;
    uint8_t b2 :1;
    uint8_t b3 :1;
    uint8_t b4 :1;
    uint8_t b5 :1;
    uint8_t b6 :1;
    uint8_t b7 :1;
    uint8_t b8 :1;
    uint8_t b9 :1;
    uint8_t b10 :1;
    uint8_t b11 :1;
    uint8_t b12 :1;
    uint8_t b13 :1;
    uint8_t b14 :1;
    uint8_t b15 :1;
  } bits;
} int16_val_t, int16_bytes_t, int16_bits_t;

#endif /* TYPEDEFS_H_ */
