/**************************************************************************
  This is a library for Adafruit 0.96" TFT Breakout w/SD card.
  https://www.adafruit.com/product/3533

  Yes, I have purchased this, as per licence ;), see it here:
  https://vpc.home.blog/2019/02/02/adafruit-0-96-160x80-color-tft-display/

  Based on code by:
   - Limor Fried/Ladyada from http://www.adafruit.com.
   - o-m-d from https://github.com/o-m-d/st7735_spi_stm32
   - LonelyWolf from https://github.com/LonelyWolf/stm32.git
   - XBM functions by me (Vasile Guta-Ciucur)
  Adapted to C and modified for VPC by Vasile Guta-Ciucur.
  MIT license.
 **************************************************************************/
#ifndef _ST7735RH_ 
#define _ST7735RH_ 

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <color565.h>
#include <font5x7.h>
#include <font7x11.h>
#include <garmin_digits.h>

/* For the SPI you choose, you have to define following parameters (in VPC)
  Mode = SPI_Mode_Master;
  BaudRatePrescaler = SPI_BaudRatePrescaler_2;
  Direction = SPI_Direction_1Line_Tx;
  CPOL = SPI_CPOL_Low;
  CPHA = SPI_CPHA_1Edge;
  CRCPolynomial = 7;
  DataSize = SPI_DataSize_8b;
  FirstBit = SPI_FirstBit_MSB;
  NSS = SPI_NSS_Soft;

  from the SPI configuration windows...
*/

#ifndef ST7735_SPI_TYPE
#define ST7735_SPI_TYPE 1
#endif

#ifndef ST7735_CS_Pin
#error "Define a GPIO_Output pin with ST7735_CS label using VPC"
#endif

#ifndef ST7735_DC_Pin
#error "Define a GPIO_Output pin with ST7735_DC label using VPC"
#endif

#ifndef ST7735_RST_Pin
#error "Define a GPIO_Output pin with ST7735_RST label using VPC"
#endif

// CS pin macros
#define ST7735_CS_LOW() GPIO_ResetBits(ST7735_CS_Port,ST7735_CS_Pin)
#define ST7735_CS_HIGH() GPIO_SetBits(ST7735_CS_Port,ST7735_CS_Pin)

// DC pin macros
#define ST7735_DC_LOW() GPIO_ResetBits(ST7735_DC_Port,ST7735_DC_Pin)
#define ST7735_DC_HIGH() GPIO_SetBits(ST7735_DC_Port,ST7735_DC_Pin)

// RESET pin macros
#define ST7735_RST_LOW() GPIO_ResetBits(ST7735_RST_Port,ST7735_RST_Pin)
#define ST7735_RST_HIGH() GPIO_SetBits(ST7735_RST_Port,ST7735_RST_Pin)


#if ST7735_SPI_TYPE   == 1
#define ST7735_SPI  SPI1
#elif ST7735_SPI_TYPE == 2
#define ST7735_SPI  SPI2
#elif ST7735_SPI_TYPE == 3
#define ST7735_SPI  SPI3
#else
#error "Unsupported SPI port type."
#endif


#define ST77XX_NOP        0x00
#define ST77XX_SWRESET    0x01
#define ST77XX_RDDID      0x04
#define ST77XX_RDDST      0x09

#define ST77XX_SLPIN      0x10
#define ST77XX_SLPOUT     0x11
#define ST77XX_PTLON      0x12
#define ST77XX_NORON      0x13

#define ST77XX_INVOFF     0x20
#define ST77XX_INVON      0x21
#define ST77XX_DISPOFF    0x28
#define ST77XX_DISPON     0x29
#define ST77XX_CASET      0x2A
#define ST77XX_RASET      0x2B
#define ST77XX_RAMWR      0x2C
#define ST77XX_RAMRD      0x2E

#define ST77XX_PTLAR      0x30
#define ST77XX_TEOFF      0x34
#define ST77XX_TEON       0x35
#define ST77XX_MADCTL     0x36
#define ST77XX_COLMOD     0x3A

#define ST77XX_MADCTL_MY  0x80
#define ST77XX_MADCTL_MX  0x40
#define ST77XX_MADCTL_MV  0x20
#define ST77XX_MADCTL_ML  0x10
#define ST77XX_MADCTL_RGB 0x00

#define ST77XX_RDID1      0xDA
#define ST77XX_RDID2      0xDB
#define ST77XX_RDID3      0xDC
#define ST77XX_RDID4      0xDD


#define ST7735_TFTWIDTH_80 80
#define ST7735_TFTHEIGHT_160 160

//#define ST7735_IS_160X80 1
//#define ST7735_XSTART 24
//#define ST7735_YSTART 0
//#define ST7735_WIDTH  80
//#define ST7735_HEIGHT 160
//#define ST7735_ROTATION (ST7735_MADCTL_MX | ST7735_MADCTL_MY | ST7735_MADCTL_BGR)


#define INITR_MINI160x80 0x04
#define INITR_HALLOWING 0x05

// Some register settings
#define ST7735_MADCTL_BGR 0x08
#define ST7735_MADCTL_MH  0x04

#define ST7735_FRMCTR1    0xB1
#define ST7735_FRMCTR2    0xB2
#define ST7735_FRMCTR3    0xB3
#define ST7735_INVCTR     0xB4
#define ST7735_DISSET5    0xB6

#define ST7735_PWCTR1     0xC0
#define ST7735_PWCTR2     0xC1
#define ST7735_PWCTR3     0xC2
#define ST7735_PWCTR4     0xC3
#define ST7735_PWCTR5     0xC4
#define ST7735_VMCTR1     0xC5

#define ST7735_PWCTR6     0xFC

#define ST7735_GMCTRP1    0xE0
#define ST7735_GMCTRN1    0xE1


// Some ready-made 16-bit ('565') color settings:
#define ST7735_BLACK COLOR565_BLACK
#define ST7735_WHITE COLOR565_WHITE
#define ST7735_RED COLOR565_RED
#define ST7735_GREEN COLOR565_GREEN
#define ST7735_BLUE COLOR565_BLUE
#define ST7735_CYAN COLOR565_CYAN
#define ST7735_MAGENTA COLOR565_MAGENTA
#define ST7735_YELLOW COLOR565_YELLOW
#define ST7735_ORANGE COLOR565_DARK_ORANGE

typedef enum {
  scr_normal = 0,
  scr_CW     = 1,
  scr_180    = 2,
  scr_CCW    = 3
} ScrOrientation_TypeDef;

typedef enum {
  BATT_EMPTY  = 0,
  BATT_LOW    = 1,
  BATT_MEDIUM = 2,
  BATT_FULL   = 3
} BattStatus_TypeDef;


static const uint8_t scr_colstart = 24, scr_rowstart = 0;
uint16_t scr_width;
uint16_t scr_height;
uint8_t  scr_xstart, scr_ystart;

void ST7735_write(uint8_t data)
{
  while (SPI_I2S_GetFlagStatus(ST7735_SPI,SPI_I2S_FLAG_TXE) == RESET);
  SPI_I2S_SendData(ST7735_SPI,data);
}

void ST7735_cmd(uint8_t cmd)
{
  ST7735_DC_LOW();
  ST7735_write(cmd);
  while (SPI_I2S_GetFlagStatus(ST7735_SPI,SPI_I2S_FLAG_BSY) == SET);
}

void ST7735_data(uint8_t data)
{
  ST7735_DC_HIGH();
  ST7735_write(data);
  while (SPI_I2S_GetFlagStatus(ST7735_SPI,SPI_I2S_FLAG_BSY) == SET);
}


uint16_t ST7735_color565(uint8_t red, uint8_t green, uint8_t blue)
{
  return ((red & 0xF8) << 8) | ((green & 0xFC) << 3) | (blue >> 3);
}


void ST7735_AddrSet(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1)
{
  uint8_t data[] = { 0x00, x0 + scr_xstart, 0x00, x1 + scr_xstart };
  ST7735_cmd(ST77XX_CASET); // Column addr set
  ST7735_data(data[0]);
  ST7735_data(data[1]);
  ST7735_data(data[2]);
  ST7735_data(data[3]);
  ST7735_cmd(ST77XX_RASET); // Row addr set
  data[1] = y0 + scr_ystart;
  data[3] = y1 + scr_ystart;
  ST7735_data(data[0]);
  ST7735_data(data[1]);
  ST7735_data(data[2]);
  ST7735_data(data[3]);
  ST7735_cmd(ST77XX_RAMWR); // write to RAM
}

void ST7735_RotationSet(ScrOrientation_TypeDef m)
{
  uint8_t madctl = 0;

  uint8_t rotation = m & 3; // can't be higher than 3

  switch (rotation) {
  case scr_CW:
    madctl = ST77XX_MADCTL_MY | ST77XX_MADCTL_MV | ST77XX_MADCTL_RGB;
    scr_width = ST7735_TFTHEIGHT_160;
    scr_height = ST7735_TFTWIDTH_80;
    scr_xstart = scr_rowstart;
    scr_ystart = scr_colstart;
    break;
  case scr_180:
    madctl = ST77XX_MADCTL_RGB;
    scr_height = ST7735_TFTHEIGHT_160;
    scr_width = ST7735_TFTWIDTH_80;
    scr_xstart = scr_colstart;
    scr_ystart = scr_rowstart;
    break;
  case scr_CCW:
    madctl = ST77XX_MADCTL_MX | ST77XX_MADCTL_MV | ST77XX_MADCTL_RGB;
    scr_width = ST7735_TFTHEIGHT_160;
    scr_height = ST7735_TFTWIDTH_80;
    scr_xstart = scr_rowstart;
    scr_ystart = scr_colstart;
    break;
  default:// scr_normal
    madctl = ST77XX_MADCTL_MX | ST77XX_MADCTL_MY | ST77XX_MADCTL_RGB;
    scr_height = ST7735_TFTHEIGHT_160;
    scr_width = ST7735_TFTWIDTH_80;
    scr_xstart = scr_colstart;
    scr_ystart = scr_rowstart;
    break;
  }
  ST7735_CS_LOW();
  ST7735_cmd(ST77XX_MADCTL);
  ST7735_data(madctl);
  ST7735_CS_HIGH();
}

void ST7735_Clear(uint16_t color)
{
  uint16_t i;
  uint8_t data[] = { color >> 8, color & 0xFF };

  ST7735_CS_LOW();
  ST7735_AddrSet(0,0,scr_width - 1,scr_height - 1);
  for (i = 0; i < scr_width * scr_height; i++) {
    ST7735_data(data[0]);
    ST7735_data(data[1]);
  }
  ST7735_CS_HIGH();
}

void ST7735_Pixel(uint16_t X, uint16_t Y, uint16_t color)
{
  uint8_t data[] = { color >> 8, color & 0xFF };
  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y,X,Y);
  ST7735_data(data[0]);
  ST7735_data(data[1]);
  ST7735_CS_HIGH();
}

void ST7735_HLine(uint16_t X1, uint16_t X2, uint16_t Y, uint16_t color)
{
  uint16_t i;
  uint8_t CH = color >> 8;
  uint8_t CL = color & 0xFF;

  ST7735_CS_LOW();
  ST7735_AddrSet(X1,Y,X2,Y);
  for (i = 0; i <= (X2 - X1); i++) {
    ST7735_data(CH);
    ST7735_data(CL);
  }
  ST7735_CS_HIGH();
}

void ST7735_VLine(uint16_t X, uint16_t Y1, uint16_t Y2, uint16_t color)
{
  uint16_t i;
  uint8_t CH = color >> 8;
  uint8_t CL = color & 0xFF;

  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y1,X,Y2);
  for (i = 0; i <= (Y2 - Y1); i++) {
    ST7735_data(CH);
    ST7735_data(CL);
  }
  ST7735_CS_HIGH();
}

void ST7735_Line(int16_t X1, int16_t Y1, int16_t X2, int16_t Y2, uint16_t color)
{
  int16_t dX = X2-X1;
  int16_t dY = Y2-Y1;
  int16_t dXsym = (dX > 0) ? 1 : -1;
  int16_t dYsym = (dY > 0) ? 1 : -1;

  if (dX == 0) {
    if (Y2>Y1) ST7735_VLine(X1,Y1,Y2,color);
    else ST7735_VLine(X1,Y2,Y1,color);
    return;
  }
  if (dY == 0) {
    if (X2>X1) ST7735_HLine(X1,X2,Y1,color);
    else ST7735_HLine(X2,X1,Y1,color);
    return;
  }

  dX *= dXsym;
  dY *= dYsym;
  int16_t dX2 = dX << 1;
  int16_t dY2 = dY << 1;
  int16_t di;

  if (dX >= dY) {
    di = dY2 - dX;
    while (X1 != X2) {
      ST7735_Pixel(X1,Y1,color);
      X1 += dXsym;
      if (di < 0) {
        di += dY2;
      } else {
        di += dY2 - dX2;
        Y1 += dYsym;
      }
    }
  } else {
    di = dX2 - dY;
    while (Y1 != Y2) {
      ST7735_Pixel(X1,Y1,color);
      Y1 += dYsym;
      if (di < 0) {
        di += dX2;
      } else {
        di += dX2 - dY2;
        X1 += dXsym;
      }
    }
  }
  ST7735_Pixel(X1,Y1,color);
}

void ST7735_Rect(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t color)
{
  ST7735_HLine(X1,X2,Y1,color);
  ST7735_HLine(X1,X2,Y2,color);
  ST7735_VLine(X1,Y1,Y2,color);
  ST7735_VLine(X2,Y1,Y2,color);
}

void ST7735_FillRect(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t color)
{
  uint16_t i;
  uint16_t FS = (X2 - X1 + 1) * (Y2 - Y1 + 1);
  uint8_t CH = color >> 8;
  uint8_t CL = color & 0xFF;

  ST7735_CS_LOW();
  ST7735_AddrSet(X1, Y1, X2, Y2);
  for (i = 0; i < FS; i++) {
    ST7735_data(CH);
    ST7735_data(CL);
  }
  ST7735_CS_HIGH();
}

void ST7735_PutChar5x7(uint8_t scale, uint16_t X, uint16_t Y, uint8_t chr, uint16_t color, uint16_t bgcolor)
{
  uint16_t i,j;
  uint8_t buffer[5];
  uint8_t CH = color >> 8;
  uint8_t CL = color & 0xFF;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;

  if ((chr >= 0x20) && (chr <= 0x7F)) {
    // ASCII[0x20-0x7F]
    memcpy(buffer,&Font5x7[(chr - 32) * 5], 5);
  } else if (chr >= 0xA0) {
    // CP1251[0xA0-0xFF]
    memcpy(buffer,&Font5x7[(chr - 64) * 5], 5);
  } else {
    // unsupported symbol
    memcpy(buffer,&Font5x7[160], 5);
  }


  // scale equals 1 drawing faster
  if (scale == 1) {
    ST7735_CS_LOW();
    ST7735_AddrSet(X, Y, X + 5, Y + 7);
    for (j = 0; j < 7; j++) {
      for (i = 0; i < 5; i++) {
        if ((buffer[i] >> j) & 0x01) {
          ST7735_data(CH);
          ST7735_data(CL);
        } else {
          ST7735_data(BCH);
          ST7735_data(BCL);
        }
      }
      // vertical spacing
      ST7735_data(BCH);
      ST7735_data(BCL);
    }

    // horizontal spacing
    for (i = 0; i < 6; i++) {
      ST7735_data(BCH);
      ST7735_data(BCL);
    }
    ST7735_CS_HIGH();
  } else {
    for (j = 0; j < 7; j++) {
      for (i = 0; i < 5; i++) {
        // pixel group
        ST7735_FillRect(X + (i * scale), Y + (j * scale), X + (i * scale) + scale - 1, Y + (j * scale) + scale - 1, ((buffer[i] >> j) & 0x01) ? color : bgcolor);
      }
      // vertical spacing
      // ST7735_FillRect(X + (i * scale), Y + (j * scale), X + (i * scale) + scale - 1, Y + (j * scale) + scale - 1, V_SEP);
      ST7735_FillRect(X + (i * scale), Y + (j * scale), X + (i * scale) + scale - 1, Y + (j * scale) + scale - 1, bgcolor);
    }
    // horizontal spacing
    // ST7735_FillRect(X, Y + (j * scale), X + (i * scale) + scale - 1, Y + (j * scale) + scale - 1, H_SEP);
    ST7735_FillRect(X, Y + (j * scale), X + (i * scale) + scale - 1, Y + (j * scale) + scale - 1, bgcolor);
  }
}

void ST7735_PutStr5x7(uint8_t scale, uint8_t X, uint8_t Y, char *str, uint16_t color, uint16_t bgcolor)
{
  // scale equals 1 drawing faster
  if (scale == 1) {
    while (*str) {
      ST7735_PutChar5x7(scale, X,Y,*str++,color,bgcolor);
      if (X < scr_width - 6) {
        X += 6;
      } else if (Y < scr_height - 8) {
        X = 0;
        Y += 8;
      } else {
        X = 0;
        Y = 0;
      }
    };
  } else {
    while (*str) {
      ST7735_PutChar5x7(scale, X,Y,*str++,color,bgcolor);
      if (X < scr_width - (scale*5) + scale) {
        X += (scale * 5) + scale;
      } else if (Y < scr_height - (scale * 7) + scale) {
        X = 0;
        Y += (scale * 7) + scale;
      } else {
        X = 0;
        Y = 0;
      }
    };
  }
}

void ST7735_PutChar7x11(uint16_t X, uint16_t Y, uint8_t chr, uint16_t color, uint16_t bgcolor)
{
  uint16_t i,j;
  uint8_t buffer[11];
  uint8_t CH = color >> 8;
  uint8_t CL = color & 0xFF;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;

  if ((chr >= 0x20) && (chr <= 0x7F)) {
    // ASCII[0x20-0x7F]
    memcpy(buffer,&Font7x11[(chr - 32) * 11], 11);
  } else if (chr >= 0xA0) {
    // CP1251[0xA0-0xFF]
    memcpy(buffer,&Font7x11[(chr - 64) * 11], 11);
  } else {
    // unsupported symbol
    memcpy(buffer,&Font7x11[160], 11);
  }

  ST7735_CS_LOW();
  ST7735_AddrSet(X, Y, X + 7, Y + 11);
  for (i = 0; i < 11; i++) {
    for (j = 0; j < 7; j++) {
      if ((buffer[i] >> j) & 0x01) {
        ST7735_data(CH);
        ST7735_data(CL);
      } else {
        ST7735_data(BCH);
        ST7735_data(BCL);
      }
    }
    // vertical spacing
    ST7735_data(BCH);
    ST7735_data(BCL);
  }

  // horizontal spacing
  for (i = 0; i < 8; i++) {
    ST7735_data(BCH);
    ST7735_data(BCL);
  }

  ST7735_CS_HIGH();
}

void ST7735_PutStr7x11(uint8_t X, uint8_t Y, char *str, uint16_t color, uint16_t bgcolor)
{
  while (*str) {
    ST7735_PutChar7x11(X,Y,*str++,color,bgcolor);
    if (X < scr_width - 8) {
      X += 8;
    } else if (Y < scr_height - 12) {
      X = 0;
      Y += 12;
    } else {
      X = 0;
      Y = 0;
    }
  };
}

void ST7735_BigDg(uint8_t digit, uint16_t X, uint16_t Y, uint16_t color, uint16_t bgcolor)
{
  uint8_t i,j;
  uint8_t CH = color >> 8;
  uint8_t CL = (uint8_t)color;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;

  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y,X + 15,Y + 43);
  for (j = 0; j < 44; j++) {
    for (i = 0; i < 16; i++) {
      if ((garmin_big_digits[(digit * 96) + i + (j / 8) * 16] >> (j % 8)) & 0x01) {
        ST7735_data(CH);
        ST7735_data(CL);
      } else {
        ST7735_data(BCH);
        ST7735_data(BCL);
      }
    }
  }
  ST7735_CS_HIGH();
}

void ST7735_MidDg(uint8_t digit, uint16_t X, uint16_t Y, uint16_t color, uint16_t bgcolor)
{
  uint8_t i,j;
  uint8_t CH = color >> 8;
  uint8_t CL = (uint8_t)color;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;

  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y,X + 11,Y + 23);
  for (j = 0; j < 24; j++) {
    for (i = 0; i < 12; i++) {
      if ((garmin_mid_digits[(digit * 36) + i + (j / 8) * 12] >> (j % 8)) & 0x01) {
        ST7735_data(CH);
        ST7735_data(CL);
      } else {
        ST7735_data(BCH);
        ST7735_data(BCL);
      }
    }
  }
  ST7735_CS_HIGH();
}

void ST7735_SmallDg(uint8_t digit, uint16_t X, uint16_t Y, uint16_t color, uint16_t bgcolor)
{
  uint8_t i,j;
  uint8_t CH = color >> 8;
  uint8_t CL = (uint8_t)color;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;

  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y,X + 10,Y + 20);
  for (j = 0; j < 21; j++) {
    for (i = 0; i < 11; i++) {
      if ((garmin_small_digits[(digit * 33) + i + (j / 8) * 11] >> (j % 8)) & 0x01) {
        ST7735_data(CH);
        ST7735_data(CL);
      } else {
        ST7735_data(BCH);
        ST7735_data(BCL);
      }
    }
  }
  ST7735_CS_HIGH();
}

void ST7735_BattIcn(BattStatus_TypeDef batt, uint16_t X, uint16_t Y, uint16_t color, uint16_t bgcolor)
{
  uint8_t i,j;
  uint8_t CH = color >> 8;
  uint8_t CL = (uint8_t)color;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;
  uint8_t x1_orig = 5;
  uint8_t x2_orig = 12; 
  uint8_t x3_orig = 19;  

  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y,X + 27,Y + 7);
  for (j = 0; j < 8; j++) {
    for (i = 0; i < 28; i++) {
      if (garmin_battery_icon[i] >> j & 0x01) {
        ST7735_data(CH);
        ST7735_data(CL);
      } else {
        ST7735_data(BCH);
        ST7735_data(BCL);
      }
    }
  }
  ST7735_CS_HIGH();  
  if (batt > 0){
    //
    if (batt == BATT_FULL){
      //
      ST7735_FillRect(X+x1_orig, Y+3, X+x1_orig+5, Y+3+4, color);
      ST7735_FillRect(X+x2_orig, Y+3, X+x2_orig+5, Y+3+4, color); 
      ST7735_FillRect(X+x3_orig, Y+3, X+x3_orig+5, Y+3+4, color); 
    }
    if (batt == BATT_MEDIUM){
      //
      ST7735_FillRect(X+x2_orig, Y+3, X+x2_orig+5, Y+3+4, color); 
      ST7735_FillRect(X+x3_orig, Y+3, X+x3_orig+5, Y+3+4, color); 
    }
    if (batt == BATT_LOW){
      //
      ST7735_FillRect(X+x3_orig, Y+3, X+x3_orig+5, Y+3+4, color); 
    }
  }
}

void ST7735_PutXBM64x64(uint16_t X, uint16_t Y, uint16_t color, uint16_t bgcolor, const uint8_t *buff)
{
  uint8_t i,j, /*xb = 0,*/ yb;
  uint16_t idx = 0;
  uint8_t CH = color >> 8;
  uint8_t CL = (uint8_t)color;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;
  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y,X + 63,Y + 63);
  for (yb=0;yb<64;yb++) {
    for (j=0;j<8;j++){
      for(i=0;i<8;i++){
        if (buff[idx] >> i & 0x01) {
          ST7735_data(CH);
          ST7735_data(CL);
        } else {
          ST7735_data(BCH);
          ST7735_data(BCL);
        }  
        //xb=xb+1; 
        //if(xb==64) xb = 0;
      }
      idx=idx+1;
    }
  }
  ST7735_CS_HIGH();
}

void ST7735_PutXBM48x48(uint16_t X, uint16_t Y, uint16_t color, uint16_t bgcolor, const uint8_t *buff)
{
  uint8_t i,j, /*xb = 0,*/ yb;
  uint16_t idx = 0;
  uint8_t CH = color >> 8;
  uint8_t CL = (uint8_t)color;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;
  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y,X + 47,Y + 47);
  for (yb=0;yb<48;yb++) {
    for (j=0;j<6;j++){
      for(i=0;i<8;i++){
        if (buff[idx] >> i & 0x01) {
          ST7735_data(CH);
          ST7735_data(CL);
        } else {
          ST7735_data(BCH);
          ST7735_data(BCL);
        }
        //xb=xb+1;
        //if(xb==48) xb = 0;
      }
      idx=idx+1;
    }
  }
  ST7735_CS_HIGH();
}

void ST7735_PutXBM32x32(uint16_t X, uint16_t Y, uint16_t color, uint16_t bgcolor, const uint8_t *buff)
{
  uint8_t i,j, /*xb = 0,*/ yb;
  uint16_t idx = 0;
  uint8_t CH = color >> 8;
  uint8_t CL = (uint8_t)color;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;
  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y,X + 31,Y + 31);
  for (yb=0;yb<32;yb++) {
    for (j=0;j<4;j++){
      for(i=0;i<8;i++){
        if (buff[idx] >> i & 0x01) {
          ST7735_data(CH);
          ST7735_data(CL);
        } else {
          ST7735_data(BCH);
          ST7735_data(BCL);
        }
        //xb=xb+1;
        //if(xb==32) xb = 0;
      }
      idx=idx+1;
    }
  }
  ST7735_CS_HIGH();
}

void ST7735_PutXBM16x16(uint16_t X, uint16_t Y, uint16_t color, uint16_t bgcolor, const uint8_t *buff)
{
  uint8_t i,j, /*xb = 0,*/ yb;
  uint16_t idx = 0;
  uint8_t CH = color >> 8;
  uint8_t CL = (uint8_t)color;
  uint8_t BCH = bgcolor >> 8;
  uint8_t BCL = bgcolor & 0xFF;
  ST7735_CS_LOW();
  ST7735_AddrSet(X,Y,X + 15,Y + 15);
  for (yb=0;yb<16;yb++) {
    for (j=0;j<2;j++){
      for(i=0;i<8;i++){
        if (buff[idx] >> i & 0x01) {
          ST7735_data(CH);
          ST7735_data(CL);
        } else {
          ST7735_data(BCH);
          ST7735_data(BCL);
        }
        //xb=xb+1; 
        //if(xb==16) xb = 0;
      }
      idx=idx+1;
    }
  }
  ST7735_CS_HIGH();
}

void ST7735_invert(bool invert)
{
  ST7735_CS_LOW();
  ST7735_cmd(invert ? ST77XX_INVON : ST77XX_INVOFF);
  ST7735_CS_HIGH();
}

void ST7735_init(void)
{
  uint8_t data = 0xC0;

  scr_height   = ST7735_TFTHEIGHT_160;
  scr_width    = ST7735_TFTWIDTH_80;
  scr_xstart = scr_colstart;
  scr_ystart = scr_rowstart;

  ST7735_CS_LOW();
  ST7735_cmd(ST77XX_SWRESET);
  my_delay_ms(  150);
  ST7735_cmd(ST77XX_SLPOUT);
  my_delay_ms(  255);
  ST7735_cmd(ST7735_FRMCTR1);
  ST7735_data(0x01);
  ST7735_data(0x2C);
  ST7735_data(0x2D);
  ST7735_cmd(ST7735_FRMCTR2);
  ST7735_data(0x01);
  ST7735_data(0x2C);
  ST7735_data(0x2D);
  ST7735_cmd(ST7735_FRMCTR3);
  ST7735_data(0x01);
  ST7735_data(0x2C);
  ST7735_data(0x2D);
  ST7735_data(0x01);
  ST7735_data(0x2C);
  ST7735_data(0x2D);
  ST7735_cmd(ST7735_INVCTR);
  ST7735_data(0x07);
  ST7735_cmd(ST7735_PWCTR1);
  ST7735_data(0xA2);
  ST7735_data(0x02);
  ST7735_data(0x84);
  ST7735_cmd(ST7735_PWCTR2);
  ST7735_data(0xC5);
  ST7735_cmd(ST7735_PWCTR3);
  ST7735_data(0x0A);
  ST7735_data(0x00);
  ST7735_cmd(ST7735_PWCTR4);
  ST7735_data(0x8A);
  ST7735_data(0x2A);
  ST7735_cmd(ST7735_PWCTR5);
  ST7735_data(0x8A);
  ST7735_data(0xEE);
  ST7735_cmd(ST7735_VMCTR1);
  ST7735_data(0x0E);
  ST7735_cmd(ST77XX_INVOFF);
  ST7735_cmd(ST77XX_MADCTL);
  ST7735_data(0xC8);
  ST7735_cmd(ST77XX_COLMOD);
  ST7735_data(0x05);
  //
  ST7735_cmd(ST77XX_CASET);
  ST7735_data(0x00);
  ST7735_data(0x00);
  ST7735_data(0x00);
  ST7735_data(0x4F);
  ST7735_cmd(ST77XX_RASET);
  ST7735_data(0x00);
  ST7735_data(0x00);
  ST7735_data(0x00);
  ST7735_data(0x9F);
  //
  ST7735_cmd(ST7735_GMCTRP1);
  ST7735_data(0x02);
  ST7735_data(0x1c);
  ST7735_data(0x07);
  ST7735_data(0x12);
  ST7735_data(0x37);
  ST7735_data(0x32);
  ST7735_data(0x29);
  ST7735_data(0x2d);
  ST7735_data(0x29);
  ST7735_data(0x25);
  ST7735_data(0x2B);
  ST7735_data(0x39);
  ST7735_data(0x00);
  ST7735_data(0x01);
  ST7735_data(0x03);
  ST7735_data(0x10);
  ST7735_cmd(ST7735_GMCTRN1);
  ST7735_data(0x03);
  ST7735_data(0x1d);
  ST7735_data(0x07);
  ST7735_data(0x06);
  ST7735_data(0x2E);
  ST7735_data(0x2C);
  ST7735_data(0x29);
  ST7735_data(0x2D);
  ST7735_data(0x2E);
  ST7735_data(0x2E);
  ST7735_data(0x37);
  ST7735_data(0x3F);
  ST7735_data(0x00);
  ST7735_data(0x00);
  ST7735_data(0x02);
  ST7735_data(0x10);
  ST7735_cmd(ST77XX_NORON);
  my_delay_ms(10);
  ST7735_cmd(ST77XX_DISPON);
  my_delay_ms(100);

  ST7735_cmd(ST77XX_MADCTL);
  ST7735_data(data);
  ST7735_CS_HIGH();

  ST7735_RotationSet(scr_normal);
}

#endif //_ST7735RH_
