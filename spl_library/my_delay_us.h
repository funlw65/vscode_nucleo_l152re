/*
 ===============================================================================
 Microsecond delay
 ===============================================================================
 * @date    31-Jan-2016
 * @author  Mapple Library

 * ...for 1MHz or more	core frequency clock

 * This library doesn't generate precise us delay, so be careful - take a few us more/less
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of author nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DELAY_US_H
#define __DELAY_US_H

// Tuned for SystemCoreClock = 32MHz and SPL library
// - us *= 24 when compiler optimization is O1, O2 or Os for the rest of the program.
// - us *= 32 when compiler optimization is O3 for the rest of the program. 
// - for O0 optimization, I can't find a suitable value as I don't have an oscilloscope.
// - the my_delay_us() functuion is alway at O3 optimization - don't touch that.
#pragma GCC push_options
#pragma GCC optimize ("O3")
static __INLINE void my_delay_us(uint32_t us)
{
  us *= 24; // it was 32
  us--;
  asm volatile("   mov r0, %[us]\n\t"
               "1: subs r0, #5\n\t"
               "   bhi 1b\n\t"
               :
               : [us] "r" (us)
               : "r0");
}
#pragma GCC pop_options

#endif /* __DELAY_US_H */

