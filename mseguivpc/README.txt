mseguivpc is a rewrite of vpc in order to reduce the library dependencies.
for that matter, does not need gtk2, gtk3, qt4, qt5 for it's GUI.
it has a better protection for projects that are opened from workspace.
better documentation and better user interaction.
comes with Visual Studio Code 1.53.2 support

so it will continue from where vpc stopped but is not yet there.
 - it should not take long as "translation" goes smoothly.

work in progress.
the executable is for linux_x64.

DRAWBACK: works well on Intel and NVidia graphics but has problems with AMD/Radeon (open drivers) on Linux/UNIX systems.
