/*
 * ll_tim6_1ms_tick.h
 *
 *  Created on: Sep 19, 2017
 *      Author: Vasile Guta-Ciucur
 *      Tools: STM32CubeMX 4.22.1 with STM32CubeL1 1.8.0 LL drivers
 *      Board: Nucleo L152RE
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef TIM6_1MS_TICK_H_
#define TIM6_1MS_TICK_H_

#include "stm32l1xx_ll_tim.h"

volatile uint32_t my_tim6_tick;

/* TIM6 init function */
static void TIM6_init(void) {

	LL_TIM_InitTypeDef TIM_InitStruct;

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock (LL_APB1_GRP1_PERIPH_TIM6);

	/* TIM6 interrupt Init */
	NVIC_SetPriority(TIM6_IRQn,
			NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
	NVIC_EnableIRQ (TIM6_IRQn);
	/* setup for Nucleo L152RE board */
	TIM_InitStruct.Prescaler = 3199;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 9;
	LL_TIM_Init(TIM6, &TIM_InitStruct);

	LL_TIM_SetTriggerOutput(TIM6, LL_TIM_TRGO_UPDATE);

	LL_TIM_DisableMasterSlaveMode (TIM6);

	/* Enable the update interrupt */
	LL_TIM_EnableIT_UPDATE(TIM6);
	/* Enable counter */
	LL_TIM_EnableCounter(TIM6);
	/* Force update generation */
	LL_TIM_GenerateEvent_UPDATE(TIM6);
	my_tim6_tick = 0;
}

/**
 * This function handles TIM6 global interrupt.
 */
void TIM6_IRQHandler(void) {
	/* Check whether update interrupt is pending */
	if (LL_TIM_IsActiveFlag_UPDATE(TIM6) == 1) {
		/* Clear the update interrupt flag*/
		LL_TIM_ClearFlag_UPDATE (TIM6);
	}

	my_tim6_tick++;
}

void TIM6_ms_delay(uint32_t ms) {
	/**/
	uint32_t tmp = my_tim6_tick + ms;
	while (my_tim6_tick < tmp)
		;
}

#endif /*TIM6_1MS_TICK_H_*/

