/**
 ******************************************************************************
 * File Name          : ll_onewire1.h
 * Description        : one wire protocol
 ******************************************************************************
 *
 * COPYRIGHT(c) 2017 Vasile Guta-Ciucur
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

#ifndef NUCLEO_ONEWIRE1_H_
#define NUCLEO_ONEWIRE1_H_

#include "stm32l1xx.h"
#include "typedefs.h"
#include "stm32l1xx_ll_gpio.h"
#include "ll_my_gpio.h"
#include "ll_my_delay_us.h"

#ifndef ONE_W_Pin
#define ONE_W_Pin GPIO_PIN_1 /* it is Arduino A1 on Arduino socket of Nucleo board*/
#endif

#ifndef ONE_W_GPIO_Port
#define ONE_W_GPIO_Port GPIOA
#endif

/* Recovery time (T_Rec) minimum 1usec - increase for long lines
 * 5 usecs is a value given in some Maxim AppNotes
 * 30u secs seem to be reliable for longer lines
 * OW_RECOVERY_TIME      =  5  -- usec
 * OW_RECOVERY_TIME      = 30  -- usec
 */
#define  OW_RECOVERY_TIME       10  // usec
#define  OW_CONF_DELAYOFFSET     0
//
#define  OW_MATCH_ROM    0x55
#define  OW_SKIP_ROM     0xCC
#define  OW_SEARCH_ROM   0xF0
//
#define  OW_SEARCH_FIRST 0xFF  // start new search
#define  OW_PRESENCE_ERR 0xFF
#define  OW_DATA_ERR     0xFE
#define  OW_DATA_OK      0x10
#define  OW_LAST_DEVICE  0x00  // last device found
// rom-code size including CRC
#define  OW_ROMCODE_SIZE 8

#define  OW_CRC8INIT     0x00
#define  OW_CRC8POLY     0x18  // 0x18 = X xor 8 + X xor 5 + X xor 4 + X xor 0

#define  OW_OUTPUT       0x00
#define  OW_INPUT        0x01

static void ow_no_interrupts(void) {
	__asm("cpsid i");
}

static void ow_interrupts(void) {
	__asm("cpsie i");
}

uint8_t ow_bus_rd(void) {
	return (uint8_t) LL_GPIO_IsInputPinSet(ONE_W_GPIO_Port, ONE_W_Pin);
}

void ow_bus_wr(uint8_t b) {
	LL_GPIO_WritePin(ONE_W_GPIO_Port, ONE_W_Pin, b);
}

void ow_bus_direction(uint8_t d) {
	LL_GPIO_InitTypeDef GPIO_InitStruct;
	if (d == OW_OUTPUT) {
		/*Configure GPIO pin as output: ONE_W_Pin */
		LL_GPIO_ResetOutputPin(ONE_W_GPIO_Port, ONE_W_Pin);
		GPIO_InitStruct.Pin = ONE_W_Pin;
		GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
		GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
		LL_GPIO_Init(ONE_W_GPIO_Port, &GPIO_InitStruct);
	} else {
		/*Configure GPIO pin as input : ONE_W_Pin */
		GPIO_InitStruct.Pin = ONE_W_Pin;
		GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
		LL_GPIO_Init(ONE_W_GPIO_Port, &GPIO_InitStruct);
	}
}

uint8_t ow_reset(void) {
	/**/
	uint8_t r;
	ow_no_interrupts();
	ow_bus_wr(LOW);
	ow_bus_direction(OW_OUTPUT);
	ow_interrupts();
	my_delay_us(500);
	ow_no_interrupts();
	ow_bus_direction(OW_INPUT);
	my_delay_us(80);
	r = ow_bus_rd();
	ow_interrupts();
	my_delay_us(420);
	return r; /* r = 0 Ok, r = 1 Error */
}

void ow_bit_wr(uint8_t v) {
	/**/
	if (v & 1) {
		ow_no_interrupts();
		ow_bus_wr(LOW);
		ow_bus_direction(OW_OUTPUT); /* drive output low*/
		my_delay_us(10);
		ow_bus_wr(HIGH); /* drive output high */
		ow_interrupts();
		my_delay_us(55);
	} else {
		ow_no_interrupts();
		ow_bus_wr(LOW);
		ow_bus_direction(OW_OUTPUT); /* drive output low*/
		my_delay_us(65);
		ow_bus_wr(HIGH); /* drive output high*/
		ow_interrupts();
		my_delay_us(5);
	}
}

uint8_t ow_bit_rd(void) {
	/**/
	uint8_t r;

	ow_no_interrupts();
	ow_bus_wr(LOW);
	ow_bus_direction(OW_OUTPUT);
	my_delay_us(3);
	ow_bus_direction(OW_INPUT); /* let pin float, pull up will raise*/
	my_delay_us(10);
	r = ow_bus_rd();
	ow_interrupts();
	my_delay_us(53);
	return r;
}

void ow_byte_wr(uint8_t v) {
	/**/
	uint8_t bitMask;

	for (bitMask = 0x01; bitMask; bitMask <<= 1) {
		ow_bit_wr((bitMask & v) ? 1 : 0);
	}

	ow_no_interrupts();
	ow_bus_direction(OW_INPUT);
	ow_bus_wr(LOW);
	ow_interrupts();
}

uint8_t ow_byte_rd(void) {
	/**/
	uint8_t bitMask;
	uint8_t r = 0;

	for (bitMask = 0x01; bitMask; bitMask <<= 1) {
		if (ow_bit_rd())
			r |= bitMask;
	}
	return r;
}

/*uint8_t ow_rom_search(uint8_t diff, uint8_t *id) {*/
/* SORRY! NOT YET. */
/*return 0;*/
/*}*/

void ow_command_intern(uint8_t command, uint8_t *id) {
	uint8_t i;
	ow_reset();
	if (id) {
		ow_byte_wr(OW_MATCH_ROM); /* to a single device*/
		i = OW_ROMCODE_SIZE;
		do {
			ow_byte_wr(*id);
			id++;
		} while (--i);
	} else {
		ow_byte_wr(OW_SKIP_ROM); /* to all devices*/
	}
	ow_byte_wr(command);
}

void ow_command(uint8_t command, uint8_t *id) {
	ow_command_intern(command, id);
}

uint8_t ow_crc8(uint8_t *addr, uint8_t len) {
	uint8_t crc = 0;

	while (len--) {
		uint8_t inbyte = *addr++;
		for (uint8_t i = 8; i; i--) {
			uint8_t mix = (crc ^ inbyte) & 0x01;
			crc >>= 1;
			if (mix)
				crc ^= 0x8C;
			inbyte >>= 1;
		}
	}
	return crc;
}
#endif
