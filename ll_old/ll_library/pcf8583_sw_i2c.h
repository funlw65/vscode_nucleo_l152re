/*
 * pcf8583.h
 *
 *  Created on: May 10, 2017
 *  Author: Vasile Guta-Ciucur
 *
 *
 * COPYRIGHT(c) 2017 Vasile Guta-Ciucur
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef PCF8583_H
#define PCF8583_H

#include "typedefs.h"
#include "ll_sw_i2c_master.h"

#if !defined(PCF8583_PHYSICAL_ADDRESS)
#define PCF8583_PHYSICAL_ADDRESS 0xA2
#endif

#define PCF8583_BASE_ADDR         0x00
#define PCF8583_W_ADDR   PCF8583_PHYSICAL_ADDRESS
#define PCF8583_R_ADDR   (PCF8583_W_ADDR + 1)
#define PCF8583_RAM_ADDR 0x10 /* from $10 to $FF NVRAM */

/* Register addresses */
#define PCF8583_CTRL_STATUS_REG   0x00
#define PCF8583_100S_REG          0x01
#define PCF8583_SECONDS_REG       0x02
#define PCF8583_MINUTES_REG       0x03
#define PCF8583_HOURS_REG         0x04
#define PCF8583_DATE_REG          0x05
#define PCF8583_MONTHS_REG        0x06
#define PCF8583_TIMER_REG         0x07
/**/
#define PCF8583_ALARM_CONTROL_REG 0x08
#define PCF8583_ALARM_100S_REG    0x09
#define PCF8583_ALARM_SECS_REG    0x0A
#define PCF8583_ALARM_MINS_REG    0x0B
#define PCF8583_ALARM_HOURS_REG   0x0C
#define PCF8583_ALARM_DATE_REG    0x0D
#define PCF8583_ALARM_MONTHS_REG  0x0E
#define PCF8583_ALARM_TIMER_REG   0x0F
/* Commands for the Control/Status register. */
#define PCF8583_START_COUNTING    0x00
#define PCF8583_STOP_COUNTING     0x80
/**/
#define PCF8583_ACK               0
#define PCF8583_NACK              1

/* Constants for Alarm Type and Status*/
typedef enum {
	PCF8583_NO_ALARM, PCF8583_DAILY_ALARM, PCF8583_WEEKDAYS_ALARM
} alarmtype_t;

typedef enum {
	PCF8583_ALARM_IDLE,
	PCF8583_ALARM_ON,
	PCF8583_ALARM_OFF,
	PCF8583_ALARM_SNOOZE
} alarmstatus_t;

// PCF8583_MONTH_ALARM /* not implemented, TODO. */

/* TODO: Timer alarm not implemented yet */
/* TODO: Event counter not implemented yet */

uint8_t RTC_seconds; /* 00 to 59 */
uint8_t RTC_minutes; /* 00 to 59 */
uint8_t RTC_hours; /* 00 to 23 */
/**/
uint8_t RTC_day; /* 1 to 31 */
uint8_t RTC_month; /* 1 to 12 */
uint8_t RTC_century; /* 20 */
uint8_t RTC_year; /* 00 to 99 */
uint8_t RTC_dayofweek; /* 0 to 6 (Sun = 0, Mon = 1, ..., Sat = 6) */
uint8_t RTC_leapyear; /* 0 to 3 (0 - leapyear and 1,2,3 not) */
/* 2012 was leapyear    ( leapyear = 0) */
/* 2014 is not leapyear ( leapyear = 2) */
alarmtype_t RTC_alarm_type;
alarmstatus_t RTC_alarm_status;

/* */

void pcf8583_getyear(uint8_t * cn, uint8_t * yr) {
	uint8_t a, b;
	//
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_RAM_ADDR);
	sw_i2c_start();
	sw_i2c_write(a | 1);
	*cn = sw_i2c_read(PCF8583_ACK);
	*yr = sw_i2c_read(PCF8583_NACK);
	sw_i2c_stop();
}

void pcf8583_setyear(uint8_t cn, uint8_t yr) {
	uint8_t a, b;
	//
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_RAM_ADDR);
	sw_i2c_write(cn);
	sw_i2c_write(yr);
	sw_i2c_stop();
}

void pcf8583_set_datetime(uint8_t hours, uint8_t minutes, uint8_t seconds,
		uint8_t dayofweek, uint8_t day, uint8_t month, uint8_t leapyear,
		uint8_t century, uint8_t year) {
	uint8_t a, b;
	uint8_t LyDd, WdMo;
	LyDd = (uint8_t)((dectobcd(leapyear) << 6) | dectobcd(day));
	WdMo = (uint8_t)((dectobcd(dayofweek) << 5) | dectobcd(month));
	/* Stop the RTC from counting, before writing to the time and date registers */
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_CTRL_STATUS_REG);
	sw_i2c_write(PCF8583_STOP_COUNTING);
	sw_i2c_stop();

	/* Write to the date and time registers */
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_100S_REG);
	sw_i2c_write(dectobcd(0));
	sw_i2c_write(dectobcd(seconds));
	sw_i2c_write(dectobcd(minutes));
	sw_i2c_write(dectobcd(hours));
	sw_i2c_write(LyDd);
	sw_i2c_write(WdMo);
	sw_i2c_stop();

	/* allow the PCF8583 to start counting again */
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_CTRL_STATUS_REG);
	sw_i2c_write(PCF8583_START_COUNTING);
	sw_i2c_stop();
	pcf8583_setyear(century, year);
}

void pcf8583_get_datetime(uint8_t * hr, uint8_t * mn, uint8_t * sc,
		uint8_t * dow, uint8_t * dy, uint8_t * mt, uint8_t * lp, uint8_t * ct,
		uint8_t * yr) {
	uint8_t a, b;
	uint8_t LyDd, WdMo, tmp;
	/* read the date and time registers */
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_SECONDS_REG);
	sw_i2c_start();
	sw_i2c_write(a | 1);
	*sc = bcdtodec(sw_i2c_read(PCF8583_ACK));
	*mn = bcdtodec(sw_i2c_read(PCF8583_ACK));
	*hr = bcdtodec((sw_i2c_read(PCF8583_ACK) & 0b00111111));
	LyDd = sw_i2c_read(PCF8583_ACK);
	WdMo = sw_i2c_read(PCF8583_NACK);
	sw_i2c_stop();
	tmp = ((WdMo & 0b11100000) >> 5);
	*dow = bcdtodec(tmp);

	tmp = (WdMo & 0b00011111);
	*mt = bcdtodec(tmp);

	tmp = ((LyDd & 0b11000000) >> 6);
	*lp = bcdtodec(tmp);

	tmp = (LyDd & 0b00111111);
	*dy = bcdtodec(tmp);

	pcf8583_getyear(ct, yr);

	/* check if RTC incremented the leapyear so we update our year in NVRAM */
	if (*lp != (*yr % 4)) {
		*yr = *yr + 1;
		pcf8583_setyear(*ct, *yr);
	}
}

uint8_t pcf8583_read_reg(uint8_t reg) {
	int8_t tmp;
	uint8_t a, b;
	//
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(reg);
	sw_i2c_start();
	sw_i2c_write(a | 1);
	tmp = sw_i2c_read(PCF8583_NACK);
	sw_i2c_stop();
	return (tmp);
}

/* Set the alarm type:
 * [atype] parameter have following values:
 *    0 = no alarm
 *    1 = daily alarm
 *    2 = weekdays alarm
 * other types are not implemented yet.
 */
void pcf8583_en_dis_alarm(alarmtype_t atype) {
	uint8_bits_t cfg, alarmcfg;
	uint8_t a, b;
	cfg.val = pcf8583_read_reg(PCF8583_CTRL_STATUS_REG);
	if (atype == PCF8583_NO_ALARM)
		cfg.bits.b2 = 0;
	else {
		cfg.bits.b0 = 0;
		cfg.bits.b1 = 0;
		cfg.bits.b2 = 1;
		alarmcfg.val = 0;
		alarmcfg.bits.b4 = atype;
		alarmcfg.bits.b5 = atype >> 1;
		alarmcfg.bits.b6 = atype >> 2;
		alarmcfg.bits.b7 = 1;
	}
	/* writing into CONTROL STATUS REG */
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_CTRL_STATUS_REG);
	sw_i2c_write(cfg.val);
	sw_i2c_stop();
	if (atype != 0) {
		/* writing into ALARM CONTROL REG */
		b = PCF8583_BASE_ADDR;
		a = (b << 1) | PCF8583_W_ADDR;
		sw_i2c_start();
		sw_i2c_write(a);
		sw_i2c_write(PCF8583_ALARM_CONTROL_REG);
		sw_i2c_write(alarmcfg.val);
		sw_i2c_stop();
	}
}

void pcf8583_set_alarm_weekdays(bit_t d0, bit_t d1, bit_t d2, bit_t d3,
		bit_t d4, bit_t d5, bit_t d6) {
	uint8_t a, b;
	uint8_bits_t wd;
	wd.bits.b0 = d0;
	wd.bits.b1 = d1;
	wd.bits.b2 = d2;
	wd.bits.b3 = d3;
	wd.bits.b4 = d4;
	wd.bits.b5 = d5;
	wd.bits.b6 = d6;
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_ALARM_MONTHS_REG);
	sw_i2c_write(wd.val);
	sw_i2c_stop();
}

void pcf8583_set_alarm_time(uint8_t hours, uint8_t minutes, uint8_t seconds) {
	/* you need a pull-up 10K resistor on INT pin of PCF8583 */
	uint8_t a, b;
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_ALARM_100S_REG);
	sw_i2c_write(dectobcd(0));
	sw_i2c_write(dectobcd(seconds));
	sw_i2c_write(dectobcd(minutes));
	sw_i2c_write(dectobcd(hours));
	sw_i2c_stop();
}

void pcf8583_stop_alarm(void) {
	/* It only stops the "sound of alarm", it doesn't disable the alarm! */
	/* I mean, the alarm will continue to sound at the same time, every day - as it is set*/
	uint8_t a, b;
	uint8_bits_t cfg;
	cfg.val = pcf8583_read_reg(PCF8583_CTRL_STATUS_REG);
	cfg.bits.b0 = FALSE; /* clears timer alarm flag */
	cfg.bits.b1 = FALSE; /* clears clock alarm flag */
	/*  writing into ALARM CONTROL REG */
	b = PCF8583_BASE_ADDR;
	a = (b << 1) | PCF8583_W_ADDR;
	sw_i2c_start();
	sw_i2c_write(a);
	sw_i2c_write(PCF8583_CTRL_STATUS_REG);
	sw_i2c_write(cfg.val);
	sw_i2c_stop();
}

#endif	/* PCF8583_H */
