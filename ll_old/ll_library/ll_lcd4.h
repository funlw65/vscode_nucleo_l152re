#ifndef NUCLEO_LCD4_H_
#define NUCLEO_LCD4_H_

#include "typedefs.h"
#include "stm32l1xx_ll_gpio.h"
#include "ll_my_gpio.h"
#include "ll_my_delay_us.h"

/* redefine LCD pins for your hardware before including this header  */
/* otherwise, the pins are defined by default for the Arduino socket */
#ifndef LCD_D7_Pin
#define LCD_D7_Pin GPIO_PIN_8  /*Arduino D7*/
#endif
#ifndef LCD_D6_Pin
#define LCD_D6_Pin GPIO_PIN_10 /*Arduino D6*/
#endif
#ifndef LCD_D5_Pin
#define LCD_D5_Pin GPIO_PIN_4  /*Arduino D5*/
#endif
#ifndef LCD_D4_Pin
#define LCD_D4_Pin GPIO_PIN_5  /*Arduino D4*/
#endif
#ifndef LCD_E_Pin
#define LCD_E_Pin GPIO_PIN_3   /*Arduino D3*/
#endif
#ifndef LCD_RS_Pin
#define LCD_RS_Pin GPIO_PIN_10 /*Arduino D2*/
#endif
/* end LCD pins definitions */

/* LCD gpio ports */
#undef LCD_PORTS_LOCALY_DEFINED
#ifndef LCD_D7_GPIO_Port /* set and init the pins from the Arduino socket*/
#define LCD_PORTS_LOCALY_DEFINED 1
#define LCD_D7_GPIO_Port GPIOA
#define LCD_D6_GPIO_Port GPIOB
#define LCD_D5_GPIO_Port GPIOB
#define LCD_D4_GPIO_Port GPIOB
#define LCD_E_GPIO_Port  GPIOB
#define LCD_RS_GPIO_Port GPIOA
#endif
/* end LCD gpio ports */

/* define the number of chars and lines of your LCD before including this header */
/* default is 16 x 2 */
#ifndef LCD_NR_CHARS
#define LCD_NR_CHARS 16
#elif ((LCD_NR_CHARS != 8) && (LCD_NR_CHARS != 16) && (LCD_NR_CHARS != 20))
#error "valid values for LCD Nr. Chars are 8, 16, 20"
#endif

#ifndef LCD_NR_LINES
#define LCD_NR_LINES 2
#elif ((LCD_NR_LINES != 1) && (LCD_NR_LINES != 2) && (LCD_NR_LINES != 4))
#error "valid values for LCD Nr. Lines are 1, 2, 4"
#endif

#define LCD_CLEAR_DISPLAY        0b00000001
#define LCD_RETURN_HOME          0b00000010
#define LCD_DISPLAY_ONOFF        0b00001000
#define LCD_CURSOR_SHIFT_R_VAL   0b00010100
#define LCD_CURSOR_SHIFT_L_VAL   0b00010000
#define LCD_DISPLAY_SHIFT_RIGHT  0b00011100
#define LCD_DISPLAY_SHIFT_LEFT   0b00011000
#define LCD_SET_DDRAM_ADDRESS    0b10000000
#define LCD_SET_CGRAM_ADDRESS    0b01000000

/* Define some chipsets*/

typedef enum {
	LCD_HD44780, LCD_ST7066U
} chipset_t;

/* global variable for cursor position*/
uint8_t LCD_POS;

void __lcd_write_nibble(uint8_t value) {
	uint8_bits_t nibble;
	nibble.val = value;
	LL_GPIO_WritePin(LCD_D4_GPIO_Port, LCD_D4_Pin, nibble.bits.b0);
	LL_GPIO_WritePin(LCD_D5_GPIO_Port, LCD_D5_Pin, nibble.bits.b1);
	LL_GPIO_WritePin(LCD_D6_GPIO_Port, LCD_D6_Pin, nibble.bits.b2);
	LL_GPIO_WritePin(LCD_D7_GPIO_Port, LCD_D7_Pin, nibble.bits.b3);
	LL_GPIO_WritePin(LCD_E_GPIO_Port, LCD_E_Pin, 1);
	my_delay_us(1);
	LL_GPIO_WritePin(LCD_E_GPIO_Port, LCD_E_Pin, 0);
}

void __lcd_write(uint8_t value) {
	__lcd_write_nibble(value >> 4); /* write high nibble */
	__lcd_write_nibble(value); /* write low nibble*/
	my_delay_us(38); /* > 37 us*/
}

void _lcd_write_data(uint8_t value) {
	LL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, 1); /* select data mode*/
	__lcd_write(value); /* write byte*/
}

void _lcd_write_command(uint8_t value) {
	LL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, 0); /* select command mode*/
	__lcd_write(value); /* write byte*/
}

uint8_t _lcd_line2index(uint8_t line) {
	/*-- force valid line number*/
	if (line < LCD_NR_LINES) {
		if (line == 0)
			return 0x00;
		else if (line == 1)
			return 0x40;
		else if (line == 2)
			return 0x00 + LCD_NR_CHARS;
		else if (line == 3)
			return 0x40 + LCD_NR_CHARS;
	}
	return 0x00;
}

void _lcd_restore_cursor(void) {
	_lcd_write_command(LCD_SET_DDRAM_ADDRESS | LCD_POS);
}

void lcd_write_char(uint8_t data) {
	_lcd_write_data(data);
}

void lcd_write_str(uint8_t *data) {
	uint8_t c;
	while ((c = *data++))
		_lcd_write_data(c);
}

void lcd_write_strF(const uint8_t *data) {
	uint8_t c;
	while ((c = *data++))
		_lcd_write_data(c);	
}

void lcd_cursor_position(uint8_t line, uint8_t pos) {
	LCD_POS = pos + _lcd_line2index(line);
	_lcd_restore_cursor();
}

void lcd_shift_left(uint8_t nr) {
	uint_fast8_t i;
	if (nr > 0) {
		for (i = 0; i < nr; i++)
			_lcd_write_command(LCD_DISPLAY_SHIFT_LEFT);
	}
}

void lcd_shift_right(uint8_t nr) {
	uint_fast8_t i;
	if (nr > 0) {
		for (i = 0; i < nr; i++)
			_lcd_write_command(LCD_DISPLAY_SHIFT_RIGHT);
	}
}

void lcd_cursor_shift_left(uint8_t nr) {
	uint_fast8_t i;
	if (nr > 0) {
		for (i = 0; i < nr; i++)
			_lcd_write_command(LCD_CURSOR_SHIFT_L_VAL);
	}
}

void lcd_cursor_shift_right(uint8_t nr) {
	uint_fast8_t i;
	if (nr > 0) {
		for (i = 0; i < nr; i++)
			_lcd_write_command(LCD_CURSOR_SHIFT_R_VAL);
	}
}

void lcd_clear_screen(void) {
	_lcd_write_command(LCD_CLEAR_DISPLAY);
	LL_mDelay(1);
	my_delay_us(800);
}

void lcd_cursor_blink_display(bit_t cursor, bit_t blink, bit_t display) {
	uint8_t reg;
	reg = LCD_DISPLAY_ONOFF;
	if (display)
		reg = reg + 4;
	if (cursor)
		reg = reg + 2;
	if (blink)
		reg = reg + 1;
	_lcd_write_command(reg);
}

void lcd_home() {
	_lcd_write_command(LCD_RETURN_HOME);
	LL_mDelay(1);
	my_delay_us(800);
}

void lcd_clear_line(uint8_t line) {
	uint_fast8_t i;
	/* set LCD-cursor at start of line*/
	LCD_POS = _lcd_line2index(line);
	_lcd_restore_cursor();
	/* now fill line with spaces*/
	for (i = 0; i < LCD_NR_CHARS; i++)
		lcd_write_char(' ');
	/* set LCD back to normal operation*/
	_lcd_restore_cursor();
}

void lcd_progress(uint8_t line, uint8_t amount, uint8_t pattern) {
	uint_fast8_t i;
	LCD_POS = _lcd_line2index(line);
	_lcd_restore_cursor();
	for (i = 0; i < amount; i++)
		lcd_write_char(pattern);
	for (i = 0; i < LCD_NR_CHARS - amount; i++)
		lcd_write_char(' ');
}

void lcd_init(chipset_t chipset) {
	/* init the gpios*/
#ifdef LCD_PORTS_LOCALY_DEFINED /* set and init the pins from the Arduino socket*/
	LL_GPIO_InitTypeDef GPIO_InitStruct;

	/**/
	GPIO_InitStruct.Pin = LCD_D6_Pin;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(LCD_D6_GPIO_Port, &GPIO_InitStruct);

	/**/
	GPIO_InitStruct.Pin = LCD_D7_Pin;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(LCD_D7_GPIO_Port, &GPIO_InitStruct);

	/**/
	GPIO_InitStruct.Pin = LCD_RS_Pin;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(LCD_RS_GPIO_Port, &GPIO_InitStruct);

	/**/
	GPIO_InitStruct.Pin = LCD_E_Pin;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(LCD_E_GPIO_Port, &GPIO_InitStruct);

	/**/
	GPIO_InitStruct.Pin = LCD_D5_Pin;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(LCD_D5_GPIO_Port, &GPIO_InitStruct);

	/**/
	GPIO_InitStruct.Pin = LCD_D4_Pin;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(LCD_D4_GPIO_Port, &GPIO_InitStruct);
	/**/
#endif
	LCD_POS = 0;
	/**/
	LL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, 0); /* set to control char mode*/
	if (chipset == LCD_HD44780) {
		LL_mDelay(16); /* power-up delay (> 15 ms)*/
		__lcd_write_nibble(0b00000011); // function set*/
		LL_mDelay(4); /* > 4.1 milliseconds*/
		my_delay_us(200);
		__lcd_write_nibble(0b00000011); /* function set*/
		my_delay_us(105); /* > 100 us*/
		__lcd_write_nibble(0b00000011); /* function set*/
		my_delay_us(38); /* > 37 us*/
		__lcd_write_nibble(0b00000010); /* select 4-bits mode*/
		my_delay_us(38); /* > 37 us*/
		_lcd_write_command(0b00101000); /* 2 lines, 5x8 dots font*/
		_lcd_write_command(0b00011100); /* cursor (not data) move right*/
		_lcd_write_command(0b00001100); /* display on, cursor off, no blink*/
		_lcd_write_command(0b00000110); /* cursor shift right, no data shift*/
		/* during read/write operations*/
		lcd_clear_screen(); /* clear display*/
	} else if (chipset == LCD_ST7066U) {
		__lcd_write_nibble(0b00000011);
		my_delay_us(40);
		__lcd_write_nibble(0b00000010);
		__lcd_write_nibble(0b00001100);
		my_delay_us(40);
		__lcd_write_nibble(0b00000010);
		__lcd_write_nibble(0b00001100);
		my_delay_us(38); /* > 37 us*/
		__lcd_write_nibble(0b00000000); /* display on / off*/
		__lcd_write_nibble(0b00001100);
		my_delay_us(38); /* > 37 us*/
		__lcd_write_nibble(0b00000000); /* display clear*/
		__lcd_write_nibble(0b00000001);
		LL_mDelay(1);
		my_delay_us(500);
		my_delay_us(40);
		__lcd_write_nibble(0b00000000); /* entry mode set*/
		__lcd_write_nibble(0b00000110);
		my_delay_us(38); /* > 37 us*/
		__lcd_write_nibble(0b00000000); /* display clear*/
		__lcd_write_nibble(0b00000001);
		LL_mDelay(1);
		my_delay_us(500);
		my_delay_us(40);
	}
}

#endif /* NUCLEO_LCD4_H_ */
