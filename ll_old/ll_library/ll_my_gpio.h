/*
 * my_ll_gpio.h
 *
 *  Created on: Aug 4, 2017
 *      Author: me
 *
 * COPYRIGHT(c) 2017 Vasile Guta-Ciucur
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef NUCLEO_MY_LL_GPIO_H_
#define NUCLEO_MY_LL_GPIO_H_

#ifdef USE_FULL_LL_DRIVER
#include "stm32l1xx_ll_gpio.h"

__STATIC_INLINE void LL_GPIO_WritePin(GPIO_TypeDef *GPIOx, uint32_t PinMask,
		uint8_t value) {
	if (value)
	WRITE_REG(GPIOx->BSRR, PinMask);
#if defined(GPIO_BRR_BR_0)
	else WRITE_REG(GPIOx->BRR, PinMask);
#else
	else WRITE_REG(GPIOx->BSRR, (PinMask << 16));
#endif /* GPIO_BRR_BR_0 */
}

#endif
#endif /* NUCLEO_MY_LL_GPIO_H_ */
