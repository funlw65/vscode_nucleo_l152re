/*
  Generated with VPC version 3.4.4.908 Beta
  Application name: l152_st7735_mini
  File name: main.c

  Copyright (c) 2019
  Author: Vasile Guta-Ciucur

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
  so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or substantial
  portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "stdint.h"
#include "main.h"
#include "my_gpio.h"
#include "my_uart.h"
#include "my_delay_us.h"
#define ST7735_SPI_TYPE 2
#include "st7735r_mini.h"

void vpc_system_init(void)
{
  NVIC_InitTypeDef NVIC_InitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_COMP, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* NVIC Priority Groups: See misc.h for details. */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);

  NVIC_InitStruct.NVIC_IRQChannel = MemoryManagement_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = BusFault_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = UsageFault_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = SVC_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = DebugMonitor_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = PendSV_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  my_ticks = 0;
  NVIC_InitStruct.NVIC_IRQChannel = SysTick_IRQn;
  NVIC_Init(&NVIC_InitStruct);

  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
  SysTick_Config(SystemCoreClock / 1000);
}

void vpc_rtc_init(void)
{
  RTC_InitTypeDef RTC_InitStructure;

  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
  RTC_InitStructure.RTC_SynchPrediv = (32768 / 128) - 1;
  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
  RTC_Init(&RTC_InitStructure);

}

void vpc_gpio_init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  EXTI_InitTypeDef EXTI_InitStruct;
  NVIC_InitTypeDef NVIC_InitStruct;

  /* Activating GPIO ports */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOH, ENABLE);

  /* The initial values (Low) of OUTPUT pins */
  GPIO_ResetBits(GPIOA, LD2_Pin );

  /* The initial values (High) of OUTPUT pins */
  GPIO_SetBits(GPIOA, ST7735_CS_Pin );
  GPIO_SetBits(GPIOB, ST7735_RST_Pin );
  GPIO_SetBits(GPIOC, ST7735_DC_Pin );

  /* Prologue for EXTI pins */
  /* PC13 */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);
  GPIO_SetPinPull(GPIOC, B1_Pin, GPIO_PuPd_NOPULL);
  GPIO_SetPinMode(GPIOC, B1_Pin, GPIO_Mode_IN);

  /* PA5 as OUTPUT pin */
  GPIO_InitStruct.GPIO_Pin = LD2_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* PA9 as OUTPUT pin */
  GPIO_InitStruct.GPIO_Pin = ST7735_CS_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* PB6 as OUTPUT pin */
  GPIO_InitStruct.GPIO_Pin = ST7735_RST_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* PC13 as INTERRUPT pin */
  EXTI_InitStruct.EXTI_Line = EXTI_Line13;
  EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStruct.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStruct);

  /* PC7 as OUTPUT pin */
  GPIO_InitStruct.GPIO_Pin = ST7735_DC_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOC, &GPIO_InitStruct);

  NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 4;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStruct);

}

void vpc_usart2_uart_init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  USART_InitTypeDef USART_InitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

  GPIO_InitStruct.GPIO_Pin = USART_TX_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);

  GPIO_InitStruct.GPIO_Pin = USART_RX_Pin;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

  USART_InitStruct.USART_BaudRate = 19200;
  USART_InitStruct.USART_WordLength = USART_WordLength_8b;
  USART_InitStruct.USART_StopBits = USART_StopBits_1;
  USART_InitStruct.USART_Parity = USART_Parity_No;
  USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_Init(USART2, &USART_InitStruct);
  USART_Cmd(USART2, ENABLE);

}

void vpc_spi2_init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  SPI_InitTypeDef   SPI_InitStruct;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
  /* Configure PB13 as SPI SCK pin */
  GPIO_InitStruct.GPIO_Pin = SCK2_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* Configure PB14 as SPI MISO pin */
  GPIO_InitStruct.GPIO_Pin = MISO2_Pin;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* Configure PB15 as SPI MOSI pin */
  GPIO_InitStruct.GPIO_Pin = MOSI2_Pin;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* Connect SCK, MISO and MOSI pins to SPI alternate */
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_SPI2);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2);

  /* Configure SPI peripheral */
  SPI_InitStruct.SPI_Direction = SPI_Direction_1Line_Tx;
  SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
  SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
  SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStruct.SPI_CRCPolynomial = 7;
  SPI_Init(SPI2, &SPI_InitStruct);

  SPI_NSSInternalSoftwareConfig(SPI2, SPI_NSSInternalSoft_Set);
  /* Enable SPI */
  SPI_Cmd(SPI2, ENABLE);

}

void SystemCoreClockUpdate (void)
{
  uint32_t tmp = 0, pllmul = 0, plldiv = 0, pllsource = 0, msirange = 0;

  tmp = RCC->CFGR & RCC_CFGR_SWS;

  switch (tmp) {
  case 0x00:
    msirange = (RCC->ICSCR & RCC_ICSCR_MSIRANGE) >> 13;
    SystemCoreClock = (32768 * (1 << (msirange + 1)));
    break;
  case 0x04:
    SystemCoreClock = HSI_VALUE;
    break;
  case 0x08:
    SystemCoreClock = HSE_VALUE;
    break;
  case 0x0C:
    pllmul = RCC->CFGR & RCC_CFGR_PLLMUL;
    plldiv = RCC->CFGR & RCC_CFGR_PLLDIV;
    pllmul = PLLMulTable[(pllmul >> 18)];
    plldiv = (plldiv >> 22) + 1;

    pllsource = RCC->CFGR & RCC_CFGR_PLLSRC;

    if (pllsource == 0x00) {
      SystemCoreClock = (((HSI_VALUE) * pllmul) / plldiv);
    } else {
      SystemCoreClock = (((HSE_VALUE) * pllmul) / plldiv);
    }
    break;
  default:
    msirange = (RCC->ICSCR & RCC_ICSCR_MSIRANGE) >> 13;
    SystemCoreClock = (32768 * (1 << (msirange + 1)));
    break;
  }
  tmp = AHBPrescTable[((RCC->CFGR & RCC_CFGR_HPRE) >> 4)];
  SystemCoreClock >>= tmp;
}

static void SetSysClock(void)
{

  FLASH->ACR |= FLASH_ACR_ACC64;
  FLASH->ACR |= FLASH_ACR_PRFTEN;
  FLASH->ACR |= FLASH_ACR_LATENCY;
  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  PWR->CR = PWR_CR_VOS_0;
  while((PWR->CSR & PWR_CSR_VOSF) != RESET) {}

  RCC_MSIRangeConfig(RCC_MSIRange_6);
  RCC_MSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_MSIRDY) == RESET) {}

  RCC_HSEConfig(RCC_HSE_OFF);

  RCC_HSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET) {}

  RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_PLLSRC | RCC_CFGR_PLLMUL | RCC_CFGR_PLLDIV));
  RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC_HSI | RCC_CFGR_PLLMUL6 | RCC_CFGR_PLLDIV3);
  RCC->CR |= RCC_CR_PLLON;
  while((RCC->CR & RCC_CR_PLLRDY) == 0) {}

  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

  RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV1;
  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV1;
  RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV1;

  RCC_MCOConfig(RCC_MCOSource_NoClock, RCC_MCODiv_1);

  PWR_RTCAccessCmd(ENABLE);
  RCC_LSEConfig(RCC_LSE_ON);
  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET) {}
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
  RCC_LSEClockSecuritySystemCmd(ENABLE);
  RCC_LSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET) {}
  RCC_RTCCLKCmd(ENABLE);

}

void SystemInit (void)
{
  RCC->CR |= (uint32_t)0x00000100;
  RCC->CFGR &= (uint32_t)0x88FFC00C;
  RCC->CR &= (uint32_t)0xEEFEFFFE;
  RCC->CR &= (uint32_t)0xFFFBFFFF;
  RCC->CFGR &= (uint32_t)0xFF02FFFF;
  RCC->CIR = 0x00000000;

  SetSysClock();

#ifdef VECT_TAB_SRAM
  SCB->VTOR = SRAM_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal SRAM. */
#else
  SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH. */
#endif
}


void NMI_Handler(void)
{
}

void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1) {
  }
}

void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1) {
  }
}

void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1) {
  }
}

void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1) {
  }
}

void SVC_Handler(void)
{
}

void DebugMon_Handler(void)
{
}

void PendSV_Handler(void)
{
}

void SysTick_Handler(void)
{
  if (my_ticks > 0) my_ticks--;
}

void EXTI15_10_IRQHandler(void)
{
  // Here would be the B1 pin interrupt handler (do where it says "do something..."):
  if ((EXTI_GetITStatus(EXTI_Line13)) != RESET) {
    // do something...
    EXTI_ClearITPendingBit(EXTI_Line13);
  }
}


int main(void)
{
  /* local variables */

  /* mandatory system initializations */
  vpc_system_init();
  //vpc_rtc_init(); /* enable if required */
  vpc_gpio_init();
  //vpc_usart2_uart_init(); /* enable if required */
  vpc_spi2_init();
  /* third-party initializations */

  /* do your own initializations below */
  ST7735_init();
  ST7735_Clear(ST7735_GREEN);
  ST7735_PutStr5x7(1, 0, 0, "HELLO!", ST7735_WHITE, ST7735_BLACK);
  my_delay_ms(3000);
  ST7735_RotationSet(scr_CW);
  ST7735_Clear(ST7735_BLACK);
  ST7735_PutStr7x11(0, 0, "HELLO!", ST7735_CYAN, ST7735_BLACK);
  ST7735_HLine(0,scr_width-1,13, ST7735_RED);
  ST7735_PutStr5x7(3, 0, 15, "HELLO!", ST7735_WHITE, ST7735_BLACK);
  ST7735_Rect(5, 37, scr_width-1-5, scr_height-1-5, ST7735_YELLOW);
  ST7735_FillRect(5+2, 37+2, scr_width-1-5-2, scr_height-1-5-2, ST7735_ORANGE);
  /* your forever repeating code */
  while(1) {
    GPIO_ToggleBits(LD2_Port, LD2_Pin);
    my_delay_ms(500);
  }
  return 0;
}

