/*
  Generated with VPC version 3.4.4.908 Beta
  Application name: l152_st7735_mini
  File name: main.h

  Copyright (c) 2019
  Author: Vasile Guta-Ciucur

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
  so, subject to the following conditions:
 
  The above copyright notice and this permission notice shall be included in all copies or substantial
  portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
 
#ifndef __MAIN_H
#define __MAIN_H
 
#define HSI_VALUE    16000000
#define HSE_VALUE    8000000
#define HCLK_VALUE   32000000
#define APB1_T_VALUE 32000000
#define APB2_T_VALUE 32000000
#include "stm32l1xx.h"
#include "stm32l1xx_comp.h"
#include "stm32l1xx_dma.h"
#include "stm32l1xx_exti.h"
#include "stm32l1xx_flash.h"
#include "stm32l1xx_fsmc.h"
#include "stm32l1xx_gpio.h"
#include "stm32l1xx_pwr.h"
#include "stm32l1xx_rcc.h"
#include "stm32l1xx_rtc.h"
#include "stm32l1xx_syscfg.h"
#include "stm32l1xx_usart.h"
#include "stm32l1xx_wwdg.h"
#include "stm32l1xx_tim.h"
#include "stm32l1xx_spi.h"
#include "misc.h"
 
#define B1_Pin GPIO_Pin_13
#define B1_Port GPIOC
#define LD2_Pin GPIO_Pin_5
#define LD2_Port GPIOA
#define ST7735_RST_Pin GPIO_Pin_6
#define ST7735_RST_Port GPIOB
#define ST7735_DC_Pin GPIO_Pin_7
#define ST7735_DC_Port GPIOC
#define ST7735_CS_Pin GPIO_Pin_9
#define ST7735_CS_Port GPIOA
#define USART_TX_Pin GPIO_Pin_2
#define USART_TX_Port GPIOA
#define USART_RX_Pin GPIO_Pin_3
#define USART_RX_Port GPIOA
#define MOSI2_Pin GPIO_Pin_15
#define MOSI2_Port GPIOB
#define MISO2_Pin GPIO_Pin_14
#define MISO2_Port GPIOB
#define SCK2_Pin GPIO_Pin_13
#define SCK2_Port GPIOB
 
uint32_t SystemCoreClock      = HCLK_VALUE;
__I uint8_t PLLMulTable[9]    = {3, 4, 6, 8, 12, 16, 24, 32, 48};
__I uint8_t AHBPrescTable[16] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 6, 7, 8, 9};
 
volatile uint32_t my_ticks;
/* Uncomment the following line if you need to relocate your vector Table in
   Internal SRAM. */
/*#define VECT_TAB_SRAM */
#define VECT_TAB_OFFSET  0x0

void my_delay_ms(uint16_t ms){
  my_ticks = ms;
  while(my_ticks > 0);
}

 
#endif /*__MAIN_H*/
 
